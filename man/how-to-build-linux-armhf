# Описание процесса сборки с нуля
# Архитектура: x86_64
# Целевая архитектура: x86_64
# ОС: Debian 9
# Компилятор: GCC
# -----------------------------------------------------------------------------
#           I. Сборка МЭК 61850
# -----------------------------------------------------------------------------
# Включает в себя: 
# - библиотеку МЭК61850 (libiec61850)
# - парсер XML (libexpat)
# - тест-фреймворк (GoogleTest)
# - тесты, сервер 61850, инструменты для работы с сервером
# 
# -----------------------------------------------------------------------------
#           1. Подготовка системы
# -----------------------------------------------------------------------------
apt update
apt install make cmake git wget g++-arm-linux-gnueabihf

# настроить кол-во параллельных сборок. Например, 4
export NCPU=4
# -----------------------------------------------------------------------------
#           2. Получение исходных кодов
# -----------------------------------------------------------------------------
cd /tmp
mkdir build
cd build
git clone https://pickle-rick@bitbucket.org/pickle-rick/rattlehead.git
cd rattlehead 
git submodule init
git submodule update
cd ..

# -----------------------------------------------------------------------------
#           3. Сборка зависимостей
# -----------------------------------------------------------------------------
# Сборку зависимостей можно пропустить если:
# - они уже были собраны
# - они были установлены из репозиториев
# Установка зависимостей из репозитория в руководстве не рассматривается

mkdir armhf_release
cd armhf_release 

# 3.1 Сборка libexpat
mkdir libexpat
cd libexpat
CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ cmake ../../rattlehead/deps/libexpat/expat/ -DCMAKE_BUILD_TYPE=Release  -DCMAKE_C_FLAGS="-pipe -O2 -DXML_POOR_ENTROPY"   -DBUILD_doc=0 -DBUILD_examples=0 -DBUILD_tests=0 -DCMAKE_INSTALL_PREFIX=/tmp/build/armhf_release
make install -j $NCPU
cd ..

# 3.2 Сборка libiec61850
mkdir libiec61850
cd libiec61850
CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ cmake ../../rattlehead/deps/libiec61850 -DCMAKE_BUILD_TYPE=Release -DCONFIG_INCLUDE_GOOSE_SUPPORT=0 -DCMAKE_INSTALL_PREFIX=/tmp/build/armhf_release 
make install -j $NCPU
cd ..

# 3.3 Сбока GTest
mkdir googletest
cd googletest
CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ cmake ../../rattlehead/deps/googletest -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/tmp/build/armhf_release 
make install -j $NCPU
cd ..

# -----------------------------------------------------------------------------
#           4. Сборка МЭК 61850
# -----------------------------------------------------------------------------
mkdir rattlehead
cd rattlehead
CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ cmake ../../rattlehead/ -DCMAKE_CXX_FLAGS="-O3 -DNDEBUG -fPIC -std=c++98" -DCMAKE_INSTALL_PREFIX=/tmp/build/armhf_release -DCMAKE_FIND_ROOT_PATH=/tmp/build/armhf_release
make install -j $NCPU
cd ..

# -----------------------------------------------------------------------------
#           5. Запуск тестов МЭК 61850
# -----------------------------------------------------------------------------
# 5.1 Указать путь для поиска собранных библиотек
#export LD_LIBRARY_PATH=/tmp/build/armhf_release/lib
#
## 5.2 Тест API (сеть, потоки, расшаренная память и т.п.)
#cd /tmp/build/armhf_release/tmp/test-base/       
#./test-base
#
## 5.3 Чтение конфигурации SCD/ICD/...
#cd /tmp/build/armhf_release/tmp/test-iec61850-config
#./test-iec61850-config 
#
## 5.4 Проверка работоспособности сервера и инструментов передачи данных
#cd /tmp/build/armhf_release/tmp/test-iec61850-server
#./test-iec61850-server-conn
#./test-iec61850-server-log
#./test-iec61850-server-model
#./test-iec61850-server-types
#./test-iec61850-server
#

# -----------------------------------------------------------------------------
#           II. Сборка сателлита Volcano
# -----------------------------------------------------------------------------
# Перед сборкой сателлита Volcano необходимо собрать сам VC и его зависимости

# -----------------------------------------------------------------------------
#           1. Получание исходных кодов
# -----------------------------------------------------------------------------
# 1.1 Boost
cd /tmp/build/
wget https://sourceforge.net/projects/boost/files/boost/1.62.0/boost_1_62_0.tar.gz

# 1.2 Volcano
git clone https://pickle-rick@bitbucket.org/pickle-rick/volcano.git

# -----------------------------------------------------------------------------
#           2. Сбока Boost
# -----------------------------------------------------------------------------
cd armhf_release
tar xvpf ../boost_1_62_0.tar.gz -C .
cd boost_1_62_0
./bootstrap.sh 
sed -i 's/using gcc ;/using gcc : arm : arm-linux-gnueabihf-g++ ;/' project-config.jam
./b2 toolset=gcc-arm variant=release link=shared threading=multi cxxflags=-fPIC install --with-system --with-date_time --with-thread --with-filesystem --with-chrono --with-log --prefix=/tmp/build/armhf_release -j $NCPU


# -----------------------------------------------------------------------------
#           3. Сбока Volcano
# -----------------------------------------------------------------------------
cd /tmp/build/armhf_release
mkdir volcano
cd volcano
CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ cmake ../../volcano -DCMAKE_CXX_FLAGS="-O3 -DNDEBUG -fPIC -std=c++98 -DBOOST_ALL_DYN_LINK" -DCMAKE_FIND_ROOT_PATH=/tmp/build/armhf_release -DCMAKE_INSTALL_PREFIX=/tmp/build/armhf_release
make install -j $NCPU
cd ..

# -----------------------------------------------------------------------------
#           4. Сбока SAT61850
# -----------------------------------------------------------------------------
cd /tmp/build/armhf_release/rattlehead
CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ cmake ../../rattlehead/ -DCMAKE_CXX_FLAGS="-O3 -DNDEBUG -fPIC -std=c++98" -DCMAKE_INSTALL_PREFIX=/tmp/build/armhf_release -DCMAKE_FIND_ROOT_PATH=/tmp/build/armhf_release -DVOLCANO_ROOT=/tmp/build/armhf_release -DVOLCANO_Static=1 -DRATTLEHEAD_VOLCANO_SATELLITE=1 
make install -j $NCPU
cd ..
# Проверить сателлит : /tmp/build/armhf_release/lib/libsat61850.so


