# Сервер 61850


## Зависимости:
  - libiec61850
  - expat
  - gtest - при сборке тестов
  - volcano - при сборке сателлита Volcano
  - boost - при сборке сателлита Volcano под старые платформы (linux 2.6, windows)

## Параметры CMake:
- RATTLEHEAD_IEC61850_CONNECTOR - разрешить сборку коннектора для сервера 61850
- RATTLEHEAD_IEC61850_TEST - разрешить сборку тестов
- RATTLEHEAD_VOLCANO_SATELLITE - разрешить сборку сателлита для Volcano
- VOLCANO_ROOT - папка с установленным Volcano [установлен RATTLEHEAD_VOLCANO_SATELLITE]
- VOLCANO_LINK_EXTERNALS - подключить внешние либы (boost, os - libs) к сателиту Volcano  [установлен RATTLEHEAD_VOLCANO_SATELLITE + сборка под старые linux / windows]
- VOLCANO_Static - статичесая линковка API-либы Volcano
- EXPAT_Static - статическая линковка Expat
- IEC61850_Static - статическая линковка libiec61850
- Boost_STATIC - статическая линковка boost [установлен VOLCANO_LINK_EXTERNALS]



