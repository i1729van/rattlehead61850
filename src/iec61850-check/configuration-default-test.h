#pragma once
#include "configuration-syntax-test.h" // for TestMandatoryParameters
#include "iec61850-check/configuration-test.h" // for TestSuite

namespace rth {
namespace iec61850 {
    namespace configuration {

        /**
 * @class DefaultDescriptionTestSuite
 * @brief Дефолтный набор тестов для конфиг. файла
 * Набор тестов может вызваться однократно, после чего его необходимо
 * пересоздать. Ограничение позволяет легко решить проблему наколпления мусора
 * и пром. результатов
 */
        class DefaultTestSuite : public configuration::TestSuite {
        public:
            DefaultTestSuite();
            ~DefaultTestSuite();
            void Setup();

        private:
            TestMandatoryParameters test1;
            TestParamtersValues test2;
            TestTypesRelations test3;
            bool used_; // Переменная для запрета повторного вызова
        };
    }
}
}
