#include "configuration-default-test.h"
#include "configuration-syntax-test.h" // for TestMandatoryParameters, Test...
#include <assert.h> // for assert
#include <list> // for list

namespace rth {
namespace iec61850 {
    namespace configuration {

        DefaultTestSuite::DefaultTestSuite()
        {
            tests_.push_back(&test1);
            tests_.push_back(&test2);
            tests_.push_back(&test3);
            used_ = false;
        }

        void DefaultTestSuite::Setup()
        {
            // Не используем тесты повторно
            assert(used_ == false);
            used_ = true;
            TestSuite::Setup();
        }

        DefaultTestSuite::~DefaultTestSuite() {}
    }
}
}
