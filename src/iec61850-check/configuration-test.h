#pragma once
#include "iec61850-common/errors-defaults.h" // for DefaultErrorList
#include "iec61850-common/errors.h" // for ErrorList
#include "iec61850-config/base-types.h" // for DataAttribute, DataType
#include "iec61850-config/reader.h" // for ReaderInterface
#include "iec61850-config/xml.h" // for ParseContext
#include <list> // for list
#include <stddef.h> // for size_t

namespace rth {
namespace iec61850 {
    namespace configuration {

        /**
 * @class DescriptionTestInterface
 * @brief Интерфейс для проверок аттрибутов данных ICD/CID/SCD.
 * Вызов проверки осуществлятеся при помощи Execute, которая возвращает 0 (если
 * все в порядке) или код ошибки в противном случае
 */
        class TestInterface {
        public:
            /**
   * @brief Имя теста
   * @return Указатель на строку с именем
   */
            virtual const char* GetTestName() const { return "NoTestName"; }

            /**
   * @brief Описание ошибки
   * @return Указатель на строку с описанием
   */
            virtual const char* GetErrorDescription() const { return "NoErrorDescr"; }

            /**
   * @brief Вызывает перед началом тестирования
   */
            virtual void Setup() {}

            /**
   * @brief Вызывается в конце тестирования
   */
            virtual void Release() {}

            /**
   * @brief Прочитан узел *Type
   * @param type - информация о типа в соотв. с МЭК 61850-6
   * @param context - контекст XML парсера
   * @return 0 - если все нормально. Иначе код ошибки
   */
            virtual int Execute(const configuration::DataType& type,
                const configuration::ParseContext& context)
                = 0;
            /**
 * @brief Прочитан узел аттрибута данных [DA,DO,SDO,BDA]
 * @param attribute - информация о типа в соотв. с МЭК 61850-6
 * @param context - контекст XML парсера
 * @return 0 - если все нормально. Иначе код ошибки
 */
            virtual int Execute(const configuration::DataAttribute& attribute,
                const configuration::ParseContext& context)
                = 0;

            /**
   * @brief Вызывается после чтения последнего узла
   * @param context - контекст парсера
   * @return 0 - если все нормально. Иначе код ошибки
   */
            virtual int Execute(const configuration::ParseContext& context) = 0;
            // ..Execute(RCB, LN, DS,...)

            virtual ~TestInterface() {}
        };

        typedef DefaultErrorList TestingErrors;

        class TestResult {
        public:
            TestResult();
            size_t GetErrorsCount() const;
            const TestingErrors& Errors() const;
            bool IsOk() const;
            void AddError(const char* error_description);

        private:
            TestingErrors errors_;
        };

        /**
 * @class DescriptionTestSuite
 * @brief Набор тестов применяемых для конфигурационных файлов
 */
        class TestSuite : public TestInterface {
        public:
            TestSuite();
            virtual ~TestSuite();
            void Setup();
            void Release();
            int Execute(const configuration::DataType& type,
                const configuration::ParseContext& context);
            int Execute(const configuration::DataAttribute& attribute,
                const configuration::ParseContext& context);
            int Execute(const configuration::ParseContext& context);

            void AddTest(TestInterface& test);
            const TestResult& GetResults() const;

        protected:
            std::list<TestInterface*> tests_; //Список выполянемых тестов
            TestResult results_; // Результаты выплднение тестов
        };

        /**
 * @class DescriptionTestRunner
 * @brief Класс для запуска набора тестов для icd/cid/scd файла
 * Позволяет увязать вместе чтение файла и вызов тестов
 */
        class TestRunner : public configuration::ReaderInterface {
        public:
            TestRunner(TestSuite& tests, int max_errors = 100);
            ~TestRunner();

            // { DataTemplateDefaultOperator
            int PrepareHandler();
            int DataTypeHandler(const configuration::DataType& type,
                const configuration::ParseContext& context);
            int DataAttributeHandler(const configuration::DataAttribute& attribute,
                const configuration::ParseContext& context);
            int DoneHandler(const configuration::ParseContext& context);
            // } DataTemplateDefaultOperator

        private:
            TestSuite& tests_;
            int errors_cnt_;
            int max_errors_; // Макс. кол-во ошибок, которые не будут приводить к
            // останову тестов
        };

        /**
 * @brief Прочитать файл, запустить тесты, получить отчет об их выполнении
 * @param filename имя проверяемого файла
 * @return возвращает ссылку на класс DescriptionTestResult - содержащий
 * инфомрацию о результататах.
 * После использования ссылка д.б. уничтожена при помощи ReleaseTestResult
 */
        TestResult RunTests(TestSuite& tests, const char* filename,
            const char* ied = "", int max_errors = 100);
    }
}
}
