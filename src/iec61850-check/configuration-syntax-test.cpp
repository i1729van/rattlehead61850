#include "configuration-syntax-test.h"
#include <sstream>

namespace rth {
namespace iec61850 {
    namespace configuration {

        TestMandatoryParameters::TestMandatoryParameters()
            : last_error_("")
        {
        }

        TestMandatoryParameters::~TestMandatoryParameters() {}

        const char* TestMandatoryParameters::GetTestName() const { return "Dummy"; }
        const char* TestMandatoryParameters::GetErrorDescription() const
        {
            return last_error_.c_str();
        }

        int TestMandatoryParameters::MakeError(
            const char* str, const configuration::ParseContext& context)
        {
            std::stringstream ss;
            ss << "Line:" << context.pos_current_ln
               << " Symb:" << context.pos_current_byte << " " << str;
            last_error_ = ss.str();
            return 1;
        }

        int TestMandatoryParameters::Execute(
            const configuration::DataType& type,
            const configuration::ParseContext& context)
        {
            // Проверка наличия аттрибутов [пока по простому]
            if (type.element == "LNodeType") {
                if (type.id == "") {
                    return MakeError("Missing 'id' in LNodeType", context);
                }
                if (type.lnClass == "") {
                    return MakeError("Missing 'lnClass' in LNodeType", context);
                }
            } else if (type.element == "DOType") {
                if (type.id == "") {
                    return MakeError("Missing 'id' in DOType", context);
                }
                if (type.cdc == "") {
                    return MakeError("Missing 'cdc' in DOType", context);
                }
            } else if (type.element == "DAType") {
                if (type.id == "") {
                    return MakeError("Missing 'id' in DAType", context);
                }
            } else if (type.element == "EnumType") {
                if (type.id == "") {
                    return MakeError("Missing 'id' in Enum", context);
                }
            }
            return 0;
        }

        int TestMandatoryParameters::Execute(
            const configuration::DataAttribute& attribute,
            const configuration::ParseContext& context)
        {
            // Проверка наличия аттрибутов [пока по простому]
            if (attribute.element == "DO") {
                if (attribute.name == "") {
                    return MakeError("Missing 'name' in DO", context);
                }
                if (attribute.type == "") {
                    return MakeError("Missing 'type' in DO", context);
                }
            } else if (attribute.element == "SDO") {
                if (attribute.name == "") {
                    return MakeError("Missing 'name' in SDO", context);
                }
                if (attribute.type == "") {
                    return MakeError("Missing 'type' in SDO", context);
                }
            } else if (attribute.element == "DA") {
                if (attribute.name == "") {
                    return MakeError("Missing 'name' in DA", context);
                }
                if (attribute.type == "" && attribute.bType == "") {
                    return MakeError("Missing 'type & bType' in DA", context);
                }
                if (attribute.fc == "") {
                    return MakeError("Missing 'fc' in DA", context);
                }
            } else if (attribute.element == "BDA") {
                if (attribute.name == "") {
                    return MakeError("Missing 'name' in BDA", context);
                }
                if (attribute.type == "" && attribute.bType == "") {
                    return MakeError("Missing 'type & bType' in BDA", context);
                }
            } else if (attribute.element == "EnumVal") {
                if (attribute.ord == "") {
                    return MakeError("Missing 'ord' in EnumVal", context);
                }
            }

            return 0;
        }

        int TestMandatoryParameters::Execute(
            const configuration::ParseContext& context)
        {
            return context.stop & 0x0;
        }
    }
}
}
