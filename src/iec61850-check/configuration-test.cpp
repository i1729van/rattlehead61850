#include "configuration-test.h"
#include <string> // for allocator, string, operator+, basic_string, char_t...

namespace rth {
namespace iec61850 {
    namespace configuration {

        TestResult::TestResult() {}

        void TestResult::AddError(const char* error_description)
        {
            errors_.Append(DefaultErrorDescription(std::string(error_description)));
        }

        size_t TestResult::GetErrorsCount() const { return errors_.Count(); }

        const DefaultErrorList& TestResult::Errors() const { return errors_; }

        bool TestResult::IsOk() const { return errors_.Count() == 0; }

        /* ----------------------------------------------------------
 * TestSuite
 * ----------------------------------------------------------*/
        TestSuite::TestSuite() {}
        TestSuite::~TestSuite() {}
        void TestSuite::AddTest(TestInterface& test) { tests_.push_back(&test); }

        const TestResult& TestSuite::GetResults() const { return results_; }
        void TestSuite::Setup()
        {
            std::list<TestInterface*>::iterator it = tests_.begin(), end = tests_.end();

            while (it != end) {
                (*it)->Setup();
                ++it;
            }
            results_ = TestResult();
        }

        void TestSuite::Release()
        {
            std::list<TestInterface*>::iterator it = tests_.begin(), end = tests_.end();

            while (it != end) {
                (*it)->Release();
                ++it;
            }
        }

        int TestSuite::Execute(const configuration::DataType& type,
            const configuration::ParseContext& context)
        {
            std::list<TestInterface*>::iterator it = tests_.begin(), end = tests_.end();

            int test_result = 0;
            while (it != end && test_result == 0) {
                if ((*it)->Execute(type, context) != 0) {
                    results_.AddError((*it)->GetErrorDescription());
                    test_result++;
                }
                ++it;
            }
            return test_result;
        }
        int TestSuite::Execute(const configuration::DataAttribute& attribute,
            const configuration::ParseContext& context)
        {
            std::list<TestInterface*>::iterator it = tests_.begin(), end = tests_.end();

            int test_result = 0;
            while (it != end && test_result == 0) {
                if ((*it)->Execute(attribute, context) != 0) {
                    results_.AddError((*it)->GetErrorDescription());
                    test_result++;
                }
                ++it;
            }
            return test_result;
        }
        int TestSuite::Execute(const configuration::ParseContext& context)
        {
            std::list<TestInterface*>::iterator it = tests_.begin(), end = tests_.end();

            int test_result = 0;
            while (it != end && test_result == 0) {
                if ((*it)->Execute(context) != 0) {
                    results_.AddError((*it)->GetErrorDescription());
                    test_result++;
                }
                ++it;
            }

            return test_result;
        }

        /* ----------------------------------------------------------
 * TestRunner
 * ----------------------------------------------------------*/
        TestRunner::TestRunner(TestSuite& tests, int max_errors)
            : tests_(tests)
            , errors_cnt_(0)
            , max_errors_(max_errors)
        {
        }

        TestRunner::~TestRunner() {}

        int TestRunner::PrepareHandler()
        {
            errors_cnt_ = 0;
            tests_.Setup();
            return 0;
        }
        int TestRunner::DataTypeHandler(const configuration::DataType& type,
            const configuration::ParseContext& context)
        {
            errors_cnt_ += tests_.Execute(type, context);
            return errors_cnt_ >= max_errors_;
        }
        int TestRunner::DataAttributeHandler(
            const configuration::DataAttribute& attribute,
            const configuration::ParseContext& context)
        {
            errors_cnt_ += tests_.Execute(attribute, context);
            return errors_cnt_ >= max_errors_;
        }
        int TestRunner::DoneHandler(const configuration::ParseContext& context)
        {
            errors_cnt_ += tests_.Execute(context);
            tests_.Release();
            return errors_cnt_ >= max_errors_;
        }
        TestResult RunTests(TestSuite& tests, const char* filename, const char* ied,
            int max_errors)
        {
            TestRunner runner(tests, max_errors);
            configuration::Reader reader(runner);
            if (reader.Read(filename, ied)) {
                return tests.GetResults();
            }

            TestResult error_result;
            error_result.AddError(std::string(std::string("Missing file '") + std::string(filename) + std::string("'"))
                                      .c_str());
            return error_result;
        }
    }
}
}
