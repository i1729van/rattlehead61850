#pragma once
#include "configuration-test.h" // for TestInterface
#include "iec61850-config/base-types.h" // for DataAttribute, DataType
#include "iec61850-config/xml.h" // for ParseContext
#include <string> // for operator!=, string

namespace rth {
namespace iec61850 {
    namespace configuration {
        /*
 * Проверки аттрибутов данных в SCD
 */

        /* Проверка наличия необходимых параметров  */
        class TestMandatoryParameters : public configuration::TestInterface {
        public:
            TestMandatoryParameters();
            ~TestMandatoryParameters();
            const char* GetTestName() const;
            const char* GetErrorDescription() const;

            // DataTemplateTestInterface{
            int Execute(const configuration::DataType& type,
                const configuration::ParseContext& context);
            int Execute(const configuration::DataAttribute& attribute,
                const configuration::ParseContext& context);
            int Execute(const configuration::ParseContext& context);
            // }DataTemplateTestInterface

        private:
            int MakeError(const char* str, const configuration::ParseContext& context);

        private:
            std::string last_error_;
        };

        /* Заготовка теста для значений аттрибутов */
        class TestParamtersValues : public configuration::TestInterface {
        public:
            TestParamtersValues() {}
            ~TestParamtersValues() {}
            const char* GetTestName() const { return "Dummy"; }
            const char* GetErrorDescription() const { return "Dummy"; }

            // DataTemplateTestInterface{
            int Execute(const configuration::DataType& type,
                const configuration::ParseContext& context)
            {
                // Чтобы не ворнинговал.
                return context.stop == !context.stop && type.id != "";
            }
            int Execute(const configuration::DataAttribute& attribute,
                const configuration::ParseContext& context)
            {
                return context.stop == !context.stop && attribute.name != "";
            }
            int Execute(const configuration::ParseContext& context)
            {
                return context.stop == !context.stop;
            }
            // }DataTemplateTestInterface
        };

        /* Заготовка теста для проверки замкнутых / отсутсвующих / двойных типов */
        class TestTypesRelations : public configuration::TestInterface {
        public:
            TestTypesRelations() {}
            ~TestTypesRelations() {}
            const char* GetTestName() const { return "Dummy"; }
            const char* GetErrorDescription() const { return "Dummy"; }

            // DataTemplateTestInterface{
            int Execute(const configuration::DataType& type,
                const configuration::ParseContext& context)
            {
                // Чтобы не ворнинговал.
                return context.stop == !context.stop && type.id != "";
            }
            int Execute(const configuration::DataAttribute& attribute,
                const configuration::ParseContext& context)
            {
                return context.stop == !context.stop && attribute.name != "";
            }
            int Execute(const configuration::ParseContext& context)
            {
                return context.stop == !context.stop;
            }
            // }DataTemplateTestInterface
        };
    }
}
}
