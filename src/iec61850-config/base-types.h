#pragma once
#include <string>

namespace rth {
namespace iec61850 {
    namespace configuration {
        using namespace std;

        /*-----------------------------------------------------------------------------------
 * Описание модели  61850-6-2009
 *-----------------------------------------------------------------------------------*/
        /* IED */
        typedef struct IED {
            string name;
            string type;

            IED()
                : name("")
                , type("")
            {
            }
            ~IED() {}

        } IED;

        /* Лог. устройство */
        typedef struct LogicalDevice {
            string inst;

            LogicalDevice()
                : inst("")
            {
            }
            ~LogicalDevice() {}
        } LogicalDevice;

        /* Аттрибуты отчетов */
        typedef struct Report {
            string name;
            string datSet;
            string intgPd;
            string rptID;
            string confRev;
            string buffered;
            string bufTime;
            string seqNum;
            string timeStamp;
            string dataSet;
            string reasonCode;
            string dataRef;
            string bufOvfl;
            string entryID;
            string configRef;
            string segmentation;
            string dchg;
            string qchg;
            string dupd;
            string period;
            string max_enabled;

            Report()
                : name("")
                , datSet("")
                , intgPd("0")
                , rptID("")
                , confRev("")
                , buffered("false")
                , bufTime("0")
                , seqNum("false")
                , timeStamp("false")
                , dataSet("false")
                , reasonCode("false")
                , dataRef("false")
                , bufOvfl("false")
                , entryID("false")
                , configRef("false")
                , segmentation("false")
                , dchg("false")
                , qchg("false")
                , dupd("false")
                , period("false")
                , max_enabled("1")
            {
            }
        } Report;

        /* DataSet */
        typedef struct DataSet {
            string name;
        } DataSet;

        /* Набор функиональных данный */
        typedef struct FCDA {
            string ldInst;
            string prefix;
            string lnClass;
            string lnInst;
            string doName;
            string daName;
            string fc;
            FCDA()
                : ldInst("")
                , prefix("")
                , lnClass("")
                , lnInst("")
                , doName("")
                , daName("")
                , fc("")
            {
            }

        } FCDA;

        /* Лог. узел */
        typedef struct LogicalNode {
            string lnType;
            string lnClass;
            string inst;
            string prefix;
            LogicalNode()
                : lnType("")
                , lnClass("")
                , inst("")
                , prefix("")
            {
            }

        } LogicalNode;

        /*-----------------------------------------------------------------------------------
 * Типы данных 61850-6-2009
 *-----------------------------------------------------------------------------------*/
        /* Объект данных */
        typedef struct DataType {
            string element;
            string id;
            string iedType;
            string lnClass;
            string cdc;
            DataType()
                : element("")
                , id("")
                , iedType("")
                , lnClass("")
                , cdc("")
            {
            }

        } DataType;

        /* Атторибут данных */
        typedef struct DataAttribute {
            string element;
            string name;
            string sAddr;
            string bType;
            string valKind;
            string type;
            string desc;
            string accessControl;
            string fc;
            string transient;
            string dchg;
            string qchg;
            string dupd;
            string count;
            string ord;
            DataAttribute()
                : element("")
                , name("")
                , sAddr("")
                , bType("")
                , valKind("")
                , type("")
                , desc("")
                , accessControl("")
                , fc("")
                , transient("")
                , dchg("")
                , qchg("")
                , dupd("")
                , count("")
                , ord("")
            {
            }

        } DataAttribute;
    }
}
}
