#pragma once
#include "iec61850-config/base-types.h" // for Report, DataAttribute, DataSet
#include "xml.h" // for ParseContext
#include <string> // for operator==, string

namespace rth {
namespace iec61850 {
    namespace configuration {

        /**
 * @class ReaderInterface
 * @brief Общий интерфейс для всех классов-обработчиков узлов ICD/CID/SCD
 */
        class ReaderInterface {
        public:
            virtual int PrepareHandler()
            {
                // Всегда 0 без проверки второго условия
                return 0;
            }

            virtual int IEDHandler(const IED& ied, const ParseContext& context)
            {
                // Всегда 0 без проверки второго условия
                return (context.stop == !context.stop) && ied.name == "";
            }

            virtual int LogicalDeviceHandler(const LogicalDevice& ld,
                const ParseContext& context)
            {
                // Всегда 0 без проверки второго условия
                return (context.stop == !context.stop) && ld.inst == "";
            }
            virtual int LogicalNodeHandler(const LogicalNode& ln,
                const ParseContext& context)
            {
                // Всегда 0 без проверки второго условия
                return (context.stop == !context.stop) && ln.inst == "";
            }

            virtual int DataSetHandler(const DataSet& ds, const ParseContext& context)
            {
                // Всегда 0 без проверки второго условия
                return (context.stop == !context.stop) && ds.name == "";
            }

            virtual int FCDAHandler(const FCDA& fcda, const ParseContext& context)
            {
                // Всегда 0 без проверки второго условия
                return (context.stop == !context.stop) && fcda.fc == "";
            }

            virtual int ReportHandler(const Report& rcb, const ParseContext& context)
            {
                // Всегда 0 без проверки второго условия
                return (context.stop == !context.stop) && rcb.name == "";
            }

            virtual int DataTypeHandler(const DataType& type,
                const ParseContext& context)
            {
                // Всегда возвращает 0 и не делает проверку второго условия
                return (context.stop == !context.stop) && type.id == "";
            }
            virtual int DataAttributeHandler(const DataAttribute& attribute,
                const ParseContext& context)
            {
                // Всегда возвращает 0 и не делает проверку второго условия
                return (context.stop == !context.stop) && attribute.accessControl == "";
            }

            virtual int DoneHandler(const ParseContext& context)
            {
                // Всегда 0 без проверки второго условия
                return (context.stop == !context.stop);
            }

            virtual ~ReaderInterface() {}
        };

        /* Описание состояний автомата
 * FindIedOrDataTemplate - поиск узла с IED / DataTemplate
 * ProcessIed - чтение параметров IED
 * FindDataTemplate - поиск узла DataTemplates
 * ProcessDataTemplate - разбор описания типов
 * ied - имя IED. ["","*","строка"]
 *
 *
 * ied ['*'/строка]->(FindIedOrDataTemplate)-> <IED>+promisc->(ProcessIed)
 *                    |        ^       |                          ^  | |
 *                    |        |       |-----> <IED>+name==строка-|  | |
 *                    |        |--------------</IED>+promisc---------| |
 *                    |                                                |
 *                    |                                            </IED>
 *                    |                                                |
 *                    |                 --------------< (FindDataTemplate)
 *                    |                 |
 *                    --------> <DataTemplates> ----->(ProcessDataTemplate)
 *                                      ^
 *                                      |
 * ied [""]--------->(FindDataTemplate)-|
 *
 * */
        enum ParseStep {
            FindIedOrDataTemplate,
            ProcessIed,
            FindDataTemplate,
            ProcessDataTemplate
        };

        /**
 * @class ConfigReader
 * @brief Класс для чтения icd/cid/scd файлов. Последовательно вычитвает узлы
 * собирая данные в осмысленные структуры. После того, как данные для узла
 * полностью
 * прочитаны, вызвает соответсвующий метод из класса-обработчика.
 * Основная задача класса - собрать и передать данные в удобной форме. Он не
 * выполняет
 * какие-то проверки на полноту / достоверность и т.п.
 */
        class Reader {
        public:
            explicit Reader(ReaderInterface& operator_inst);
            ~Reader();
            /**
   * @brief Чтение параметров / типов данных из icd/cid/scd
   * @param filename - имя файла
   * @param ied_name - имя IED.  "" - приведет к пропуску всех IED и чтению
   * только DataTempalte
   * "*" - приведет к разбору всех устройств.
   * @return true в случае успеха
   */
            bool Read(const char* filename, const char* ied_name);

        private:
            int ParseDataTypes(const char* name, const char** atts,
                const ParseContext& context);
            int ParseIedSettings(const char* name, const char** atts,
                const ParseContext& context);

        private:
            static int StartCb(void* user_data, const char* name, const char** atts,
                const ParseContext& context);
            static int EndCb(void* user_data, const char* name,
                const ParseContext& context);

        private:
            ReaderInterface& operator_;
            string ied_name_;
            ParseStep parse_step_;
            bool promisc_mode_;
            Report rcb_settings_;
        };
    }
}
}
