#if defined(__linux__)
#include "xml.h"
#include "base/config.h" // for RTH_XML_READ_BUFER_SIZE
#include <assert.h> // for assert
#include <limits.h> // for INT_MAX
#include <string> // for string, operator==, basic_string

namespace rth {
namespace iec61850 {
    namespace configuration {

        ParseContext::ParseContext()
            : pos_current_ln(0)
            , pos_current_column(0)
            , pos_current_byte(0)
            , pos_current_byte_idx(0)
            , handler_rs(0)
            , parser_error(0)
            , stop(false)
        {
        }

        ParseContext::ParseContext(const XML_Parser parser)
        {
            stop = false;
            pos_current_ln = XML_GetCurrentLineNumber(parser);
            pos_current_column = XML_GetCurrentColumnNumber(parser);
            pos_current_byte = XML_GetCurrentByteCount(parser);
            pos_current_byte_idx = XML_GetCurrentByteIndex(parser);
            parser_error = XML_GetErrorCode(parser);
            handler_rs = 0;
        }

        const char* GetAttributeByName(const char* name, const char** atts,
            const char* def_value)
        {
            const char* kEmpty = "";
            std::string looking_for = name;

            while (*atts) {
                if (looking_for == *atts) {
                    return *(atts + 1);
                }
                atts++;
            }
            return def_value ? def_value : kEmpty;
        }

        Parser::Parser()
            : parser_(NULL)
            , start_element_handler_(NULL)
            , end_element_handler_(NULL)
            , character_data_handler_(NULL)
            , user_data_(NULL)
        {
        }

        Parser::~Parser() {}

        void Parser::Init()
        {
            assert(parser_ == NULL);
            parser_ = XML_ParserCreate(NULL);
            XML_SetStartElementHandler(parser_, StartElementWrapper);
            XML_SetEndElementHandler(parser_, EndElementWrapper);
            XML_SetCharacterDataHandler(parser_, CharacterDataWrapper);
            XML_SetUserData(parser_, this);
        }

        void Parser::Release()
        {
            assert(parser_ != NULL);
            XML_ParserFree(parser_);
            parser_ = NULL;
            start_element_handler_ = NULL;
            end_element_handler_ = NULL;
            character_data_handler_ = NULL;
            context_ = ParseContext();
        }

        void Parser::SetStartElementHandler(StartElementCb cb)
        {
            start_element_handler_ = cb;
        }

        void Parser::SetEndElementHandler(EndElementCb cb)
        {
            end_element_handler_ = cb;
        }

        void Parser::SetCharacterDataHandler(CharacterDataCb cb)
        {
            character_data_handler_ = cb;
        }

        void Parser::SetUserData(void* data) { user_data_ = data; }

        const ParseContext& Parser::GetParseContext() const { return context_; }

        ssize_t Parser::Process(const void* data, size_t size)
        {
            assert(parser_);
            assert(size <= INT_MAX);

            int isize = static_cast<int>(size);
            const char* ptr = static_cast<const char*>(data);
            const int kSinglePortionSize = RTH_XML_READ_BUFER_SIZE;
            ssize_t processed_len = 0;

            while (processed_len < isize && !context_.stop) {
                int next_parse_len = isize > kSinglePortionSize ? kSinglePortionSize : isize;

                if (XML_Parse(parser_, ptr, isize, context_.stop) != XML_STATUS_ERROR) {
                    ptr += next_parse_len;
                    processed_len += next_parse_len;
                    isize -= next_parse_len;
                } else {
                    context_.parser_error = XML_GetErrorCode(parser_);
                    break;
                }
            }

            // Нет ошибок парсинга и чтения - возвращаем кол-во ппрочитанных байт
            return context_.handler_rs == 0 && context_.parser_error == 0 ? processed_len
                                                                          : 0;
        }

        void Parser::StartElementWrapper(void* user_data, const char* name,
            const char** atts)
        {
            Parser* self = static_cast<Parser*>(user_data);
            ParseContext* ctx = &self->context_;

            // Обработчик выхывается если он есть и нет флага остановки
            if (self->start_element_handler_ && !ctx->stop) {
                ParseContext tmp_ctx(self->parser_);
                tmp_ctx.handler_rs = self->start_element_handler_(self->user_data_, name, atts, tmp_ctx);
                tmp_ctx.stop = tmp_ctx.handler_rs != 0;
                *ctx = tmp_ctx;
            }
        }

        void Parser::EndElementWrapper(void* user_data, const char* name)
        {
            Parser* self = static_cast<Parser*>(user_data);
            ParseContext* ctx = &self->context_;

            if (self->end_element_handler_ && !ctx->stop) {
                ParseContext tmp_ctx(self->parser_);
                tmp_ctx.handler_rs = self->end_element_handler_(self->user_data_, name, tmp_ctx) != 0;
                tmp_ctx.stop = tmp_ctx.handler_rs != 0;
                *ctx = tmp_ctx;
            }
        }

        void Parser::CharacterDataWrapper(void* user_data, const char* name, int len)
        {
            Parser* self = static_cast<Parser*>(user_data);
            ParseContext* ctx = &self->context_;

            if (self->character_data_handler_ && !ctx->stop) {
                ParseContext tmp_ctx(self->parser_);
                tmp_ctx.handler_rs = self->character_data_handler_(self->user_data_, name,
                                         len, tmp_ctx)
                    != 0;
                tmp_ctx.stop = tmp_ctx.handler_rs != 0;
                *ctx = tmp_ctx;
            }
        }
    }
}
}
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include "base/config.h" // for RTH_XML_READ_BUFER_SIZE
#include "xml.h"
#include <assert.h> // for assert
#include <limits.h> // for INT_MAX
#include <string> // for string, operator==, basic_string

namespace rth {
namespace iec61850 {
    namespace configuration {

        ParseContext::ParseContext()
            : pos_current_ln(0)
            , pos_current_column(0)
            , pos_current_byte(0)
            , pos_current_byte_idx(0)
            , handler_rs(0)
            , parser_error(0)
            , stop(false)
        {
        }

        ParseContext::ParseContext(const XML_Parser parser)
        {
            stop = false;
            pos_current_ln = XML_GetCurrentLineNumber(parser);
            pos_current_column = XML_GetCurrentColumnNumber(parser);
            pos_current_byte = XML_GetCurrentByteCount(parser);
            pos_current_byte_idx = XML_GetCurrentByteIndex(parser);
            parser_error = XML_GetErrorCode(parser);
            handler_rs = 0;
        }

        const char* GetAttributeByName(const char* name, const char** atts,
            const char* def_value)
        {
            const char* kEmpty = "";
            std::string looking_for = name;

            while (*atts) {
                if (looking_for == *atts) {
                    return *(atts + 1);
                }
                atts++;
            }
            return def_value ? def_value : kEmpty;
        }

        Parser::Parser()
            : parser_(NULL)
            , start_element_handler_(NULL)
            , end_element_handler_(NULL)
            , character_data_handler_(NULL)
            , user_data_(NULL)
        {
        }

        Parser::~Parser() {}

        void Parser::Init()
        {
            assert(parser_ == NULL);
            parser_ = XML_ParserCreate(NULL);
            XML_SetStartElementHandler(parser_, StartElementWrapper);
            XML_SetEndElementHandler(parser_, EndElementWrapper);
            XML_SetCharacterDataHandler(parser_, CharacterDataWrapper);
            XML_SetUserData(parser_, this);
        }

        void Parser::Release()
        {
            assert(parser_ != NULL);
            XML_ParserFree(parser_);
            parser_ = NULL;
            start_element_handler_ = NULL;
            end_element_handler_ = NULL;
            character_data_handler_ = NULL;
            context_ = ParseContext();
        }

        void Parser::SetStartElementHandler(StartElementCb cb)
        {
            start_element_handler_ = cb;
        }

        void Parser::SetEndElementHandler(EndElementCb cb)
        {
            end_element_handler_ = cb;
        }

        void Parser::SetCharacterDataHandler(CharacterDataCb cb)
        {
            character_data_handler_ = cb;
        }

        void Parser::SetUserData(void* data) { user_data_ = data; }

        const ParseContext& Parser::GetParseContext() const { return context_; }

        ssize_t Parser::Process(const void* data, size_t size)
        {
            assert(parser_);
            assert(size <= INT_MAX);

            int isize = static_cast<int>(size);
            const char* ptr = static_cast<const char*>(data);
            const int kSinglePortionSize = RTH_XML_READ_BUFER_SIZE;
            ssize_t processed_len = 0;

            while (processed_len < isize && !context_.stop) {
                int next_parse_len = isize > kSinglePortionSize ? kSinglePortionSize : isize;

                if (XML_Parse(parser_, ptr, isize, context_.stop) != XML_STATUS_ERROR) {
                    ptr += next_parse_len;
                    processed_len += next_parse_len;
                    isize -= next_parse_len;
                } else {
                    context_.parser_error = XML_GetErrorCode(parser_);
                    break;
                }
            }

            // Нет ошибок парсинга и чтения - возвращаем кол-во ппрочитанных байт
            return context_.handler_rs == 0 && context_.parser_error == 0 ? processed_len
                                                                          : 0;
        }

        void Parser::StartElementWrapper(void* user_data, const char* name,
            const char** atts)
        {
            Parser* self = static_cast<Parser*>(user_data);
            ParseContext* ctx = &self->context_;

            // Обработчик выхывается если он есть и нет флага остановки
            if (self->start_element_handler_ && !ctx->stop) {
                ParseContext tmp_ctx(self->parser_);
                tmp_ctx.handler_rs = self->start_element_handler_(self->user_data_, name, atts, tmp_ctx);
                tmp_ctx.stop = tmp_ctx.handler_rs != 0;
                *ctx = tmp_ctx;
            }
        }

        void Parser::EndElementWrapper(void* user_data, const char* name)
        {
            Parser* self = static_cast<Parser*>(user_data);
            ParseContext* ctx = &self->context_;

            if (self->end_element_handler_ && !ctx->stop) {
                ParseContext tmp_ctx(self->parser_);
                tmp_ctx.handler_rs = self->end_element_handler_(self->user_data_, name, tmp_ctx) != 0;
                tmp_ctx.stop = tmp_ctx.handler_rs != 0;
                *ctx = tmp_ctx;
            }
        }

        void Parser::CharacterDataWrapper(void* user_data, const char* name, int len)
        {
            Parser* self = static_cast<Parser*>(user_data);
            ParseContext* ctx = &self->context_;

            if (self->character_data_handler_ && !ctx->stop) {
                ParseContext tmp_ctx(self->parser_);
                tmp_ctx.handler_rs = self->character_data_handler_(self->user_data_, name,
                                         len, tmp_ctx)
                    != 0;
                tmp_ctx.stop = tmp_ctx.handler_rs != 0;
                *ctx = tmp_ctx;
            }
        }
    }
}
}
#endif //#if defined(_WIN32)
