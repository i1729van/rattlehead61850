#pragma once
#if defined(__linux__)
#include <expat.h> // for XML_Parser, XML_ParserStruct
#include <stddef.h> // for size_t, NULL
#include <sys/types.h> // for ssize_t

namespace rth {
namespace iec61850 {
    namespace configuration {

        /* Контекст работы парсера */
        typedef struct ParseContext {
            size_t pos_current_ln;
            size_t pos_current_column;
            ssize_t pos_current_byte;
            ssize_t pos_current_byte_idx;

            int handler_rs; // Результат вызова последнего хендрела
            int parser_error; // Внутрення ошибка expat

            bool stop; // Флаг остановки
            ParseContext();
            explicit ParseContext(const XML_Parser parser);
        } ParseContext;

        /**
 * @class Parser
 * @date 03/06/17
 * @brief XML парсер. Вызывает функции обработчики при входы-выход из узла
 * + чтении строковых данных.
 * Обработчик должен возвращать 0, чтобы продолжить оазбор файла
 */
        /*template <typename UserDataPtrType> */

        class Parser {
            /* Объявления обработчиков для различных читаемых узлов */
            typedef int (*StartElementCb)(void* user_data, const char* name,
                const char** atts, const ParseContext& context);
            typedef int (*EndElementCb)(void* user_data, const char* name,
                const ParseContext& context);
            typedef int (*CharacterDataCb)(void* user_data, const char* name, int len,
                const ParseContext& context);

        public:
            Parser();
            ~Parser();
            void Init();
            void Release();
            void SetStartElementHandler(StartElementCb cb);
            void SetEndElementHandler(EndElementCb cb);
            void SetCharacterDataHandler(CharacterDataCb cb);
            void SetUserData(void* data);
            const ParseContext& GetParseContext() const;
            ssize_t Process(const void* data, size_t size);

        private:
            static void StartElementWrapper(void* user_data, const char* name,
                const char** atts);
            static void EndElementWrapper(void* user_data, const char* name);
            static void CharacterDataWrapper(void* user_data, const char* name, int len);

        private:
            XML_Parser parser_;
            ParseContext context_;
            StartElementCb start_element_handler_;
            EndElementCb end_element_handler_;
            CharacterDataCb character_data_handler_;
            void* user_data_;
        };

        const char* GetAttributeByName(const char* name, const char** atts,
            const char* def_value = NULL);
    }
}
}
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include "base/defs.h"
#include <expat.h> // for XML_Parser, XML_ParserStruct
#include <stddef.h> // for size_t, NULL
#include <sys/types.h> // for ssize_t

namespace rth {
namespace iec61850 {
    namespace configuration {

        /* Контекст работы парсера */
        typedef struct ParseContext {
            size_t pos_current_ln;
            size_t pos_current_column;
            ssize_t pos_current_byte;
            ssize_t pos_current_byte_idx;

            int handler_rs; // Результат вызова последнего хендрела
            int parser_error; // Внутрення ошибка expat

            bool stop; // Флаг остановки
            ParseContext();
            explicit ParseContext(const XML_Parser parser);
        } ParseContext;

        /**
* @class Parser
* @date 03/06/17
* @brief XML парсер. Вызывает функции обработчики при входы-выход из узла
* + чтении строковых данных.
* Обработчик должен возвращать 0, чтобы продолжить оазбор файла
*/
        /*template <typename UserDataPtrType> */

        class Parser {
            /* Объявления обработчиков для различных читаемых узлов */
            typedef int (*StartElementCb)(void* user_data, const char* name,
                const char** atts, const ParseContext& context);
            typedef int (*EndElementCb)(void* user_data, const char* name,
                const ParseContext& context);
            typedef int (*CharacterDataCb)(void* user_data, const char* name, int len,
                const ParseContext& context);

        public:
            Parser();
            ~Parser();
            void Init();
            void Release();
            void SetStartElementHandler(StartElementCb cb);
            void SetEndElementHandler(EndElementCb cb);
            void SetCharacterDataHandler(CharacterDataCb cb);
            void SetUserData(void* data);
            const ParseContext& GetParseContext() const;
            ssize_t Process(const void* data, size_t size);

        private:
            static void StartElementWrapper(void* user_data, const char* name,
                const char** atts);
            static void EndElementWrapper(void* user_data, const char* name);
            static void CharacterDataWrapper(void* user_data, const char* name, int len);

        private:
            XML_Parser parser_;
            ParseContext context_;
            StartElementCb start_element_handler_;
            EndElementCb end_element_handler_;
            CharacterDataCb character_data_handler_;
            void* user_data_;
        };

        const char* GetAttributeByName(const char* name, const char** atts,
            const char* def_value = NULL);
    }
}
}
#endif //#if defined(_WIN32)
