set(BUILD_NAME iec61850-config)
project(${BUILD_NAME})

#----------------------------------------------------
# Sources
#----------------------------------------------------
set(iec61850-config_SRCs  
  reader.cpp
  xml.cpp
)

include_directories(${RATTLEHEAD_ROOT_FOLDER})

#----------------------------------------------------
# Add targets
#----------------------------------------------------
add_library(${BUILD_NAME} OBJECT 
  ${iec61850-config_SRCs}
)
