#include "reader.h"
#include "base/config.h" // for RTH_XML_READ_BUFER_SIZE
#include "base/file.h" // for File
#include "iec61850-config/base-types.h" // for Report, DataAttribute, FCDA
#include <stddef.h> // for size_t
#include <sys/types.h> // for ssize_t

namespace rth {
namespace iec61850 {
    namespace configuration {

        Reader::Reader(ReaderInterface& operator_inst)
            : operator_(operator_inst)
            , ied_name_("")
            , parse_step_(FindIedOrDataTemplate)
            , promisc_mode_(false)
        {
        }
        Reader::~Reader() {}

        bool Reader::Read(const char* filename, const char* ied_name)
        {
            using namespace rth::file;

            File file;
            if (!file.Open(filename))
                return false;

            bool parse_error = false;
            char buffer[RTH_XML_READ_BUFER_SIZE];
            ssize_t read_result;
            ssize_t total_read = 0;

            Parser parser;
            parser.Init();
            parser.SetUserData(this);
            parser.SetStartElementHandler(StartCb);
            parser.SetEndElementHandler(EndCb);

            // Запомнить имя искомого устройства
            // "" - приведет к пропуску всех IED и чтению только DataTempalte
            // "*" - приведет к разбору всех устройств. см StartCb
            ied_name_ = ied_name;

            parse_step_ = ied_name_ == "" ? FindDataTemplate : FindIedOrDataTemplate;
            promisc_mode_ = ied_name_ == "*";

            operator_.PrepareHandler();
            // Парсим файл
            while ((read_result = file.Read(buffer, sizeof(buffer))) > 0) {
                if (parser.Process(buffer, static_cast<size_t>(read_result)) == 0) {
                    parse_error = true;
                    break;
                }
                total_read += read_result;
            }
            operator_.DoneHandler(parser.GetParseContext());
            parser.Release();

            return total_read > 0 && parse_error == false;
        }

        int Reader::ParseDataTypes(const char* name, const char** atts,
            const ParseContext& context)
        {
            string node_name = name;
            if (node_name == "DO" || node_name == "DA" || node_name == "BDA" || node_name == "SDO" || node_name == "EnumVal") {
                DataAttribute attribute;
                attribute.element = node_name;
                // В зависиомсти от типа элемента набор элементов будет меняться
                // Пока не заморачиваемся и читаем все, что можно
                attribute.name = GetAttributeByName("name", atts);
                attribute.sAddr = GetAttributeByName("sAddr", atts);
                attribute.bType = GetAttributeByName("bType", atts);
                attribute.valKind = GetAttributeByName("valKind", atts);
                attribute.type = GetAttributeByName("type", atts);
                attribute.desc = GetAttributeByName("desc", atts);
                attribute.accessControl = GetAttributeByName("accessControl", atts);
                attribute.fc = GetAttributeByName("fc", atts);
                attribute.count = GetAttributeByName("count", atts);
                attribute.transient = GetAttributeByName("transient", atts);
                attribute.dchg = GetAttributeByName("dchg", atts);
                attribute.qchg = GetAttributeByName("qchg", atts);
                attribute.dupd = GetAttributeByName("dupd", atts);
                attribute.ord = GetAttributeByName("ord", atts);
                return operator_.DataAttributeHandler(attribute, context);
            } else if (node_name == "LNodeType" || node_name == "DOType" || node_name == "DAType" || node_name == "EnumType") {
                DataType type;
                type.element = node_name;
                type.id = GetAttributeByName("id", atts);
                type.iedType = GetAttributeByName("iedType", atts);
                type.lnClass = GetAttributeByName("lnClass", atts);
                type.cdc = GetAttributeByName("cdc", atts);
                return operator_.DataTypeHandler(type, context);

            } else
                return 0;
        }

        int Reader::ParseIedSettings(const char* name, const char** atts,
            const ParseContext& context)
        {
            /*
  * Немного статистики по распространеннсоти узлов
  ICD N: Total:845 IED:1 LD:1 DS:5 FCDA:66 RCB:8 LN:38 DT:102 DA:624
  ICD %: IED:0.118343 LD:0.118343 DS:0.591716 FCDA:7.81065 RCB:0.946746
  LN:4.49704 DT:12.071 DA:73.8462

  SCD N: Total:15425 IED:62 LD:94 DS:987 FCDA:10603 RCB:1042 LN:1911 DT:102
  DA:624
  SCD %: FCDA:68.7391 LN:12.389 DS:6.3987 DA:4.04538 RCB:6.75527(~1.5% на
  запись) DT:0.661264 LD:0.6094  IED:0.401945
  */
            string node_name = name;
            if (node_name == "FCDA") {
                FCDA fcda;
                fcda.ldInst = GetAttributeByName("ldInst", atts);
                fcda.prefix = GetAttributeByName("prefix", atts);
                fcda.lnClass = GetAttributeByName("lnClass", atts);
                fcda.lnInst = GetAttributeByName("lnInst", atts);
                fcda.doName = GetAttributeByName("doName", atts);
                fcda.daName = GetAttributeByName("daName", atts);
                fcda.fc = GetAttributeByName("fc", atts);
                return operator_.FCDAHandler(fcda, context);
            } else if (node_name == "LN" || node_name == "LN0") {
                LogicalNode ln;
                ln.inst = GetAttributeByName("inst", atts);
                ln.lnClass = GetAttributeByName("lnClass", atts);
                ln.lnType = GetAttributeByName("lnType", atts);
                ln.prefix = GetAttributeByName("prefix", atts);
                return operator_.LogicalNodeHandler(ln, context);
            } else if (node_name == "DataSet") {
                DataSet ds;
                ds.name = GetAttributeByName("name", atts);
                return operator_.DataSetHandler(ds, context);
            } else if (node_name == "IED") {
                IED ied;
                ied.name = GetAttributeByName("name", atts);
                ied.type = GetAttributeByName("type", atts);
                return operator_.IEDHandler(ied, context);
            } else if (node_name == "LDevice") {
                LogicalDevice ld;
                ld.inst = GetAttributeByName("inst", atts);
                return operator_.LogicalDeviceHandler(ld, context);
            } else if (node_name == "ReportControl") {
                //Начинаем разбирать репорт. Они собирательные, так что инфу необходимо
                //будет накапливать
                // ReportControl
                // TrgOps
                // OptFields
                // RptEnabled
                rcb_settings_ = Report();
                rcb_settings_.name = GetAttributeByName("name", atts);
                rcb_settings_.intgPd = GetAttributeByName("intgPd", atts, rcb_settings_.intgPd.c_str());
                rcb_settings_.rptID = GetAttributeByName("rptID", atts);
                rcb_settings_.datSet = GetAttributeByName("datSet", atts);
                rcb_settings_.confRev = GetAttributeByName("confRev", atts);
                rcb_settings_.bufTime = GetAttributeByName("bufTime", atts, rcb_settings_.bufTime.c_str());
                rcb_settings_.buffered = GetAttributeByName("buffered", atts, rcb_settings_.buffered.c_str());
            } else if (node_name == "OptFields") {
                rcb_settings_.seqNum = GetAttributeByName("seqNum", atts, rcb_settings_.seqNum.c_str());
                rcb_settings_.timeStamp = GetAttributeByName("timeStamp", atts, rcb_settings_.timeStamp.c_str());
                rcb_settings_.dataSet = GetAttributeByName("dataSet", atts, rcb_settings_.dataSet.c_str());
                rcb_settings_.reasonCode = GetAttributeByName(
                    "reasonCode", atts, rcb_settings_.reasonCode.c_str());
                rcb_settings_.dataRef = GetAttributeByName("dataRef", atts, rcb_settings_.dataRef.c_str());
                rcb_settings_.bufOvfl = GetAttributeByName("bufOvfl", atts, rcb_settings_.bufTime.c_str());
                rcb_settings_.entryID = GetAttributeByName("entryID", atts, rcb_settings_.buffered.c_str());
                rcb_settings_.configRef = GetAttributeByName("configRef", atts, rcb_settings_.buffered.c_str());
                rcb_settings_.segmentation = GetAttributeByName(
                    "segmentation", atts, rcb_settings_.buffered.c_str());
            } else if (node_name == "TrgOps") {
                rcb_settings_.dchg = GetAttributeByName("dchg", atts, rcb_settings_.dchg.c_str());
                rcb_settings_.qchg = GetAttributeByName("qchg", atts, rcb_settings_.qchg.c_str());
                rcb_settings_.dupd = GetAttributeByName("dupd", atts, rcb_settings_.dupd.c_str());
                rcb_settings_.period = GetAttributeByName("period", atts, rcb_settings_.period.c_str());
            } else if (node_name == "RptEnabled") {
                rcb_settings_.max_enabled = GetAttributeByName("max", atts, rcb_settings_.max_enabled.c_str());
            }

            return 0;
        }

        int Reader::StartCb(void* user_data, const char* name, const char** atts,
            const ParseContext& context)
        {
            if (context.stop) {
                return 0;
            }

            Reader* raw_reader = static_cast<Reader*>(user_data);

            // Переходы
            switch (raw_reader->parse_step_) {
            case FindIedOrDataTemplate: {
                // Если найден IED с заданным именем или установлен неразборчивый режим -
                // парсим IED
                // Если надено начало области типов данных - переходим в режим парсинга
                // типов
                // данных
                string node_name = name;
                if (node_name == "IED") {
                    if (raw_reader->promisc_mode_ || GetAttributeByName("name", atts) == raw_reader->ied_name_) {
                        raw_reader->parse_step_ = ProcessIed;
                    }
                } else if (node_name == "DataTypeTemplates") {
                    raw_reader->parse_step_ = ProcessDataTemplate;
                }
            } break;

            case FindDataTemplate: {
                // Задан поиск данных и найден узел с данными
                string node_name = name;
                if (node_name == "DataTypeTemplates") {
                    raw_reader->parse_step_ = ProcessDataTemplate;
                }
            } break;

            case ProcessIed:
            case ProcessDataTemplate:
            default:
                break;
            }

            // Действия
            switch (raw_reader->parse_step_) {
            case ProcessIed:
                return raw_reader->ParseIedSettings(name, atts, context);

            case ProcessDataTemplate:
                return raw_reader->ParseDataTypes(name, atts, context);

            case FindIedOrDataTemplate:
            case FindDataTemplate:
            default:
                break;
            }

            return 0;
        }

        int Reader::EndCb(void* user_data, const char* name,
            const ParseContext& context)
        {
            if (context.stop) {
                return 0;
            }

            Reader* raw_reader = static_cast<Reader*>(user_data);

            // Переходы
            switch (raw_reader->parse_step_) {
            case ProcessIed: {
                // Выход из узла IED.
                // Если мы в неразборчиво режиме, то ищем сл. IED или тип данных
                // Если грузим конкретный IED, то ищем описание данных
                string node_name = name;
                if (node_name == "IED") {
                    raw_reader->parse_step_ = raw_reader->promisc_mode_
                        ? FindIedOrDataTemplate
                        : FindDataTemplate;
                }
                break;
            }
            case FindDataTemplate:
            case FindIedOrDataTemplate:
            case ProcessDataTemplate:
            default:
                break;
            }

            // Действия
            switch (raw_reader->parse_step_) {
            case ProcessIed: {
                // Выход из блока ReportControl. Пока уведомить клиента
                string node_name = name;
                if (node_name == "ReportControl") {
                    int result = 0;
                    result = raw_reader->operator_.ReportHandler(raw_reader->rcb_settings_,
                        context);
                    raw_reader->rcb_settings_ = Report();
                    return result;
                }
                break;
            }
            case FindDataTemplate:
            case FindIedOrDataTemplate:
            case ProcessDataTemplate:
            default:
                break;
            }

            return 0;
        }
    }
}
}
