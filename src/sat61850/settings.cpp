﻿#include "settings.h"
#include "app_common.h"

namespace SE {
namespace Volcano {
    namespace IEC61850 {

        SatelliteSetting::SatelliteSetting(const VcLibInParams* params)
            : vc_api_(params->pVcAPI)
            , log_(params->log)
            , config_name_(params->ParamStr)
            , connection_name_("")
        {
            ReadFile();
        }

        bool SatelliteSetting::IsValid() const
        {
            return vc_api_ != NULL && config_name_.size() > 0 && connection_name_ != "";
        }

        const char* SatelliteSetting::GetFileName() const
        {
            return config_name_.c_str();
        }

        const char* SatelliteSetting::GetConnectionName() const
        {
            return connection_name_.c_str();
        }

        IVcAPI& SatelliteSetting::GetAPI() const { return *vc_api_; }

        logger_t SatelliteSetting::GetLog() const { return log_; }
        bool SatelliteSetting::ReadFile()
        {
            SE::General::DomDocument cfgFile;
            if (cfgFile.loadFromFile(config_name_.c_str(), logger_none)) {
                for (const SE::General::IDomElement* node = cfgFile.documentElement()->getFirstChild();
                     node; node = node->getNextSibling()) {
                    General::NamedParameters p(node);
                    if (std::string(node->c_name()) == "Connection") {
                        return p.getObligatoryParameter("name", &connection_name_, logger_none);
                    }
                }
            }
            return false;
        }

    } // IEC61850
} // Volcano
} // SE
