﻿#include "tag-map.h"

namespace SE {
namespace Volcano {
    namespace IEC61850 {

        ValueTagMapReadResult& ReadFromFile(const char* name)
        {
            assert(name);
            VisitedMap visited;
            ValueTagMapReadResult* result = new ValueTagMapReadResult;
            ValueTagMap& map = result->GetData();
            DefaultErrorList& errors = result->GetErrorList();
            SE::General::DomDocument cfgFile;

            if (!cfgFile.loadFromFile(name, logger_none)) {
                errors.Append(DefaultErrorDescription("Can't open config file"));
            } else {
                for (const SE::General::IDomElement* node = cfgFile.documentElement()->getFirstChild();
                     node; node = node->getNextSibling()) {
                    General::NamedParameters p(node);

                    if (std::string(node->c_name()) == "ValueMap") {
                        std::string tag, value, quality, tstamp;
                        size_t error_cnt = errors.Count();

                        /* Проверить наличие параметров */
                        if (!p.getObligatoryParameter("tag", &tag, logger_none)) {
                            if (!errors.Append(DefaultErrorDescription(
                                    std::string(
                                        cfgFile.documentElement()->getLocation() + std::string(" Missing 'tag' parameter")))))
                                break;
                        }

                        if (!p.getObligatoryParameter("value", &value, logger_none)) {
                            if (!errors.Append(DefaultErrorDescription(
                                    std::string(
                                        cfgFile.documentElement()->getLocation() + std::string(" Missing 'value' parameter")))))
                                break;
                        }

                        if (!p.getObligatoryParameter("quality", &quality, logger_none)) {
                            if (!errors.Append(DefaultErrorDescription(
                                    std::string(
                                        cfgFile.documentElement()->getLocation() + std::string(" Missing 'quality' parameter")))))
                                break;
                        }

                        if (!p.getObligatoryParameter("tstamp", &tstamp, logger_none)) {
                            if (!errors.Append(DefaultErrorDescription(
                                    std::string(
                                        cfgFile.documentElement()->getLocation() + std::string(" Missing 'tstamp' parameter")))))
                                break;
                        }

                        /* Запись в один тэг 61850 с разных мест запрещена */
                        if (visited.find(value) != visited.end()) {
                            if (!errors.Append(DefaultErrorDescription(
                                    std::string(
                                        cfgFile.documentElement()->getLocation() + std::string(" Duplicated 'value' destination in ") + tag))))
                                break;
                        }
                        if (visited.find(quality) != visited.end()) {
                            if (!errors.Append(DefaultErrorDescription(
                                    std::string(
                                        cfgFile.documentElement()->getLocation()
                                        + std::string(" Duplicated 'quality' destination in ") + tag))))
                                break;
                        }
                        if (visited.find(tstamp) != visited.end()) {
                            if (!errors.Append(DefaultErrorDescription(
                                    std::string(
                                        cfgFile.documentElement()->getLocation()
                                        + std::string(" Duplicated 'tstamp' destination in ") + tag))))
                                break;
                        }

                        /* Двойное кофнигурирование запрещено */
                        if (map.find(tag) != map.end()) {
                            if (!errors.Append(DefaultErrorDescription(
                                    std::string(
                                        cfgFile.documentElement()->getLocation() + std::string(" Duplicated tag name ") + tag))))
                                break;
                        }

                        // За цикл новых ошибок не появилось - можно добавлять запись
                        if (error_cnt == errors.Count()) {
                            map[tag] = ShareValueInfo(value, tstamp, quality);
                            visited[value] = true;
                            visited[quality] = true;
                            visited[tstamp] = true;
                        }
                    }
                }

                if (map.size() == 0) {
                    errors.Append(DefaultErrorDescription(
                        std::string(cfgFile.documentElement()->getLocation() + std::string(" Can't find value mapping info"))));
                }
            }
            return *result;
        }

    } // IEC61850
} // Volcano
} // SE
