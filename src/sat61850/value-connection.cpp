﻿#include "satellite.h"

namespace SE {
namespace Volcano {
    namespace IEC61850 {

        // TODO. Подумать и при необходимости пренести в настройки
        static const size_t kWriteLowLimit = 128; // Кол-во сообщений в очереди,
        // которые не будут приводить к
        // немедленной отправке в срвер

        static const size_t kWriteDelayMicro = 8 * 1000; // Задержка перед отправклй на
        // сервер, если число сообщений
        // меньше kWriteLowLimit

        static void ErrorsToLog(const TransactionErrors& list, logger_t log)
        {
            std::stringstream stream;
            ErrorsToStream<TransactionErrors, std::stringstream>(list, stream);
            LOG_ERR(log, stream.str().c_str());
        }

        static bool GotDisconnectReason(const TransactionErrors& errors)
        {
            TransactionErrors::ConstIterator it = errors.Begin(), end = errors.End();
            while (it != end) {
                DataAttributeUpdateResult result = it->GetResult();
                if (result == DataAttributeUpdateResultUnk || result == DataAttributeUpdateResultNotRespond || result == DataAttributeUpdateResultInvalidData) {
                    return true;
                }
                it++;
            }
            return false;
        }

        static DataValue IECTimeTag(const char* name, const VcValueData& data)
        {
            return DataValue(name, DataValueTypeUInt64, data.time_utc);
        }

        static DataValue IECQualityTag(const char* name, const VcValueData& data)
        {
            uint16_t quality = 0;
            // IECQuals. См 61850-7-3 / libIEC iec61850_common.h
            const int QUALITY_VALIDITY_GOOD = 0;
            const int QUALITY_VALIDITY_INVALID = 2;
            // const int QUALITY_VALIDITY_RESERVED =1;
            const int QUALITY_VALIDITY_QUESTIONABLE = 3;
            // const int QUALITY_DETAIL_OVERFLOW =4;
            const int QUALITY_DETAIL_OUT_OF_RANGE = 8;
            const int QUALITY_DETAIL_BAD_REFERENCE = 16;
            // const int QUALITY_DETAIL_OSCILLATORY =32;
            const int QUALITY_DETAIL_FAILURE = 64;
            // const int QUALITY_DETAIL_OLD_DATA =128;
            const int QUALITY_DETAIL_INCONSISTENT = 256;
            const int QUALITY_DETAIL_INACCURATE = 512;
            /*const int QUALITY_SOURCE_SUBSTITUTED =1024;
  const int QUALITY_TEST =2048;
  const int QUALITY_OPERATOR_BLOCKED = 4096   ; */

            switch (data.Quality) {
            case VC_VQ__GOOD:
                quality = QUALITY_VALIDITY_GOOD;
                break;
            case VC_VQ__NOT_INIT:
                quality = (QUALITY_VALIDITY_QUESTIONABLE | QUALITY_DETAIL_INCONSISTENT);
                break;
            case VC_VQ__LOW:
            case VC_VQ__HI:
                quality = (QUALITY_VALIDITY_INVALID | QUALITY_DETAIL_OUT_OF_RANGE);
                break;
            case VC_VQ__COMM:
                quality = (QUALITY_VALIDITY_INVALID | QUALITY_DETAIL_FAILURE);
                break;
            case VC_VQ__CONVERT:
                quality = (QUALITY_VALIDITY_QUESTIONABLE | QUALITY_DETAIL_INACCURATE);
                break;
            case VC_VQ__NA:
            case VC_VQ__NA_TEMP:
                quality = (QUALITY_VALIDITY_INVALID | QUALITY_DETAIL_BAD_REFERENCE);
                break;
            default:
                quality = QUALITY_VALIDITY_INVALID;
                break;
            }
            return DataValue(name, DataValueTypeUInt16, quality);
        }

        static DataValue IECValueTag(const char* name, const VcValueData& data)
        {
            switch (data.vt()) {
            case VC_TAG_TYPE_BOOL:
                return DataValue(name, DataValueTypeUInt8, data.Value.get_bool());

            case VC_TAG_TYPE_I32:
                return DataValue(name, DataValueTypeInt32, data.Value.get_i32());

            case VC_TAG_TYPE_I64:
                return DataValue(name, DataValueTypeInt64, data.Value.get_i64());

            case VC_TAG_TYPE_UI32:
                return DataValue(name, DataValueTypeUInt32, data.Value.get_ui32());

            case VC_TAG_TYPE_UI64:
                return DataValue(name, DataValueTypeUInt64, data.Value.get_ui64());

            case VC_TAG_TYPE_F32:
                return DataValue(name, DataValueTypeFloat, data.Value.get_f32());

            case VC_TAG_TYPE_F64:
                return DataValue(name, DataValueTypeFloat, data.Value.get_f64());

            default:
                return DataValue(name, DataValueTypeUnk, data.Value.get_ui64());
            }
        }

        ValueConnection::ValueConnection(const SatelliteSetting& settings)
            : settings_(settings)
            , is_connected_(false)
            , cmd_stop_(false)
        {
            queue_thread_ = boost::thread(boost::ref(*this));
        }

        ValueConnection::~ValueConnection() {}

        bool ValueConnection::CheckConnection()
        {
            boost::lock_guard<boost::mutex> locker(data_mutex_);
            return is_connected_;
        }

        bool ValueConnection::Push(const VcValuePair& value)
        {
            // SE::General::MutexLocker locker(&data_mutex_);
            return queue_.Push(value);
        }

        bool ValueConnection::Release()
        {
            cmd_stop_mutex_.lock();
            cmd_stop_ = true;
            cmd_stop_mutex_.unlock();

            return queue_thread_.try_join_until(boost::chrono::steady_clock().now() + boost::chrono::seconds(1));
        }

        bool ValueConnection::NeedStop() const
        {
            boost::lock_guard<boost::mutex> locker(cmd_stop_mutex_);
            return cmd_stop_;
        }

        void ValueConnection::SetConnectionState(bool connected)
        {
            boost::lock_guard<boost::mutex> locker(data_mutex_);
            is_connected_ = connected;
        }

        size_t ValueConnection::GetOutputSize() const { return queue_.Size(); }

        void ValueConnection::operator()()
        {
            const size_t kFailLimit = 3;
            const size_t kTransSize = connector::Connection::GetMaxTransactionSize();
            DataBlock* out_buffer = new DataBlock[kTransSize];
            //ValueConnection* self = reinterpret_cast<ValueConnection*>(args);
            connector::Connection* connection = new connector::Connection(settings_.GetConnectionName());

            size_t fail_cnt = 0;
            while (!NeedStop()) {
                if (connection->IsOpen()) {
                    // TODO: Добавить проверку связи. Иначе при медленном обмене мы долго не
                    // узнаем
                    // об ошибке

                    // Если данных меньше, чем граница сработки - немного тупим
                    if (queue_.Size() < kWriteLowLimit) {
                        //SE::Platform::Thread::sleepMs(kWriteDelayMicro / 1000);
                        boost::this_thread::sleep_until(boost::chrono::steady_clock().now() + boost::chrono::microseconds(kWriteDelayMicro));
                    }

                    // Проверяем очередь и приступаем к упаквке данных
                    size_t to_send_cnt = queue_.Size();
                    size_t processed_cnt = 0;
                    size_t block_idx = 0;

                    // Добавляем данные в блоки
                    for (block_idx = 0; block_idx < kTransSize && processed_cnt < to_send_cnt;
                         block_idx++) {
                        DataBlock block; // Пром. блок, чтобы не чистить буфер
                        DataBlockStream stream(block); // TODO. Сделать обработку масива блоков
                        while (block.GetFreeSize() >= 3 && processed_cnt < to_send_cnt) {
                            const VcValuePair& pair = queue_.Pop();
                            const ShareValueInfo& info = pair.info;
                            const VcValueData& vd = pair.data;

                            stream << IECTimeTag(info.tstamp_name.c_str(), vd);
                            stream << IECQualityTag(info.quality_name.c_str(), vd);
                            stream << IECValueTag(info.value_name.c_str(), vd);

                            processed_cnt += 3;
                        }
                        // Записать в выходной буфер
                        out_buffer[block_idx] = block;
                    }

                    // Отпраить подготовленные блоки
                    SendResult result = connection->Send(out_buffer, block_idx);
                    if (!result.IsOk()) {
                        // Проверить критичные ошибки. Если они есть - это повод для
                        // переподключения
                        fail_cnt += GotDisconnectReason(result.GetErrorList());
                        if (fail_cnt >= kFailLimit) {
                            connection->Close();
                            // Lock
                            LOG_ERR(settings_.GetLog(), "Connection lost");
                            // Unlock
                            fail_cnt = 0;
                        }

                        // Lock
                        ErrorsToLog(result.GetErrorList(), settings_.GetLog());
                        // Unlock
                    }

                } else {
                    // Переподключение
                    if (!connection->Open()) {
                        SetConnectionState(false);
                        //SE::Platform::Thread::sleepMs(1000);
                        boost::this_thread::sleep_until(boost::chrono::steady_clock().now() + boost::chrono::milliseconds(1000));
                    } else {
                        SetConnectionState(true);
                    }
                }
            }

            delete connection;
            delete[] out_buffer;
        }

    } // IEC61850
} // Volcano
} // SE
