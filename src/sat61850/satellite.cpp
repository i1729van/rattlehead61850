﻿#include "satellite.h"
#include "general/logger.h"
#include "vc/config.h"
#include "vc/messages.h"
#include "vc/pipe.h"

namespace SE {
namespace Volcano {
    namespace IEC61850 {

        /* enum SatelliteTimers {
            SatelliteTimersCheckConnection = 1,
            SatelliteTimersTagDebug = 2
        }; */

        static void ErrorsToLog(const DefaultErrorList& list, logger_t log)
        {
            std::stringstream stream;
            ErrorsToStream<DefaultErrorList, std::stringstream>(list, stream);
            LOG_ERR(log, stream.str().c_str());
        }

// TODO. Удалить после подключения симуляции
#ifdef INSAT_GENERATION
        static const size_t kGenerateLimit = 12000;
        static HashTable<int, VcTagInfo> gDebugTags;
        static void AddImitTagDebugOnly(int tag_id, const VcTagInfo& info)
        {
            gDebugTags[info.Id] = info;
        }

        static void ImitTagDebugOnly(Satellite& self)
        {
            HashTable<int, VcTagInfo>::const_iterator it = gDebugTags.begin(),
                                                      end = gDebugTags.end();
            while (it != end) {
                const VcTagInfo& info = it->second;
                VcValueData data(info.vt);
                data.Quality = 0;
                data.TimeUTC = SE::Platform::DateTime::now();
                switch (info.vt) {
                case VC_TAG_TYPE_BOOL:
                    data.Value.Bool = (rand() % 2) == 0;
                    break;
                case VC_TAG_TYPE_I32:
                    data.Value.I32 = rand();
                    break;
                case VC_TAG_TYPE_UI32:
                    data.Value.UI32 = static_cast<uint32_t>(rand());
                    break;
                case VC_TAG_TYPE_I64:
                    data.Value.I64 = rand();
                    break;
                case VC_TAG_TYPE_UI64:
                    data.Value.UI64 = static_cast<uint64_t>(rand());
                    break;
                case VC_TAG_TYPE_F32:
                    data.Value.F32 = static_cast<float>(rand());
                    break;
                case VC_TAG_TYPE_F64:
                    data.Value.F64 = static_cast<double>(rand());
                    break;
                default:
                    WTF;
                }
                self.OnTagUpdated(0, info.Id, &data);
                ++it;
            }
        }
#endif

        Satellite::Satellite(const SatelliteSetting& settings)
            : settings_(settings)
            , value_connection_(settings)
        {
        }
        Satellite::~Satellite() {}

        int Satellite::onLoad_Sync()
        {
            return OnLoad();
        }
        int Satellite::onConnect_Sync()
        {
            return OnAfterLoad();
        }

        int Satellite::runSatellite(SE::Volcano::ISatellitePipe* pipe)
        {
            bool still_working = true;
            uint8_t counter = 0;

            while (still_working) {

                const MsgToSat* msg = pipe->pop_st(VOLCANO_SAT_DEFAULT_POP_TIMEOUT);
                if (!msg)
                    continue;

                switch (msg->Type) {
                case VC_MSG_ID_2SAT__UPDATE:
                    OnTagUpdated(msg->Data.Update.SubsId, msg->Data.Update.TagId, &msg->vd);
                    break;

                case VC_MSG_ID_2SAT__STOP_SIG:
                    OnStopSig();
                    pipe->push_prod_stopped_mt();
                    pipe->flush_mt();
                    still_working = false;
                    break;

                case VC_MSG_ID_2SAT__PIPE_SHUTDOWN:
                    OnStopSig();
                    still_working = false;
                    break;

                default:
                    break;

                } //

                // Периодические проверки. Не слишком критичны к точности интервалов
                // Раньше были в OnTimer
                if (counter++ > 50) {

#ifdef INSAT_GENERATION
                    // TODO. Удалить. Начало затычки {
                    for (size_t i = 0; i < kGenerateLimit; i++)
                        ImitTagDebugOnly(*this);
                        // } TODO. конец затычки
#endif

                    if (!value_connection_.CheckConnection()) {
                        LOG_WARN(settings_.GetLog(), "Connection lost");
                    }
                    counter = 0;
                }
            }

            return 0;
        }

        int Satellite::OnLoad()
        {
            if (!InitValueTags()) {
                LOG_ERR(settings_.GetLog(), "Mapping data error ");
                value_connection_.Release();
                return 1;
            }

            return 0;
        }

        int Satellite::OnAfterLoad() { return 0; }

        int Satellite::OnStart() { return 0; }

        int Satellite::OnTagUpdated(int subId, int tagId, const VcValueData* vd)
        {
            std::stringstream stream;
            stream << tagId;

            ValueTagMap::const_iterator it = value_map_.find(stream.str());

            if (it != value_map_.end()) {
                /*const ShareValueInfo& info = it->second;
                std::cout << "V: " << info.value_name << "\n";
    std::cout << "T: " << info.tstamp_name << "\n";
    std::cout << "Q: " << info.quality_name << "\n";
    std::cout << "\n";*/
                if (!value_connection_.Push(VcValuePair(it->second, *vd))) {
                    LOG_ERR(settings_.GetLog(), "Value queue overflow");
                }
            }

            return 0;
        }

        /*   int Satellite::OnTimer(int timerId, void* timerUserData)
        {
            switch (timerId) {
            case SatelliteTimersCheckConnection:
                if (!value_connection_.CheckConnection()) {
                    LOG_WARN(settings_.GetLog(), "Connection lost");
                }
                break;

#ifdef INSAT_GENERATION
            // TODO. Удалить. Начало затычки {
            case SatelliteTimersTagDebug:
                for (size_t i = 0; i < kGenerateLimit; i++)
                    ImitTagDebugOnly(*this);
                break;
// } TODO. конец затычки
#endif

            default:
                break;
            }

            return 0;
        } */
        void Satellite::OnStopSig() { value_connection_.Release(); }

        bool Satellite::OnIsProductionClosed() { return true; }

        int Satellite::OnRelease() { return 0; }

        bool Satellite::InitValueTags()
        {
            ValueTagMapReadResult& result = ReadFromFile(settings_.GetFileName());
            bool load_success = result.IsValid();
            bool map_success = false;

            if (result.IsValid()) {
                // Заполнить таблицу id -> value tag
                const VcTagInfo* info;
                IVcAPI& api = settings_.GetAPI();
                ValueTagMap& map = result.GetData();
                ValueTagMap::const_iterator it = map.begin(), end = map.end();
                DefaultErrorList& errors = result.GetErrorList();
                while (it != end) {
                    const std::string& key = it->first;

                    // Пропуск пустых значений
                    if (key == "") {
                        continue;
                    }

                    info = api.Db_FindTagByLongName_Sync(key.c_str());
                    if (info) {
                        /*! Дублирование тэгов уже отсечено на этапе чтения из файла. Здесь
         * можно не проверять. */
                        std::stringstream stream;
                        stream << info->Id;
                        value_map_[stream.str()] = it->second;

                        // Подписаться
                        api.Db_Subscribe_Load_Sync(info->Id, info->Id, NULL, settings_.GetLog());

#ifdef INSAT_GENERATION
                        // TODO. Rm
                        AddImitTagDebugOnly(info->Id, *info);
#endif
                    } else {
                        /* Тэг, отсусвующий в БД будет приводить к добавленю записи об ошибке,
         * но
         * не помещает запустить сервер */
                        if (!errors.Append(DefaultErrorDescription(
                                key + std::string(" is missing in VC database")))) {
                            break;
                        }
                    }
                    ++it;
                }
                map_success = value_map_.size() > 0;
                if (!map_success) {
                    errors.Append(DefaultErrorDescription("Value map is empty"));
                }
                // api.AddTimer(1000, SatelliteTimersCheckConnection, NULL);
                // api.AddTimer(1000, SatelliteTimersTagDebug, NULL);
            }

            if (!(result.IsValid() && map_success)) {
                // ErrorsToStream<DefaultErrorList, std::ostream>(result.GetErrorList(),
                //                                               std::cerr);
                ErrorsToLog(result.GetErrorList(), settings_.GetLog());
            }

            ValueTagMapReadResult::Release(result);
            return load_success && map_success;
        }

    } // IEC61850
} // Volcano
} // SE
