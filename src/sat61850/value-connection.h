﻿#ifndef VOLCANO_SRC_SAT61850_VALUE_CONNECTION_H
#define VOLCANO_SRC_SAT61850_VALUE_CONNECTION_H

#include "iec61850-common/errors-defaults.h"
#include "iec61850-common/errors.h"
#include "iec61850-connector/connection.h"
#include "queue.h"
#include "settings.h"
#include "tag-map.h"
#include "vc/value_data.h"
#include <boost/thread.hpp>

namespace SE {
namespace Volcano {
    namespace IEC61850 {

        using namespace rth::iec61850;
        using namespace rth::iec61850::connector;

        typedef struct VcValuePair {
            ShareValueInfo info;
            VcValueData data;

            VcValuePair() {}
            VcValuePair(const ShareValueInfo& tag_info, const VcValueData& tag_data)
                : info(tag_info)
                , data(tag_data)
            {
            }

        } VcValuePair;

        class ValueConnection : public boost::noncopyable {
        public:
            explicit ValueConnection(const SatelliteSetting& settings);
            ~ValueConnection();
            bool Release();
            bool CheckConnection();
            bool Push(const VcValuePair& value);
            size_t GetOutputSize() const;
            void operator()();

        private:
            bool NeedStop() const;
            void SetConnectionState(bool connected);
            // bool Connect();
            //  bool Disconnect();

        private:
            SatelliteSetting settings_;
            bool is_connected_;
            bool cmd_stop_;

            mutable boost::mutex cmd_stop_mutex_;
            mutable boost::mutex data_mutex_;

            ThreadSafeQueue<VcValuePair> queue_;
            boost::thread queue_thread_;
        };

    } // IEC61850
} // Volcano
} // SE
#endif
