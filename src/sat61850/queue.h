﻿#ifndef VOLCANO_SRC_SAT61850_QUEUE_H
#define VOLCANO_SRC_SAT61850_QUEUE_H

#include <boost/thread/lock_guard.hpp>
#include <boost/thread/mutex.hpp>
#include <queue>

namespace SE {
namespace Volcano {
    namespace IEC61850 {

        template <typename T>
        class ThreadSafeQueue {
        public:
            explicit ThreadSafeQueue(size_t limit = 32000)
                : limit_(limit)
            {
            }
            ~ThreadSafeQueue() {}

            size_t Size() const
            {
                boost::lock_guard<boost::mutex> locker(mutex_);
                return queue_.size();
            }

            T Pop()
            {
                boost::lock_guard<boost::mutex> locker(mutex_);
                if (queue_.size() == 0) {
                    return T();
                }
                T result = queue_.front();
                queue_.pop();
                return result;
            }

            bool Push(const T& value)
            {
                boost::lock_guard<boost::mutex> locker(mutex_);
                if (queue_.size() >= limit_) {
                    return false;
                }
                queue_.push(value);
                return true;
            }

        private:
            mutable boost::mutex mutex_;
            std::queue<T> queue_;
            size_t limit_;
        };

    } // IEC61850
} // Volcano
} // SE

#endif
