﻿#ifndef VOLCANO_SRC_SAT61850_SATELLITE_H
#define VOLCANO_SRC_SAT61850_SATELLITE_H

#include "iec61850-common/errors-defaults.h"
#include "iec61850-common/errors.h"
#include "iec61850-connector/connection.h"
#include "settings.h"
#include "tag-map.h"
#include "value-connection.h"

namespace SE {
namespace Volcano {

    namespace IEC61850 {

        using namespace rth::iec61850;
        using namespace rth::iec61850::connector;

        class Satellite : public ISatellite {
        public:
            explicit Satellite(const SatelliteSetting& settings);
            ~Satellite();

        public:
            virtual int onLoad_Sync();
            virtual int onConnect_Sync();
            virtual int runSatellite(SE::Volcano::ISatellitePipe* pipe);

        private:
            int OnLoad();
            int OnAfterLoad();
            int OnStart();
            int OnTagUpdated(int subId, int tagId, const VcValueData* vd);
            int OnTimer(int timerId, void* timerUserData);
            void OnStopSig();
            bool OnIsProductionClosed();
            int OnRelease();

        private:
            bool InitValueTags();
            bool CheckConnection();

        private:
            SatelliteSetting settings_;
            ValueTagMap value_map_;
            ValueConnection value_connection_;
        };

    } // IEC61850
} // Volcano
} // SE
#endif
