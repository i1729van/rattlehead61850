﻿#ifndef VOLCANO_SRC_SAT61850_SETTINGS_H
#define VOLCANO_SRC_SAT61850_SETTINGS_H

#include "app_common.h"
#include "vc/library.h"
#include <string>

namespace SE {
namespace Volcano {
    namespace IEC61850 {

        class SatelliteSetting {
        public:
            explicit SatelliteSetting(const VcLibInParams* params);
            bool IsValid() const;
            const char* GetFileName() const;
            const char* GetConnectionName() const;
            IVcAPI& GetAPI() const;
            logger_t GetLog() const;

        private:
            bool ReadFile();

        public:
            mutable IVcAPI* vc_api_;
            logger_t log_;
            std::string config_name_;
            std::string connection_name_;
        };

    } // IEC61850
} // Volcano
} // SE
#endif
