﻿#include "satellite.h"

#include <iostream>

using namespace SE::Volcano;

VC_DLL_EXPORT_DECL_SPEC int CreateSatellite(const VcLibInParams* inParams,
    VcLibOutParams* outParams);

VC_DLL_EXPORT_DECL_SPEC int CreateSatellite(const VcLibInParams* inParams,
    VcLibOutParams* outParams)
{
    if (inParams->_MySize != sizeof(*inParams)) {
        std::cerr << "! Dll build is incompatible with core. Check you are using "
                     "same builds (release/debug)\n";
        return VC_LIB_INIT_CODE__MISMATCH_SIZE;
    }

    logger_t log = inParams->log;
    if (inParams->APIVersion != VC_API_VERSION) {
        LOG_ERR(log,
            "Version mismatch. Core v" << inParams->APIVersion << ", Dll v"
                                       << VC_API_VERSION);
        return VC_LIB_INIT_CODE__WRONG_API_VERSION;
    }

    IEC61850::SatelliteSetting settings(inParams);
    if (settings.IsValid()) {
        outParams->Module = new IEC61850::Satellite(settings); // module;
        return 0;
    }

    return 1;
}
