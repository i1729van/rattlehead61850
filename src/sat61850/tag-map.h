﻿#ifndef VOLCANO_SRC_SAT61850_TAG_MAP_H
#define VOLCANO_SRC_SAT61850_TAG_MAP_H

#if __cplusplus >= 201103
#include <unordered_map>
#define HashTable std::unordered_map
#else
#include <map>
#define HashTable std::map
#endif

#include "app_common.h"
#include "iec61850-common/errors-defaults.h"
#include "iec61850-common/errors.h"
#include <string>

namespace SE {
namespace Volcano {
    namespace IEC61850 {

        using namespace rth::iec61850;

        typedef struct ShareValueInfo {
            std::string value_name;
            std::string tstamp_name;
            std::string quality_name;
            bool IsValid() const
            {
                return !(value_name.empty() && tstamp_name.empty() && quality_name.empty());
            }
            ShareValueInfo() {}
            ShareValueInfo(const std::string& name, const std::string& tstamp, const std::string& quality)
                : value_name(name)
                , tstamp_name(tstamp)
                , quality_name(quality)
            {
            }

        } ShareValueInfo;

        template <typename MapType>
        class TagMapInfoReadResult {
        public:
            TagMapInfoReadResult() {}
            ~TagMapInfoReadResult() {}

            MapType& GetData() { return map_; }
            const MapType& GetData() const { return map_; }

            const DefaultErrorList& GetErrorList() const { return errors_; }
            DefaultErrorList& GetErrorList() { return errors_; }

            bool IsValid() const { return map_.size() > 0 && errors_.Count() == 0; }

        public:
            static void Release(TagMapInfoReadResult& result) { delete &result; }

        private:
            MapType map_;
            DefaultErrorList errors_;
        };

        typedef HashTable<std::string, bool> VisitedMap;
        typedef HashTable<std::string, ShareValueInfo> ValueTagMap;
        typedef TagMapInfoReadResult<ValueTagMap> ValueTagMapReadResult;

        ValueTagMapReadResult& ReadFromFile(const char* name);

        // ControlMap
        // SettingMap
        //...

    } // IEC61850
} // Volcano
} // SE
#endif
