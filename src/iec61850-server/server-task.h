#pragma once
#include "base/thread.h" // for Mutex

namespace rth {
namespace iec61850 {
    namespace server {

        class TaskController {
        public:
            TaskController();
            ~TaskController();
            bool IsRunning() const;
            bool IsDone() const;
            bool Stop() const;

            void IsRunning(bool value);
            void IsDone(bool value);
            void Stop(bool value);

        private:
            mutable rth::thread::Mutex mutex_;
            bool is_running_;
            bool is_done_;
            bool cmd_stop_;
        };
    }
}
}
