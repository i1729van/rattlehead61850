#include <libiec61850/iec61850_common.h> // for FunctionalConstraint
#include <libiec61850/iec61850_model.h> // for DataAttributeType
#include <ostream> // for ostream

namespace rth {
namespace iec61850 {
    namespace model {
        /* Немного вспомогательных функцаек  */
        DataAttributeType StringToDataType(const char* str);

        FunctionalConstraint StringToFC(const char* str);
        std::ostream& operator<<(std::ostream& stream, FunctionalConstraint fc);
    }
}
}
