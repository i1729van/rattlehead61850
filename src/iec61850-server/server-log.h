#pragma once

#include "iec61850-common/app-log.h" // for LogMessage

namespace rth {
namespace iec61850 {
    namespace server {
        class LogInfo : public LogMessage {
        public:
            LogInfo();

        public:
            static void Enable();
            static void Disable();
            static bool IsEnabled();
        };

        class LogWarning : public LogMessage {
        public:
            LogWarning();

        public:
            static void Enable();
            static void Disable();
            static bool IsEnabled();
        };

        class LogError : public LogMessage {
        public:
            LogError();

        public:
            static void Enable();
            static void Disable();
            static bool IsEnabled();
        };

        class ServerSettings;
        void EnableLoggers(const ServerSettings& settings);
    }
}
}
