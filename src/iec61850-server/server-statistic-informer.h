#pragma once

#include "server-task.h" // for TaskController
#include <stddef.h> // for size_t
#include <string> // for string
namespace rth {
namespace iec61850 {
    namespace server {
        class Server;
    }
}
} // lines 7-7

namespace rth {
namespace iec61850 {
    namespace server {

        class StatisticInformer {
        public:
            /* -----------------------------------------------------------------------
   * Методы работающие в потоке вызывающего кода
   * -----------------------------------------------------------------------*/
            StatisticInformer(const Server& server, const char* name,
                size_t period_ms = 1000);

            /* -----------------------------------------------------------------------
   * Методы работающие в потоке класса
   * -----------------------------------------------------------------------*/
            void operator()();

            /* -----------------------------------------------------------------------
   * Методы работающие везде
   * -----------------------------------------------------------------------*/
            TaskController& Controller();

        private:
            TaskController controller_;
            const size_t period_ms_;
            const std::string name_;
            const Server& server_;
        };
    }
}
}
