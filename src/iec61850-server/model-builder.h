#pragma once

#include "iec61850-config/reader.h" // for ReaderInterface
#include "iec61850-server/model-types.h" // for DataTemplate, DataTemplateR...
#include <libiec61850/iec61850_model.h> // for IedModel, DataSet, LogicalD...
#include <map> // for _Rb_tree_iterator
#include <stdbool.h> // for bool, false, true
#include <stddef.h> // for NULL

namespace rth {
namespace iec61850 {
    namespace model {

        /**
 * @class ModelBuildResult
 * @brief Результат посторения модели
 */
        class BuildResult {
        public:
            BuildResult()
                : ied_device_(NULL)
                , data_template_(NULL)
            {
            }
            BuildResult(IedModel* ied_device, DataTemplate* data_template)
                : ied_device_(ied_device)
                , data_template_(data_template)
            {
            }

            IedModel* GetModel() { return ied_device_; }
            bool IsModelValid() const { return ied_device_ != NULL; }

            DataTemplate* GetDataTemplate() { return data_template_; }
            bool IsDataTemplateValid() const
            {
                return data_template_ != NULL && data_template_->begin() != data_template_->end();
            }

        private:
            IedModel* ied_device_;
            DataTemplate* data_template_;
        };

        /**
 * @class ModelBuilder
 * @brief Родительский класс для билдеров
 */
        class Builder : public configuration::ReaderInterface {
        public:
            Builder()
                : ied_found_(false)
                , datatype_node_reading_(false)
                , ied_device_(NULL)
                , logical_device_(NULL)
                , logical_node_(NULL)
                , dataset_(NULL)
                , data_template_(NULL)
            {
            }

            virtual ~Builder() {}
            virtual bool IsDone() const { return true; }

            BuildResult& GetResults() const;

        protected:
            bool ied_found_; // IED найден? Если да, то можно включать подробный разбор
            bool datatype_node_reading_; // Начато чтение типа даных, но не все аттрибуты
            // прочитаны

            /* Модель МЭК61850 содержащая вс устройства, узлы, объекты...*/
            IedModel* ied_device_;

            /* Активное логичесое устройство */
            LogicalDevice* logical_device_;

            /* Выбранный в данный момент узел (не касается DataSet'ов)*/
            LogicalNode* logical_node_;

            /* DataSet  в который будут добавлятся FCDA */
            DataSet* dataset_;

            /* Определения типов данных */
            DataTemplate* data_template_;

            /* Активный тип данных. В него происходит добавление аттрибутов.
   * После чтения всех аттрибутов типа попадает в массив с определениями */
            DataTemplateRecord current_record_;
        };

        BuildResult& Build(Builder& builder, const char* filename, const char* ied);
        void ReleaseBuild(BuildResult& model);
    }
}
}
