#include "server.h"
#include "base/process.h" // for CurrentPID
#include "base/thread.h" // for Millisleep
#include "base/time.h" // for GetRealTimeMilli
#include "iec61850-common/app-log.h" // for operator<<
#include "iec61850-server/model-default-builder.h" // for DefaultBuilder
#include "iec61850-server/server-errors.h" // for ServerError
#include "iec61850-server/server-input-monitor.h" // for InputMonitor
#include "iec61850-server/server-settings.h" // for ServerSettings
#include "iec61850-server/server-statistic-informer.h" // for StatisticInfo...
#include "iec61850-server/server-statistic.h" // for ServerStatistic
#include "model-builder.h" // for ReleaseBuild
#include "server-checks.h" // for RunConfigTests
#include "server-log.h" // for LogInfo, LogE...
#include "server-task.h" // for TaskController
#include <assert.h> // for assert
#include <stddef.h> // for NULL

namespace rth {
namespace iec61850 {
    namespace server {

        Server::Server(ServerSettings settings)
            : settings_(settings)
            , ied_server_(NULL)
            , input_monitor_(NULL)
            , input_monitor_thread_(NULL)
            , informer_(NULL)
            , inforemer_thread(NULL)
            , build_(NULL)
            , error_(ServerErrorNone)
            , is_started_(false)
        {
            EnableLoggers(settings);
        }

        Server::~Server() {}

        void Server::Error(ServerError error)
        {
            if (error != ServerErrorNone && error != error_) {
                LogError() << "Catch server error: " << error << "\n";
                error_ = error;
            }
        }

        bool Server::BuildModel()
        {
            model::DefaultBuilder builder;
            model::BuildResult& build_result = model::Build(builder, settings_.ConfigFileName(), settings_.DeviceName());

            bool model_ok = build_result.IsModelValid(),
                 template_ok = build_result.IsDataTemplateValid();

            if (model_ok && template_ok) {
                build_ = &build_result;
                ied_server_ = IedServer_create(build_->GetModel());
            } else {
                model::ReleaseBuild(build_result);
            }

            if (model_ok && template_ok) {
                LogInfo() << "Building " << settings_.DeviceName() << " model...OK\n";
            } else {
                LogInfo() << "Building " << settings_.DeviceName() << " model...FAIL\n";
                LogError() << "IED model status: " << model_ok << "\n";
                LogError() << "IED template status: " << template_ok << "\n";
            }

            return model_ok && template_ok;
        }

        bool Server::RunInputMonitor()
        {
            input_monitor_ = new InputMonitor(
                settings_.ConnectionName(), ied_server_, settings_.MaxBufferSize(),
                settings_.MaxTransactionSize(), settings_.ReadDelayMicro());

            IedServer_start(ied_server_, settings_.Port());

            if (IedServer_isRunning(ied_server_)) {
                input_monitor_thread_ = new thread::Thread(*input_monitor_);

                for (int i = 0; i < 10 && !input_monitor_->Controller().IsRunning(); i++) {
                    thread::Millisleep(100);
                }

                if (input_monitor_->Controller().IsRunning()) {
                    LogInfo() << "Running input monitor...OK\n";
                    return true;
                }
            }

            Error(ServerErrorServiceError);
            LogInfo() << "Running input monitor...FAIL\n";

            return false;
        }

        bool Server::StopInputMonitor()
        {
            LogInfo() << "Stopping input monitor...\n";
            input_monitor_->Controller().Stop(true);

            for (int i = 0; i < 10 && !input_monitor_->Controller().IsDone(); i++) {
                thread::Millisleep(100);
            }
            if (input_monitor_->Controller().IsDone()) {
                LogInfo() << "Input monitor stopped\n";
                input_monitor_thread_->Join();
                delete input_monitor_thread_;
                delete input_monitor_;

                input_monitor_thread_ = NULL;
                input_monitor_ = NULL;

                return true;
            }

            LogError() << "Can't stop input monitor\n";
            return false;
        }

        bool Server::RunInformer()
        {
            informer_ = new StatisticInformer(*this, settings_.ConnectionName());
            inforemer_thread = new thread::Thread(*informer_);

            for (int i = 0; i < 100 && !informer_->Controller().IsRunning(); i++) {
                thread::Millisleep(100);
            }

            if (informer_->Controller().IsRunning()) {
                LogInfo() << "Running informer...OK\n";
                return true;
            }

            Error(ServerErrorServiceError);
            LogInfo() << "Running informer...FAIL\n";

            return false;
        }

        bool Server::StopInformer()
        {
            LogInfo() << "Stopping informer...\n";
            informer_->Controller().Stop(true);
            for (int i = 0; i < 100 && !informer_->Controller().IsDone(); i++) {
                thread::Millisleep(100);
            }
            if (informer_->Controller().IsDone()) {
                LogInfo() << "Informer stopped\n";
                inforemer_thread->Join();

                delete inforemer_thread;
                delete informer_;

                inforemer_thread = NULL;
                informer_ = NULL;

                return true;
            }

            LogError() << "Can't stop informer \n";
            return false;
        }

        /**
 * @brief Запуск сервера
 * @param conn_name - имя коннекта. Используется для обмена через именовынне
 * области памяти
 * @param port - номер порта для подключения клиентов
 * @return true - в случае успеха
 */
        bool Server::StartServer()
        {
            assert(!is_started_);

            LogInfo() << "Starting server...\n";
            if (!RunConfigTests(settings_)) {
                Error(ServerErrorInvalidConfig);
                return false;
            }
            if (!BuildModel()) {
                Error(ServerErrorInvalidModel);
                return false;
            }

            is_started_ = RunInputMonitor() && RunInformer();
            return is_started_;
        }

        /**
 * @brief Останов
 */
        void Server::StopServer()
        {
            if (is_started_) {
                // Останов и освобождение ресурсов
                if (StopInformer() && StopInputMonitor()) {
                    IedServer_stop(ied_server_);
                    IedServer_destroy(ied_server_);
                    model::ReleaseBuild(*build_);
                }

                // Зануление. Позволит нам стартануть ещё раз
                build_ = NULL;
                ied_server_ = NULL;
                is_started_ = false;
            }
        }

        bool Server::IsRunning() const { return is_started_; }

        ServerError Server::GetLastError() const { return error_; }

        ServerStatistic Server::GetStatistic() const
        {
            ServerStatistic stat;
            stat.pid = process::CurrentPID();
            stat.input = input_monitor_->GetStatistic();
            stat.tstamp = rth::time::GetRealTimeMilli();
            return stat;
        }
    }
}
}
