#pragma once

namespace rth {
namespace iec61850 {
    namespace server {

        class ServerSettings;
        bool RunConfigTests(const ServerSettings& settings);
    }
}
}
