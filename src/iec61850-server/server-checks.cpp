#include "server-checks.h"
#include "iec61850-check/configuration-default-test.h" // for DefaultTestSuite
#include "iec61850-check/configuration-test.h" // for TestResult
#include "iec61850-common/app-log.h" // for operator<<
#include "iec61850-common/errors-defaults.h" // for DefaultErrorD...
#include "iec61850-common/errors.h" // for ErrorList<>::...
#include "iec61850-server/server-settings.h" // for ServerSettings
#include "server-log.h" // for LogError, Log...
#include <list> // for _List_const_i...
#include <string> // for operator==

namespace rth {
namespace iec61850 {
    namespace server {

        bool RunConfigTests(const ServerSettings& settings)
        {
            std::string test_mode = settings.TestMode();

            if (test_mode == "" || test_mode == "none") {
                return true;
            }

            if (test_mode == "default") {
                configuration::DefaultTestSuite default_test_suite;
                configuration::TestResult test_results = configuration::RunTests(
                    default_test_suite, settings.ConfigFileName(), settings.DeviceName());

                if (test_results.IsOk()) {
                    LogInfo() << "Running default tests...OK\n";
                } else {
                    LogInfo() << "Running default tests...FAIL\n";
                    configuration::TestingErrors::ConstIterator
                        it
                        = test_results.Errors().Begin(),
                        end = test_results.Errors().End();
                    while (it != end) {
                        LogError() << settings.ConfigFileName() << ": " << it->ToString()
                                   << "\n";
                        it++;
                    }
                }
                return test_results.IsOk();
            }

            LogError() << "Unsupported test mode '" << test_mode << "'\n";
            return true;
        }
    }
}
}
