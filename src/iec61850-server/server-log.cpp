#include "server-log.h"
#include "base/network.h" // for IsValidIP, MakeIpAddress
#include "base/process.h" // for CurrentPID
#include "base/thread.h" // for Mutex
#include "base/time.h" // for GetRealTimeMilli
#include "iec61850-common/log-writers.h" // for LogUdpWriter, LogStdout...
#include "iec61850-server/server-settings.h" // for ServerSettings
#include <ostream> // for operator<<, basic_ostream
#include <string> // for string, basic_string

namespace rth {
namespace iec61850 {
    namespace server {

        static thread::Mutex gInfoMutex;
        static thread::Mutex gWarningMutex;
        static thread::Mutex gErrorMutex;

        static bool gInfoEnabled = false;
        static bool gWarningEnabled = false;
        static bool gErrorEnabled = false;

        static LogStdoutWriter& GetGlobalStdoutLogger()
        {
            static LogStdoutWriter logger;
            return logger;
        }

        static LogUdpWriter& GetGlobalUdpLogger()
        {
            static LogUdpWriter logger;
            return logger;
        }

        LogError::LogError()
        {
            enabled_ = LogError::IsEnabled();
            if (enabled_) {
                stream_ << rth::time::GetRealTimeMilli() << "  " << process::CurrentPID()
                        << "\t[ERROR]    ";
            }
        }

        void LogError::Enable()
        {
            gErrorMutex.Lock();
            gErrorEnabled = true;
            gErrorMutex.Unlock();
        }
        void LogError::Disable()
        {
            gErrorMutex.Lock();
            gErrorEnabled = false;
            gErrorMutex.Unlock();
        }
        bool LogError::IsEnabled()
        {
            bool result;
            gErrorMutex.Lock();
            result = gErrorEnabled;
            gErrorMutex.Unlock();
            return result;
        }

        LogWarning::LogWarning()
        {
            enabled_ = LogWarning::IsEnabled();
            if (enabled_) {
                stream_ << rth::time::GetRealTimeMilli() << "  " << process::CurrentPID()
                        << "\t[WARNING]  ";
            }
        }

        void LogWarning::Enable()
        {
            gWarningMutex.Lock();
            gWarningEnabled = true;
            gWarningMutex.Unlock();
        }
        void LogWarning::Disable()
        {
            gWarningMutex.Lock();
            gWarningEnabled = false;
            gWarningMutex.Unlock();
        }
        bool LogWarning::IsEnabled()
        {
            bool result;
            gWarningMutex.Lock();
            result = gWarningEnabled;
            gWarningMutex.Unlock();
            return result;
        }

        LogInfo::LogInfo()
        {
            enabled_ = LogInfo::IsEnabled();
            if (enabled_) {
                stream_ << rth::time::GetRealTimeMilli() << "  " << process::CurrentPID()
                        << "\t[OK]       ";
            }
        }

        void LogInfo::Enable()
        {
            gInfoMutex.Lock();
            gInfoEnabled = true;
            gInfoMutex.Unlock();
        }
        void LogInfo::Disable()
        {
            gInfoMutex.Lock();
            gInfoEnabled = false;
            gInfoMutex.Unlock();
        }
        bool LogInfo::IsEnabled()
        {
            bool result;
            gInfoMutex.Lock();
            result = gInfoEnabled;
            gInfoMutex.Unlock();
            return result;
        }

        void EnableLoggers(const ServerSettings& settings)
        {
            // std::string logger_mode_; // "none", "info", "warning", "error"
            std::string mode = settings.LogMode();
            std::string netlog = settings.LogAddress();
            std::string filelog = settings.LogFile();

            if (mode.find("info") != std::string::npos) {
                LogInfo::Enable();
            }
            if (mode.find("warning") != std::string::npos) {
                LogWarning::Enable();
            }
            if (mode.find("error") != std::string::npos) {
                LogError::Enable();
            }

            if (filelog == "stdout") {
                AppLogger::AddWriter(GetGlobalStdoutLogger());
            }

            if (netlog != "") {
                rth::network::IpAddress address = rth::network::MakeIpAddress(netlog.c_str());
                if (!IsValidIP(address)) {
                    LogError() << "Wrong logger IP " << netlog << "\n";
                }
                GetGlobalUdpLogger().SetSendtoAddress(address);
                AppLogger::AddWriter(GetGlobalUdpLogger());
            }
        }
    }
}
}
