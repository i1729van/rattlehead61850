#include "server-task.h"

namespace rth {
namespace iec61850 {
    namespace server {

        TaskController::TaskController()
            : is_running_(false)
            , is_done_(false)
            , cmd_stop_(false)
        {
        }
        TaskController::~TaskController() {}
        bool TaskController::IsRunning() const
        {
            thread::MutexAutoLock lock(mutex_);
            return is_running_;
        }
        bool TaskController::IsDone() const
        {
            thread::MutexAutoLock lock(mutex_);
            return is_done_;
        }

        void TaskController::IsRunning(bool value)
        {
            thread::MutexAutoLock lock(mutex_);
            is_running_ = value;
        }

        void TaskController::IsDone(bool value)
        {
            thread::MutexAutoLock lock(mutex_);
            is_done_ = value;
        }

        bool TaskController::Stop() const
        {
            thread::MutexAutoLock lock(mutex_);
            return cmd_stop_;
        }

        void TaskController::Stop(bool value)
        {
            thread::MutexAutoLock lock(mutex_);
            cmd_stop_ = value;
        }
    }
}
}
