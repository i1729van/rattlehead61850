#pragma once

#include "base/process.h"
#include "iec61850-common/config.h" // for RTH_IEC61850_SERVER_API_VER
#include <stddef.h> // for size_t
#include <stdint.h> // for uint64_t
#include <string> // for string
namespace rth {
namespace ipc {
    class SharedMemory;
}
}

namespace rth {
namespace iec61850 {
    namespace server {

        /* Немного счетчиков и статистика для наблюдения за сервером.
 * Макс. значение счетчика = 18446744073709551615
 * Если сервер будет обрабатывать 20000 сообщений в секунду [д.р. словами
 * счетчик будет увеличиваться
 * на 20000 каждую секунду], то его хватит на
 * 18446744073709551615 / 20000 / 3600 / 24 / 365 = 701930900 лет
 * В общем, за обнуление статистики можно не париться до тех пор, пока значения
 * не нарастают миллиардами миллиардов в секунду. Либо система не должна
 * прорпаботаьт миллиарды миллиардов лет.
 * */
        class InputStatistic {
        public:
            InputStatistic();

            void AddErrorCounter(uint64_t value);
            void AddDBCounter(uint64_t value);
            void AddValueCounter(uint64_t value);
            void AddCycleTime(uint64_t value);
            void AddLockTime(uint64_t value);
            void LastUpdateTimestamp(uint64_t value);

            uint64_t Errors() const;
            uint64_t Blocks() const;
            uint64_t Values() const;
            uint64_t AvgCycleMilli() const;
            uint64_t AvgLockMilli() const;
            uint64_t LastUpdateTimestamp() const;

        private:
            uint64_t error_cnt_;
            uint64_t block_cnt_;
            uint64_t values_cnt_;
            uint64_t total_cycle_ms_;
            uint64_t total_cycle_cnt_;
            uint64_t total_lock_ms_;
            uint64_t total_lock_cnt_;
            uint64_t avg_cycle_ms_;
            uint64_t avg_lock_ms_;
            uint64_t last_update_ms_; // ms c 1970-01-01
        };

        typedef struct ServerStatistic {
            process::ProcessHandler pid;
            const char* api_version;
            uint64_t tstamp;
            InputStatistic input;

            ServerStatistic()
                : pid(process::kInvalidProcessHandler)
                , api_version(RTH_IEC61850_SERVER_API_VERSION)
                , tstamp(0)
            {
            }

        } ServerStatistic;

        std::string ToString(const ServerStatistic& statistic);
        // std::string FromString(const ServerStatistic& statistic);

        class SharedStatistic {
        public:
            explicit SharedStatistic(const char* name);
            ~SharedStatistic();
            bool Create();
            bool Update(const ServerStatistic& statistic);
            void Close();
            bool IsOpen() const;

        public:
            static const size_t kChunkSize = 4096;

        private:
            std::string name_;
            rth::ipc::SharedMemory* memory_;
        };
    }
}
}
