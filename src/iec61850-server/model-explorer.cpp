#include "model-explorer.h"
#include "base/config.h" // for RTH_MAX_SCD_STACK_DEPTH
#include <assert.h> // for assert
#include <stddef.h> // for NULL

namespace rth {
namespace iec61850 {
    namespace model {

        static void VisitNodes(NodesList* nodes, const ModelNode* root,
            std::string& name, int depth = 0)
        {
            assert(depth < RTH_MAX_SCD_STACK_DEPTH);

            /* Для каждого узла вызываем обработчик и переходим на сл . уровень
   * вложенности.
   * Если перейти в глубину дальше невозомжно - заносим тэг в список и
   * возвращаемся на предю уровень.
   * И так до упора. */
            while (root != NULL) {
                /* Немного накладно, зато нет побочных эффектов. Оно того стоит */
                std::string node_name = name;

                /* Оформление имени:
     {IED}{LOGDEV}\{DA/DO...}.{DA/DO...}. */
                if (depth == 1) {
                    node_name += "/";
                } else if (depth > 1) {
                    node_name += ".";
                }

                /* Добавить имя текущего узла и пройти на сл. уровень*/
                node_name += root->name;
                VisitNodes(nodes, root->firstChild, node_name, depth + 1);

                /* Перейти к сл. узлу этого же уровня */
                root = root->sibling;
            }
            nodes->push_back(name);
        }

        NodesList& CreateNodeList(const IedModel* model)
        {
            assert(model);
            NodesList* nodes = new NodesList;
            std::string name = model->name;
            VisitNodes(nodes, reinterpret_cast<const ModelNode*>(model->firstChild),
                name, 0);
            return *nodes;
        }

        void DeleteNodeList(NodesList& nodes) { delete &nodes; }
    }
}
}
