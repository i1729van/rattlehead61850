#include "model-default-builder.h"
#include "base/config.h" // for RTH_MAX_SCD_STACK_DEPTH
#include "model-helpers.h" // for StringToDataType
#include <algorithm> // for replace
#include <assert.h> // for assert
#include <iomanip> // for operator<<, setfill
#include <libiec61850/iec61850_dynamic_model.h> // for DataAttribute_create
#include <map> // for map<>::mapped_type
#include <sstream> // for stringstream, basic_...
#include <stddef.h> // for NULL
#include <string> // for allocator, operator==
#include <utility> // for pair
#include <vector> // for vector

namespace rth {
namespace iec61850 {
    namespace model {

        DefaultBuilder::DefaultBuilder()
        {
            data_template_ = new DataTemplate;
            current_record_ = DataTemplateRecord();
            state_ = ReadData;
        }

        DefaultBuilder::~DefaultBuilder() {}

        int DefaultBuilder::PrepareHandler()
        {
            // Однокартное использование билдера
            assert(ied_device_ == NULL);
            return 0;
        }

        int DefaultBuilder::IEDHandler(const configuration::IED& ied,
            const configuration::ParseContext& context)
        {
            // Дефолтный билдер не умеет работать в неразборчиов режиме
            // и не предназначен для многократного запуска
            assert(!ied_found_);

            // Билдер отработал. Повторный вызов запрещен
            assert(state_ != Done);

            if (context.stop) {
                return context.handler_rs;
            }

            if (state_ == CreateModel) {
                // std::cout << "Create IED: " << ied.name << "\n";
                ied_found_ = true;
                ied_device_ = IedModel_create(ied.name.c_str());
            }

            return 0;
        }

        int DefaultBuilder::LogicalDeviceHandler(
            const configuration::LogicalDevice& ld,
            const configuration::ParseContext& context)
        {
            if (context.stop) {
                return context.handler_rs;
            }

            if (state_ == CreateModel && ied_found_ && ied_device_) {
                // std::cout << "Create LD: " << ld.inst << "\n";
                logical_device_ = LogicalDevice_create(ld.inst.c_str(), ied_device_);
            }

            return 0;
        }

        int DefaultBuilder::LogicalNodeHandler(
            const configuration::LogicalNode& ln,
            const configuration::ParseContext& context)
        {
            if (context.stop) {
                return context.handler_rs;
            }

            if (state_ == CreateModel && ied_found_ && logical_device_) {
                /* Создаем логический узел и заносим в него данные */
                std::string full_name = ln.prefix + ln.lnClass + ln.inst;
                std::string type_name = ln.lnType;
                // std::cout << "Create LN[" << type_name << "]:" << full_name << "\n";
                logical_node_ = LogicalNode_create(full_name.c_str(), logical_device_);
                FillNode(reinterpret_cast<ModelNode*>(logical_node_), type_name.c_str(),
                    *data_template_);
            }

            return 0;
        }

        int DefaultBuilder::DataSetHandler(const configuration::DataSet& ds,
            const configuration::ParseContext& context)
        {
            if (context.stop) {
                return context.handler_rs;
            }

            if (state_ == CreateModel && ied_found_ && logical_node_) {
                /* Создаем логический узел и заносим в него данные */
                // std::cout << "Create DS: " << ds.name << "\n";
                dataset_ = DataSet_create(ds.name.c_str(), logical_node_);
            }
            return 0;
        }

        int DefaultBuilder::FCDAHandler(const configuration::FCDA& fcda,
            const configuration::ParseContext& context)
        {
            if (context.stop) {
                return context.handler_rs;
            }

            if (state_ == CreateModel && ied_found_ && dataset_) {
                std::string full_name = fcda.prefix + fcda.lnClass + fcda.lnInst + '$' + fcda.fc + '$' + fcda.doName;

                if (fcda.daName != "") {
                    full_name += '$' + fcda.daName;
                }

                std::replace(full_name.begin(), full_name.end(), '.', '$');
                // std::cout << "Create FCDA: " << full_name << "\n";
                DataSetEntry_create(dataset_, full_name.c_str(), -1, NULL);
            }
            return 0;
        }

        int DefaultBuilder::ReportHandler(const configuration::Report& rcb,
            const configuration::ParseContext& context)
        {
            if (context.stop) {
                return context.handler_rs;
            }

            if (state_ == CreateModel && ied_found_ && logical_node_) {
                // http://libiec61850.com/api/group__DYNAMIC__MODEL.html#gaf63c9112ff9e8d96653a21ebac93ba86
                // RptID = NULL, если поле пустое. Тогда libIEC создаст запишет в RptID
                // полное имя
                char* rptId = rcb.rptID == "" ? NULL : const_cast<char*>(rcb.rptID.c_str());
                bool isBuffered = rcb.buffered == "true";
                char* dataSetName = const_cast<char*>(rcb.datSet.c_str());

                uint32_t confRef = 0;
                std::stringstream(rcb.confRev.c_str()) >> confRef;

                // TRG_OPT_GI: в 61850-6 я не нашел GI в SCL. В целом, этот бит на 99,99%
                // взведен везде. Пока счиатем его дефолтным, чтобы описание RCB билось с
                // документом.
                uint8_t trgOps = TRG_OPT_GI | (rcb.dchg == "true" ? TRG_OPT_DATA_CHANGED : 0) | (rcb.qchg == "true" ? TRG_OPT_QUALITY_CHANGED : 0) | (rcb.dupd == "true" ? TRG_OPT_DATA_UPDATE : 0) | (rcb.period == "true" ? TRG_OPT_INTEGRITY : 0);

                uint8_t options = (rcb.seqNum == "true" ? RPT_OPT_SEQ_NUM : 0) | (rcb.timeStamp == "true" ? RPT_OPT_TIME_STAMP : 0) | (rcb.dataSet == "true" ? RPT_OPT_DATA_SET : 0) | (rcb.reasonCode == "true" ? (RPT_OPT_REASON_FOR_INCLUSION | RPT_OPT_BUFFER_OVERFLOW) : 0)
                    | (rcb.dataRef == "true" ? RPT_OPT_DATA_REFERENCE : 0) | (rcb.entryID == "true" ? RPT_OPT_ENTRY_ID : 0) | (rcb.bufOvfl == "true" ? RPT_OPT_CONF_REV : 0);

                uint32_t bufTime = 0;
                std::stringstream(rcb.bufTime.c_str()) >> bufTime;

                uint32_t intgPd = 0;
                std::stringstream(rcb.intgPd.c_str()) >> intgPd;

                int max = 0;
                std::stringstream(rcb.max_enabled.c_str()) >> max;

                for (int i = 0; i < max; i++) {
                    std::stringstream stream;
                    stream << rcb.name;

                    /* Не до конца понятно, что следует делать с номерами экземляров отчетов.
       * Лепить в конец, лепить в конец, если имя не содержит номер, лепить,
       * если
       * экземпляров больше 1 ?
       * Пока просто леплю. Всегда и везде.
       * */
                    stream << std::setfill('0') << std::setw(2) << i + 1;

                    // std::cout << "Create RCB :" << stream.str() << "\n";
                    ReportControlBlock_create(stream.str().c_str(), logical_node_, rptId,
                        isBuffered, dataSetName, confRef, trgOps,
                        options, bufTime, intgPd);
                }
            }

            return 0;
        }

        int DefaultBuilder::DataTypeHandler(
            const configuration::DataType& type,
            const configuration::ParseContext& context)
        {
            // Билдер отработал. Повторный вызов запрещен
            assert(state_ != Done);

            if (context.stop)
                return 1;

            // Работает только на этапе чтения данных
            if (state_ == ReadData) {
                // Если чтение узла было начато и мы получили уведомление о чтении нового,
                // то заносим старый тип в словарь и присутпаем к чтению нового
                if (datatype_node_reading_) {
                    (*data_template_)[current_record_.GetID()] = current_record_;
                    datatype_node_reading_ = false;
                }

                current_record_ = DataTemplateRecord();
                current_record_.type = type;
                datatype_node_reading_ = true;
            }
            return 0;
        }

        int DefaultBuilder::DataAttributeHandler(
            const configuration::DataAttribute& attribute,
            const configuration::ParseContext& context)
        {
            // Билдер отработал. Больше нет смысла его вызывать
            assert(state_ != Done);

            if (context.stop)
                return 1;

            // Работает только на этапе чтения данных
            if (state_ == ReadData) {
                current_record_.attributes.push_back(attribute);
            }

            return 0;
        }

        int DefaultBuilder::DoneHandler(const configuration::ParseContext& context)
        {
            // Если чтение узла было начато и мы подошли к концу конф. файла, то
            // необъодимо
            // записать тип в словарь
            if (state_ == ReadData && datatype_node_reading_ && context.handler_rs == 0) {
                (*data_template_)[current_record_.GetID()] = current_record_;
                datatype_node_reading_ = false;
            }

            switch (state_) {
            case ReadData:
                state_ = CreateModel;
                break;

            case CreateModel:
                state_ = Done;
                break;

            case Done:
            default:
                break;
            }

            return 0;
        }

        bool DefaultBuilder::IsDone() const { return state_ == Done; }

        int DefaultBuilder::FillNode(ModelNode* node, const char* data_type_name,
            const DataTemplate& data_template,
            FunctionalConstraint fc, uint8_t trigger,
            int step)
        {
            assert(step < RTH_MAX_SCD_STACK_DEPTH);

            // Некуда спускаться. Если тип простой, то он был создан на предыдущем уровне
            // рекурсии. Если составной и без типа - то либо мы дошли до конца, либо это
            // косяк
            // в описании.
            if (std::string(data_type_name) == "") {
                return 0;
            }

            const DataTemplateRecord& rec = data_template.find(data_type_name)->second;
            DataTypeAttributesConstIter attribute_iterator = rec.AttributesBegin(),
                                        last_attribute_itarator = rec.AttributesEnd();

            // EnumType нам не интересны
            if (rec.type.element == "EnumType") {
                return 0;
            }
            // Если тип определен, то проходим по всем аттрибутам и заполняем модель
            // std::cout << "<<<<" << rec.type.id << "\n";
            while (attribute_iterator != last_attribute_itarator) {
                // cout << "Next Attribute:\n";
                ModelNode* next_node = NULL;
                configuration::DataAttribute attribute = *attribute_iterator;

                // FC проходят от родительского узла ко всем дочерним
                FunctionalConstraint attribute_fc = StringToFC(attribute.fc.c_str());
                if (attribute_fc < fc) {
                    attribute_fc = fc;
                }

                /* Триггеры проходят от родительского узла ко всем дочерним
    Не вижу period в SCL 61850-6   (attribute.period == "true" ?
    TRG_OPT_INTEGRITY : 0)
    Не вижу GI в SCL 61850-6 */
                uint8_t attribute_trigger = (attribute.dchg == "true" ? TRG_OPT_DATA_CHANGED : 0) | (attribute.qchg == "true" ? TRG_OPT_QUALITY_CHANGED : 0) | (attribute.dupd == "true" ? TRG_OPT_DATA_UPDATE : 0);

                if (attribute_trigger < trigger) {
                    attribute_trigger = trigger;
                }

                /*std::cout << "Object[" << attribute.name << "]:\n\ttype: " <<
       attribute.type
              << "\n\tbType:" << attribute.bType
              << "\n\tFC:" << (FunctionalConstraint)attribute_fc
              << "\n\tTRG:" << (int)attribute_trigger << "\n";*/

                if (attribute.element == "DO" || attribute.element == "SDO") {
                    next_node = reinterpret_cast<ModelNode*>(
                        DataObject_create(attribute.name.c_str(), node, 0));
                } else if (attribute.element == "DA" || attribute.element == "BDA") {
                    next_node = reinterpret_cast<ModelNode*>(
                        DataAttribute_create(attribute.name.c_str(), node,
                            StringToDataType(attribute.bType.c_str()),
                            attribute_fc, attribute_trigger, 0, 0));
                }

                FillNode(next_node, attribute.type.c_str(), data_template, attribute_fc,
                    attribute_trigger, step + 1);

                attribute_iterator++;
            }
            // std::cout << ">>>>" << rec.type.id << "\n\n";
            return 0;
        }
    }
}
}
