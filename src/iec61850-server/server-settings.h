#pragma once
#include "iec61850-common/config.h"
#include <stdint.h>
#include <string>

namespace rth {
namespace iec61850 {
    namespace server {
        class ServerSettings {
        public:
            ServerSettings() {}
            ServerSettings(const char* file_name, const char* device_name,
                const char* connection_name, uint16_t port = 102,
                const char* test_mode = "default",
                const char* log_mode = "error", const char* log_address = "",
                const char* log_file = "",
                size_t max_buffer_size = RTH_IEC61850_DEF_SHMBUFFER_SIZE,
                size_t trans_size = RTH_IEC61850_DEF_TRANSACTION_SIZE,
                uint32_t read_delay = 10 * 1000)
                : file_name_(file_name)
                , device_name_(device_name)
                , connection_name_(connection_name)
                , port_(port)
                , test_mode_(test_mode)
                , log_mode_(log_mode)
                , log_address_(log_address)
                , log_file_(log_file)
                , max_buffer_size_(max_buffer_size)
                , max_trans_size_(trans_size)
                , read_deleay_micro_(read_delay)
            {
            }

            const char* ConnectionName() const { return connection_name_.c_str(); }
            const char* ConfigFileName() const { return file_name_.c_str(); }
            const char* DeviceName() const { return device_name_.c_str(); }
            const char* TestMode() const { return test_mode_.c_str(); }
            const char* LogMode() const { return log_mode_.c_str(); }
            const char* LogAddress() const { return log_address_.c_str(); }
            const char* LogFile() const { return log_file_.c_str(); }
            uint16_t Port() const { return port_; }
            size_t MaxBufferSize() const { return max_buffer_size_; }
            size_t MaxTransactionSize() const { return max_trans_size_; }
            unsigned int ReadDelayMicro() const { return read_deleay_micro_; }

        private:
            std::string file_name_; // Путь к icd/cid,scd
            std::string device_name_; // Имя IED
            std::string
                connection_name_; // Имя коннекта для связи с внешними приложениями
            uint16_t port_;
            std::string test_mode_; // "none", "default", "paranoid"
            std::string log_mode_; // "none", "info", "warning", "error"
            std::string log_address_; // ip:port
            std::string log_file_; // stdout

            size_t max_buffer_size_; // Кол-во DataBlocks в буфере
            size_t max_trans_size_; // Кол-во блоков, которые моугт быть обработаны
            // за цикл
            uint32_t
                read_deleay_micro_; // Задержка на чтение из буфера, микросек [работает
            // если запией на обработку < datablock_per_cycle]
        };
    }
}
}
