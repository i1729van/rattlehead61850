#include "model-helpers.h"
#include <stddef.h> // for size_t
#include <string> // for operator==, string, basic_string

namespace rth {
namespace iec61850 {
    namespace model {

        DataAttributeType StringToDataType(const char* str)
        {
            const struct pairs {
                const char* name;
                DataAttributeType value;
            } fc_pairs[] = {
                { "BOOLEAN", IEC61850_BOOLEAN },
                { "INT8", IEC61850_INT8 },
                { "INT16", IEC61850_INT16 },
                { "INT32", IEC61850_INT32 },
                { "INT64", IEC61850_INT64 },
                { "INT128", IEC61850_INT128 },
                { "INT8U", IEC61850_INT8U },
                { "INT16U", IEC61850_INT16U },
                { "INT24U", IEC61850_INT24U },
                { "INT32U", IEC61850_INT32U },
                { "FLOAT32", IEC61850_FLOAT32 },
                { "FLOAT64", IEC61850_FLOAT64 },
                { "Enum", IEC61850_ENUMERATED },
                { "Octet64", IEC61850_OCTET_STRING_64 },
                //{"OCTET_STRING_6", IEC61850_OCTET_STRING_6},
                { "EntryID", IEC61850_OCTET_STRING_8 },
                { "VisString32", IEC61850_VISIBLE_STRING_32 },
                { "VisString64", IEC61850_VISIBLE_STRING_64 },
                { "VisString65", IEC61850_VISIBLE_STRING_65 },
                { "VisString129", IEC61850_VISIBLE_STRING_129 },
                { "VisString255", IEC61850_VISIBLE_STRING_255 },
                { "Unicode255", IEC61850_UNICODE_STRING_255 },
                { "Timestamp", IEC61850_TIMESTAMP },
                { "Quality", IEC61850_QUALITY },
                { "Check", IEC61850_CHECK },
                { "Tcmd", IEC61850_CODEDENUM },
                { "Dbpos", IEC61850_CODEDENUM },
                { "OptFlds", IEC61850_GENERIC_BITSTRING },
                { "TrgOps", IEC61850_GENERIC_BITSTRING },
                { "Struct", IEC61850_CONSTRUCTED },
                { "EntryTime", IEC61850_ENTRY_TIME },
                { "PhyComAddr", IEC61850_PHYCOMADDR }
#if (CONFIG_IEC61850_USE_COMPAT_TYPE_DECLARATIONS == 1)
                ,
                { "BOOLEAN", BOOLEAN },
                { "INT8", INT8 },
                { "INT16", INT16 },
                { "INT32", INT32 },
                { "INT64", INT64 },
                { "INT64", INT64 },
                { "INT8U", INT8U },
                { "INT16U", INT16U },
                { "INT24U", INT24U },
                { "INT32U", INT32U },
                { "FLOAT32", FLOAT32 },
                { "FLOAT64", FLOAT64 },
                { "Enum", ENUMERATED },
                { "Octet64", OCTET_STRING_64 },
                //{"OCTET_STRING_6", OCTET_STRING_6},
                { "EntryID", OCTET_STRING_8 },
                { "VisString32", VISIBLE_STRING_32 },
                { "VisString64", VISIBLE_STRING_64 },
                { "VisString65", VISIBLE_STRING_65 },
                { "VisString129", VISIBLE_STRING_129 },
                { "VisString255", VISIBLE_STRING_255 },
                { "Unicode255", UNICODE_STRING_255 },
                { "Timestamp", TIMESTAMP },
                { "Quality", QUALITY },
                { "Check", CHECK },
                { "Tcmd", CODEDENUM },
                { "Dbpos", CODEDENUM },
                { "OptFlds", GENERIC_BITSTRING },
                { "TrgOps", GENERIC_BITSTRING },
                { "Struct", CONSTRUCTED },
                { "EntryTime", ENTRY_TIME },
                { "PhyComAddr", PHYCOMADDR }
#endif
            };

            std::string search_type = str;

            for (size_t i = 0; i < sizeof(fc_pairs) / sizeof(fc_pairs[0]); i++) {
                if (search_type == fc_pairs[i].name) {
                    return fc_pairs[i].value;
                }
            }

            // assert("Missing attribute type" == 0);
            return IEC61850_BOOLEAN;
        }

        FunctionalConstraint StringToFC(const char* str)
        {
            const struct pairs {
                const char* name;
                FunctionalConstraint value;
            } fc_pairs[] = {
                { "", IEC61850_FC_NONE }, { "ST", IEC61850_FC_ST }, { "MX", IEC61850_FC_MX },
                { "SP", IEC61850_FC_SP }, { "SV", IEC61850_FC_SV }, { "CF", IEC61850_FC_CF },
                { "DC", IEC61850_FC_DC }, { "SG", IEC61850_FC_SG }, { "SE", IEC61850_FC_SE },
                { "SR", IEC61850_FC_SR }, { "OR", IEC61850_FC_OR }, { "BL", IEC61850_FC_BL },
                { "EX", IEC61850_FC_EX }, { "CO", IEC61850_FC_CO }, { "US", IEC61850_FC_US },
                { "MS", IEC61850_FC_MS }, { "RP", IEC61850_FC_RP }, { "BR", IEC61850_FC_BR },
                { "LG", IEC61850_FC_LG }, { "ALL", IEC61850_FC_ALL }
            };

            std::string search_fc = str;
            for (size_t i = 0; i < sizeof(fc_pairs) / sizeof(fc_pairs[0]); i++) {
                if (search_fc == fc_pairs[i].name) {
                    return fc_pairs[i].value;
                }
            }

            return IEC61850_FC_NONE;
        }

        /*
std::ostream &operator<<(std::ostream &stream, FunctionalConstraint fc) {
  switch (fc) {
  case IEC61850_FC_ST:
    return stream << "ST";
  case IEC61850_FC_MX:
    return stream << "MX";
  case IEC61850_FC_SP:
    return stream << "SP";
  case IEC61850_FC_SV:
    return stream << "SV";
  case IEC61850_FC_CF:
    return stream << "CF";
  case IEC61850_FC_DC:
    return stream << "DC";
  case IEC61850_FC_SG:
    return stream << "SG";
  case IEC61850_FC_SE:
    return stream << "SE";
  case IEC61850_FC_SR:
    return stream << "SR";
  case IEC61850_FC_OR:
    return stream << "OR";
  case IEC61850_FC_BL:
    return stream << "BL";
  case IEC61850_FC_EX:
    return stream << "EX";
  case IEC61850_FC_CO:
    return stream << "CO";
  case IEC61850_FC_US:
    return stream << "US";
  case IEC61850_FC_MS:
    return stream << "MS";
  case IEC61850_FC_RP:
    return stream << "RP";
  case IEC61850_FC_BR:
    return stream << "BR";
  case IEC61850_FC_LG:
    return stream << "LG";
  case IEC61850_FC_ALL:
    return stream << "ALL";
  case IEC61850_FC_NONE:
    return stream << "NONE";
  }
  return stream << "UNK";
}*/
    }
}
}
