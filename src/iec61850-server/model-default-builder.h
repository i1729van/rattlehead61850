#pragma once
#include "iec61850-config/base-types.h" // for DataAttribute, DataSet, Dat...
#include "iec61850-config/xml.h" // for ParseContext
#include "model-builder.h" // for Builder
#include "model-types.h" // for DataTemplate
#include <libiec61850/iec61850_common.h> // for eFunctionalConstraint::IEC6...
#include <libiec61850/iec61850_model.h> // for ModelNode
#include <stdbool.h> // for bool
#include <stdint.h> // for uint8_t

namespace rth {
namespace iec61850 {
    namespace model {

        enum BuilderState { ReadData,
            CreateModel,
            Done };

        /**
 * @class DefaultModelBuilder
 * @brief Дефолтный билдер для модели
 * Билдер может быть использован однократно. Такой подход позволяет упростить
 * реализацию и избежать проблем с остаточными/пром. данными
 */
        class DefaultBuilder : public Builder {
        public:
            DefaultBuilder();
            ~DefaultBuilder();

            int PrepareHandler();
            int IEDHandler(const configuration::IED& ied,
                const configuration::ParseContext& context);
            int LogicalDeviceHandler(const configuration::LogicalDevice& ld,
                const configuration::ParseContext& context);
            int LogicalNodeHandler(const configuration::LogicalNode& ln,
                const configuration::ParseContext& context);
            int DataSetHandler(const configuration::DataSet& ds,
                const configuration::ParseContext& context);
            int FCDAHandler(const configuration::FCDA& fcda,
                const configuration::ParseContext& context);
            int ReportHandler(const configuration::Report& rcb,
                const configuration::ParseContext& context);

            int DataTypeHandler(const configuration::DataType& type,
                const configuration::ParseContext& context);
            int DataAttributeHandler(const configuration::DataAttribute& attribute,
                const configuration::ParseContext& context);
            int DoneHandler(const configuration::ParseContext& context);

            bool IsDone() const;

        private:
            int FillNode(ModelNode* node, const char* data_type_name,
                const DataTemplate& data_template,
                FunctionalConstraint fc = IEC61850_FC_NONE, uint8_t trigger = 0,
                int step = 0);

        private:
            BuilderState state_;
        };
    }
}
}
