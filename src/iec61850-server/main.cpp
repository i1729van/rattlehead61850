#include "server-settings.h" // for ServerSettings
#include "server.h" // for Server

using namespace rth::iec61850::server;

int main()
{
    const char* default_config = "data/B20.icd";
    const char* default_ied = "TEMPLATE";
    const char* conn_name = "TEST_SERVER";
    ServerSettings settings(default_config, default_ied, conn_name);
    Server server(settings);
    // server.LoadConfig(default_config, default_ied);
    server.StartServer();
    server.StopServer();

    /*LogError err;
  LogWarning warn;
  LogInfo info;

  err << "Error w flush!\n";
  warn << "Warning!\n";
  info << "Info!\n";*/

    return 0;
}
