#include "model-types.h"
#include "model-explorer.h" // for NodesListConstIterator, NodesList, Creat...
#include <assert.h> // for assert
#include <list> // for _List_const_iterator
#include <utility> // for pair

namespace rth {
namespace iec61850 {
    namespace model {
        const char* DataTemplateRecord::GetID() const { return type.id.c_str(); }

        DataTypeAttributesConstIter DataTemplateRecord::AttributesBegin() const
        {
            return attributes.begin();
        }

        DataTypeAttributesConstIter DataTemplateRecord::AttributesEnd() const
        {
            return attributes.end();
        }

        bool IsEmpty(const DataTemplate& dt) { return dt.end() == dt.begin(); }

        DataAttributeMap::DataAttributeMap()
            : busy_(false)
        {
        }

        DataAttributeMap::~DataAttributeMap() {}

        DataAttributeMapEntry DataAttributeMap::Get(const char* name)
        {
            AttributesCacheContIterator it = map_.find(name);

            if (it != map_.end()) {
                return it->second;
            }

            return DataAttributeMapEntry();
        }

        void DataAttributeMap::Clear()
        {
            map_.clear();
            busy_ = false;
        }

        size_t DataAttributeMap::Create(const IedModel* model)
        {
            assert(!busy_);

            size_t result = 0;
            NodesList& nodes = CreateNodeList(model);
            NodesListConstIterator it = nodes.begin(), end = nodes.end();

            while (it != end) {
                DataAttribute* attrib = reinterpret_cast<DataAttribute*>(
                    IedModel_getModelNodeByObjectReference(const_cast<IedModel*>(model),
                        it->c_str()));

                if (attrib) {
                    map_[*it] = DataAttributeMapEntry(attrib);
                    result++;
                }

                ++it;
            }

            DeleteNodeList(nodes);

            if (result > 0) {
                busy_ = true;
            }
            return result;
        }
    }
}
}
