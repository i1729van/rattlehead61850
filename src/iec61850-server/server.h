#pragma once

#include "iec61850-server/server-errors.h" // for ServerError
#include "iec61850-server/server-settings.h" // for ServerSettings
#include "iec61850-server/server-statistic.h" // for ServerStatistic
#include <libiec61850/iec61850_server.h> // for IedServer, sIedServer
#include <stdbool.h> // for bool

namespace rth {
namespace iec61850 {
    namespace model {
        class BuildResult;
    }
}
} // lines 12-12
namespace rth {
namespace iec61850 {
    namespace server {
        class InputMonitor;
    }
}
} // lines 19-19
namespace rth {
namespace iec61850 {
    namespace server {
        class StatisticInformer;
    }
}
} // lines 20-20
namespace rth {
namespace thread {
    class Thread;
}
} // lines 27-27

namespace rth {
namespace iec61850 {
    namespace server {

        class Server {
        public:
            explicit Server(ServerSettings settings);
            ~Server();

            bool StartServer();
            void StopServer();
            bool IsRunning() const;
            ServerError GetLastError() const;
            ServerStatistic GetStatistic() const;

        private:
            bool BuildModel();
            void Error(ServerError error);
            bool RunInputMonitor();
            bool StopInputMonitor();

            bool RunInformer();
            bool StopInformer();

        private:
            // Запрет копирвоания и присваивания
            Server(const Server&);
            Server& operator=(const Server&);

        private:
            ServerSettings settings_;
            IedServer ied_server_;

            InputMonitor* input_monitor_;
            thread::Thread* input_monitor_thread_;

            StatisticInformer* informer_;
            thread::Thread* inforemer_thread;

            model::BuildResult* build_;

            ServerError error_;
            bool is_started_;
        };
    }
}
}
