#include <assert.h>
#include <sstream>
#include <string.h>

#include "base/shared-memory.h"
#include "server-statistic.h"

namespace rth {
namespace iec61850 {
    namespace server {
        InputStatistic::InputStatistic()
            : error_cnt_(0)
            , block_cnt_(0)
            , values_cnt_(0)
            , total_cycle_ms_(0)
            , total_cycle_cnt_(0)
            , total_lock_ms_(0)
            , total_lock_cnt_(0)
            , avg_cycle_ms_(0)
            , avg_lock_ms_(0)
            , last_update_ms_(0)

        {
        }

        void InputStatistic::AddErrorCounter(uint64_t value) { error_cnt_ += value; }
        void InputStatistic::AddDBCounter(uint64_t value) { block_cnt_ += value; }
        void InputStatistic::AddValueCounter(uint64_t value) { values_cnt_ += value; }

        void InputStatistic::AddCycleTime(uint64_t tm)
        {
            total_cycle_ms_ += tm;
            total_cycle_cnt_++;
            avg_cycle_ms_ = total_cycle_ms_ / total_cycle_cnt_;
        }
        void InputStatistic::AddLockTime(uint64_t tm)
        {
            total_lock_ms_ += tm;
            total_lock_cnt_++;
            avg_lock_ms_ = total_lock_ms_ / total_lock_cnt_;
        }

        void InputStatistic::LastUpdateTimestamp(uint64_t value)
        {
            last_update_ms_ = value;
        }

        uint64_t InputStatistic::Errors() const { return error_cnt_; }
        uint64_t InputStatistic::Blocks() const { return block_cnt_; }
        uint64_t InputStatistic::Values() const { return values_cnt_; }
        uint64_t InputStatistic::AvgCycleMilli() const { return avg_cycle_ms_; }
        uint64_t InputStatistic::AvgLockMilli() const { return avg_lock_ms_; }
        uint64_t InputStatistic::LastUpdateTimestamp() const { return last_update_ms_; }

        std::string ToString(const ServerStatistic& statistic)
        {
            std::stringstream ss;
            ss << "Common\n";
            ss << "  pid:" << statistic.pid << "\n";
            ss << "  api:" << statistic.api_version << "\n";
            ss << "  gen_time:" << statistic.tstamp << "\n";
            ss << "Input\n";
            ss << "  avg_cycle_ms:" << statistic.input.AvgCycleMilli() << "\n";
            ss << "  avg_lock_ms:" << statistic.input.AvgLockMilli() << "\n";
            ss << "  block_proc:" << statistic.input.Blocks() << "\n";
            ss << "  values_proc:" << statistic.input.Values() << "\n";
            ss << "  errors:" << statistic.input.Errors() << "\n";
            ss << "  last_update:" << statistic.input.LastUpdateTimestamp() << "\n";
            return ss.str();
        }

        SharedStatistic::SharedStatistic(const char* name)
            : name_(std::string(name) + std::string(RTH_IEC61850_SERVER_STAT_SUFFIX))
            , memory_(NULL)
        {
        }
        SharedStatistic::~SharedStatistic() { Close(); }

        bool SharedStatistic::Create()
        {
            assert(memory_ == NULL);
            memory_ = new rth::ipc::SharedMemory();
            if (memory_->Create(name_.c_str(), kChunkSize)) {
                return true;
            }

            delete memory_;
            return false;
        }

        bool SharedStatistic::Update(const ServerStatistic& statistic)
        {
            assert(memory_);

            static const size_t limit = kChunkSize - 1;
            std::string str = ToString(statistic);
            size_t str_size = str.length();

            str_size = str_size > limit ? limit : str_size;

            memory_->Lock();
            memcpy(memory_->Data(), str.c_str(), str_size + 1);
            memory_->Unlock();

            return true;
        }

        void SharedStatistic::Close()
        {
            delete memory_;
            memory_ = NULL;
        }

        bool SharedStatistic::IsOpen() const
        {
            return memory_ ? memory_->IsOpen() : false;
        }
    }
}
}
