#include "server-helpers.h"
#include <stdbool.h> // for bool
#include <stdint.h> // for int32_t, uint32_t, uint16_t, uint64_t

namespace rth {
namespace iec61850 {
    namespace server {
        /**
 * @brief Обновдение значений
 * @param server - сервер iec
 * @param dst - аттрибут-применик
 * @param src - источник данных
 */
        DataAttributeUpdateResult UpdateIecValue(IedServer server, DataAttribute& dst,
            const DataValue& src)
        {
            DataAttributeUpdateResult result = AnalyzeTypes(dst, src);
            // Обновлять занчения можно в двух случаях - типы совместимы и потеря точности
            if (result == DataAttributeUpdateResultOK || result == DataAttributeUpdateResultPrecisionLoss) {
                switch (dst.type) {
                case IEC61850_BOOLEAN:
                    IedServer_updateBooleanAttributeValue(server, &dst,
                        src.Read<uint64_t>() != 0);
                    break;
                case IEC61850_INT8:
                case IEC61850_INT16:
                case IEC61850_INT32:
                    IedServer_updateInt32AttributeValue(server, &dst, src.Read<int32_t>());
                    break;

                case IEC61850_INT64:
                case IEC61850_INT128: {
                    MmsValue* tmpValue = MmsValue_newIntegerFromInt64(src.Read<int64_t>());
                    IedServer_updateAttributeValue(server, &dst, tmpValue);
                    MmsValue_delete(tmpValue);
                    break;
                }
                case IEC61850_INT8U:
                case IEC61850_INT16U:
                case IEC61850_INT32U:
                    IedServer_updateUnsignedAttributeValue(server, &dst,
                        src.Read<uint32_t>());
                    break;

                case IEC61850_FLOAT32:
                    IedServer_updateFloatAttributeValue(server, &dst, src.Read<float>());
                    break;

                case IEC61850_FLOAT64: {
                    MmsValue* tmpValue = MmsValue_newDouble(src.Read<double>());
                    IedServer_updateAttributeValue(server, &dst, tmpValue);
                    MmsValue_delete(tmpValue);
                    break;
                }
                case IEC61850_TIMESTAMP:
                    IedServer_updateUTCTimeAttributeValue(server, &dst,
                        src.Read<uint64_t>());
                    break;

                case IEC61850_QUALITY:
                    IedServer_updateQuality(server, &dst, src.Read<uint16_t>());
                    break;

                case IEC61850_ENUMERATED:
                    IedServer_updateInt32AttributeValue(server, &dst, src.Read<int32_t>());
                    break;

                case IEC61850_CODEDENUM:
                    IedServer_updateBitStringAttributeValue(server, &dst,
                        src.Read<uint8_t>());
                    break;

                case IEC61850_INT24U:
                case IEC61850_OCTET_STRING_64:
                case IEC61850_OCTET_STRING_6:
                case IEC61850_OCTET_STRING_8:
                case IEC61850_VISIBLE_STRING_32:
                case IEC61850_VISIBLE_STRING_64:
                case IEC61850_VISIBLE_STRING_65:
                case IEC61850_VISIBLE_STRING_129:
                case IEC61850_VISIBLE_STRING_255:
                case IEC61850_UNICODE_STRING_255:
                case IEC61850_CHECK:
                case IEC61850_GENERIC_BITSTRING:
                case IEC61850_CONSTRUCTED:
                case IEC61850_ENTRY_TIME:
                case IEC61850_PHYCOMADDR:
                    /*case IEC61850_CURRENCY:*/ /* not supported in v.1.0.1*/
                default:
                    break;
                }
            }
            return result;
        }

        /**
 * @brief Проверка совместимости типов
 * @param dst
 * @param src
 * @return
 */
        DataAttributeUpdateResult AnalyzeTypes(const DataAttribute& dst,
            const DataValue& src)
        {
            // Особвый случай. Может возникнуть при создание CO тэгов без обработчиков:
            if (dst.mmsValue == NULL) {
                return DataAttributeUpdateResultUnsupportedTypes;
            }

            // Проверить тип
            switch (dst.type) {
            case IEC61850_BOOLEAN:
                switch (src.GetType()) {
                case DataValueTypeInt8:
                case DataValueTypeUInt8:
                case DataValueTypeInt16:
                case DataValueTypeUInt16:
                case DataValueTypeInt32:
                case DataValueTypeUInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_INT8:
                switch (src.GetType()) {
                case DataValueTypeInt8:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeUInt8:
                case DataValueTypeInt16:
                case DataValueTypeUInt16:
                case DataValueTypeInt32:
                case DataValueTypeUInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultPrecisionLoss;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_INT16:
                switch (src.GetType()) {
                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeUInt8:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeUInt16:
                case DataValueTypeInt32:
                case DataValueTypeUInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultPrecisionLoss;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_INT32:
                switch (src.GetType()) {
                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeUInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultPrecisionLoss;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_INT64:
                switch (src.GetType()) {
                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                case DataValueTypeUInt32:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultPrecisionLoss;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_INT128:
                /* Unsupported */
                /*tmpValue = MmsValue_newIntegerFromInt64(newData->Value.I64);
      MmsValue_setInt64(tmpValue, newData->Value.I64);
      IedServer_updateAttributeValue(server, attribute, tmpValue);
      MmsValue_delete(tmpValue);*/
                return DataAttributeUpdateResultUnsupportedTypes;

            case IEC61850_INT8U:
                switch (src.GetType()) {
                case DataValueTypeUInt8:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeInt64:

                case DataValueTypeUInt16:
                case DataValueTypeUInt32:
                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultPrecisionLoss;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_INT16U:
                switch (src.GetType()) {
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeInt64:

                case DataValueTypeUInt32:
                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultPrecisionLoss;
                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_INT32U:
                switch (src.GetType()) {
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                case DataValueTypeUInt32:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultPrecisionLoss;
                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_FLOAT32:
                switch (src.GetType()) {
                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                case DataValueTypeFloat:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeUInt32:
                case DataValueTypeInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt64:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultPrecisionLoss;
                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_FLOAT64:
                switch (src.GetType()) {
                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                case DataValueTypeInt32:
                case DataValueTypeUInt32:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeInt64:
                case DataValueTypeUInt64:
                    return DataAttributeUpdateResultPrecisionLoss;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_TIMESTAMP:
                switch (src.GetType()) {
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                case DataValueTypeUInt32:
                case DataValueTypeUInt64:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultIncompatibleTypes;
                }

            case IEC61850_QUALITY:
                switch (src.GetType()) {
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt32:
                case DataValueTypeUInt64:
                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultIncompatibleTypes;
                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_ENUMERATED:
                // Enum в libIEC = int32. На стороне сервера я могу записать и прочитать
                // его
                // как Int32. НО,чтение числа клиентом [Avenue] приводит к урезанию
                // значения
                // до
                // [-128;127].
                // Я посмотрет Wireshark'ом. В нем видно следующее:
                // Записываю число 65535 [0xFFFF] в сервер.
                // При чтении с сервера вижу в посылке 0xFFFF.
                // при этом клиент показывает -1.
                // Аналогично с 4-х байтными числами.
                // Пока оставлю Enum = 4 байта, но если все клиенты ожидают, что enum д.б.
                // 1
                // байт,
                // это необходимо будет исправить.
                switch (src.GetType()) {
                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeUInt8:
                case DataValueTypeUInt16:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeUInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt64:
                    return DataAttributeUpdateResultPrecisionLoss;

                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultIncompatibleTypes;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_CODEDENUM:
                // BitString - 1 байт
                switch (src.GetType()) {
                case DataValueTypeUInt8:
                    return DataAttributeUpdateResultOK;

                case DataValueTypeInt8:
                case DataValueTypeInt16:
                case DataValueTypeInt32:
                case DataValueTypeInt64:
                case DataValueTypeUInt16:
                case DataValueTypeUInt32:
                case DataValueTypeUInt64:
                    return DataAttributeUpdateResultPrecisionLoss;

                case DataValueTypeFloat:
                case DataValueTypeDouble:
                    return DataAttributeUpdateResultIncompatibleTypes;

                case DataValueTypeUnk:
                default:
                    return DataAttributeUpdateResultUnsupportedTypes;
                }

            case IEC61850_INT24U:
            case IEC61850_OCTET_STRING_64:
            case IEC61850_OCTET_STRING_6:
            case IEC61850_OCTET_STRING_8:
            case IEC61850_VISIBLE_STRING_32:
            case IEC61850_VISIBLE_STRING_64:
            case IEC61850_VISIBLE_STRING_65:
            case IEC61850_VISIBLE_STRING_129:
            case IEC61850_VISIBLE_STRING_255:
            case IEC61850_UNICODE_STRING_255:
            case IEC61850_CHECK:
            case IEC61850_GENERIC_BITSTRING:
            case IEC61850_CONSTRUCTED:
            case IEC61850_ENTRY_TIME:
            case IEC61850_PHYCOMADDR:
                /*case IEC61850_CURRENCY:*/ /* not supported in v.1.0.1*/
            default:
                return DataAttributeUpdateResultUnsupportedTypes;
            }

            /*return DataAttributeUpdateResultUnk;*/
        }
    }
}
}
