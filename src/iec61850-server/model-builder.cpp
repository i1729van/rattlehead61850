#include "model-builder.h"
#include "iec61850-server/model-types.h" // for DataTemplate
#include <assert.h> // for assert
#include <libiec61850/iec61850_dynamic_model.h> // for IedModel_destroy

namespace rth {
namespace iec61850 {
    namespace model {
        BuildResult& Builder::GetResults() const
        {
            return *new BuildResult(ied_device_, data_template_);
        }

        BuildResult& Build(Builder& builder, const char* filename, const char* ied)
        {
            assert(filename);
            assert(ied);

            configuration::Reader reader(builder);
            // Чтение типов данных, а затем создание модели
            bool last_result = true;
            while (last_result && !builder.IsDone()) {
                last_result = reader.Read(filename, ied);
            }
            return builder.GetResults();
        }

        void ReleaseBuild(BuildResult& model)
        {
            if (model.GetModel()) {
                IedModel_destroy(model.GetModel());
            }

            if (model.GetDataTemplate()) {
                delete model.GetDataTemplate();
            }

            delete &model;
        }
    }
}
}
