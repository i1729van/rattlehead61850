#pragma once

#include "base/thread.h" // for Mutex
#include "iec61850-common/defs.h" // for DataBlock, DataQueue
#include "iec61850-server/model-types.h" // for DataAttributeMap
#include "iec61850-server/server-statistic.h" // for InputStatistic
#include "server-task.h" // for TaskController
#include <libiec61850/iec61850_model.h> // for IedModel
#include <libiec61850/iec61850_server.h> // for IedServer, sIedServer
#include <stddef.h> // for size_t
#include <stdint.h> // for uint32_t
#include <string> // for string

namespace rth {
namespace iec61850 {
    namespace server {

        /**
 * @class InputMonitor
 * @brief Класс отвечает за получение данных из вне и обновление данных в
 * сервере
 * IEC61850
 */
        class InputMonitor {
        public:
            /* -----------------------------------------------------------------------
   * Методы работающие в потоке вызывающего кода
   * -----------------------------------------------------------------------*/
            InputMonitor(const char* name, IedServer server, size_t buffer_size,
                size_t transaction_size, unsigned int read_delay_micro);

            /* -----------------------------------------------------------------------
   * Методы работающие в потоке монитора
   * -----------------------------------------------------------------------*/
            void operator()();

            /* -----------------------------------------------------------------------
   * Методы работающие везде
   * -----------------------------------------------------------------------*/
            TaskController& Controller();
            InputStatistic GetStatistic() const;

        private:
            void Scan();

            /* -----------------------------------------------------------------------
   * Данные
   * -----------------------------------------------------------------------*/
        private:
            TaskController controller_;
            const size_t buffer_size_;
            const size_t transaction_size_;
            const uint32_t read_delay_micro_;

            std::string name_;
            IedServer server_;
            IedModel* model_;
            DataQueue input_;
            DataQueue acknowledge_;

            DataBlock* input_buffer_;
            DataBlock* ack_buffer_;
            model::DataAttributeMap attributes_map_; // карта имя-аттрибутов 61850

            InputStatistic stat_; // Статистика
            mutable thread::Mutex stat_mutex_; // Мьютекс для доступа к статистике
        };
    }
}
}
