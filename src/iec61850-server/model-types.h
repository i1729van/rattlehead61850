#pragma once
#include "base/hash-table.h" // for HashTable
#include "iec61850-config/base-types.h" // for DataAttribute, DataType
#include <libiec61850/iec61850_model.h> // for DataAttribute, IedModel
#include <map> // for map<>::const_iterator, map<>...
#include <stdbool.h> // for bool
#include <stddef.h> // for NULL, size_t
#include <string> // for string
#include <vector> // for vector, vector<>::const_iter...

namespace rth {
namespace iec61850 {
    namespace model {

        typedef std::vector<configuration::DataAttribute> DataTypeAttributes;
        typedef std::vector<configuration::DataAttribute>::const_iterator
            DataTypeAttributesConstIter;

        /**
 * @class DataTemplateRecord
 * @brief Описание LNodeType / DOType /DAtype / EnumType
 */
        typedef struct DataTemplateRecord {
            configuration::DataType type; // Определение типа данных *Type
            DataTypeAttributes attributes; // Набор аттрибутов в типе [DO/DA/SDO/BDA]

            DataTemplateRecord() {}
            const char* GetID() const;
            DataTypeAttributesConstIter AttributesBegin() const;
            DataTypeAttributesConstIter AttributesEnd() const;

        } DataTemplateRecord;

        typedef HashTable<std::string, DataTemplateRecord> DataTemplate;
        typedef HashTable<std::string, DataTemplateRecord>::const_iterator
            DataTemplateConstIter;

        bool IsEmpty(const DataTemplate& dt);

        /**
 * @class IEC61850CachedAttribute
 * @brief Запись из кэша аттрибутов
 */
        typedef struct DataAttributeMapEntry {
            DataAttribute* attribute;
            DataAttributeMapEntry()
                : attribute(NULL)
            {
            }
            explicit DataAttributeMapEntry(DataAttribute* attrib)
                : attribute(attrib)
            {
            }
            bool IsValid() const { return attribute != NULL; }
            const DataAttribute& GetAttribute() const { return *attribute; }
            DataAttribute& GetAttribute() { return *attribute; }
        } DataAttributeMapEntry;

        typedef HashTable<std::string, DataAttributeMapEntry>::const_iterator
            AttributesCacheContIterator;
        /**
 * @class AttributesCache
 * @brief Связка имя-аттрибут 61850. Необходим для быстрого доступа к данным
 * модели по имени.
 * Стандартный функции поиска libiec не слишком шустрые.
 * Кэш следует применять на уже проинициализированной модели (выполнен
 * IedServer_create).
 * Хотя составлять можно и на только что созданной. Если модель не
 * проиницилазированна, то
 * поля Mms будут заполнены всяким мусором и работа с Mms будет приводить к
 * печальным результатам.
 */
        class DataAttributeMap {
        public:
            DataAttributeMap();
            ~DataAttributeMap();

            /**
   * @brief Закэшировать узлы модели
   * Модель должна быть загружена и проинициализированна
   * @param model - указатель на модель
   * @return Кол-во закэшированных узлов
   */
            size_t Create(const IedModel* model);

            DataAttributeMapEntry Get(const char* name);
            void Clear();

        private:
            HashTable<std::string, DataAttributeMapEntry> map_;
            bool busy_;
        };
    }
}
}
