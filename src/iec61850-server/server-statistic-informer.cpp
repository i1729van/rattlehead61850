#include "server-statistic-informer.h"
#include "base/thread.h" // for Millisleep
#include "iec61850-server/server-statistic.h" // for SharedStatistic, Serve...
#include "server.h" // for Server
#include <stdbool.h> // for true, false

namespace rth {
namespace iec61850 {
    namespace server {

        StatisticInformer::StatisticInformer(const Server& server, const char* name,
            size_t period_ms)
            : period_ms_(period_ms)
            , name_(name)
            , server_(server)
        {
        }

        /* -----------------------------------------------------------------------
 * Методы работающие в потоке монитора
 * -----------------------------------------------------------------------*/
        void StatisticInformer::operator()()
        {
            const size_t kPauseMs = 50;
            size_t elapsed_time_ms = 0;

            SharedStatistic shared_statistic(name_.c_str());
            if (!shared_statistic.Create()) {
                return;
            }
            controller_.IsRunning(true);

            while (!controller_.Stop()) {
                // обновление статисктика сразу после старта, либо по истечению таймаута
                if (elapsed_time_ms == 0 || elapsed_time_ms > period_ms_) {
                    const ServerStatistic& stat = server_.GetStatistic();
                    shared_statistic.Update(stat);
                    elapsed_time_ms = 0;
                }
                elapsed_time_ms += kPauseMs;
                rth::thread::Millisleep(kPauseMs);
            }

            controller_.IsDone(true);
            controller_.IsRunning(false);
        }

        TaskController& StatisticInformer::Controller() { return controller_; }
    }
}
}
