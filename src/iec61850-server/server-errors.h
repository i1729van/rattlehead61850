#pragma once

namespace rth {
namespace iec61850 {
    namespace server {
        enum ServerError {
            ServerErrorNone,
            ServerErrorInvalidConfig,
            ServerErrorInvalidModel,
            ServerErrorServiceError
        };
    }
}
}
