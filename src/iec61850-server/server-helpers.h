#pragma once
#include "iec61850-common/defs.h" // for DataAttributeUpdateResult
#include <libiec61850/iec61850_model.h> // for DataAttribute
#include <libiec61850/iec61850_server.h> // for IedServer
namespace rth {
namespace iec61850 {
    namespace server {
        DataAttributeUpdateResult UpdateIecValue(IedServer server, DataAttribute& dst,
            const DataValue& src);

        DataAttributeUpdateResult AnalyzeTypes(const DataAttribute& dst,
            const DataValue& src);
    }
}
}
