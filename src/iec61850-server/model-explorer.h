#pragma once

#include <libiec61850/iec61850_model.h>
#include <list>
#include <string>

namespace rth {
namespace iec61850 {
    namespace model {

        typedef std::list<std::string> NodesList;
        typedef std::list<std::string>::const_iterator NodesListConstIterator;

        /**
 * @brief Создать список узлов модели. Список должен быть удален при помощи
 * DeleteNodeList
 * @param model - указатель на модель IEC61850
 * @return возвращает массив строк с именами узлов
 */
        NodesList& CreateNodeList(const IedModel* model);

        /**
 * @brief Уничтожить список тегов
 * @param nodes - ссылка на список
 */
        void DeleteNodeList(NodesList& nodes);
    }
}
}
