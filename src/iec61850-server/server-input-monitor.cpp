#include "server-input-monitor.h"
#include "base/thread.h" // for Mutex, Microsleep
#include "base/time.h" // for GetMonoTimeMilli, GetR...
#include "iec61850-common/app-log.h" // for operator<<, LogMessage
#include "iec61850-common/config.h" // for RTH_IEC61850_SERVER_IN...
#include "iec61850-server/server-statistic.h" // for InputStatistic
#include "server-helpers.h" // for UpdateIecValue
#include "server-log.h" // for LogWarning
#include <libiec61850/iec61850_server.h> // for IedServer_getDataModel
#include <stdbool.h> // for bool, true, false
#include <stdint.h> // for uint64_t

namespace rth {
namespace iec61850 {
    namespace server {
        InputMonitor::InputMonitor(const char* name, IedServer server,
            size_t buffer_size, size_t transaction_size,
            unsigned int read_delay_micro)
            : buffer_size_(buffer_size)
            , transaction_size_(transaction_size)
            , read_delay_micro_(read_delay_micro)
            , name_(name)
            , server_(server)
            , model_(IedServer_getDataModel(server))
            , input_buffer_(NULL)
            , ack_buffer_(NULL)
        {
        }

        void InputMonitor::operator()()
        {
            /* Лишние локи мьютексов не нужны, т.к. операции инициализации
   * и использования перменных у нас разнесены по времени.
   * Единственная разделяемая перменная - cmd_stop_.
   * Если что-то поменяется, необходимо будет пересмотреть подход к блокировкам
   * /
   * заныкать внутренние переменные необходимые потоку */
            // 1. Подготовить данные
            std::string input_name = std::string(name_) + std::string(RTH_IEC61850_SERVER_INPUT_QUEUE_SUFFIX);
            std::string ack_name = std::string(name_) + std::string(RTH_IEC61850_SERVER_INPUT_ACK_QUEUE_SUFFIX);

            // Области обмена данными
            bool in_queue_ok = input_.Create(input_name.c_str(), buffer_size_);
            bool ack_queue_ok = acknowledge_.Create(ack_name.c_str(), buffer_size_);

            // Связка имени тжа и информации о нем
            bool cache_ok = attributes_map_.Create(model_);

            // Пром. буфер для обработки транзакций
            input_buffer_ = new DataBlock[transaction_size_];
            ack_buffer_ = new DataBlock[transaction_size_];

            if (in_queue_ok && ack_queue_ok && cache_ok) {
                controller_.IsRunning(true);
                while (!controller_.Stop()) {
                    Scan();
                }
            }

            delete[] input_buffer_;
            delete[] ack_buffer_;

            controller_.IsDone(true);
            controller_.IsRunning(false);
        }

        InputStatistic InputMonitor::GetStatistic() const
        {
            InputStatistic result;
            stat_mutex_.Lock();
            result = stat_;
            stat_mutex_.Unlock();
            return result;
        }

        TaskController& InputMonitor::Controller() { return controller_; }

        /**
 * @brief Обработка получения данных через очередь обмена
 * Блокировки данных выполянются в вызывающих методах -> работать с данными[но
 * не командами]
 * можно без блокировок
 */
        void InputMonitor::Scan()
        {
            uint64_t lock_start = 0;
            uint64_t cycle_start = rth::time::GetMonoTimeMilli();

            // Если не накоплен лимит данных, то пытаемся подождать, чтобы получить больше
            // данных
            if (input_.Size() < transaction_size_) {
                thread::Microsleep(read_delay_micro_);
            }

            size_t nerrors = 0, nvalues = 0,
                   nblocks = input_.Pop(input_buffer_, transaction_size_);

            if (nblocks > 0) {
                DataBlockStream input_stream(input_buffer_, nblocks);
                DataBlockStream ack_stream(ack_buffer_, nblocks);
                DataValue cur_record;

                // Обновить модель данных
                lock_start = rth::time::GetMonoTimeMilli();
                IedServer_lockDataModel(server_);

                while (input_stream >> cur_record) {
                    DataAttributeUpdateResult result;
                    model::DataAttributeMapEntry reference = attributes_map_.Get(cur_record.GetName());

                    if (reference.IsValid()) {
                        result = UpdateIecValue(server_, reference.GetAttribute(), cur_record);
                    } else {
                        result = DataAttributeUpdateResultTagIsMissing;
                    }

                    // Формирование блока подвреждения
                    cur_record.SetType(DataValueTypeInt32);
                    cur_record.WriteInt32(result);
                    ack_stream << cur_record;

                    nerrors += (result != DataAttributeUpdateResultOK) && (result != DataAttributeUpdateResultPrecisionLoss);
                    nvalues++;

                    // Формирование лога
                    if (result != DataAttributeUpdateResultOK) {
                        LogWarning() << cur_record.GetName() << " updated with code " << result
                                     << "\n";

                        if (result == DataAttributeUpdateResultIncompatibleTypes || result == DataAttributeUpdateResultPrecisionLoss || result == DataAttributeUpdateResultUnsupportedTypes) {
                            LogWarning() << cur_record.GetName()
                                         << " rth_type=" << cur_record.GetType()
                                         << ":iec_type=" << reference.GetAttribute().type << "\n";
                        }
                    }
                }

                IedServer_unlockDataModel(server_);

                // Проставить ID ответных сообщений и записать в очередь подтверждений
                for (size_t iblock = 0; iblock < nblocks; iblock++) {
                    ack_stream.Blocks()[iblock].SetID(input_stream.Blocks()[iblock].GetID());
                }
                acknowledge_.Push(ack_stream.Blocks(), ack_stream.Size());
            }

            // Обновить статистику
            uint64_t cycle_end = rth::time::GetMonoTimeMilli();
            stat_mutex_.Lock();
            if (lock_start) {
                stat_.AddLockTime(cycle_end - lock_start);
                stat_.AddCycleTime(cycle_end - cycle_start);
            }

            stat_.AddErrorCounter(nerrors);
            stat_.AddValueCounter(nvalues);
            stat_.AddDBCounter(nblocks);

            if (nblocks > 0) {
                stat_.LastUpdateTimestamp(rth::time::GetRealTimeMilli());
            }

            stat_mutex_.Unlock();
        }
    }
}
}
