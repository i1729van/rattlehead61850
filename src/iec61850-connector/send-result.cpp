#include "iec61850-connector/send-result.h" // for SendResult, SingleRecor...
#include "iec61850-common/defs.h" // for DataAttributeUpdateResult
#include "iec61850-common/errors-defaults.h" // for DefaultErrorDescription
#include <stddef.h> // for size_t
#include <string> // for allocator, operator+

namespace rth {
namespace iec61850 {
    namespace connector {

        SendResult MakeErrorNotConnected() { return SendResult("MemoryChannel : "); }

        SendResult MakeErrorOverflow() { return SendResult("MemoryChannel : "); }

        SendResult MakeErrorIDMistmatch()
        {
            return SendResult("MemoryChannel : ", DataAttributeUpdateResultInvalidData);
        }

        SendResult MakeErrorTimeout()
        {
            return SendResult("MemoryChannel : ", DataAttributeUpdateResultNotRespond);
        }

        static const char* UpdateResultToString(DataAttributeUpdateResult result)
        {
            /*DataAttributeUpdateResultUnk,
  DataAttributeUpdateResultOK,
  DataAttributeUpdateResultNotRespond,
  DataAttributeUpdateResultInvalidData,
  DataAttributeUpdateResultTagIsMissing,
  DataAttributeUpdateResultIncompatibleTypes,
  DataAttributeUpdateResultPrecisionLoss,
  DataAttributeUpdateResultUnsupportedTypes*/
            static const char* messages[] = { "Unknown", "OK",
                "Not respond", "Invalid data",
                "Tag is missing", "Incompatible types",
                "Precision loss", "Unsupported type" };
            return messages[result];
        }

        /*-----------------------------------------------------------------------------
 * Errors
 * ---------------------------------------------------------------------------*/
        SingleRecordError::SingleRecordError(const char* name,
            DataAttributeUpdateResult result)
            : DefaultErrorDescription(std::string(name) + std::string(" ") + std::string(UpdateResultToString(result)))
            , update_result(result)
        {
        }

        DataAttributeUpdateResult SingleRecordError::GetResult() const
        {
            return update_result;
        }

        /*-----------------------------------------------------------------------------
 *  Send result
 * ---------------------------------------------------------------------------*/
        SendResult::SendResult()
            : sent_counter_(0)
            , ack_counter_(0)
        {
        }

        SendResult::SendResult(const char* name, DataAttributeUpdateResult result)
            : sent_counter_(0)
            , ack_counter_(0)
        {
            error_list_.Append(SingleRecordError(name, result));
        }

        SendResult::SendResult(const DataBlock& block)
            : sent_counter_(0)
            , ack_counter_(0)
        {
            Analyze(block);
        }

        bool SendResult::IsOk() const { return error_list_.Count() == 0; }
        size_t SendResult::GetErrorsCount() const { return error_list_.Count(); }

        const TransactionErrors& SendResult::GetErrorList() const
        {
            return error_list_;
        }

        void SendResult::Analyze(const DataBlock& block)
        {
            size_t last_idx = block.GetSize();
            const DataValue* data = block.GetData();
            for (size_t i = 0; i < last_idx; i++, data++) {
                DataAttributeUpdateResult upd_result = static_cast<DataAttributeUpdateResult>(data->ReadInt32());
                if (upd_result != DataAttributeUpdateResultOK) {
                    error_list_.Append(SingleRecordError(data->GetName(), upd_result));
                }
            }
            ack_counter_++;
        }

        void SendResult::SentCounterInc() { sent_counter_++; }

        void SendResult::SetSentCounter(size_t value) { sent_counter_ = value; }

        size_t SendResult::GetSentCounter() const { return sent_counter_; }
        size_t SendResult::GetAckCounter() const { return ack_counter_; }
    }
}
}
