#pragma once

#include "iec61850-common/config.h" // for RTH_IEC61850_DEF_TRANSAC...
#include "iec61850-common/defs.h" // for DataBlock, DataQueue
#include "iec61850-connector/send-result.h" // for SendResult
#include <stddef.h> // for size_t
#include <stdint.h> // for uint32_t
#include <string> // for string

namespace rth {
namespace iec61850 {
    namespace connector {

        /**
 * @class Connection
 * @brief Подключение к серверу через именованный участок памяти
 */
        class Connection {
        public:
            Connection(const char* name, size_t timeout_milli = 1000);
            ~Connection();

            bool Open();
            void Close();
            bool IsOpen() const;

            /* Отрпавить блок данных без ожидания */
            bool SendDataBlock(const DataBlock& block);

            /* Прочитать подтверждение без ожидания */
            bool ReceiveDataBlockAck(DataBlock* block);

            /* Отправить блок данных и подождать подтверждения */
            SendResult Send(const DataBlock& block);

            /* Отправить несколько блоков данных и подождать подтверждения */
            SendResult Send(const DataBlock* blocks, size_t nblocks);

        public:
            static size_t GetMaxTransactionSize();

        private:
            uint32_t GetNextID();

            //Noncpoyable
            Connection(const Connection& conn);
            Connection& operator=(const Connection& conn);

        private:
            static const size_t kWaitPeriodMilli = RTH_IEC61850_MINIMAL_TIMEOUT_MS;
            static const size_t kMaxTransSize = RTH_IEC61850_DEF_TRANSACTION_SIZE;

        private:
            DataQueue send_queue_;
            DataQueue ack_queue_;
            std::string name_;
            uint32_t trans_id_;
            size_t timeout_milli_;

            DataBlock* send_buffer_;
            DataBlock* ack_buffer_;
        };

        std::string GetServerInfo(const char* connection);
    }
}
}
