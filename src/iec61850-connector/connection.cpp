#include "connection.h"
#include "base/shared-memory.h" // for SharedMemory
#include "base/thread.h" // for Millisleep
#include "iec61850-connector/send-result.h" // for SendResult, MakeErrorNot...
#include <assert.h> // for assert
#include <ostream> // for operator<<, stringstream

namespace rth {
namespace iec61850 {
    namespace connector {

        /*-----------------------------------------------------------------------------
 * Connection
 * ---------------------------------------------------------------------------*/
        Connection::Connection(const char* name, size_t timeout_milli)
            : name_(name)
            , trans_id_(0)
            , timeout_milli_(timeout_milli)
        {
            send_buffer_ = new DataBlock[kMaxTransSize];
            ack_buffer_ = new DataBlock[kMaxTransSize];
        }

        Connection::~Connection()
        {
            Close();
            delete[] send_buffer_;
            delete[] ack_buffer_;
        }

        bool Connection::Open()
        {
            /* Подключение уже открыто */
            if (send_queue_.IsOpen() || ack_queue_.IsOpen()) {
                return false;
            }

            // Простая проверка версий API.
            std::stringstream api_version;
            std::string server_info = GetServerInfo(name_.c_str());
            api_version << "api:" << RTH_IEC61850_SERVER_API_VERSION;
            if (server_info.find(api_version.str()) == std::string::npos) {
                return false;
            }

            std::string send_queue_name = name_ + std::string(RTH_IEC61850_SERVER_INPUT_QUEUE_SUFFIX);
            std::string ack_queue_name = name_ + std::string(RTH_IEC61850_SERVER_INPUT_ACK_QUEUE_SUFFIX);

            bool open_ok = send_queue_.Open(send_queue_name.c_str()) && ack_queue_.Open(ack_queue_name.c_str()) && send_queue_.Clear() && ack_queue_.Clear();

            if (!open_ok) {
                send_queue_.Close();
                ack_queue_.Close();
            }

            return open_ok;
        }

        void Connection::Close()
        {
            send_queue_.Close();
            ack_queue_.Close();
        }

        bool Connection::SendDataBlock(const DataBlock& block)
        {
            if (!send_queue_.IsOpen())
                return 0;

            DataBlock out = block;
            out.SetID(GetNextID());
            return send_queue_.Push(&out) > 0;
        }

        bool Connection::IsOpen() const
        {
            return send_queue_.IsOpen() && ack_queue_.IsOpen();
        }

        bool Connection::ReceiveDataBlockAck(DataBlock* block)
        {
            if (!ack_queue_.IsOpen())
                return 0;

            return ack_queue_.Pop(block) > 0;
        }

        SendResult Connection::Send(const DataBlock& block) { return Send(&block, 1); }

        SendResult Connection::Send(const DataBlock* blocks, size_t nblocks)
        {
            /* Возврат SendResult не через ссылку - не очепятка.
   * При номральной работе он почти ничего не весит и копирование простое
   * При косяках копировние списка ошибок более накладное - но:
   * 1 - работа с большим кол-вом косяков скорее исключение, чем правило
   * 2 - в списке ошибок введено ограничение, чтобы не раздуть его до таких
   * рамеров,
   * что невозможно будет скопировать */
            assert(blocks);

            if (!send_queue_.IsOpen()) {
                return MakeErrorNotConnected();
            }

            if (!ack_queue_.IsOpen()) {
                return MakeErrorNotConnected();
            }

            SendResult result;
            nblocks = nblocks > kMaxTransSize ? kMaxTransSize : nblocks;

            /* Ещё одно коипирование, чтобы не ломать пользовательсик данные.
   * Если будеи сильно напрягать, можно изменить. Но вряд ли это станет
   * критичным. */
            for (size_t i = 0; i < nblocks; i++) {
                send_buffer_[i] = blocks[i];
                send_buffer_[i].SetID(GetNextID());
            }

            size_t out_cnt = send_queue_.Push(send_buffer_, nblocks);
            if (out_cnt != nblocks) {
                return MakeErrorOverflow();
            }

            result.SetSentCounter(out_cnt);

            // Ждем ответов
            size_t elapsed_time = 0;
            bool wait = true;

            while (wait && elapsed_time < timeout_milli_) {
                if (ack_queue_.Size() < out_cnt) {
                    elapsed_time += kWaitPeriodMilli;
                    thread::Millisleep(kWaitPeriodMilli);
                } else {
                    wait = false;
                }
            }

            size_t in_cnt = ack_queue_.Pop(ack_buffer_, out_cnt);
            if (wait || in_cnt != out_cnt) {
                return MakeErrorTimeout();
            }

            for (size_t i = 0; i < nblocks; i++) {
                if (ack_buffer_[i].GetID() != send_buffer_[i].GetID()) {
                    return MakeErrorIDMistmatch();
                }

                result.Analyze(ack_buffer_[i]);
            }

            return result;
        }

        uint32_t Connection::GetNextID() { return trans_id_++; }

        size_t Connection::GetMaxTransactionSize() { return Connection::kMaxTransSize; }

        std::string GetServerInfo(const char* connection)
        {
            assert(connection);
            std::string result = "none";
            rth::ipc::SharedMemory memory;
            std::string full_name = connection;
            full_name += RTH_IEC61850_SERVER_STAT_SUFFIX;

            if (!memory.Open(full_name.c_str())) {
                return result;
            }

            memory.Lock();
            result = static_cast<char*>(memory.Data());
            memory.Unlock();

            return result;
        }
    }
}
}
