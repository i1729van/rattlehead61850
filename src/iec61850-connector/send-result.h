#pragma once

#include "iec61850-common/defs.h" // for DataAttributeUpdateResult
#include "iec61850-common/errors-defaults.h" // for DefaultErrorDescription
#include "iec61850-common/errors.h" // for ErrorList
#include <stddef.h> // for size_t

namespace rth {
namespace iec61850 {
    namespace connector {

        /**
 * @class ErrorDescription
 * @brief Описание ошибки запроса
 */
        typedef struct SingleRecordError : DefaultErrorDescription {
            SingleRecordError(const char* name, DataAttributeUpdateResult result);
            DataAttributeUpdateResult GetResult() const;

            DataAttributeUpdateResult update_result;
        } SingleRecordError;

        typedef ErrorList<SingleRecordError> TransactionErrors;

        /**
 * @class SendResult
 * @brief Результат выполнения запроса
 */
        class SendResult {
            friend class Connection;

        public:
            SendResult();
            SendResult(const char* name,
                DataAttributeUpdateResult result = DataAttributeUpdateResultUnk);
            SendResult(const DataBlock& block);

            bool IsOk() const;
            size_t GetErrorsCount() const;
            const TransactionErrors& GetErrorList() const;

            size_t GetSentCounter() const;
            size_t GetAckCounter() const;

        private:
            void Analyze(const DataBlock& block);
            void SentCounterInc();
            void SetSentCounter(size_t value);

        private:
            TransactionErrors error_list_;
            size_t sent_counter_;
            size_t ack_counter_;
        };

        SendResult MakeErrorNotConnected();
        SendResult MakeErrorOverflow();
        SendResult MakeErrorIDMistmatch();
        SendResult MakeErrorTimeout();
    }
}
}
