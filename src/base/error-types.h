#pragma once

#if defined(__linux__)
#include <errno.h>
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include <windows.h>
#endif //#if defined(_WIN32)

namespace rth {
namespace error {
#if defined(__linux__)
    typedef int Error;
    enum Codes {
        kNoError = 0,
        kNoEntry = ENOENT,
        kBadDescriptor = EBADF,
        kTryAgain = EAGAIN,
        kAlreadyExists = EEXIST,
        kTimeout = ETIMEDOUT
    };

#endif //#if defined(__linux__)

#if defined(_WIN32)
    typedef DWORD Error;
    enum Codes {
        kNoError = 0,
        kNoEntry = ERROR_FILE_NOT_FOUND,
        kBadDescriptor = 512,
        kTryAgain = 1024,
        kAlreadyExists = ERROR_ALREADY_EXISTS,
        kTimeout = 2048
    };
#endif //#if defined(_WIN32)
}
}
