#pragma once
#if defined(__linux__)
#include "network.h"
#include <stddef.h> // for size_t
#include <sys/types.h> // for ssize_t
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include "defs.h"
#include "network.h"
#endif //#if defined(_WIN32)

namespace rth {
namespace network {

    /**
 * @class UdpSocket
 * @date 03/06/17
 * @brief UDP сокет
 */
    class UdpSocket {
    public:
        UdpSocket();
        ~UdpSocket();
        bool Create();
        bool Listen(const IpAddress& address);
        ssize_t RecvFrom(void* data, size_t size, IpAddress* from) const;
        ssize_t SendTo(const void* data, size_t size, const IpAddress& to) const;
        bool Close();
        bool SetProperty(const char* property, const char* value);

    private:
        SocketHandle handle_;
    };
}
}
