#pragma once

#if defined(__linux__)
#include <stdlib.h> // for size_t
#include <sys/types.h> // for ssize_t
typedef int FileHandler;
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include "defs.h"
#include <stdint.h>
#include <windows.h>
typedef HANDLE FileHandler;
#endif //#if defined(_WIN32)

namespace rth {
namespace file {

#if defined(__linux__)
    typedef int FileHandler;
#endif //#if defined(__linux__)

#if defined(_WIN32)
    typedef HANDLE FileHandler;
#endif //#if defined(_WIN32)

    /**
 * @class File
 * @date 03/06/17
 * @brief Класс для работы с файлами
 */
    class File {
    public:
        File();
        ~File();
        /**
   * @brief Создать
   * @param name - имя файла
   * @param fail_if_exists - вернет ошибку, если флаг взведен и файл существует
   * @return true в случае успеха
   */
        bool Create(const char* name, bool fail_if_exists = true);

        /**
   * @brief Открыть файл
   * @param name - имя
   * @param read_only - 1 для открытия только для чтения
   * @return true в случае успеха
   */
        bool Open(const char* name, bool read_only = true);

        /**
   * @brief Прочитать данные из файла
   * @param data - указатель на буфер-приемник
   * @param size - кол-во байт для чтения
   * @return Кол-во прочитанных байт
   */
        ssize_t Read(void* data, size_t size) const;

        /**
   * @brief Записать данные в файл
   * @param data - указатель на буфер с данными
   * @param size - размер для записи
   * @return Кол-во записанных байт
   */
        ssize_t Write(const void* data, size_t size) const;

        /**
   * @brief Закрыть файл
   * @return true в случае успеха
   */
        bool Close();

    private:
        FileHandler handler_;
    };
}
}
