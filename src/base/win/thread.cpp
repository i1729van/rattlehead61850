#if defined(_WIN32)
#include "../thread.h"

namespace rth {
namespace thread {

    /*-----------------------------------------------------------------------------
* MUTEX
*-----------------------------------------------------------------------------*/
    Mutex::Mutex() { InitializeCriticalSection(&mutex_); }

    Mutex::~Mutex() { DeleteCriticalSection(&mutex_); }

    bool Mutex::Lock()
    {
        EnterCriticalSection(&mutex_);
        return true;
    }

    bool Mutex::TryLock() { return TryEnterCriticalSection(&mutex_); }

    void Mutex::Unlock() { LeaveCriticalSection(&mutex_); }

    void Microsleep(unsigned int microsec) { Sleep(microsec / 1000); }

    void Millisleep(unsigned int millisec) { Sleep(millisec); }
}
}
#endif //#if defined(_WIN32)
