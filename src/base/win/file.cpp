#if defined(_WIN32)
#include "../file.h"
#include <assert.h> // for assert

namespace rth {
namespace file {

    File::File()
        : handler_(INVALID_HANDLE_VALUE)
    {
    }

    File::~File() {}

    bool File::Create(const char* name, bool fail_if_exists)
    {
        assert(handler_ == INVALID_HANDLE_VALUE);
        assert(name);
        handler_ = CreateFileA(name, GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
            fail_if_exists ? CREATE_NEW : CREATE_ALWAYS, 0, NULL);
        return handler_ != INVALID_HANDLE_VALUE;
    }

    bool File::Open(const char* name, bool read_only)
    {
        assert(handler_ == INVALID_HANDLE_VALUE);
        assert(name);
        //  Более подробно по совместимости флагов при создании / открытии
        // https://msdn.microsoft.com/en-us/library/windows/desktop/aa363874(v=vs.85).aspx
        handler_ = CreateFileA(
            name, read_only ? GENERIC_READ : GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
        return handler_ != INVALID_HANDLE_VALUE;
    }

    ssize_t File::Read(void* data, size_t size) const
    {
        assert(handler_ != INVALID_HANDLE_VALUE);
        assert(data);
        DWORD result;
        if (ReadFile(handler_, data, size, &result, NULL)) {
            return result;
        }
        return -1;
    }

    ssize_t File::Write(const void* data, size_t size) const
    {
        assert(handler_ != INVALID_HANDLE_VALUE);
        assert(data);
        assert(size > 0);
        DWORD result;
        if (WriteFile(handler_, data, size, &result, NULL)) {
            return result;
        }
        return -1;
    }

    bool File::Close()
    {
        if (handler_ == INVALID_HANDLE_VALUE) {
            return false;
        }
        CloseHandle(handler_);
        handler_ = INVALID_HANDLE_VALUE;
        return true;
    }
}
}
#endif //#if defined(_WIN32)
