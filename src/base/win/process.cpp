#if defined(_WIN32)
#include "../process.h"
#include <windows.h>

namespace rth {
namespace process {

    // Пустой обработчик. Используется по умолчанию, чтобы не обрабатывать нулевые
    // значения gSignalHandler
    void DefaultHandler(int signum) {}

    static void (*gSignalHandler)(int signum) = DefaultHandler;

    static BOOL WINAPI HandlerRoutine(DWORD dwCtrlType)
    {
        // https://msdn.microsoft.com/en-us/library/windows/desktop/ms683242(v=vs.85).aspx
        // dwCtrlType лежит в диапазоне от 0 до 6. Так что преобразование в int не
        // приведет
        // к потере данных.
        gSignalHandler(static_cast<int>(dwCtrlType));
        return true;
    }

    ProcessHandler CurrentPID() { return GetCurrentProcessId(); }

    bool SetSignalHandler(int signum, void (*handler)(int))
    {
        // В 99.99% обработчики устанавливаются в самом начале программы и не меняются
        // в ходе выполнения
        // из разных потоков. Это допущение позволяет не создавать мьютекс для доступа
        // к gSignalHandler
        gSignalHandler = handler;
        return SetConsoleCtrlHandler(HandlerRoutine, true);
    }
}
}
#endif //#if defined(_WIN32)
