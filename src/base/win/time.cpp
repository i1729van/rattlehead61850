#if defined(_WIN32)
#include "../time.h"
#include <windows.h>

#if !defined(_WIN32_WINNT) || (_WIN32_WINNT < 0x600)
#warning "You have to define _WIN32_WINNT >= 0x600"
#endif

namespace rth {
namespace time {

    uint64_t GetMonoTimeMilli() { return GetTickCount64(); }

    uint64_t GetRealTimeMilli()
    {
        /* разница между 01.01.1601 и 01.01.1970 в 100 нс тиках */
        const uint64_t kEpochDifference = 116444736000000000ULL;

        /* кол-во 100 нс тиков в 1 миллисекунде */
        const uint64_t k100NsTicksInMilli = 10000ULL;

        SYSTEMTIME st;
        FILETIME ft;
        GetSystemTime(&st);
        SystemTimeToFileTime(&st, &ft);

        uint64_t result_millis = ((static_cast<uint64_t>(ft.dwHighDateTime) << 32 | static_cast<uint64_t>(ft.dwLowDateTime)) - kEpochDifference) / k100NsTicksInMilli;

        uint64_t seconds = result_millis / 1000ULL;
        uint64_t millis = result_millis - seconds * 1000ULL;

        return seconds * 1000ULL + millis;
    }
}
}
#endif //#if defined(_WIN32)
