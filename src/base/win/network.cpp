#if defined(_WIN32)
#include "../network.h"
#include <assert.h> // for assert
#include <ctype.h> // for isdigit
#include <stdlib.h> // for atoi

namespace rth {
namespace network {

    namespace {
        class NetworkInitializer {
        public:
            NetworkInitializer()
            {
                WORD wVersionRequested;
                WSADATA wsaData;
                wVersionRequested = MAKEWORD(2, 2);
                WSAStartup(wVersionRequested, &wsaData);
            }
            ~NetworkInitializer() { WSACleanup(); }
        };

        static NetworkInitializer gNetworkInitializer;
    }

    bool IsValidIP(const IpAddress& address)
    {
        return address.ip.s_addr != INADDR_NONE;
    }

    IpAddress MakeIpAddress(const char* address) // << 192.168.1.200:5000
    {
        assert(address);

        struct in_addr ip_addr;
        IpAddress result;
        memset(&result, 0, sizeof(result));
        result.ip.s_addr = INADDR_NONE;

        const int kMaxBufferSize = 128;
        const int kMaxPortLen = 5;

        char ip[kMaxBufferSize + 1] = { 0 };
        int ip_len = 0;

        char port[kMaxBufferSize + 1] = { 0 };
        int port_len = 0;

        const char* ptr = address;

        /* Пропуск мусорных символов*/
        while (*ptr && !isdigit(*ptr++))
            ;
        ptr--;

        /* Чтение первой части xс IP адресом */
        while (ip_len < kMaxBufferSize && *ptr && *ptr != ':' && (isdigit(*ptr) || *ptr == '.')) {
            ip[ip_len++] = *ptr++;
        }
        ptr++;

        /* Чтение номера порта */
        while (port_len < kMaxBufferSize && *ptr && isdigit(*ptr)) {
            port[port_len++] = *ptr++;
        }

        /* Парсим / проверяем */
        ip_addr.s_addr = inet_addr(ip);
        if (ip_addr.s_addr == INADDR_NONE) {
            return result;
        }
        if (port_len == 0 || port_len > kMaxPortLen) {
            return result;
        }

        int parsed_port = atoi(port);
        if (parsed_port < 0 || parsed_port > 65535) {
            return result;
        }

        /* Запоминаем */
        result.ip.s_addr = ip_addr.s_addr;
        result.port = htons(static_cast<uint16_t>(parsed_port));

        return result;
    }

    IpAddress MakeIpAddress(const char* ip,
        long port) // << (192.168.1.200,5000)
    {
        struct in_addr ip_addr;
        IpAddress result;
        memset(&result, 0, sizeof(result));
        result.ip.s_addr = INADDR_NONE;

        if (port < 0 || port > 65535) {
            return result;
        }

        ip_addr.s_addr = inet_addr(ip);
        if (ip_addr.s_addr == INADDR_NONE) {
            return result;
        }

        if (port > 65535) {
            return result;
        }

        result.ip.s_addr = ip_addr.s_addr;
        result.port = htons(static_cast<uint16_t>(port));

        return result;
    }
}
}
#endif //#if defined(_WIN32)
