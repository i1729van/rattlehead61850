#if defined(_WIN32)
#include "../shared-memory.h"
#include "../named-semaphore.h" // for NamedSemaphore
#include <assert.h> // for assert
#include <errno.h> // for errno
#include <fcntl.h> // for O_RDWR, O_CREAT, O_EXCL
#include <limits.h> // for LONG_MAX
#include <sys/stat.h> // for stat, fstat, mode_t

namespace rth {
namespace ipc {

    SharedMemory::SharedMemory()
        : handle_(NULL)
        , semaphore_(NULL)
        , name_("")
        , byte_size_(0)
        , data_(NULL)
        , last_error_(0)
        , owner_(false)
        , info_handle_(NULL)
        , info_data_(NULL)
    {
    }

    SharedMemory::~SharedMemory() { Close(); }

    bool SharedMemory::Create(const char* name, size_t nbytes)
    {
        assert(name);
        assert(nbytes > 0);
        assert(nbytes <= LONG_MAX);

        /* Создать расшаренную область памяти и область инфомрмации для неё */
        std::string info_name("info.");
        info_name += name;

        HANDLE fd = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0,
            nbytes, name);

        HANDLE info_fd = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE,
            0, sizeof(byte_size_), info_name.c_str());

        void* data = NULL;
        void* info_data = NULL;

        NamedSemaphore* sema = NULL;

        if (fd == NULL || info_fd == NULL) {
            goto error;
        }

        data = MapViewOfFile(fd, FILE_MAP_ALL_ACCESS, 0, 0, nbytes);
        info_data = MapViewOfFile(info_fd, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(byte_size_));

        if (data == NULL || info_data == NULL) {
            goto error;
        }

        /* Создать семафор для регулирования доступа к паммяти */
        sema = new NamedSemaphore;
        if (!sema->Create(name, 1))
            goto error;

        /* Заполнить поля класса */
        handle_ = fd;
        name_ = name;
        byte_size_ = nbytes;
        data_ = data;
        info_data_ = info_data;
        semaphore_ = sema;
        owner_ = true;
        last_error_ = 0;

        /* Заполнить нофрмацию об области паямти */
        CopyMemory(info_data_, &byte_size_, sizeof(byte_size_));

        return true;

    error:

        /* Освободить расшаренную память */
        if (data) {
            UnmapViewOfFile(data);
        }
        if (info_data) {
            UnmapViewOfFile(info_data);
        }

        if (fd != NULL) {
            CloseHandle(fd);
        }
        if (info_fd != NULL) {
            CloseHandle(info_fd);
        }

        /* Закрыть семафор */
        if (sema) {
            sema->Close();
            delete sema;
        }

        /* Обнулить поля класса */
        handle_ = NULL;
        name_ = "";
        byte_size_ = 0;
        data_ = NULL;
        info_data_ = NULL;
        owner_ = false;
        last_error_ = ::GetLastError();
        return false;
    }

    bool SharedMemory::Open(const char* name)
    {
        assert(name);

        std::string info_name("info.");
        info_name += name;

        /* Открыть область памяти. */
        HANDLE fd = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, name);
        HANDLE info_fd = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, info_name.c_str());
        void* data = NULL;
        void* info_data = NULL;
        NamedSemaphore* sema = NULL;

        if (fd == NULL || info_fd == NULL) {
            goto error;
        }

        data = MapViewOfFile(fd, FILE_MAP_ALL_ACCESS, 0, 0, 0);
        info_data = MapViewOfFile(info_fd, FILE_MAP_ALL_ACCESS, 0, 0, 0);

        if (data == NULL || info_data == NULL) {
            goto error;
        }

        /* Создать семафор и подцепить его */
        sema = new NamedSemaphore;
        if (!sema->Open(name))
            goto error;

        /* Заполнить поля класса */
        handle_ = fd;
        name_ = name;
        data_ = data;
        info_data_ = info_data;
        semaphore_ = sema;
        owner_ = false;
        last_error_ = 0;
        CopyMemory(&byte_size_, info_data, sizeof(byte_size_));

        return true;

    error:
        /* Закрыть дескриптор */
        if (data) {
            UnmapViewOfFile(data);
        }
        if (info_data) {
            UnmapViewOfFile(info_data);
        }
        if (fd != NULL) {
            CloseHandle(fd);
        }
        if (info_fd != NULL) {
            CloseHandle(info_fd);
        }

        /* Отцепить семафор */
        if (sema) {
            sema->Close();
            delete sema;
        }

        /* Обнулить поля */
        handle_ = NULL;
        name_ = "";
        byte_size_ = 0;
        data_ = NULL;
        info_data_ = NULL;
        owner_ = false;
        last_error_ = ::GetLastError();
        return false;
    }

    void SharedMemory::Close()
    {
        if (data_) {
            UnmapViewOfFile(data_);
        }

        if (info_data_) {
            UnmapViewOfFile(info_data_);
        }

        if (handle_ != NULL) {
            CloseHandle(handle_);
        }

        if (info_handle_ != NULL) {
            CloseHandle(info_handle_);
        }

        if (semaphore_) {
            semaphore_->Close();
            delete semaphore_;
        }

        /* Сбросить все */
        data_ = NULL;
        info_data_ = NULL;
        handle_ = NULL;
        name_ = "";
        semaphore_ = NULL;
        owner_ = false;
        last_error_ = 0;
    }

    void* SharedMemory::Data() { return data_; }

    size_t SharedMemory::Size() const { return byte_size_; }

    bool SharedMemory::Lock()
    {
        assert(semaphore_);
        if (semaphore_->Wait()) {
            return true;
        }
        last_error_ = semaphore_->GetLastError();
        return false;
    }

    bool SharedMemory::TryLock(unsigned int ms)
    {
        assert(semaphore_);

        bool result = ms == 0 ? semaphore_->TryWait() : semaphore_->TimedWait(ms);

        if (result == false) {
            last_error_ = semaphore_->GetLastError();
        }

        return result;
    }

    bool SharedMemory::Unlock()
    {
        assert(semaphore_);
        if (semaphore_->Post()) {
            return true;
        }

        last_error_ = semaphore_->GetLastError();
        return false;
    }

    bool SharedMemory::IsOpen() const { return handle_ != NULL; }

    error::Error SharedMemory::GetLastError() const { return last_error_; }
}
}
#endif //#if defined(_WIN32)
