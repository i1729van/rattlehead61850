#if defined(_WIN32)
#include "../named-semaphore.h"
#include <assert.h> // for assert
#include <errno.h> // for errno, EAGAIN
#include <fcntl.h> // for O_CREAT, O_EXCL, O_RDWR
#include <stddef.h> // for NULL
#include <time.h>

namespace rth {
namespace ipc {

    const int kMaxSemaphoreCount = 0xFFFF;

    NamedSemaphore::NamedSemaphore()
        : semaphore_(NULL)
        , name_("")
        , owner_(false)
        , last_error_(0)
    {
    }

    NamedSemaphore::~NamedSemaphore() { Close(); }

    bool NamedSemaphore::Create(const char* name, unsigned int value)
    {
        assert(name);
        assert(semaphore_ == NULL);

        /* Добавить префикс для семафора. Это избавит от проблем в тех случаях,
   * когда в системе уже есть такие же имена, но для других типов примитивов
   * (например, разделяемая память с таким же именем)
  */
        std::string full_name("sem.");
        full_name += name;

        semaphore_ = CreateSemaphoreA(NULL, value, kMaxSemaphoreCount, full_name.c_str());
        if (semaphore_ != NULL && ::GetLastError() != ERROR_ALREADY_EXISTS) {
            name_ = name;
            owner_ = true;
            last_error_ = 0;
            return true;
        }

        last_error_ = ::GetLastError();
        return false;
    }

    bool NamedSemaphore::Open(const char* name)
    {
        assert(name);
        assert(semaphore_ == NULL);

        /* Добавить префикс для семафора. Это избавит от проблем в тех случаях,
  * когда в системе уже есть такие же имена, но для других типов примитивов
  * (например, разделяемая память с таким же именем)
  */
        std::string full_name("sem.");
        full_name += name;
        semaphore_ = OpenSemaphoreA(SYNCHRONIZE | SEMAPHORE_MODIFY_STATE, false,
            full_name.c_str());

        if (semaphore_ != NULL) {
            name_ = name;
            owner_ = false;
            last_error_ = 0;
            return true;
        }

        last_error_ = ::GetLastError();
        return false;
    }

    void NamedSemaphore::Close()
    {
        /* Отсецить семафор ои имени */
        if (semaphore_ != NULL) {
            CloseHandle(semaphore_);
        }

        /* Обнулить */
        semaphore_ = NULL;
        name_ = "";
        owner_ = false;
        last_error_ = 0;
    }

    bool NamedSemaphore::Post() const
    {
        assert(semaphore_);
        if (ReleaseSemaphore(semaphore_, 1, NULL) != 0) {
            return true;
        }

        last_error_ = ::GetLastError();
        return false;
    }

    bool NamedSemaphore::Wait() const
    {
        assert(semaphore_);
        if (WaitForSingleObject(semaphore_, INFINITE) == WAIT_OBJECT_0) {
            return true;
        }

        last_error_ = ::GetLastError();
        return false;
    }

    bool NamedSemaphore::TryWait() const
    {
        assert(semaphore_);
        if (WaitForSingleObject(semaphore_, 0L) == WAIT_OBJECT_0) {
            return true;
        }
        last_error_ = error::kTryAgain;
        return false;
    }

    bool NamedSemaphore::TimedWait(unsigned int ms) const
    {
        assert(semaphore_);
        if (WaitForSingleObject(semaphore_, ms) == WAIT_OBJECT_0) {
            return true;
        }
        last_error_ = error::kTimeout;
        return false;
    }

    int NamedSemaphore::Value() const
    {
        assert("Not supported in Windows" == 0);
        return -1;
    }

    error::Error NamedSemaphore::GetLastError() const { return last_error_; }
}
}
#endif //#if defined(_WIN32)
