#if defined(_WIN32)
#include "../udp-socket.h"
#include <assert.h> // for assert
#include <fcntl.h> // for fcntl, F_GETFL, F_SETFL, O_NONBLOCK
#include <string>

namespace rth {
namespace network {

    UdpSocket::UdpSocket()
        : handle_(kInvalidSocketHandler)
    {
    }

    UdpSocket::~UdpSocket() { handle_ = kInvalidSocketHandler; }

    bool UdpSocket::Create()
    {
        assert(handle_ == kInvalidSocketHandler);
        handle_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        return handle_ != kInvalidSocketHandler;
    }

    bool UdpSocket::Listen(const IpAddress& address)
    {
        assert(handle_ != kInvalidSocketHandler);

        struct sockaddr_in host;
        int len = sizeof(host);
        memset(&host, 0, sizeof(host));
        host.sin_family = AF_INET;
        host.sin_addr.s_addr = address.ip.s_addr;
        host.sin_port = address.port;

        return bind(handle_, reinterpret_cast<struct sockaddr*>(&host), len) == 0;
    }

    ssize_t UdpSocket::RecvFrom(void* data, size_t size, IpAddress* from) const
    {
        assert(handle_ != kInvalidSocketHandler);
        assert(data);
        assert(size > 0);

        struct sockaddr_in host;
        int len = sizeof(host);
        ssize_t result = recvfrom(handle_, static_cast<char*>(data), size, 0,
            reinterpret_cast<struct sockaddr*>(&host), &len);

        if (from) {
            from->ip.s_addr = host.sin_addr.s_addr;
            from->port = host.sin_port;
        }
        return result;
    }

    ssize_t UdpSocket::SendTo(const void* data, size_t size,
        const IpAddress& to) const
    {
        assert(handle_ != kInvalidSocketHandler);
        assert(data);

        struct sockaddr_in host;
        int len = sizeof(host);

        memset(&host, 0, sizeof(host));
        host.sin_family = AF_INET;
        host.sin_addr.s_addr = to.ip.s_addr;
        host.sin_port = to.port;

        ssize_t result = sendto(handle_, static_cast<const char*>(data), size, 0,
            reinterpret_cast<struct sockaddr*>(&host), len);
        return result;
    }

    bool UdpSocket::Close()
    {
        if (handle_ == kInvalidSocketHandler)
            return false;

        closesocket(handle_);
        handle_ = kInvalidSocketHandler;
        return true;
    }

    bool UdpSocket::SetProperty(const char* property, const char* value)
    {

        assert(property);
        assert(value);
        std::string cmd(property);
        std::string val(value);

        if (cmd == "block") {
            u_long non_block = val == "true" ? 0 : 1;
            return ioctlsocket(handle_, FIONBIO, &non_block) == NO_ERROR;
        }

        return false;
    }
}
}
#endif //#if defined(_WIN32)
