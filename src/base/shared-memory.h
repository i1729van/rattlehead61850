#pragma once
#if defined(__linux__)
#include <stddef.h> // for size_t
#include <string> // for string
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include <stddef.h> // for size_t
#include <string> // for string
#include <windows.h>
#endif //#if defined(_WIN32)

#include "error-types.h"

namespace rth {
namespace ipc {

#if defined(__linux__)
    typedef int ShmHandler;
#endif //#if defined(__linux__)

#if defined(_WIN32)
    typedef HANDLE ShmHandler;
#endif //#if defined(_WIN32)

    class NamedSemaphore;
    class SharedMemory {
    public:
        SharedMemory();
        ~SharedMemory();

        /**
   * @brief Создать новую область расшаренной памяти
   * @param name - имя области
   * @param nbytes - размер в байтах
   * @return Возвращает 1 в случае успеха
   */
        bool Create(const char* name, size_t nbytes);

        /**
   * @brief Уничтожить область памяти.
   * Закрывает дескрипторы + вызывает unlink, что делает невозможным
   * новые подключения к области. Хотя, старые есил они были, продолжат
   * существовать
   */
        void Close();

        /**
   * @brief Подключиться к области памяти
   * @param name - имя области
   * @return 1 в случае успеха
   */
        bool Open(const char* name);

        /**
   * @brief  Отключиться от области памяти.
   * Закрывает дескрипторы, но не запрещает новые подключения к памяти.
   */
        // void Detach();

        void* Data();
        size_t Size() const;

        bool Lock();
        bool TryLock(unsigned int ms);
        bool Unlock();

        bool IsOpen() const;
        error::Error GetLastError() const;

    private:
        SharedMemory(const SharedMemory&);
        SharedMemory& operator=(const SharedMemory&);

    private:
        ShmHandler handle_;

        NamedSemaphore* semaphore_;
        std::string name_;
        size_t byte_size_;
        void* data_;

        mutable error::Error last_error_;
        bool owner_;

#if defined(_WIN32)
        ShmHandler info_handle_;
        void* info_data_;
#endif

        /*TODO: Возможно, будет полезно сохранять флаг занятости / PID занявшего
   * процесса.
   * Подумать на досуге */
    };
}
}
/*
private:
  HANDLE handle_;
  HANDLE info_handle_;
  NamedSemaphore *semaphore_;
  std::string name_;
  size_t byte_size_;
  void *data_;
  void *info_data_;
  int last_error_;
  bool owner_;
*/
