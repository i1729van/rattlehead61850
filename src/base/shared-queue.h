#pragma once
#include <assert.h>
#include <errno.h>

#include "error-types.h"
#include "shared-memory.h"

namespace rth {
namespace ipc {

    /**
 * @class SharedQueue
 * @brief Очередь в разделяемой памяти. Предназначена для реализации обмена
 * между различными процессами с автоматической блокировкой при
 * добавлении / извлечении элементов */
    template <typename T>
    class SharedQueue {
    public:
        SharedQueue()
            : queue_(NULL)
            , lock_timeout_ms_(1000)
            , last_error_(0)
        {
        }

        ~SharedQueue() { Close(); }

        /**
   * @brief Создать очередь
   * @param name - имя очереди
   * @param nelement - макс. кол-во элементов
   * @return true в случае успеха
   */
        bool Create(const char* name, size_t nelement)
        {
            assert(name);
            assert(nelement > 0);
            assert(queue_ == NULL);

            size_t memory_chunk_size = sizeof(*queue_) + sizeof(T) * nelement;

            if (memory_.Create(name, memory_chunk_size)) {
                char* data = static_cast<char*>(memory_.Data());
                queue_ = reinterpret_cast<SharedQueueData*>(data);
                assert(queue_);

                queue_->max = nelement;
                queue_->size = 0;
                queue_->pos = 0;
                queue_->element_size = sizeof(T);
                // queue_->busy = false;
                last_error_ = 0;
                return true;
            }

            last_error_ = memory_.GetLastError();
            return false;
        }

        /**
   * @brief Открыть существующую очередь
   * @param name - имя
   * @return true в случае успеха
   */
        bool Open(const char* name)
        {
            assert(name);
            assert(queue_ == NULL);

            if (!memory_.Open(name)) {
                last_error_ = memory_.GetLastError();
                return false;
            }

            if (memory_.Size() < sizeof(*queue_)) {
                last_error_ = memory_.GetLastError();
                memory_.Close();
                return false;
            }

            char* data = static_cast<char*>(memory_.Data());
            queue_ = reinterpret_cast<SharedQueueData*>(data);

            assert(queue_);

            /* Размеры ожидаемых/фактических элементов не совпадают */
            assert(queue_->element_size == sizeof(T));
            last_error_ = 0;
            return true;
        }

        /**
   * @brief Вставить в очередь N элементов [с автоблокировкой]
   * @param src - указатель на массив элементов
   * @param nelem - кол-во элементов
   * @return Кол-во фактически записанных элементов
   */
        size_t Push(const T* src, size_t nelem = 1)
        {
            assert(src);
            assert(queue_);

            size_t cnt = 0;

            if (memory_.TryLock(lock_timeout_ms_)) {
                // assert busy + usleep могут сильно помочь, если есть подозрения на косу.
                // синхронизацию
                // assert(queue_->busy == false);
                // queue_->busy = true;
                while (nelem > 0 && queue_->size < queue_->max) {
                    queue_->elements[(queue_->pos + queue_->size) % queue_->max] = *src;
                    queue_->size++;
                    cnt++;
                    nelem--;
                    src++;
                    // usleep(1000*1000);
                }
                // queue_->busy = false;
                memory_.Unlock();
            } else {
                last_error_ = memory_.GetLastError();
            }

            return cnt;
        }

        /**
   * @brief Извлечь N элементов  [с автоблокировкой]
   * @param dst - указатель на буфер - приемнк
   * @param nelem - кол-во элементов
   * @return Кол-во фактически прочитанных элементов
   */
        size_t Pop(T* dst, size_t nelem = 1)
        {
            assert(dst);
            assert(queue_);

            size_t cnt = 0;

            if (memory_.TryLock(lock_timeout_ms_)) {
                // assert busy + usleep могут сильно помочь, если есть подозрения на косу.
                // синхронизацию
                // assert(queue_->busy == false);
                // queue_->busy = true;
                while (nelem > 0 && queue_->size > 0) {
                    // std::cout<< "PopElement\n";
                    *dst = queue_->elements[queue_->pos];
                    queue_->pos = (queue_->pos + 1) % queue_->max;
                    queue_->size--;
                    cnt++;
                    nelem--;
                    dst++;
                    //  usleep(100000);
                }
                // queue_->busy = false;
                memory_.Unlock();
            } else {
                last_error_ = memory_.GetLastError();
            }

            return cnt;
        }

        /* Очистка */
        bool Clear()
        {
            if (queue_ && memory_.TryLock(lock_timeout_ms_)) {
                queue_->pos = 0;
                queue_->size = 0;
                memory_.Unlock();
                return true;
            }

            return false;
        }

        /**
   * @brief Закрыть очередь
   */
        void Close()
        {
            queue_ = NULL;
            memory_.Close();
            last_error_ = 0;
        }

        /**
   * @brief очередь открыта?
   * @return true если да
   */
        bool IsOpen() const { return queue_ != NULL; }

        /**
   * @brief Размер очереди
   * @return Размер в элементах если очередь существует и к ней получен доступ.
   * Иначе 0.
   */
        size_t Size()
        {
            size_t result = 0;
            if (queue_ && memory_.TryLock(lock_timeout_ms_)) {
                result = queue_->size;
                memory_.Unlock();
            }
            return result;
        }

        /**
   * @brief Макс. размер очереди
   * @return Макс. размер в элементах если очередь существует и к ней получен
   * доступ. Иначе 0.
   */
        size_t MaxSize()
        {
            size_t result = 0;
            if (queue_ && memory_.TryLock(lock_timeout_ms_)) {
                result = queue_->max;
                memory_.Unlock();
            }
            return result;
        }

        /**
   * @brief Настроить таймауты для блокировки
   * @param ms - таймаут в мс
   */
        void SetLockTimeout(unsigned int ms) { lock_timeout_ms_ = ms; }

        /**
   * @brief Получить код последней ошибки
   * @return Код ошибки
   */
        error::Error GetLastError() const { return last_error_; }

    private:
        SharedQueue(const SharedQueue&);
        SharedQueue& operator=(const SharedQueue&);

    private:
        /* Структура для хранения данных очереди */
        typedef struct {
            size_t max; /* Макс. кол-во элементов */
            size_t element_size;

            size_t size; /* Текущий размер */
            size_t pos; /* Текущая позиция чтения */
            // bool busy;
            T elements[]; /* Указатель на массив элементов */
        } SharedQueueData;

        SharedMemory memory_;
        SharedQueueData* queue_;

        unsigned int lock_timeout_ms_;
        mutable error::Error last_error_;
    };
}
}
