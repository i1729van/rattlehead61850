#pragma once
#if defined(__linux__)
#include <netinet/in.h> // for in_addr_t
#include <stdint.h> // for uint16_t
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include <stdint.h> // for uint16_t
#include <windows.h>
#include <winsock.h>
#endif //#if defined(_WIN32)

namespace rth {
namespace network {

#if defined(__linux__)
    typedef int SocketHandle;
    typedef in_addr_t PackedIP;
    const int kInvalidSocketHandler = -1;
#endif //#if defined(__linux__)

#if defined(_WIN32)
    typedef SOCKET SocketHandle;
    typedef IN_ADDR PackedIP;
    const SOCKET kInvalidSocketHandler = INVALID_SOCKET;
#endif //#if defined(_WIN32)

    typedef struct {
        PackedIP ip;
        uint16_t port;
    } IpAddress;

    bool IsValidIP(const IpAddress& address);
    IpAddress MakeIpAddress(const char* address); // << 192.168.1.200:5000
    IpAddress MakeIpAddress(const char* ip, long port); // << (192.168.1.200,5000)
}
}
