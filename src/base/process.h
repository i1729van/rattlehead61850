#pragma once
#if defined(__linux__)
#include <unistd.h>
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include <windows.h>
#endif //#if defined(_WIN32)

namespace rth {
namespace process {

#if defined(__linux__)
    typedef pid_t ProcessHandler;
    const pid_t kInvalidProcessHandler = -1;
#endif //#if defined(__linux__)

#if defined(_WIN32)
    typedef DWORD ProcessHandler;
    const DWORD kInvalidProcessHandler = 0;
#endif //#if defined(_WIN32)

    ProcessHandler Create(const char* command);
    ProcessHandler CurrentPID();
    bool TryWait(ProcessHandler handler);
    int Kill(ProcessHandler handler);

    int SendSignal(ProcessHandler handler, int signum);
    bool SetSignalHandler(int signum, void (*handler)(int));
    bool Daemonize(bool nochdir = 0, bool noclose = 0);
}
}
