#pragma once
#if defined(__linux__)
#include <pthread.h> // for pthread_join, pthread_create, pthread_mutex_t
#include <stddef.h> // for NULL
typedef void ThreadResult;
#define THREAD_CALLTYPE
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include <stddef.h> // for NULL
#include <windows.h>
typedef DWORD ThreadResult;
#define THREAD_CALLTYPE WINAPI
#endif //#if defined(_WIN32)

namespace rth {
namespace thread {

#if defined(__linux__)
    typedef pthread_mutex_t SysMutex;
    typedef void (*ThreadFunc)(void*);
    typedef pthread_t SysThread;
#endif //#if defined(__linux__)

#if defined(_WIN32)
    typedef CRITICAL_SECTION SysMutex;
    typedef void (*ThreadFunc)(void*);
    typedef HANDLE SysThread;
#endif //#if defined(_WIN32)

    /* Системно зависимые типы */
    /**
 * @class Mutex
 * @brief Мьютекс.Больше нечего сказать
 */
    class Mutex {
    public:
        Mutex();
        ~Mutex();
        bool Lock();
        bool TryLock();
        void Unlock();

    private:
        /* копировать мьютекс - плохая идея*/
        Mutex(const Mutex&);
        Mutex& operator=(const Mutex&);

    private:
        SysMutex mutex_;
    };

    /**
 * @class AutoLock
 * @brief Блокировка, которая захватывает объект при создании и
 * особождает объект при разрушении
 */
    template <typename LockObject>
    class AutoLock {
    public:
        explicit AutoLock(LockObject& lock)
            : lock_(lock)
        {
            lock_.Lock();
        }
        ~AutoLock() { lock_.Unlock(); }

    private:
        AutoLock(const AutoLock& lock);
        AutoLock& operator=(const AutoLock& lock);

    private:
        LockObject& lock_;
    };

    typedef AutoLock<Mutex> MutexAutoLock;

    /* Обертка, создающая из функций (функций и данных) вызываемый объект.
 * Все Callable имеют operator(), который используюется для вызова.*/
    template <typename DataType>
    class Callable {
    public:
        typedef void (*FuncType)(DataType);
        Callable()
            : func_(NULL)
            , data_(NULL)
        {
        }
        explicit Callable(FuncType func)
            : func_(func)
            , data_(NULL)
        {
        }
        Callable(FuncType func, DataType data)
            : func_(func)
            , data_(data)
        {
        }
        void operator()() { func_(data_); }

    private:
        FuncType func_;
        DataType data_;
    };

    /* Специлазиция для функций без аргументов */
    template <>
    class Callable<void> {
    public:
        typedef void (*FuncType)(void);
        Callable()
            : func_(NULL)
        {
        }
        explicit Callable(FuncType func)
            : func_(func)
        {
        }
        void operator()() { func_(); }

    private:
        FuncType func_;
    };

    /**
 * @class Thread
 * @brief Создает отдельный поток и вызывает в нем Callable-объект (есть
 * operator ())
 */
    class Thread {
    public:
        /*
  * Создать поток
  */
        template <typename C>
        explicit Thread(C& callable)
        {
#if defined(__linux__)
            void (*ptr)(void* args) = ThreadCaller<C>;
            pthread_create(&thread_, NULL, reinterpret_cast<void* (*)(void*)>(ptr),
                &callable);
#endif // #if defined(__linux__)

#if defined(_WIN32)
            DWORD(WINAPI * ptr)
            (void* args) = Thread::ThreadCaller<C>;
            thread_ = CreateThread(NULL, 0, reinterpret_cast<DWORD(WINAPI*)(void*)>(ptr),
                &callable, 0, NULL);
#endif //#if defined(_WIN32)
        }

        /*
  * Вызов callable-объекта
  */
        template <typename C>
        static ThreadResult THREAD_CALLTYPE ThreadCaller(void* args)
        {
            C* callable = static_cast<C*>(args);
            (*callable)();

#if defined(_WIN32)
            return 0;
#endif
        }

        /*
   * Подожлать завершения потока
   */
        bool Join()
        {
#if defined(__linux__)
            return pthread_join(thread_, NULL) == 0;
#endif // #if defined(__linux__)

#if defined(_WIN32)
            if (thread_ == INVALID_HANDLE_VALUE) {
                return false;
            }

            WaitForSingleObject(thread_, INFINITE);
            thread_ = INVALID_HANDLE_VALUE;
            return true;
#endif //#if defined(_WIN32)
        }

    private:
        /* поток не копируемый */
        Thread(const Thread&);
        Thread& operator=(const Thread&);

    private:
        SysThread thread_;
    };

    void Microsleep(unsigned int microsec);
    void Millisleep(unsigned int millisec);
}
}
