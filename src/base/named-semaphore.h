#pragma once
#if defined(__linux__)
#include <semaphore.h>
#include <string>
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include <iostream>
#include <windows.h>
#endif //#if defined(_WIN32)

#include "error-types.h"

namespace rth {
namespace ipc {

#if defined(__linux__)
    typedef sem_t* SemaphoreHandler;
#endif //#if defined(__linux__)

#if defined(_WIN32)
    typedef HANDLE SemaphoreHandler;
#endif //#if defined(_WIN32)

    /**
 * @class NamedSemaphore
 * @date 03/06/17
 * @brief Именованный семафор для работы с общими данными
 */
    class NamedSemaphore {
    public:
        NamedSemaphore();
        ~NamedSemaphore();

        /**
   * @brief Создать семафор
   * @param name - имя
   * @param value - начальное значение
   * @return true - в случае успеха
   */
        bool Create(const char* name, unsigned int value);

        /**
   * @brief Отрыть существующий семафор
   * @param name - имя
   * @return true в случае успеха
   */
        bool Open(const char* name);

        /**
   * @brief Закрыть семафор
   */
        void Close();

        /* Базовые операции с семафором. */
        bool Post() const;
        bool Wait() const;
        bool TryWait() const;
        bool TimedWait(unsigned int ms) const;

        /* Полуичть текущее значение семафора */
        int Value() const;
        error::Error GetLastError() const;

    private:
        NamedSemaphore(const NamedSemaphore&);
        NamedSemaphore& operator=(const NamedSemaphore&);

    private:
        SemaphoreHandler semaphore_;
        std::string name_;
        bool owner_;
        mutable error::Error last_error_;
    };
}
}
