#pragma once
#if __cplusplus >= 201103
#include <unordered_map>
#define HashTable std::unordered_map
#else
#include <map>
#define HashTable std::map
#endif
