#if defined(__linux__)
#include "../process.h"
#include <assert.h> // for assert
#include <signal.h> // for kill, SIGKILL
#include <stddef.h> // for NULL
#include <sys/wait.h> // for waitpid, WNOHANG

namespace rth {
namespace process {

    ProcessHandler Create(const char* command)
    {
        assert(command);
        const char* kShell = "bash";
        pid_t child = fork();

        if (child > 0) {
            /* Это - процесс родитель. Сохраняем PID потомка для дальнейших операций.*/
            return child;
        } else if (child == 0) {
            execlp(kShell, kShell, "-c", command, NULL);

            // Excelp подменяет форкнутый процесс. Если все хорошо, то сюда мы уже не
            // попадем,
            // т.к. наш код будет подменен кодом execut'ной программы.
            return 0;
        }

        /* Это - ошибка. Не получилось создать потомка*/
        return kInvalidProcessHandler;
    }

    ProcessHandler CurrentPID() { return getpid(); }

    bool TryWait(ProcessHandler handler)
    {
        int status = 0;
        return waitpid(handler, &status, WNOHANG) > 0;
    }

    int Kill(ProcessHandler handler) { return kill(handler, SIGKILL); }
    int SendSignal(ProcessHandler handler, int signum)
    {
        return kill(handler, signum);
    }

    bool SetSignalHandler(int signum, void (*handler)(int))
    {
        return signal(signum, handler) != SIG_ERR;
    }

    bool Daemonize(bool nochdir, bool noclose)
    {
        return daemon(nochdir, noclose) == 0;
    }
}
}
#endif //#if defined(__linux__)
