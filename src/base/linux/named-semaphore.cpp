#if defined(__linux__)
#include "../named-semaphore.h"
#include <assert.h> // for assert
#include <errno.h> // for errno, EAGAIN
#include <fcntl.h> // for O_CREAT, O_EXCL, O_RDWR
#include <semaphore.h> // for sem_open, SEM_FAILED, sem_close
#include <stddef.h> // for NULL
#include <sys/time.h> // for CLOCK_REALTIME
#include <time.h>

namespace rth {
namespace ipc {

    NamedSemaphore::NamedSemaphore()
        : semaphore_(NULL)
        , name_("")
        , owner_(false)
        , last_error_(0)
    {
    }

    NamedSemaphore::~NamedSemaphore() { Close(); }

    bool NamedSemaphore::Create(const char* name, unsigned int value)
    {
        assert(name);
        assert(semaphore_ == NULL);

        semaphore_ = sem_open(name, O_EXCL | O_CREAT, 0644, value);
        if (semaphore_ != SEM_FAILED) {
            name_ = name;
            owner_ = true;
            last_error_ = 0;
            return true;
        }

        last_error_ = errno;
        return false;
    }

    bool NamedSemaphore::Open(const char* name)
    {
        assert(name);
        assert(semaphore_ == NULL);

        semaphore_ = sem_open(name, O_RDWR);

        if (semaphore_ != SEM_FAILED) {
            name_ = name;
            owner_ = false;
            last_error_ = 0;
            return true;
        }

        last_error_ = errno;
        return false;
    }

    void NamedSemaphore::Close()
    {
        /* Отсецить семафор ои имени */
        if (semaphore_) {
            if (owner_) {
                sem_unlink(name_.c_str());
            }
            sem_close(semaphore_);
        }

        /* Обнулить */
        semaphore_ = NULL;
        name_ = "";
        owner_ = false;
        last_error_ = 0;
    }

    bool NamedSemaphore::Post() const
    {
        assert(semaphore_);
        if (sem_post(semaphore_) == 0) {
            return true;
        }

        last_error_ = errno;
        return false;
    }

    bool NamedSemaphore::Wait() const
    {
        assert(semaphore_);
        if (sem_wait(semaphore_) == 0) {
            return true;
        }

        last_error_ = errno;
        return false;
    }

    bool NamedSemaphore::TryWait() const
    {
        assert(semaphore_);

        if (sem_trywait(semaphore_) == 0) {
            return true;
        }
        last_error_ = errno;
        return false;
    }

    bool NamedSemaphore::TimedWait(unsigned int ms) const
    {
        assert(semaphore_);
        struct timespec ts;

        if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
            return false;
        unsigned int sec = ms / 1000;
        long millisec = ms - sec * 1000;

        ts.tv_sec += sec;
        ts.tv_nsec += millisec * 1000L * 1000L;

        // 1sec = 1000 milli = 1000000 micro = 1000000000 nano
        if (ts.tv_nsec > 1000000000L) {
            ts.tv_sec++;
            ts.tv_nsec -= 1000000000L;
        }

        // return sem_timedwait(semaphore_, &ts) == 0 ? (errno == EAGAIN ? 0 : 1) : 0;
        if (sem_timedwait(semaphore_, &ts) == 0 && errno != EAGAIN) {
            return true;
        }

        last_error_ = errno;
        return false;
    }

    int NamedSemaphore::Value() const
    {
        assert(semaphore_);
        int result;
        if (sem_getvalue(semaphore_, &result) == 0) {
            return result;
        }

        last_error_ = errno;
        return 0;
    }

    error::Error NamedSemaphore::GetLastError() const { return last_error_; }
}
}
#endif //#if defined(__linux__)
