#if defined(__linux__)
#include "../udp-socket.h"
#include <assert.h> // for assert
#include <fcntl.h> // for fcntl, F_GETFL, F_SETFL, O_NONBLOCK
#include <netinet/in.h> // for sockaddr_in, in_addr, IPPROTO_UDP
#include <string.h> // for memset, strncasecmp, size_t
#include <sys/socket.h> // for socklen_t, AF_INET, MSG_NOSIGNAL, bind, recv...
#include <unistd.h> // for ssize_t, close

namespace rth {
namespace network {

    namespace {

        bool UdpSetFlag(int sock, int flag)
        {
            int param = fcntl(sock, F_GETFL, 0);
            param |= flag;
            return fcntl(sock, F_SETFL, param) == 0;
        }

        bool UdpResetFlag(int sock, int flag)
        {
            int param = fcntl(sock, F_GETFL, 0);
            param &= ~flag;
            return fcntl(sock, F_SETFL, param) == 0;
        }
    }

    UdpSocket::UdpSocket()
        : handle_(kInvalidSocketHandler)
    {
    }

    UdpSocket::~UdpSocket() { handle_ = kInvalidSocketHandler; }

    bool UdpSocket::Create()
    {
        assert(handle_ == kInvalidSocketHandler);
        handle_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        return handle_ != kInvalidSocketHandler;
    }

    bool UdpSocket::Listen(const IpAddress& address)
    {
        assert(handle_ != kInvalidSocketHandler);

        struct sockaddr_in host;
        socklen_t len = sizeof(host);
        memset(&host, 0, sizeof(host));
        host.sin_family = AF_INET;
        host.sin_addr.s_addr = address.ip;
        host.sin_port = address.port;

        return bind(handle_, reinterpret_cast<struct sockaddr*>(&host), len) == 0;
    }

    ssize_t UdpSocket::RecvFrom(void* data, size_t size, IpAddress* from) const
    {
        assert(handle_ != kInvalidSocketHandler);
        assert(data);
        assert(size > 0);

        struct sockaddr_in host;
        socklen_t len = sizeof(host);
        ssize_t result = recvfrom(handle_, data, size, MSG_NOSIGNAL,
            reinterpret_cast<struct sockaddr*>(&host), &len);

        if (from) {
            from->ip = host.sin_addr.s_addr;
            from->port = host.sin_port;
        }
        return result;
    }

    ssize_t UdpSocket::SendTo(const void* data, size_t size,
        const IpAddress& to) const
    {
        assert(handle_ != kInvalidSocketHandler);
        assert(data);

        struct sockaddr_in host;
        socklen_t len = sizeof(host);

        memset(&host, 0, sizeof(host));
        host.sin_addr.s_addr = to.ip;
        host.sin_port = to.port;

        ssize_t result = sendto(handle_, data, size, MSG_NOSIGNAL,
            reinterpret_cast<struct sockaddr*>(&host), len);

        return result;
    }

    bool UdpSocket::Close()
    {
        if (handle_ == kInvalidSocketHandler)
            return false;

        shutdown(handle_, SHUT_RDWR);
        close(handle_);
        handle_ = kInvalidSocketHandler;
        return true;
    }

    bool UdpSocket::SetProperty(const char* property, const char* value)
    {
        const int kMaxPropLen = 64;

        assert(property);
        assert(value);

        if (strncasecmp(property, "block", kMaxPropLen) == 0) {
            if (strncasecmp(value, "true", kMaxPropLen) == 0) {
                return UdpResetFlag(handle_, O_NONBLOCK);
            } else {
                return UdpSetFlag(handle_, O_NONBLOCK);
            }
        }

        return false;
    }
}
}
#endif //#if defined(__linux__)
