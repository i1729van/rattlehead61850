#if defined(__linux__)
#include "../time.h"
#include <sys/time.h> // for CLOCK_MONOTONIC, CLOCK_REALTIME
#include <time.h> // for clock_gettime

namespace rth {
namespace time {

    uint64_t GetMonoTimeMilli()
    {
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        return static_cast<uint64_t>(ts.tv_sec) * 1000UL + static_cast<uint64_t>(ts.tv_nsec) / 1000000UL;
    }

    uint64_t GetRealTimeMilli()
    {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        return static_cast<uint64_t>(ts.tv_sec) * 1000UL + static_cast<uint64_t>(ts.tv_nsec) / 1000000UL;
    }
}
}
#endif //#if defined(__linux__)
