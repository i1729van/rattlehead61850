#if defined(__linux__)
#include "../shared-memory.h"
#include "../named-semaphore.h" // for NamedSemaphore
#include <assert.h> // for assert
#include <errno.h> // for errno
#include <fcntl.h> // for O_RDWR, O_CREAT, O_EXCL
#include <limits.h> // for LONG_MAX
#include <pthread.h> // for pthread_mutex_lock, pthread_mutex_unlock
#include <sys/mman.h> // for mmap, shm_unlink, MAP_FAILED, MAP_SHARED
#include <sys/stat.h> // for stat, fstat, mode_t
#include <unistd.h> // for close, ftruncate, off_t

namespace rth {
namespace ipc {

    const int kInvalidSharedMemory = -1;

    /* Реализации pthread могут отличаться [MT-Safe, AC-Safe, Single]. Для некоторых
 * thread san будет показывать data race в shm_open.
 * Избежать этого можно доп. блокировкой */
    static int shm_open_mt(const char* name, int oflag, mode_t mode)
    {
        static pthread_mutex_t shm_open_mutex = PTHREAD_MUTEX_INITIALIZER;
        pthread_mutex_lock(&shm_open_mutex);
        int fd = shm_open(name, oflag, mode);
        pthread_mutex_unlock(&shm_open_mutex);
        return fd;
    }

    SharedMemory::SharedMemory()
        : handle_(kInvalidSharedMemory)
        , semaphore_(NULL)
        , name_("")
        , byte_size_(0)
        , data_(NULL)
        , last_error_(0)
        , owner_(false)
    {
    }

    SharedMemory::~SharedMemory() { Close(); }

    bool SharedMemory::Create(const char* name, size_t nbytes)
    {
        assert(name);
        assert(nbytes > 0);
        assert(nbytes <= LONG_MAX);

        /* Инициализировать область памяти.
   * Состоит из создания расшаренной памяти, задания размера и переноса
   * в адресное пространстов процесса
   * 0644 - rw - для пользователя, r - для группы, r - для всех остальных */
        int fd = shm_open_mt(name, O_EXCL | O_CREAT | O_RDWR, 0644);

        void* data = NULL;
        NamedSemaphore* sema = NULL;

        if (fd == kInvalidSharedMemory) {
            goto error;
        }

        // Нереально большие nbytes отсекли на assert
        if (ftruncate(fd, static_cast<off_t>(nbytes)) != 0) {
            goto error;
        }

        data = mmap(0, nbytes, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (data == MAP_FAILED) {
            goto error;
        }

        /* Создать семафор для регулирования доступа к паммяти */
        sema = new NamedSemaphore;
        if (!sema->Create(name, 1))
            goto error;

        /* Заполнить поля класса */
        handle_ = fd;
        name_ = name;
        byte_size_ = nbytes;
        data_ = data;
        semaphore_ = sema;
        owner_ = true;
        last_error_ = 0;
        return true;

    error:

        /* Освободить расшаренную память */
        if (fd != kInvalidSharedMemory) {
            close(fd);
        }
        shm_unlink(name);

        /* Закрыть семафор */
        if (sema) {
            sema->Close();
            delete sema;
        }

        /* Обнулить поля класса */
        handle_ = kInvalidSharedMemory;
        name_ = "";
        byte_size_ = 0;
        data_ = NULL;
        owner_ = false;
        last_error_ = errno;
        return false;
    }

    bool SharedMemory::Open(const char* name)
    {
        assert(name);

        /* Открыть область памяти.
   * Мы не выполняем никаких операций с ней. Можно не делать блокировки
   * 0644 - rw - для пользователя, r - для группы, r - для всех остальных */
        struct stat buf;
        int fd = shm_open_mt(name, O_RDWR, 0644);
        void* data = NULL;
        NamedSemaphore* sema = NULL;
        size_t size = 0;

        if (fd == kInvalidSharedMemory) {
            goto error;
        }

        if (fstat(fd, &buf) != 0) {
            goto error;
        }

        if (buf.st_size <= 0) {
            goto error;
        }

        size = static_cast<size_t>(buf.st_size);
        data = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (data == MAP_FAILED) {
            goto error;
        }

        /* Создать семафор и подцепить его */
        sema = new NamedSemaphore;
        if (!sema->Open(name))
            goto error;

        /* Заполнить поля класса */
        handle_ = fd;
        name_ = name;
        byte_size_ = size;
        data_ = data;
        semaphore_ = sema;
        owner_ = false;
        last_error_ = 0;
        return true;

    error:
        /* Закрыть дескриптор */
        if (fd != kInvalidSharedMemory) {
            close(fd);
        }

        /* Отцепить семафор */
        if (sema) {
            sema->Close();
            delete sema;
        }

        /* Обнулить поля */
        handle_ = kInvalidSharedMemory;
        name_ = "";
        byte_size_ = 0;
        data_ = NULL;
        owner_ = false;
        last_error_ = errno;
        return false;
    }

    void SharedMemory::Close()
    {
        if (owner_) {
            shm_unlink(name_.c_str());
        }

        if (semaphore_) {
            semaphore_->Close();
            delete semaphore_;
        }

        if (data_) {
            munmap(data_, byte_size_);
        }

        if (handle_ != kInvalidSharedMemory) {
            close(handle_);
        }

        /* Сбросить все */
        data_ = NULL;
        handle_ = kInvalidSharedMemory;
        name_ = "";
        semaphore_ = NULL;
        owner_ = false;
        last_error_ = 0;
    }

    void* SharedMemory::Data() { return data_; }

    size_t SharedMemory::Size() const { return byte_size_; }

    bool SharedMemory::Lock()
    {
        assert(semaphore_);
        if (semaphore_->Wait()) {
            return true;
        }
        last_error_ = semaphore_->GetLastError();
        return false;
    }

    bool SharedMemory::TryLock(unsigned int ms)
    {
        assert(semaphore_);

        bool result = ms == 0 ? semaphore_->TryWait() : semaphore_->TimedWait(ms);

        if (result == false) {
            last_error_ = semaphore_->GetLastError();
        }

        return result;
    }

    bool SharedMemory::Unlock()
    {
        assert(semaphore_);
        if (semaphore_->Post()) {
            return true;
        }

        last_error_ = semaphore_->GetLastError();
        return false;
    }

    bool SharedMemory::IsOpen() const { return handle_ != kInvalidSharedMemory; }

    error::Error SharedMemory::GetLastError() const { return last_error_; }
}
}
#endif //#if defined(__linux__)
