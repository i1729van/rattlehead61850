#if defined(__linux__)
#include "../file.h"
#include <assert.h> // for assert
#include <fcntl.h> // for open, O_RDWR, O_CREAT, O_EXCL, O_RDONLY
#include <sys/stat.h> // for S_IRGRP, S_IROTH, S_IRUSR, S_IWUSR
#include <unistd.h> // for close, read, ssize_t, write

namespace rth {
namespace file {

    const int kInvalidFileHandler = -1;

    File::File()
        : handler_(kInvalidFileHandler)
    {
    }

    File::~File() {}

    bool File::Create(const char* name, bool fail_if_exists)
    {
        assert(handler_ == kInvalidFileHandler);
        assert(name);
        handler_ = open(name, O_CREAT | O_RDWR | (fail_if_exists ? O_EXCL : 0),
            S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        return handler_ != kInvalidFileHandler;
    }

    bool File::Open(const char* name, bool read_only)
    {
        assert(handler_ == kInvalidFileHandler);
        assert(name);
        handler_ = open(name, read_only ? O_RDONLY : O_RDWR,
            S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        return handler_ != kInvalidFileHandler;
    }

    ssize_t File::Read(void* data, size_t size) const
    {
        assert(handler_ != kInvalidFileHandler);
        assert(data);
        return read(handler_, data, size);
    }

    ssize_t File::Write(const void* data, size_t size) const
    {
        assert(handler_ != kInvalidFileHandler);
        assert(data);
        assert(size > 0);
        return write(handler_, data, size);
    }

    bool File::Close()
    {
        if (handler_ == kInvalidFileHandler) {
            return false;
        }
        close(handler_);
        handler_ = kInvalidFileHandler;
        return true;
    }
}
}
#endif //#if defined(__linux__)
