#if defined(__linux__)
#include "../thread.h"
#include <unistd.h> // for usleep

namespace rth {
namespace thread {

    /*-----------------------------------------------------------------------------
 * MUTEX
 *-----------------------------------------------------------------------------*/
    Mutex::Mutex() { pthread_mutex_init(&mutex_, NULL); }

    Mutex::~Mutex() { pthread_mutex_destroy(&mutex_); }

    bool Mutex::Lock() { return pthread_mutex_lock(&mutex_) == 0; }

    bool Mutex::TryLock() { return pthread_mutex_trylock(&mutex_) == 0; }

    void Mutex::Unlock() { pthread_mutex_unlock(&mutex_); }

    void Microsleep(unsigned int microsec) { usleep(microsec); }

    void Millisleep(unsigned int millisec) { usleep(millisec * 1000); }
}
}
#endif //#if defined(__linux__)
