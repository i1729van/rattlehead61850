#pragma once

#include <stdint.h>

namespace rth {
namespace time {

    uint64_t GetMonoTimeMilli();
    uint64_t GetRealTimeMilli();
}
}
