#pragma once
#include "base/network.h" // for IpAddress
#include "base/udp-socket.h" // for UdpSocket

namespace rth {
namespace iec61850 {

    class LogWriterInterface {
    public:
        virtual int Write(const char* msg) = 0;
        virtual ~LogWriterInterface();
    };

    /**
 * @class LogStdoutWriter
 * @date 03/06/17
 * @brief Логгер в stdout
 */
    class LogStdoutWriter : public LogWriterInterface {
    public:
        LogStdoutWriter();
        ~LogStdoutWriter();
        virtual int Write(const char* msg);
    };

    /**
 * @class LogUdpWriter
 * @date 03/06/17
 * @brief UDP логгер
 */
    class LogUdpWriter : public LogWriterInterface {
    public:
        LogUdpWriter();
        explicit LogUdpWriter(const network::IpAddress& address);
        bool SetSendtoAddress(const network::IpAddress& address);
        ~LogUdpWriter();
        virtual int Write(const char* msg);

    private:
        network::UdpSocket socket_;
        network::IpAddress sendto_address_;
    };
}
}
