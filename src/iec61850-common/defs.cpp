#include "defs.h"
#include "iec61850-common/config.h" // for RTH_IEC61850_DATABLOCK_SIZE, RTH...
#include <assert.h> // for assert
#include <string.h> // for strncpy, memset

namespace rth {
namespace iec61850 {

    DataValue::DataValue()
    {
        memset(name, 0, RTH_IEC61850_MAX_NAME_LEN);
        type = DataValueTypeUnk;
        ui64 = 0;
    }

    DataValue::DataValue(const char* value_name, DataValueType value_type)
    {
        assert(value_name);
        strncpy(name, value_name, RTH_IEC61850_MAX_NAME_LEN);
        type = value_type;
        ui64 = 0;
    }

    DataValue::DataValue(const char* value_name, DataValueType value_type,
        int8_t value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<int8_t>(value);
    }
    DataValue::DataValue(const char* value_name, DataValueType value_type,
        int16_t value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<int16_t>(value);
    }
    DataValue::DataValue(const char* value_name, DataValueType value_type,
        int32_t value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<int32_t>(value);
    }
    DataValue::DataValue(const char* value_name, DataValueType value_type,
        int64_t value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<int64_t>(value);
    }

    DataValue::DataValue(const char* value_name, DataValueType value_type,
        uint8_t value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<uint8_t>(value);
    }
    DataValue::DataValue(const char* value_name, DataValueType value_type,
        uint16_t value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<uint16_t>(value);
    }
    DataValue::DataValue(const char* value_name, DataValueType value_type,
        uint32_t value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<uint32_t>(value);
    }
    DataValue::DataValue(const char* value_name, DataValueType value_type,
        uint64_t value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<uint64_t>(value);
    }

    DataValue::DataValue(const char* value_name, DataValueType value_type,
        float value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<float>(value);
    }
    DataValue::DataValue(const char* value_name, DataValueType value_type,
        double value)
    {
        SetName(value_name);
        SetType(value_type);
        Write<double>(value);
    }
    void DataValue::SetName(const char* value_name)
    {
        assert(value_name);
        strncpy(name, value_name, RTH_IEC61850_MAX_NAME_LEN);
    }
    void DataValue::SetType(DataValueType value_type) { type = value_type; }

    bool DataValue::WriteInt8(int8_t value)
    {
        switch (type) {
        case DataValueTypeInt8:
            i8 = value;
            break;

        case DataValueTypeInt16:
            i16 = value;
            break;

        case DataValueTypeInt32:
            i32 = value;
            break;

        case DataValueTypeInt64:
            i64 = value;
            break;

        case DataValueTypeFloat:
            f32 = value;
            break;

        case DataValueTypeDouble:
            f64 = value;
            break;

        case DataValueTypeUnk:
        case DataValueTypeUInt8:
        case DataValueTypeUInt16:
        case DataValueTypeUInt32:
        case DataValueTypeUInt64:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteInt16(int16_t value)
    {
        switch (type) {
        case DataValueTypeInt16:
            i16 = value;
            break;

        case DataValueTypeInt32:
            i32 = value;
            break;

        case DataValueTypeInt64:
            i64 = value;
            break;

        case DataValueTypeFloat:
            f32 = value;
            break;

        case DataValueTypeDouble:
            f64 = value;
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        case DataValueTypeUInt8:
        case DataValueTypeUInt16:
        case DataValueTypeUInt32:
        case DataValueTypeUInt64:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteInt32(int32_t value)
    {
        switch (type) {
        case DataValueTypeInt32:
            i32 = value;
            break;

        case DataValueTypeInt64:
            i64 = value;
            break;

        case DataValueTypeDouble:
            f64 = value;
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        case DataValueTypeUInt8:
        case DataValueTypeInt16:
        case DataValueTypeUInt16:
        case DataValueTypeUInt32:
        case DataValueTypeUInt64:
        case DataValueTypeFloat:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteInt64(int64_t value)
    {
        switch (type) {
        case DataValueTypeInt64:
            i64 = value;
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        case DataValueTypeUInt8:
        case DataValueTypeInt16:
        case DataValueTypeUInt16:
        case DataValueTypeInt32:
        case DataValueTypeUInt32:
        case DataValueTypeUInt64:
        case DataValueTypeFloat:
        case DataValueTypeDouble:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteUInt8(uint8_t value)
    {
        switch (type) {
        case DataValueTypeUInt8:
            ui8 = value;
            break;

        case DataValueTypeInt16:
            i16 = value;
            break;

        case DataValueTypeUInt16:
            ui16 = value;
            break;

        case DataValueTypeInt32:
            i32 = value;
            break;

        case DataValueTypeUInt32:
            ui32 = value;
            break;

        case DataValueTypeInt64:
            i64 = value;
            break;

        case DataValueTypeUInt64:
            ui64 = value;
            break;

        case DataValueTypeFloat:
            f32 = value;
            break;

        case DataValueTypeDouble:
            f64 = value;
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteUInt16(uint16_t value)
    {
        switch (type) {
        case DataValueTypeUInt16:
            ui16 = value;
            break;

        case DataValueTypeInt32:
            i32 = value;
            break;

        case DataValueTypeUInt32:
            ui32 = value;
            break;

        case DataValueTypeInt64:
            i64 = value;
            break;

        case DataValueTypeUInt64:
            ui64 = value;
            break;

        case DataValueTypeFloat:
            f32 = value;
            break;

        case DataValueTypeDouble:
            f64 = value;
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        case DataValueTypeUInt8:
        case DataValueTypeInt16:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteUInt32(uint32_t value)
    {
        switch (type) {
        case DataValueTypeUInt32:
            ui32 = value;
            break;

        case DataValueTypeInt64:
            i64 = value;
            break;

        case DataValueTypeUInt64:
            ui64 = value;
            break;

        case DataValueTypeDouble:
            f64 = value;
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        case DataValueTypeUInt8:
        case DataValueTypeInt16:
        case DataValueTypeUInt16:
        case DataValueTypeInt32:
        case DataValueTypeFloat:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteUInt64(uint64_t value)
    {
        switch (type) {
        case DataValueTypeUInt64:
            ui64 = value;
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        case DataValueTypeUInt8:
        case DataValueTypeInt16:
        case DataValueTypeUInt16:
        case DataValueTypeInt32:
        case DataValueTypeUInt32:
        case DataValueTypeInt64:
        case DataValueTypeFloat:
        case DataValueTypeDouble:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteFloat(float value)
    {
        switch (type) {
        case DataValueTypeFloat:
            f32 = value;
            break;
        case DataValueTypeDouble:
            f64 = static_cast<double>(value);
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        case DataValueTypeUInt8:
        case DataValueTypeInt16:
        case DataValueTypeUInt16:
        case DataValueTypeInt32:
        case DataValueTypeUInt32:
        case DataValueTypeInt64:
        case DataValueTypeUInt64:
        default:
            return false;
        }

        return true;
    }

    bool DataValue::WriteDouble(double value)
    {
        switch (type) {
        case DataValueTypeDouble:
            f64 = value;
            break;
        case DataValueTypeUnk:
        case DataValueTypeInt8:
        case DataValueTypeUInt8:
        case DataValueTypeInt16:
        case DataValueTypeUInt16:
        case DataValueTypeInt32:
        case DataValueTypeUInt32:
        case DataValueTypeInt64:
        case DataValueTypeUInt64:
        case DataValueTypeFloat:
        default:
            return false;
        }

        return true;
    }

    int8_t DataValue::ReadInt8() const { return i8; }
    int16_t DataValue::ReadInt16() const { return i16; }
    int32_t DataValue::ReadInt32() const { return i32; }
    int64_t DataValue::ReadInt64() const { return i64; }

    uint8_t DataValue::ReadUInt8() const { return ui8; }
    uint16_t DataValue::ReadUInt16() const { return ui16; }
    uint32_t DataValue::ReadUInt32() const { return ui32; }
    uint64_t DataValue::ReadUInt64() const { return ui64; }

    float DataValue::ReadFloat() const { return f32; }
    double DataValue::ReadDouble() const { return f64; }

    const char* DataValue::GetName() const { return name; }

    DataValueType DataValue::GetType() const { return type; }

    DataBlock::DataBlock()
        : id(0)
        , size(0)
    {
    }

    bool DataBlock::Prepare(uint32_t trans_id, uint32_t trans_size,
        const DataValue* trans_records)
    {
        // assert(trans_size < RTH_MAX_TRANS_SIZE);
        if (trans_size > RTH_IEC61850_DATABLOCK_SIZE) {
            return false;
        }

        id = trans_id;
        size = trans_size;

        for (uint32_t i = 0; i < trans_size; i++) {
            records[i] = trans_records[i];
        }

        return true;
    }
    void DataBlock::Clear()
    {
        id = 0;
        size = 0;
    }

    void DataBlock::SetID(uint32_t block_id) { id = block_id; }
    uint32_t DataBlock::GetID() const { return id; }
    uint32_t DataBlock::GetSize() const { return size; }
    uint32_t DataBlock::GetFreeSize() const
    {
        return RTH_IEC61850_DATABLOCK_SIZE - size;
    }

    uint32_t DataBlock::GetMaxSize() { return RTH_IEC61850_DATABLOCK_SIZE; }

    const DataValue* DataBlock::GetData() const { return records; }

    DataBlockStream::DataBlockStream(DataBlock& block)
        : block_(&block)
        , nblocks_(1)
        , read_block_idx_(0)
        , read_value_idx_(0)
        , write_block_idx_(0)
        , write_value_idx_(0)
    {
    }

    DataBlockStream::DataBlockStream(DataBlock* block, size_t nblocks)
        : block_(block)
        , nblocks_(nblocks)
        , read_block_idx_(0)
        , read_value_idx_(0)
        , write_block_idx_(0)
        , write_value_idx_(0)
    {
    }

    size_t DataBlockStream::Size() const { return nblocks_; }

    bool DataBlockStream::operator<<(const DataValue& value)
    {
        if (write_block_idx_ >= nblocks_) {
            return false;
        }

        DataBlock* currend_block = block_ + write_block_idx_;
        currend_block->records[write_value_idx_++] = value;
        currend_block->size = write_value_idx_;

        write_value_idx_ = write_value_idx_ % RTH_IEC61850_DATABLOCK_SIZE;
        if (write_value_idx_ == 0) {
            write_block_idx_++;
        }

        return true;
    }

    bool DataBlockStream::operator>>(DataValue& value)
    {
        if (read_block_idx_ >= nblocks_) {
            return false;
        }

        DataBlock* current_block = block_ + read_block_idx_;

        if (current_block->size == 0) {
            return false;
        }

        value = current_block->records[read_value_idx_];

        read_value_idx_ = (read_value_idx_ + 1) % current_block->size;

        if (read_value_idx_ == 0) {
            read_block_idx_++;
        }

        return true;
    }

    const DataBlock* DataBlockStream::Blocks() const { return block_; }
    DataBlock* DataBlockStream::Blocks() { return block_; }
}
}
