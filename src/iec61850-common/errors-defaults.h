#pragma once

#include "errors.h"

namespace rth {
namespace iec61850 {

    /*  Описание ошибки. Самое тупое. */
    class DefaultErrorDescription {
    public:
        DefaultErrorDescription(const char* description = "")
            : description_(description)
        {
        }

        DefaultErrorDescription(const std::string& description)
            : description_(description)
        {
        }

        const char* ToString() const { return description_.c_str(); }

    protected:
        std::string description_;
    };

    /* Вывод списка ошибок через поток. Поток существует.
 * Выводит все, что имеет методы Type, Name, Description */
    template <typename T, typename S>
    void ErrorsToStream(const T& self, S& stream)
    {
        typename T::ConstIterator it = self.Begin(), end = self.End();
        while (it != end) {
            stream << it->ToString() << std::endl;
            it++;
        }
    }

    /* Вывод списка ошибок через поток. Поток создается и уничтожается в процессе
 * вывода */
    template <typename T, typename S>
    void ErrorsToStream(const T& self)
    {
        S stream;
        typename T::ConstIterator it = self.Begin(), end = self.End();
        while (it != end) {
            stream << it->ToString() << std::endl;
            it++;
        }
    }

    typedef ErrorList<DefaultErrorDescription> DefaultErrorList;
}
}
