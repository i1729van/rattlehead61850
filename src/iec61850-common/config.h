#pragma once
/* Параметры для конфигурации */

/* Кол-во значений в одном блоке данных */
#define RTH_IEC61850_DATABLOCK_SIZE 64U

/* Дефолтный размер транзакции [в DataBlocks].
 * Определяет кол-во блоков данных, которые могут быть переданы за один запрос.
 * Кол-во запсей, передаваемых за один раз будет определено как
 * RTH_IEC61850_DATABLOCK_SIZE * RTH_IEC61850_DEF_TRANSACTION_SIZE */
#define RTH_IEC61850_DEF_TRANSACTION_SIZE 64U

/* Дефолтный размер буфера [в DataBlocks]*/
#define RTH_IEC61850_DEF_SHMBUFFER_SIZE 256U

/* Минимальный таймаут для ожидания */
#define RTH_IEC61850_MINIMAL_TIMEOUT_MS 8U

/* Макс .длина тэга 61850 */
#define RTH_IEC61850_MAX_NAME_LEN 129U

/* Версия API */
#define RTH_IEC61850_SERVER_API_VERSION "0.2"

/* Суффикс для входной очереди сервера */
#define RTH_IEC61850_SERVER_INPUT_QUEUE_SUFFIX "_din"

/* Суффикс для очереди подтверждения */
#define RTH_IEC61850_SERVER_INPUT_ACK_QUEUE_SUFFIX "_din_ack"

/* Суффикс для статистики */
#define RTH_IEC61850_SERVER_STAT_SUFFIX "_stat"
