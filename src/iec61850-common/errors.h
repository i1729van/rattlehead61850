#pragma once

#include <list>
#include <sstream>
#include <string>

namespace rth {
namespace iec61850 {

    /* Шаблонный класс для списков ошибок
  T - шаблонный аргумент, хранящий тип описания ошибок. */
    template <typename T>
    class ErrorList {
    public:
        typedef typename std::list<T>::const_iterator ConstIterator;

    public:
        ErrorList(size_t max_errors = 32)
            : error_cnt_(0)
            , max_error_cnt_(max_errors)
        {
        }

        ConstIterator Begin() const { return errors_.begin(); }
        ConstIterator End() const { return errors_.end(); }

        bool Append(const T& error)
        {
            if (error_cnt_ < max_error_cnt_) {
                errors_.push_back(error);
                error_cnt_++;
                return true;
            }
            return false;
        }
        size_t Count() const { return error_cnt_; }

    private:
        std::list<T> errors_;
        size_t error_cnt_; //< list.size - обычно проходит по всему списку. Счетчик
        //луше хранить отдельно
        size_t max_error_cnt_;
    };
}
}
