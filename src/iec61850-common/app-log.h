#pragma once
#include "base/thread.h" // for Mutex
#include <list> // for list
#include <sstream> // for ostream, stringstream
#include <string> // for string

namespace rth {
namespace iec61850 {

    class LogWriterInterface;
    class AppLogger {
    public:
        static AppLogger& Instance();
        static bool AddWriter(LogWriterInterface& writer);
        static void Write(const char* message);

    private:
        /* Уникальный объект */
        AppLogger();
        AppLogger(const AppLogger&);
        AppLogger& operator=(const AppLogger&);
        ~AppLogger();

    private:
        mutable rth::thread::Mutex mutex_;
        std::list<LogWriterInterface*> writers_;
    };

    class LogMessage {
    public:
        LogMessage();
        ~LogMessage();
        LogMessage(const LogMessage&);
        LogMessage& operator=(const LogMessage&);
        void Flush();
        std::stringstream& GetStream();
        bool IsEnabled() const;

    protected:
        std::stringstream stream_;
        bool enabled_;
    };

    const LogMessage& operator<<(const LogMessage& log, const char* str);
    const LogMessage& operator<<(const LogMessage& log, const std::string& str);
    const LogMessage& operator<<(const LogMessage& log, int value);
    const LogMessage& operator<<(const LogMessage& log,
        std::ostream& (*f)(std::ostream&));
}
}
