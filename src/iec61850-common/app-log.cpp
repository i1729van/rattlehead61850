#include "app-log.h"
#include "iec61850-common/log-writers.h" // for LogWriterInterface

namespace rth {
namespace iec61850 {

    AppLogger::AppLogger() {}
    AppLogger::AppLogger(const AppLogger&) {}
    AppLogger& AppLogger::operator=(const AppLogger&) { return *this; }
    AppLogger::~AppLogger() {}

    void AppLogger::Write(const char* message)
    {
        std::list<LogWriterInterface*>::const_iterator it = AppLogger::Instance().writers_.begin();
        std::list<LogWriterInterface*>::const_iterator end = AppLogger::Instance().writers_.end();
        AppLogger::Instance().mutex_.Lock();
        while (it != end) {
            LogWriterInterface* current = *it++;
            current->Write(message);
            ;
        }
        AppLogger::Instance().mutex_.Unlock();
    }

    bool AppLogger::AddWriter(LogWriterInterface& writer)
    {
        AppLogger::Instance().mutex_.Lock();
        AppLogger::Instance().writers_.push_back(&writer);
        AppLogger::Instance().mutex_.Unlock();
        return true;
    }

    AppLogger& AppLogger::Instance()
    {
        static AppLogger logger;
        return logger;
    }

    LogMessage::LogMessage()
        : enabled_(false)
    {
    }
    LogMessage::~LogMessage()
    {
        if (enabled_) {
            Flush();
        }
    }
    void LogMessage::Flush() { AppLogger::Instance().Write(stream_.str().c_str()); }
    std::stringstream& LogMessage::GetStream() { return stream_; }

    bool LogMessage::IsEnabled() const { return enabled_; }

    const LogMessage& operator<<(const LogMessage& log, const char* str)
    {
        if (log.IsEnabled()) {
            const_cast<LogMessage&>(log).GetStream() << std::string(str);
        }
        return log;
    }

    const LogMessage& operator<<(const LogMessage& log, const std::string& str)
    {
        if (log.IsEnabled()) {
            const_cast<LogMessage&>(log).GetStream() << str;
        }
        return log;
    }

    const LogMessage& operator<<(const LogMessage& log, int value)
    {
        if (log.IsEnabled()) {
            const_cast<LogMessage&>(log).GetStream() << value;
        }
        return log;
    }

    const LogMessage& operator<<(const LogMessage& log,
        std::ostream& (*f)(std::ostream&))
    {
        if (log.IsEnabled()) {
            const_cast<LogMessage&>(log).GetStream() << "\n";
        }
        return log;
    }
}
}
