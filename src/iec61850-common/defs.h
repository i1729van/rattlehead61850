#pragma once

#include "base/shared-queue.h" // for SharedQueue
#include "config.h" // for RTH_IEC61850_DATABLOCK_SIZE, RTH_IEC6...
#include <stddef.h> // for size_t
#include <stdint.h> // for uint32_t, int16_t, int32_t, int64_t
#include <sys/types.h> // for int8_t

namespace rth {
namespace iec61850 {

    enum DataValueType {
        DataValueTypeUnk,
        DataValueTypeInt8,
        DataValueTypeUInt8,
        DataValueTypeInt16,
        DataValueTypeUInt16,
        DataValueTypeInt32,
        DataValueTypeUInt32,
        DataValueTypeInt64,
        DataValueTypeUInt64,
        DataValueTypeFloat,
        DataValueTypeDouble
        // Timestamp
    };

    /* Рузультат обновления данных */
    enum DataAttributeUpdateResult {
        DataAttributeUpdateResultUnk,
        DataAttributeUpdateResultOK,
        DataAttributeUpdateResultNotRespond,
        DataAttributeUpdateResultInvalidData,
        DataAttributeUpdateResultTagIsMissing,
        DataAttributeUpdateResultIncompatibleTypes,
        DataAttributeUpdateResultPrecisionLoss,
        DataAttributeUpdateResultUnsupportedTypes
    };

    typedef struct DataValue {
        DataValueType type;
        union {
            int8_t i8;
            int16_t i16;
            int32_t i32;
            int64_t i64;

            uint8_t ui8;
            uint16_t ui16;
            uint32_t ui32;
            uint64_t ui64;

            float f32;
            double f64;
            // Timestamp
        };
        char name[RTH_IEC61850_MAX_NAME_LEN];

        DataValue();

        DataValue(const char* value_name, DataValueType value_type);

        explicit DataValue(const char* value_name, DataValueType value_type,
            int8_t value);
        explicit DataValue(const char* value_name, DataValueType value_type,
            int16_t value);
        explicit DataValue(const char* value_name, DataValueType value_type,
            int32_t value);
        explicit DataValue(const char* value_name, DataValueType value_type,
            int64_t value);

        explicit DataValue(const char* value_name, DataValueType value_type,
            uint8_t value);
        explicit DataValue(const char* value_name, DataValueType value_type,
            uint16_t value);
        explicit DataValue(const char* value_name, DataValueType value_type,
            uint32_t value);
        explicit DataValue(const char* value_name, DataValueType value_type,
            uint64_t value);

        explicit DataValue(const char* value_name, DataValueType value_type,
            float value);
        explicit DataValue(const char* value_name, DataValueType value_type,
            double value);

        void SetName(const char* value_name);
        void SetType(DataValueType value_type);
        bool WriteInt8(int8_t value);
        bool WriteInt16(int16_t value);
        bool WriteInt32(int32_t value);
        bool WriteInt64(int64_t value);

        bool WriteUInt8(uint8_t value);
        bool WriteUInt16(uint16_t value);
        bool WriteUInt32(uint32_t value);
        bool WriteUInt64(uint64_t value);

        bool WriteFloat(float value);
        bool WriteDouble(double value);

        int8_t ReadInt8() const;
        int16_t ReadInt16() const;
        int32_t ReadInt32() const;
        int64_t ReadInt64() const;

        uint8_t ReadUInt8() const;
        uint16_t ReadUInt16() const;
        uint32_t ReadUInt32() const;
        uint64_t ReadUInt64() const;

        float ReadFloat() const;
        double ReadDouble() const;

        const char* GetName() const;
        DataValueType GetType() const;

        /* Записать новое значение в соответвии с типом данных
   * DataValueType не изменяется.
   */
        template <typename T>
        bool Write(T value)
        {
            switch (type) {
            case DataValueTypeInt8:
                return WriteInt8(static_cast<int8_t>(value));

            case DataValueTypeInt16:
                return WriteInt16(static_cast<int16_t>(value));

            case DataValueTypeInt32:
                return WriteInt32(static_cast<int32_t>(value));

            case DataValueTypeInt64:
                return WriteInt64(static_cast<int64_t>(value));

            case DataValueTypeUInt8:
                return WriteUInt8(static_cast<uint8_t>(value));

            case DataValueTypeUInt16:
                return WriteUInt16(static_cast<uint16_t>(value));

            case DataValueTypeUInt32:
                return WriteUInt32(static_cast<uint32_t>(value));

            case DataValueTypeUInt64:
                return WriteUInt64(static_cast<uint64_t>(value));

            case DataValueTypeFloat:
                return WriteFloat(static_cast<float>(value));

            case DataValueTypeDouble:
                return WriteDouble(static_cast<double>(value));
            case DataValueTypeUnk:
            default:
                return false;
            }
            return false;
        }

        template <typename T>
        T Read() const
        {
            switch (type) {
            case DataValueTypeInt8:
                return static_cast<T>(ReadInt8());
            case DataValueTypeInt16:
                return static_cast<T>(ReadInt16());
            case DataValueTypeInt32:
                return static_cast<T>(ReadInt32());
            case DataValueTypeInt64:
                return static_cast<T>(ReadInt64());
            case DataValueTypeUInt8:
                return static_cast<T>(ReadUInt8());
            case DataValueTypeUInt16:
                return static_cast<T>(ReadUInt16());
            case DataValueTypeUInt32:
                return static_cast<T>(ReadUInt32());
            case DataValueTypeUInt64:
                return static_cast<T>(ReadUInt64());
            case DataValueTypeFloat:
                return static_cast<T>(ReadFloat());
            case DataValueTypeDouble:
                return static_cast<T>(ReadDouble());
            case DataValueTypeUnk:
            default:
                return 0;
            }
            return 0;
        }

    } DataValue;

    typedef struct DataBlock {
        uint32_t id;
        uint32_t size;

        DataValue records[RTH_IEC61850_DATABLOCK_SIZE];

        DataBlock();

        bool Prepare(uint32_t trans_id, uint32_t trans_size,
            const DataValue* trans_records);
        void Clear();

        void SetID(uint32_t block_id);
        uint32_t GetID() const;
        uint32_t GetSize() const;
        uint32_t GetFreeSize() const;
        static uint32_t GetMaxSize();
        const DataValue* GetData() const;

    } DataBlock;

    class DataBlockStream {
    public:
        explicit DataBlockStream(DataBlock& block);
        DataBlockStream(DataBlock* block, size_t nblocks);
        bool operator<<(const DataValue& value);
        bool operator>>(DataValue& value);
        size_t Size() const;
        const DataBlock* Blocks() const;
        DataBlock* Blocks();

    private:
        DataBlock* block_;
        size_t nblocks_;

        uint32_t read_block_idx_;
        uint32_t read_value_idx_;

        uint32_t write_block_idx_;
        uint32_t write_value_idx_;
    };

    typedef rth::ipc::SharedQueue<DataBlock> DataQueue;
}
}
