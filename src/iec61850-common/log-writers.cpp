#include "log-writers.h"
#include <assert.h> // for assert
#include <string.h> // for strnlen

#if defined(__linux__)
#include <unistd.h> // for STDOUT_FILENO, fsync, write
#endif // #if defined(__linux__)

namespace rth {
namespace iec61850 {

    LogWriterInterface::~LogWriterInterface() {}

    LogStdoutWriter::LogStdoutWriter() {}

#if defined(__linux__)
    LogStdoutWriter::~LogStdoutWriter()
    {
        fsync(STDOUT_FILENO);
    }

    int LogStdoutWriter::Write(const char* msg)
    {
        const int kMaxStroudMessageLen = 1024;
        return write(STDOUT_FILENO, msg, strnlen(msg, kMaxStroudMessageLen)) > 0;
    }
#endif // #if defined(__linux__)

#if defined(_WIN32)
    LogStdoutWriter::~LogStdoutWriter()
    {
        FlushFileBuffers(GetStdHandle(STD_OUTPUT_HANDLE));
    }

    int LogStdoutWriter::Write(const char* msg)
    {
        const int kMaxStroudMessageLen = 1024;
        return WriteFile(GetStdHandle(STD_OUTPUT_HANDLE), msg,
                   strnlen(msg, kMaxStroudMessageLen), NULL, NULL)
            > 0;
    }
#endif // #if defined(_WIN32)

    LogUdpWriter::LogUdpWriter()
    {
        socket_.Create();
    }

    LogUdpWriter::LogUdpWriter(const network::IpAddress& address)

    {
        socket_.Create();
        sendto_address_ = address;
    }

    bool LogUdpWriter::SetSendtoAddress(const network::IpAddress& address)
    {
        if (network::IsValidIP(address)) {
            sendto_address_ = address;
            return true;
        }
        return false;
    }

    LogUdpWriter::~LogUdpWriter() { socket_.Close(); }

    int LogUdpWriter::Write(const char* msg)
    {
        assert(msg);
        assert(network::IsValidIP(sendto_address_));

        const int kMTUFrameLen = 1024;
        return socket_.SendTo(msg, strnlen(msg, kMTUFrameLen), sendto_address_) > 0;
    }
}
}
