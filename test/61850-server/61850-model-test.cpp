#include "iec61850-server/model-builder.h" // for Build, ReleaseBuild
#include "iec61850-server/model-default-builder.h" // for DefaultBuilder
#include "iec61850-server/model-explorer.h" // for NodesListConstIte...
#include "iec61850-server/model-types.h" // for DataTemplateConst...
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for GetBoolAssertionF...
#include <gtest/gtest.h> // for AssertionResult
#include <libiec61850/iec61850_common.h> // for TRG_OPT_DATA_CHANGED
#include <libiec61850/iec61850_model.h> // for DataAttribute
#include <list> // for _List_const_iterator
#include <map> // for _Rb_tree_const_it...
#include <stdbool.h> // for false, bool, true
#include <stdint.h> // for uint8_t
#include <stdlib.h> // for NULL, size_t
#include <string> // for string, operator==
#include <utility> // for pair
#include <vector> // for vector

using namespace rth::iec61850;
using namespace rth::iec61850::model;

TEST(IEC61850ServerModel, LoadMissingIcd)
{
    const char* filename = "data/~~Missing--!!--B20.icd";

    model::DefaultBuilder mb1;
    model::BuildResult& model = model::Build(mb1, filename, "TEMPLATE");
    ASSERT_FALSE(model.IsModelValid());
    model::ReleaseBuild(model);
}

TEST(IEC61850ServerModel, LoadIcd)
{
    const char* filename = "data/B20.icd";

    model::DefaultBuilder mb1;
    model::BuildResult& model = model::Build(mb1, filename, "TEMPLATE");
    model::ReleaseBuild(model);
}

TEST(IEC61850ServerModel, GetModelNodes)
{
    const char* filename = "data/B20.icd";

    struct CheckNodes {
        const char* name;
        bool visited;
    } nodes[] = {
        { "TEMPLATELD0/CSWI1.Loc.stVal", false },
        { "TEMPLATELD0/CSWI1.Loc.q", false },
        { "TEMPLATELD0/CSWI1.Loc.t", false },
        { "TEMPLATELD0/CSWI1.Pos.SBOw.ctlVal", false },
        { "TEMPLATELD0/M114_GGIO1.Beh.q", false },
        { "TEMPLATELD0/MMXU3.Hz", false },
        { "TEMPLATELD0/MMXU3.Hz.mag", false },
        { "TEMPLATELD0/MMXU3.Hz.mag.f", false },
        { "TEMPLATELD0/MMXU3.PhV.phsC", false },
        { "TEMPLATELD0/MSQI1.Mod.t", false },
        { "TEMPLATELD0/XCBR1.Pos.t", false },
        { "TEMPLATELD0/A27_PTUV2.Str.general", false },
        { "TEMPLATELD0/A59N_PTOV2.Mod.ctlModel", false },
        { "TEMPLATELD0/XCBR1.OpTmms.dataNs", false },
    };

    model::DefaultBuilder mb1;
    model::BuildResult& model = model::Build(mb1, filename, "TEMPLATE");

    ASSERT_TRUE(model.IsModelValid());
    NodesList& tags = CreateNodeList(model.GetModel());

    NodesListConstIterator it = tags.begin(), end = tags.end();
    const int kMaxCheckNodes = sizeof(nodes) / sizeof(nodes[0]);
    int found = 0;
    while (it != end) {
        for (int i = 0; i < kMaxCheckNodes && found != kMaxCheckNodes; i++) {
            if (nodes[i].visited == false && *it == nodes[i].name) {
                nodes[i].visited = true;
                found++;
            }
        }
        ++it;
        // std::cout << *it++ << "\n";
    }
    ASSERT_EQ(found, kMaxCheckNodes);
    DeleteNodeList(tags);

    model::ReleaseBuild(model);
}

TEST(IEC61850ServerModel, BasicDatatypeBuilding)
{
    const char* filename = "data/B20.icd";
    /* Прочитать описание данных в фале и выборочно проверить записи */
    model::DefaultBuilder builder1;
    model::BuildResult& build_result1 = model::Build(builder1, filename, "");
    ASSERT_TRUE(build_result1.IsDataTemplateValid());

    DataTemplate* template1 = build_result1.GetDataTemplate();
    DataTemplateConstIter it = template1->begin(), end = template1->end();

    it = template1->find("XCBR_Sepam2040");
    ASSERT_TRUE(it != end);

    it = template1->find("WyeABCr_Sepam204080");
    ASSERT_TRUE(it != end);

    it = template1->find("ctlModel");
    ASSERT_TRUE(it != end);

    it = template1->find("No-data-type");
    ASSERT_TRUE(it == end);
    model::ReleaseBuild(build_result1);

    // Повторно прочитать записи и обойти все из них
    model::DefaultBuilder builder2;
    model::BuildResult& build_result2 = model::Build(builder2, filename, "");
    ASSERT_TRUE(build_result2.IsDataTemplateValid());
    DataTemplate* template2 = build_result2.GetDataTemplate();
    it = template2->begin();
    end = template2->end();
    while (it != end) {
        const DataTemplateRecord& record = it->second;
        DataTypeAttributesConstIter attrib_it = record.AttributesBegin(),
                                    attrib_end = record.AttributesEnd();
        /*std::cout << "\n\n---- < ";
    std::cout << record.type.id << "[" << record.type.cdc << "] " <<
     ">----\n";*/

        while (attrib_it != attrib_end) {
            /* const DataAttribute &attribute = *attrib_it;
       std::cout << "Name:" << attribute.name << " type:" << attribute.type <<
       "bType:"  << attribute.bType << " FC:" << attribute.fc << " ord:" <<
          attribute.ord <<"\n";*/
            ++attrib_it;
        }
        ++it;
    }

    // Выборочно проверить записи
    it = template2->find("XCBR_Sepam2040");
    ASSERT_TRUE(it != end);

    it = template2->find("WyeABCr_Sepam204080");
    ASSERT_TRUE(it != end);

    it = template2->find("ctlModel");
    ASSERT_TRUE(it != end);

    it = template2->find("No-data-type");
    ASSERT_TRUE(it == end);

    model::ReleaseBuild(build_result2);

    // чтение несуществюущего файла
    model::DefaultBuilder builder3;
    model::BuildResult& build_result3 = model::Build(builder3, "~~NoExsistingFile~~", "");
    ASSERT_FALSE(build_result3.IsDataTemplateValid());
    model::ReleaseBuild(build_result3);
}

TEST(IEC61850ServerModel, BasicDatatypeScdBuilding)
{
    const char* filename = "data/HSR_MSC-SPB.0.3.scd";
    model::DefaultBuilder builder1;
    model::BuildResult& build_result = model::Build(builder1, filename, "");
    ASSERT_TRUE(build_result.IsDataTemplateValid());

    DataTemplate* template1 = build_result.GetDataTemplate();
    DataTemplateConstIter data_it = template1->begin(),
                          data_end = template1->end();

    while (data_it != data_end) {
        const DataTemplateRecord& record = data_it->second;
        DataTypeAttributesConstIter attrib_it = record.AttributesBegin(),
                                    attrib_end = record.AttributesEnd();

        /*std::cout << "\n\n---- < ";
    std::cout << record.type.id << "[" << record.type.cdc << "] " <<
    ">----\n";*/

        while (attrib_it != attrib_end) {
            /*const DataAttribute &attribute = *attrib_it;
      std::cout << "Name:" << attribute.name << " type:" << attribute.type <<
      "bType:"  << attribute.bType << " FC:" << attribute.fc << " ord:" <<
         attribute.ord <<"\n";*/
            ++attrib_it;
        }

        ++data_it;
    }

    model::ReleaseBuild(build_result);
}

TEST(IEC61850ServerModel, CheckModelNodes)
{
    /* Прочитать список аттрибутов из файла, загрузить модель
   * и подтянуть эти аттрибуты. */
    const char* filename = "data/B20.icd";

    model::DefaultBuilder mb1;
    model::BuildResult& model = model::Build(mb1, filename, "TEMPLATE");

    ASSERT_TRUE(model.IsModelValid());

    NodesList& tags = CreateNodeList(model.GetModel());
    NodesListConstIterator it = tags.begin(), end = tags.end();
    int cnt = 0;
    while (it != end) {
        DataAttribute* attrib = reinterpret_cast<DataAttribute*>(
            IedModel_getModelNodeByObjectReference(model.GetModel(), it->c_str()));

        // if(attrib)
        //  std::cout << *it << " " << attrib->name << "" << attrib << "\n";

        // Есть только два варианта, когда аттрибут равен 0
        ASSERT_TRUE(attrib != 0 || *it == "TEMPLATELD0" || *it == "TEMPLATE");

        ++it;
        ++cnt;
    }

    DeleteNodeList(tags);
    model::ReleaseBuild(model);

    ASSERT_GT(cnt, 700);
}

TEST(IEC61850ServerModel, CheckModelCache)
{
    /* Закэшировать тэги и сравнить их данными из утсройства */
    const char* filename = "data/B20.icd";
    DataAttributeMap cache;
    model::DefaultBuilder mb1;
    model::BuildResult& model = model::Build(mb1, filename, "TEMPLATE");

    ASSERT_TRUE(model.IsModelValid());
    ASSERT_GT(cache.Create(model.GetModel()), 700U);

    NodesList& tags = CreateNodeList(model.GetModel());
    NodesListConstIterator it = tags.begin(), end = tags.end();
    int cnt = 0;
    while (it != end) {
        DataAttributeMapEntry data = cache.Get(it->c_str());

        if (data.IsValid()) {
            DataAttribute* cv_attrib = reinterpret_cast<DataAttribute*>(
                IedModel_getModelNodeByObjectReference(model.GetModel(),
                    it->c_str()));
            ASSERT_TRUE(data.attribute != NULL);
            ASSERT_TRUE(cv_attrib != NULL);
            ASSERT_TRUE(data.attribute == cv_attrib);
            ++cnt;
        } else {
            ASSERT_TRUE(*it == "TEMPLATELD0" || *it == "TEMPLATE");
        }

        ++it;
    }

    DeleteNodeList(tags);
    model::ReleaseBuild(model);

    ASSERT_GT(cnt, 700);
}

TEST(IEC61850ServerModel, CheckCacheTime)
{
    /* Скорость кэша. Для периодической проверки */
    const char* filename = "data/B20.icd";
    DataAttributeMap cache;
    model::DefaultBuilder mb1;
    model::BuildResult& model = model::Build(mb1, filename, "TEMPLATE");

    ASSERT_TRUE(model.IsModelValid());
    ASSERT_GT(cache.Create(model.GetModel()), 700U);

    NodesList& tags = CreateNodeList(model.GetModel());
    for (int i = 0; i < 1000; i++) {
        NodesListConstIterator it = tags.begin(), end = tags.end();
        while (it != end) {
            DataAttributeMapEntry data = cache.Get(it->c_str());

            if (!data.IsValid()) {
                ASSERT_TRUE(*it == "TEMPLATELD0" || *it == "TEMPLATE");
            } else {
            }
            ++it;
        }
    }

    DeleteNodeList(tags);
    model::ReleaseBuild(model);
}

TEST(IEC61850ServerModel, CheckTrgOpts)
{
    /* Скорость кэша. Для периодической проверки */
    const char* filename = "data/B20.icd";
    DataAttributeMap cache;
    model::DefaultBuilder mb1;
    model::BuildResult& model = model::Build(mb1, filename, "TEMPLATE");

    ASSERT_TRUE(model.IsModelValid());
    ASSERT_GT(cache.Create(model.GetModel()), 700U);

    const char* tag1 = "TEMPLATELD0/MMXU3.PhV.phsA.cVal.mag.f";
    const uint8_t tag1_trgs = TRG_OPT_DATA_CHANGED;

    const char* tag2 = "TEMPLATELD0/MMXU3.PhV.phsA.q";
    const uint8_t tag2_trgs = TRG_OPT_QUALITY_CHANGED;

    const char* tag3 = "TEMPLATELD0/CSWI1.Loc.stVal";
    const uint8_t tag3_trgs = TRG_OPT_DATA_CHANGED;

    DataAttributeMapEntry data = cache.Get(tag1);

    ASSERT_TRUE(data.IsValid());
    ASSERT_EQ(data.GetAttribute().triggerOptions, tag1_trgs);

    data = cache.Get(tag2);
    ASSERT_EQ(data.GetAttribute().triggerOptions, tag2_trgs);

    data = cache.Get(tag3);
    ASSERT_EQ(data.GetAttribute().triggerOptions, tag3_trgs);

    model::ReleaseBuild(model);
    // std::cout << sel_cnt << "\n";
}

TEST(DISABLED_ScdSuite, BasicDatatypePromiscModeDisabled)
{
    /* Дефолтный билдер не предназначен для работы в неразборчивом режиме
   * Этот тест должен приводдить к останову через assert */
    const char* filename = "data/HSR_MSC-SPB.0.3.scd";
    model::DefaultBuilder builder1;
    model::BuildResult& build_result = model::Build(builder1, filename, "*");

    ASSERT_TRUE(build_result.IsDataTemplateValid());

    model::ReleaseBuild(build_result);
}

TEST(DISABLED_ScdSuite, BasicDatatypeMultipleBuilderCalls)
{
    /* Дефолтный билдер не предназначен для многократного  вызова.
   * Этот тест должен приводдить к останову через assert */
    const char* filename = "data/HSR_MSC-SPB.0.3.scd";
    model::DefaultBuilder builder1;
    model::BuildResult& build_result = model::Build(builder1, filename, "");
    model::BuildResult& build_result1 = model::Build(builder1, filename, "");
    model::ReleaseBuild(build_result);
    model::ReleaseBuild(build_result1);
}
