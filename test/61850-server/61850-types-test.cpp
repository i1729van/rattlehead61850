#include "iec61850-common/config.h" // for RTH_IEC61850_DATABLOCK_SIZE
#include "iec61850-common/defs.h" // for DataValue, DataBlock, DataValue:...
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for GetBoolAssertionFailureMessage
#include <gtest/gtest.h> // for AssertionResult, ASSERT_TRUE
#include <ostream> // for basic_ostream::operator<<, strin...
#include <stdint.h> // for int32_t, int16_t, int64_t, uint16_t
#include <string.h> // for memset
#include <string> // for string, operator==, basic_string
#include <sys/types.h> // for int8_t

using namespace rth::iec61850;

TEST(IEC61850ServerTypes, CheckTypes)
{
    DataValue trans_array[10];

    int8_t i8_cv = 0x7F;
    int16_t i16_cv = 0x7FFF;
    int32_t i32_cv = 0x7FFFFFFF;
    int64_t i64_cv = 0x7FFFFFFFFFFFFFLL;

    uint8_t ui8_cv = 0xFF;
    uint16_t ui16_cv = 0xFFFF;
    uint32_t ui32_cv = 0xFFFFFFFFUL;
    uint64_t ui64_cv = 0xFFFFFFFFFFFFFFULL;

    float fl_cv = 99999.11111F;
    double dbl_cv = 99999999.123456;

    trans_array[0] = DataValue("i8-type", DataValueTypeInt8, i8_cv /*{.i8 = i8_cv}*/);
    trans_array[1] = DataValue("i16-type", DataValueTypeInt16, i16_cv);
    trans_array[2] = DataValue("i32-type", DataValueTypeInt32, i32_cv);
    trans_array[3] = DataValue("i64-type", DataValueTypeInt64, i64_cv);
    trans_array[4] = DataValue("ui8-type", DataValueTypeUInt8, ui8_cv);
    trans_array[5] = DataValue("ui16-type", DataValueTypeUInt16, ui16_cv);
    trans_array[6] = DataValue("ui32-type", DataValueTypeUInt32, ui32_cv);
    trans_array[7] = DataValue("ui64-type", DataValueTypeUInt64, ui64_cv);

    trans_array[8] = DataValue("f32-type", DataValueTypeFloat, fl_cv);
    trans_array[9] = DataValue("f64-type", DataValueTypeDouble, dbl_cv);

    ASSERT_EQ(trans_array[0].i8, i8_cv);
    ASSERT_EQ(trans_array[1].i16, i16_cv);
    ASSERT_EQ(trans_array[2].i32, i32_cv);
    ASSERT_EQ(trans_array[3].i64, i64_cv);

    ASSERT_EQ(trans_array[4].ui8, ui8_cv);
    ASSERT_EQ(trans_array[5].ui16, ui16_cv);
    ASSERT_EQ(trans_array[6].ui32, ui32_cv);
    ASSERT_EQ(trans_array[7].ui64, ui64_cv);

    ASSERT_EQ(trans_array[8].f32, fl_cv);
    ASSERT_EQ(trans_array[9].f64, dbl_cv);

    // Запись с контролем типа
    DataValue rec("1", DataValueTypeInt8);
    ASSERT_TRUE(rec.WriteInt8(-1));
    ASSERT_FALSE(rec.WriteInt16(-1));
    ASSERT_FALSE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_FALSE(rec.WriteUInt8(1));
    ASSERT_FALSE(rec.WriteUInt16(1));
    ASSERT_FALSE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_FALSE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeInt16);
    ASSERT_TRUE(rec.WriteInt8(-1));
    ASSERT_TRUE(rec.WriteInt16(-1));
    ASSERT_FALSE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_FALSE(rec.WriteUInt16(1));
    ASSERT_FALSE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_FALSE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeInt32);
    ASSERT_TRUE(rec.WriteInt8(-1));
    ASSERT_TRUE(rec.WriteInt16(-1));
    ASSERT_TRUE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_TRUE(rec.WriteUInt16(1));
    ASSERT_FALSE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_FALSE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeInt64);
    ASSERT_TRUE(rec.WriteInt8(-1));
    ASSERT_TRUE(rec.WriteInt16(-1));
    ASSERT_TRUE(rec.WriteInt32(-1));
    ASSERT_TRUE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_TRUE(rec.WriteUInt16(1));
    ASSERT_TRUE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_FALSE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeUInt8);
    ASSERT_FALSE(rec.WriteInt8(-1));
    ASSERT_FALSE(rec.WriteInt16(-1));
    ASSERT_FALSE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_FALSE(rec.WriteUInt16(1));
    ASSERT_FALSE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_FALSE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeUInt16);
    ASSERT_FALSE(rec.WriteInt8(-1));
    ASSERT_FALSE(rec.WriteInt16(-1));
    ASSERT_FALSE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_TRUE(rec.WriteUInt16(1));
    ASSERT_FALSE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_FALSE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeUInt32);
    ASSERT_FALSE(rec.WriteInt8(-1));
    ASSERT_FALSE(rec.WriteInt16(-1));
    ASSERT_FALSE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_TRUE(rec.WriteUInt16(1));
    ASSERT_TRUE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_FALSE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeUInt64);
    ASSERT_FALSE(rec.WriteInt8(-1));
    ASSERT_FALSE(rec.WriteInt16(-1));
    ASSERT_FALSE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_TRUE(rec.WriteUInt16(1));
    ASSERT_TRUE(rec.WriteUInt32(1));
    ASSERT_TRUE(rec.WriteUInt64(1));
    ASSERT_FALSE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeFloat);
    ASSERT_TRUE(rec.WriteInt8(-1));
    ASSERT_TRUE(rec.WriteInt16(-1));
    ASSERT_FALSE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_TRUE(rec.WriteUInt16(1));
    ASSERT_FALSE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_TRUE(rec.WriteFloat(1));
    ASSERT_FALSE(rec.WriteDouble(1));

    rec.SetType(DataValueTypeDouble);
    ASSERT_TRUE(rec.WriteInt8(-1));
    ASSERT_TRUE(rec.WriteInt16(-1));
    ASSERT_TRUE(rec.WriteInt32(-1));
    ASSERT_FALSE(rec.WriteInt64(-1));
    ASSERT_TRUE(rec.WriteUInt8(1));
    ASSERT_TRUE(rec.WriteUInt16(1));
    ASSERT_TRUE(rec.WriteUInt32(1));
    ASSERT_FALSE(rec.WriteUInt64(1));
    ASSERT_TRUE(rec.WriteFloat(1));
    ASSERT_TRUE(rec.WriteDouble(1));

    // Запись - чтение
    DataValue i8_rec("1", DataValueTypeInt8);
    ASSERT_TRUE(i8_rec.WriteInt8(i8_cv));
    ASSERT_EQ(i8_rec.ReadInt8(), i8_cv);

    DataValue i16_rec("1", DataValueTypeInt16);
    ASSERT_TRUE(i16_rec.WriteInt16(i16_cv));
    ASSERT_EQ(i16_rec.ReadInt16(), i16_cv);

    DataValue i32_rec("1", DataValueTypeInt32);
    ASSERT_TRUE(i32_rec.WriteInt32(i32_cv));
    ASSERT_EQ(i32_rec.ReadInt32(), i32_cv);

    DataValue i64_rec("1", DataValueTypeInt64);
    ASSERT_TRUE(i64_rec.WriteInt64(i64_cv));
    ASSERT_EQ(i64_rec.ReadInt64(), i64_cv);

    DataValue ui8_rec("1", DataValueTypeUInt8);
    ASSERT_TRUE(ui8_rec.WriteUInt8(ui8_cv));
    ASSERT_EQ(ui8_rec.ReadUInt8(), ui8_cv);

    DataValue ui16_rec("1", DataValueTypeUInt16);
    ASSERT_TRUE(ui16_rec.WriteUInt16(ui16_cv));
    ASSERT_EQ(ui16_rec.ReadUInt16(), ui16_cv);

    DataValue ui32_rec("1", DataValueTypeUInt32);
    ASSERT_TRUE(ui32_rec.WriteUInt32(ui32_cv));
    ASSERT_EQ(ui32_rec.ReadUInt32(), ui32_cv);

    DataValue ui64_rec("1", DataValueTypeUInt64);
    ASSERT_TRUE(ui64_rec.WriteUInt64(ui64_cv));
    ASSERT_EQ(ui64_rec.ReadUInt64(), ui64_cv);

    DataValue fl_rec("1", DataValueTypeFloat);
    ASSERT_TRUE(fl_rec.WriteFloat(fl_cv));
    ASSERT_EQ(fl_rec.ReadFloat(), fl_cv);

    DataValue dbl_rec("1", DataValueTypeDouble);
    ASSERT_TRUE(dbl_rec.WriteDouble(dbl_cv));
    ASSERT_EQ(dbl_rec.ReadDouble(), dbl_cv);

    // Тест шаблонных функций
    memset(trans_array, 0, sizeof(trans_array));
    trans_array[0].SetType(DataValueTypeInt8);
    trans_array[1].SetType(DataValueTypeInt16);
    trans_array[2].SetType(DataValueTypeInt32);
    trans_array[3].SetType(DataValueTypeInt64);
    trans_array[4].SetType(DataValueTypeUInt8);
    trans_array[5].SetType(DataValueTypeUInt16);
    trans_array[6].SetType(DataValueTypeUInt32);
    trans_array[7].SetType(DataValueTypeUInt64);
    trans_array[8].SetType(DataValueTypeFloat);
    trans_array[9].SetType(DataValueTypeDouble);

    trans_array[0].Write<int8_t>(i8_cv);
    trans_array[1].Write<int16_t>(i16_cv);
    trans_array[2].Write<int32_t>(i32_cv);
    trans_array[3].Write<int64_t>(i64_cv);
    trans_array[4].Write<uint8_t>(ui8_cv);
    trans_array[5].Write<uint16_t>(ui16_cv);
    trans_array[6].Write<uint32_t>(ui32_cv);
    trans_array[7].Write<uint64_t>(ui64_cv);
    trans_array[8].Write<float>(fl_cv);
    trans_array[9].Write<double>(dbl_cv);

    ASSERT_EQ(trans_array[0].Read<int8_t>(), i8_cv);
    ASSERT_EQ(trans_array[1].Read<int16_t>(), i16_cv);
    ASSERT_EQ(trans_array[2].Read<int32_t>(), i32_cv);
    ASSERT_EQ(trans_array[3].Read<int64_t>(), i64_cv);

    ASSERT_EQ(trans_array[4].Read<uint8_t>(), ui8_cv);
    ASSERT_EQ(trans_array[5].Read<uint16_t>(), ui16_cv);
    ASSERT_EQ(trans_array[6].Read<uint32_t>(), ui32_cv);
    ASSERT_EQ(trans_array[7].Read<uint64_t>(), ui64_cv);

    ASSERT_EQ(trans_array[8].Read<float>(), fl_cv);
    ASSERT_EQ(trans_array[9].Read<double>(), dbl_cv);

    // Имя
    std::string new_name = "ololo-name";
    rec.SetName(new_name.c_str());
    ASSERT_TRUE(new_name == rec.name);

    ASSERT_EQ(DataBlock::GetMaxSize(), RTH_IEC61850_DATABLOCK_SIZE);
}

TEST(IEC61850ServerTypes, CheckTransactionData)
{
    DataBlock t1;

    DataValue trans_array[RTH_IEC61850_DATABLOCK_SIZE];

    int8_t i8_cv = 0x7F;
    int16_t i16_cv = 0x7FFF;
    int32_t i32_cv = 0x7FFFFFFFL;
    int64_t i64_cv = 0x7FFFFFFFFFFFFFLL;

    uint8_t ui8_cv = 0xFF;
    uint16_t ui16_cv = 0xFFFF;
    uint32_t ui32_cv = 0xFFFFFFFFUL;
    uint64_t ui64_cv = 0xFFFFFFFFFFFFFFULL;

    float fl_cv = 99999.11111F;
    double dbl_cv = 99999999.123456;

    /* Первый блок данных для проверки */
    trans_array[0] = DataValue("i8-type", DataValueTypeInt8, i8_cv);
    trans_array[1] = DataValue("i16-type", DataValueTypeInt16, i16_cv);
    trans_array[2] = DataValue("i32-type", DataValueTypeInt32, i32_cv);
    trans_array[3] = DataValue("i64-type", DataValueTypeInt64, i64_cv);
    t1.Prepare(1, 4, trans_array);

    ASSERT_EQ(t1.GetID(), 1U);
    ASSERT_EQ(t1.GetSize(), 4U);

    const DataValue* it = t1.GetData();

    ASSERT_TRUE(std::string(it->GetName()) == "i8-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeInt8);
    ASSERT_TRUE(it->ReadInt8() == i8_cv);
    it++;

    ASSERT_TRUE(std::string(it->GetName()) == "i16-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeInt16);
    ASSERT_TRUE(it->ReadInt16() == i16_cv);
    it++;

    ASSERT_TRUE(std::string(it->GetName()) == "i32-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeInt32);
    ASSERT_TRUE(it->ReadInt32() == i32_cv);
    it++;
    ASSERT_TRUE(std::string(it->GetName()) == "i64-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeInt64);
    ASSERT_TRUE(it->ReadInt64() == i64_cv);

    t1.Clear();
    ASSERT_EQ(t1.GetID(), 0U);
    ASSERT_EQ(t1.GetSize(), 0U);

    /* Второй блок данных */
    trans_array[0] = DataValue("ui8-type", DataValueTypeUInt8, ui8_cv);
    trans_array[1] = DataValue("ui16-type", DataValueTypeUInt16, ui16_cv);
    trans_array[2] = DataValue("ui32-type", DataValueTypeUInt32, ui32_cv);
    trans_array[3] = DataValue("ui64-type", DataValueTypeUInt64, ui64_cv);

    t1.Prepare(2, 4, trans_array);
    ASSERT_EQ(t1.GetID(), 2U);
    ASSERT_EQ(t1.GetSize(), 4U);

    it = t1.GetData();
    ASSERT_TRUE(std::string(it->GetName()) == "ui8-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeUInt8);
    ASSERT_TRUE(it->ReadUInt8() == ui8_cv);
    ++it;

    ASSERT_TRUE(std::string(it->GetName()) == "ui16-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeUInt16);
    ASSERT_TRUE(it->ReadUInt16() == ui16_cv);
    ++it;

    ASSERT_TRUE(std::string(it->GetName()) == "ui32-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeUInt32);
    ASSERT_TRUE(it->ReadUInt32() == ui32_cv);
    ++it;

    ASSERT_TRUE(std::string(it->GetName()) == "ui64-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeUInt64);
    ASSERT_TRUE(it->ReadUInt64() == ui64_cv);

    t1.Clear();
    ASSERT_EQ(t1.GetID(), 0U);
    ASSERT_EQ(t1.GetSize(), 0U);

    /* Третий блок */
    trans_array[0] = DataValue("f32-type", DataValueTypeFloat, fl_cv);
    trans_array[1] = DataValue("f64-type", DataValueTypeDouble, dbl_cv);

    t1.Prepare(3, 2, trans_array);
    ASSERT_EQ(t1.GetID(), 3U);
    ASSERT_EQ(t1.GetSize(), 2U);
    it = t1.GetData();

    ASSERT_TRUE(std::string(it->GetName()) == "f32-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeFloat);
    ASSERT_EQ(it->ReadFloat(), fl_cv);
    ++it;

    ASSERT_TRUE(std::string(it->GetName()) == "f64-type");
    ASSERT_TRUE(it->GetType() == DataValueTypeDouble);
    ASSERT_EQ(it->ReadDouble(), dbl_cv);

    // Копирование / присваивание
    DataBlock t2 = t1;
    const DataValue* it1 = t2.GetData();
    ASSERT_EQ(t2.GetID(), 3U);
    ASSERT_EQ(t2.GetSize(), 2U);

    ASSERT_TRUE(std::string(it1->GetName()) == "f32-type");
    ASSERT_TRUE(it1->GetType() == DataValueTypeFloat);
    ASSERT_EQ(it1->ReadFloat(), fl_cv);
    ++it1;

    ASSERT_TRUE(std::string(it1->GetName()) == "f64-type");
    ASSERT_TRUE(it1->GetType() == DataValueTypeDouble);
    ASSERT_EQ(it1->ReadDouble(), dbl_cv);
}

TEST(IEC61850ServerTypes, CheckDBStream)
{
    DataBlock block;
    DataBlockStream write_stream(block);
    DataBlockStream read_stream(block);
    for (size_t i = 0; i < RTH_IEC61850_DATABLOCK_SIZE; i++) {
        std::stringstream str;
        str << "TestValue" << i;
        ASSERT_TRUE(write_stream
            << DataValue(str.str().c_str(), DataValueTypeInt32, i));
    }

    ASSERT_FALSE(write_stream << DataValue("Overflow", DataValueTypeInt32,
                     RTH_IEC61850_DATABLOCK_SIZE + 100));

    DataValue read_value;
    for (size_t i = 0; i < RTH_IEC61850_DATABLOCK_SIZE; i++) {
        std::stringstream str;
        str << "TestValue" << i;

        ASSERT_TRUE(read_stream >> read_value);
        ASSERT_TRUE(std::string(read_value.GetName()) == str.str());
        ASSERT_EQ(read_value.GetType(), DataValueTypeInt32);
        ASSERT_EQ(read_value.ReadInt32(), static_cast<int32_t>(i));
    }

    ASSERT_FALSE(read_stream >> read_value);
}

TEST(IEC61850ServerTypes, CheckDBStreamMulti)
{
    const size_t kTestSize = 512;

    DataBlock* buffer = new DataBlock[kTestSize];

    DataBlockStream write_stream(buffer, kTestSize);
    DataBlockStream read_stream(buffer, kTestSize);

    for (size_t nblock = 0; nblock < kTestSize; nblock++) {
        for (size_t i = 0; i < RTH_IEC61850_DATABLOCK_SIZE; i++) {
            std::stringstream str;
            str << nblock << "TestValue" << i;
            ASSERT_TRUE(write_stream
                << DataValue(str.str().c_str(), DataValueTypeInt32, i));
        }
    }
    ASSERT_FALSE(write_stream << DataValue("Overflow", DataValueTypeInt32,
                     RTH_IEC61850_DATABLOCK_SIZE + 100));

    DataValue read_value;
    for (size_t nblock = 0; nblock < kTestSize; nblock++) {
        for (size_t i = 0; i < RTH_IEC61850_DATABLOCK_SIZE; i++) {
            std::stringstream str;
            str << nblock << "TestValue" << i;

            ASSERT_TRUE(read_stream >> read_value);
            ASSERT_TRUE(std::string(read_value.GetName()) == str.str());
            ASSERT_EQ(read_value.GetType(), DataValueTypeInt32);
            ASSERT_EQ(read_value.ReadInt32(), static_cast<int32_t>(i));
        }
    }

    ASSERT_FALSE(read_stream >> read_value);

    delete[] buffer;
}
