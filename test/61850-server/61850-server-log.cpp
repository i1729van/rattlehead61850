#include "base/network.h" // for MakeIpAddress
#include "iec61850-common/app-log.h" // for AppLogger
#include "iec61850-common/log-writers.h" // for LogStdoutWriter, LogUdp...
#include "iec61850-server/server-errors.h" // for ServerError, ServerErro...
#include "iec61850-server/server-log.h" // for LogError, LogInfo
#include "iec61850-server/server-settings.h" // for ServerSettings
#include "iec61850-server/server.h" // for Server
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPar...
#include <gtest/gtest.h> // for AssertionResult, GetBoo...
#include <string> // for string

using namespace rth::network;
using namespace rth::iec61850;
using namespace rth::iec61850::server;

/*
typedef struct {
  int i;
  float f;
  double d;
  const char *str;
} UserType;
*/
/*static const LogMessage &operator<<(const LogMessage &log,
                                    const UserType &utype) {
  // if (AppLogger::Instance().IsInfoEnable()) {
  std::stringstream &buffer = const_cast<LogMessage &>(log).GetStream();

  buffer << "[TEST USERTYPE] "
         << " int:" << utype.i << "  "
         << " float:" << utype.f << "  "
         << " double:" << utype.d << "  "
         << " str:" << utype.str << "  ";
  //}
  return log;
}*/

TEST(LoggingSuite, AllInOne)
{
    const char* dst_addr = "127.0.0.1:8888";
    LogStdoutWriter single_writer;
    LogUdpWriter udp_writer(MakeIpAddress(dst_addr));

    AppLogger::AddWriter(single_writer);
    AppLogger::AddWriter(udp_writer);

    LogInfo::Enable();
    LogError::Enable();

    /* Запись не разрежшена. Тестим. */
    /*LogInfo() << "\n\n-------- DUMMY TEST ---------\n";
  LogInfo() << "Normal message!\n";
  LogInfo() << dst_addr << " : UDP + StdOut error message!\n";

  UserType utype = {1, 1.111111F, 2.222222, "UserType!"};
  LogInfo() << utype << std::endl;
  LogInfo() << "-------- DUMMY TEST ---------\n\n\n";*/

    /* Запуск / останов*/
    const char* filename = "data/B20.icd";
    const char* iedname = "TEMPLATE";
    const char* conn_name = "RTH.BasicTestsInvalidConfig";

    const char* wrong_filename = "data/~~Non-existing-fie~~.icd";
    const char* wrong_iedname = "~~Wrong-Ied-Name~~";
    const char* config = "data/B20.icd";
    const char* config_with_errors = "data/B20-error.icd";

    ServerSettings settings1(wrong_filename, iedname, conn_name, 8888);
    Server server1(settings1);

    ASSERT_FALSE(server1.StartServer());
    ASSERT_FALSE(server1.IsRunning());
    ASSERT_EQ(server1.GetLastError(), ServerErrorInvalidConfig);
    server1.StopServer();
    ASSERT_FALSE(server1.IsRunning());

    ServerSettings settings2(filename, wrong_iedname, conn_name, 8888);
    Server server2(settings2);

    ASSERT_FALSE(server2.StartServer());
    ASSERT_FALSE(server2.IsRunning());
    ASSERT_EQ(server2.GetLastError(), ServerErrorInvalidModel);
    server2.StopServer();
    ASSERT_FALSE(server2.IsRunning());

    ServerSettings settings3(config_with_errors, iedname, conn_name, 8888);
    Server server3(settings3);

    ASSERT_FALSE(server3.StartServer());
    ASSERT_FALSE(server3.IsRunning());
    ASSERT_EQ(server3.GetLastError(), ServerErrorInvalidConfig);
    server3.StopServer();
    ASSERT_FALSE(server3.IsRunning());

    ServerSettings settings4(config, iedname, conn_name, 8888);
    Server server4(settings4);

    ASSERT_TRUE(server4.StartServer());
    ASSERT_TRUE(server4.IsRunning());
    ASSERT_EQ(server4.GetLastError(), ServerErrorNone);
    server4.StopServer();
    ASSERT_FALSE(server4.IsRunning());
}
