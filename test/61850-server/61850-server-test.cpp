#include "base/shared-memory.h" // for SharedMemory
#include "base/thread.h" // for Millisleep
#include "base/time.h" // for GetRealTimeMilli
#include "iec61850-common/config.h" // for RTH_IEC61850_SERVER_ST...
#include "iec61850-common/defs.h" // for DataAttributeUpdateResult
#include "iec61850-common/errors.h" // for ErrorList<>::ConstIter...
#include "iec61850-connector/connection.h" // for Connection
#include "iec61850-connector/send-result.h" // for SendResult, SingleReco...
#include "iec61850-server/server-errors.h" // for ServerError, ServerErr...
#include "iec61850-server/server-settings.h" // for ServerSettings
#include "iec61850-server/server-statistic.h" // for ServerStatistic, Input...
#include "iec61850-server/server.h" // for Server
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for GetBoolAssertionFailur...
#include <gtest/gtest.h> // for AssertionResult, ASSER...
#include <iostream> // for operator<<, basic_ostream
#include <list> // for _List_const_iterator
#include <stdlib.h> // for rand, exit
#include <string.h> // for memcpy
#include <string> // for string, allocator, ope...
#if defined(__linux__)
#include <sys/wait.h> // for waitpid
#include <unistd.h> // for fork, pid_t
#endif //#if defined (__linux__)

using namespace rth::iec61850;
using namespace rth::iec61850::server;
using namespace rth::iec61850::connector;

TEST(Server, BasicTests)
{
    /* Запуск / останов*/
    ServerSettings settings("data/B20.icd", "TEMPLATE", "RTH.BasicTests", 8888);
    Server server(settings);

    ASSERT_TRUE(server.StartServer());
    ASSERT_TRUE(server.IsRunning());
    server.StopServer();

    ASSERT_FALSE(server.IsRunning());
}

TEST(Server, BasicTestsRunTwice)
{
    /* Запуск / останов*/
    ServerSettings settings("data/B20.icd", "TEMPLATE", "RTH.BasicTestsRunTwice",
        8888);
    Server server(settings);

    ASSERT_TRUE(server.StartServer());
    ASSERT_TRUE(server.IsRunning());
    server.StopServer();
    ASSERT_FALSE(server.IsRunning());

    ASSERT_TRUE(server.StartServer());
    ASSERT_TRUE(server.IsRunning());
    server.StopServer();
    ASSERT_FALSE(server.IsRunning());
}

TEST(Server, BasicTestsInvalidConfig)
{
    /* Запуск / останов*/
    const char* filename = "data/B20.icd";
    const char* iedname = "TEMPLATE";
    const char* conn_name = "RTH.BasicTestsInvalidConfig";

    const char* wrong_filename = "data/~~Non-existing-fie~~.icd";
    const char* wrong_iedname = "~~Wrong-Ied-Name~~";

    const char* config_with_errors = "data/B20-error.icd";

    ServerSettings settings1(wrong_filename, iedname, conn_name, 8888);
    Server server1(settings1);

    ASSERT_FALSE(server1.StartServer());
    ASSERT_FALSE(server1.IsRunning());
    ASSERT_EQ(server1.GetLastError(), ServerErrorInvalidConfig);
    server1.StopServer();
    ASSERT_FALSE(server1.IsRunning());

    ServerSettings settings2(filename, wrong_iedname, conn_name, 8888);
    Server server2(settings2);

    ASSERT_FALSE(server2.StartServer());
    ASSERT_FALSE(server2.IsRunning());
    ASSERT_EQ(server2.GetLastError(), ServerErrorInvalidModel);
    server2.StopServer();
    ASSERT_FALSE(server2.IsRunning());

    ServerSettings settings3(config_with_errors, iedname, conn_name, 8888);
    Server server3(settings3);

    ASSERT_FALSE(server3.StartServer());
    ASSERT_FALSE(server3.IsRunning());
    ASSERT_EQ(server3.GetLastError(), ServerErrorInvalidConfig);
    server3.StopServer();
    ASSERT_FALSE(server3.IsRunning());
}

TEST(Server, BasicConnector)
{
    Connection connector1("asdfsdfsd");
    ASSERT_FALSE(connector1.Open());
    ASSERT_FALSE(connector1.IsOpen());

    const char* base_name = "RTH.BasicConnector";

    std::string full_input_name = std::string(base_name) + std::string(RTH_IEC61850_SERVER_INPUT_QUEUE_SUFFIX);
    std::string full_ack_name = std::string(base_name) + std::string(RTH_IEC61850_SERVER_INPUT_ACK_QUEUE_SUFFIX);

    DataQueue test_queue_in;
    ASSERT_TRUE(test_queue_in.Create(full_input_name.c_str(), 100));

    DataQueue test_queue_ack;
    ASSERT_TRUE(test_queue_ack.Create(full_ack_name.c_str(), 100));

    // Для коннектора необходима информаиця с версией API, иначе он не
    // подключится.
    // Обычно эту информацию предоставляет сервер. Но если его нет, запилим
    // руками.
    std::string api = "api:";
    std::string api_shm_name = base_name;
    api += RTH_IEC61850_SERVER_API_VERSION;
    api_shm_name += RTH_IEC61850_SERVER_STAT_SUFFIX;
    rth::ipc::SharedMemory api_info;
    ASSERT_TRUE(api_info.Create(api_shm_name.c_str(), 1024));
    memcpy(api_info.Data(), api.c_str(), api.size());

    // Подключаемся
    Connection connector2(base_name);
    ASSERT_TRUE(connector2.Open());
    ASSERT_TRUE(connector2.IsOpen());
    connector2.Close();

    ASSERT_TRUE(connector2.Open());
    ASSERT_TRUE(connector2.IsOpen());
    connector2.Close();
}

TEST(Server, ConnectorWithWrongApi)
{
    const char* base_name = "RTH.ConnectorWithWrongApi";
    std::string api = "api:";
    std::string api_shm_name = base_name;
    api += "wrong_api_ver";
    api_shm_name += RTH_IEC61850_SERVER_STAT_SUFFIX;
    rth::ipc::SharedMemory api_info;
    ASSERT_TRUE(api_info.Create(api_shm_name.c_str(), 1024));
    memcpy(api_info.Data(), api.c_str(), api.size());

    // подключатсья к серверу с другим API запрещено
    Connection connector2(base_name);
    ASSERT_FALSE(connector2.Open());
    ASSERT_FALSE(connector2.IsOpen());
    connector2.Close();
}

/* TODO: Добавить аналогичные тесты для Windows, но использовать потоки, т.к.
* fork в Windows плохо применим.
*/
#if defined(__linux__)

TEST(Server, RunServer)
{
    using namespace rth::thread;

    /* Запустить сервер. подключится к его входной очередиданных
   * по имени [постфикс _din] и предать туда значения */
    const char* conn_name = "RTH.RunServer";
    size_t ncycles = 50;

    pid_t pid = fork();
    ASSERT_NE(pid, -1);

    if (pid > 0) {
        const char* filename = "data/B20.icd";
        const char* iedname = "TEMPLATE";
        /* Родительский процесс - создаем сервер */
        ServerSettings settings(filename, iedname, conn_name);
        Server server(settings);

        ASSERT_TRUE(server.StartServer());
        ASSERT_TRUE(server.IsRunning());

        /* Ждем завершения дочернего процесса */
        waitpid(pid, 0, 0);
        ServerStatistic stat = server.GetStatistic();

        ASSERT_EQ(stat.input.Blocks(), ncycles);
        ASSERT_EQ(stat.input.Values(), ncycles * 4);
        ASSERT_EQ(stat.input.Errors(), ncycles);

        std::cout << "Blocks:" << stat.input.Blocks() << "\n";
        std::cout << "Values:" << stat.input.Values() << "\n";
        std::cout << "Errors:" << stat.input.Errors() << "\n";
        std::cout << "AvgCycle, ms:" << stat.input.AvgCycleMilli() << "\n";
        std::cout << "AvgLock, ms:" << stat.input.AvgLockMilli() << "\n";

        server.StopServer();
    } else if (pid == 0) {
        /* Дочерний процесс. Подцепляемся к серверу и отправляем данные */
        Connection connection(conn_name);

        for (int i = 0; i < 10; i++) {
            if (connection.Open()) {
                break;
            } else {
                Millisleep(100);
            }
        }

        ASSERT_TRUE(connection.IsOpen());

        do {
            DataBlock out;
            DataBlockStream stream(out);
            ASSERT_TRUE(stream << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.cVal.mag.f",
                            DataValueTypeFloat, ncycles));

            ASSERT_TRUE(stream << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.t",
                            DataValueTypeUInt64,
                            rth::time::GetRealTimeMilli()));

            ASSERT_TRUE(stream << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.q",
                            DataValueTypeUInt16, rand() % 8));

            ASSERT_TRUE(
                stream << DataValue("InvalidTag", DataValueTypeUInt16, rand() % 8));

            SendResult result = connection.Send(*stream.Blocks());
            TransactionErrors errors = result.GetErrorList();
            TransactionErrors::ConstIterator it = errors.Begin();

            // ErrorsToStream<TransactionErrors, std::ostream>(errors, std::cout);
            ASSERT_EQ(result.GetSentCounter(), 1U);
            ASSERT_EQ(result.GetAckCounter(), 1U);
            ASSERT_FALSE(result.IsOk());
            ASSERT_EQ(result.GetErrorsCount(), 1U);
            ASSERT_EQ(it++->GetResult(), DataAttributeUpdateResultTagIsMissing);

            Millisleep(90);
            if (ncycles % 10 == 0) {
                std::cout << "Client works..." << ncycles / 10 << "\n";
            }

        } while (--ncycles > 0);
        connection.Close();
        exit(0);
    }
}

TEST(Server, RunServerMultiblocks)
{
    using namespace rth::thread;

    /* Запустить сервер. подключится к его входной очередиданных
   * по имени [постфикс _din] и предать туда значения */
    const char* conn_name = "RTH.RunServerMultiblocks";

    pid_t pid = fork();
    ASSERT_NE(pid, -1);

    if (pid > 0) {
        const char* filename = "data/B20.icd";
        const char* iedname = "TEMPLATE";
        /* Родительский процесс - создаем сервер */
        ServerSettings settings(filename, iedname, conn_name);
        Server server(settings);

        ASSERT_TRUE(server.StartServer());
        ASSERT_TRUE(server.IsRunning());

        /* Ждем завершения дочернего процесса */
        waitpid(pid, 0, 0);
        server.StopServer();
    } else if (pid == 0) {
        /* Дочерний процесс. Подцепляемся к серверу и отправляем данные */
        Connection connection(conn_name);

        for (size_t i = 0; i < 10; i++) {
            if (connection.Open()) {
                break;
            } else {
                Millisleep(100);
            }
        }

        ASSERT_TRUE(connection.IsOpen());
        size_t timeout = 50;

        do {
            DataBlock out[4];
            DataBlockStream stream(out, 4);
            for (size_t i = 0; i < 4; i++) {
                for (size_t k = 0; k < RTH_IEC61850_DATABLOCK_SIZE; k += 4) {
                    ASSERT_TRUE(stream
                        << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.cVal.mag.f",
                               DataValueTypeFloat, timeout));

                    ASSERT_TRUE(stream << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.t",
                                    DataValueTypeUInt64,
                                    rth::time::GetRealTimeMilli()));

                    ASSERT_TRUE(stream << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.q",
                                    DataValueTypeUInt16, rand() % 8));
                }
            }
            SendResult result = connection.Send(stream.Blocks(), stream.Size());

            ASSERT_EQ(result.GetSentCounter(), 4U);
            ASSERT_EQ(result.GetAckCounter(), 4U);
            ASSERT_TRUE(result.IsOk());

            Millisleep(80);
            if (timeout % 10 == 0) {
                std::cout << "Client works..." << timeout / 10 << "\n";
            }

        } while (--timeout > 0);
        connection.Close();
        exit(0);
    }
}

TEST(Server, RunServerMultiblocksWithEmpties)
{
    using namespace rth::thread;

    /* Запустить сервер. подключится к его входной очередиданных
   * по имени [постфикс _din] и предать туда значения */
    const char* conn_name = "RTH.RunServerMultiblocks";

    pid_t pid = fork();
    ASSERT_NE(pid, -1);

    if (pid > 0) {
        const char* filename = "data/B20.icd";
        const char* iedname = "TEMPLATE";
        /* Родительский процесс - создаем сервер */
        ServerSettings settings(filename, iedname, conn_name);
        Server server(settings);

        ASSERT_TRUE(server.StartServer());
        ASSERT_TRUE(server.IsRunning());

        /* Ждем завершения дочернего процесса */
        waitpid(pid, 0, 0);
        server.StopServer();
    } else if (pid == 0) {
        /* Дочерний процесс. Подцепляемся к серверу и отправляем данные */
        Connection connection(conn_name);

        for (int i = 0; i < 10; i++) {
            if (connection.Open()) {
                break;
            } else {
                Millisleep(100);
            }
        }

        ASSERT_TRUE(connection.IsOpen());
        size_t timeout = 50;

        do {
            DataBlock out[4];
            DataBlockStream stream(out, 4);
            for (int i = 0; i < 4; i++) {
                ASSERT_TRUE(stream << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.cVal.mag.f",
                                DataValueTypeFloat, timeout));

                ASSERT_TRUE(stream << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.t",
                                DataValueTypeUInt64,
                                rth::time::GetRealTimeMilli()));

                ASSERT_TRUE(stream << DataValue("TEMPLATELD0/MMXU3.PhV.phsA.q",
                                DataValueTypeUInt16, rand() % 8));

                ASSERT_TRUE(
                    stream << DataValue("InvalidTag", DataValueTypeUInt16, rand() % 8));
            }

            SendResult result = connection.Send(stream.Blocks(), stream.Size());
            TransactionErrors errors = result.GetErrorList();
            TransactionErrors::ConstIterator it = errors.Begin();

            ASSERT_EQ(result.GetSentCounter(), 4U);
            ASSERT_EQ(result.GetAckCounter(), 4U);
            ASSERT_FALSE(result.IsOk());
            ASSERT_EQ(result.GetErrorsCount(), 4U);
            ASSERT_EQ(it++->GetResult(), DataAttributeUpdateResultTagIsMissing);
            ASSERT_EQ(it++->GetResult(), DataAttributeUpdateResultTagIsMissing);
            ASSERT_EQ(it++->GetResult(), DataAttributeUpdateResultTagIsMissing);
            ASSERT_EQ(it++->GetResult(), DataAttributeUpdateResultTagIsMissing);

            Millisleep(80);
            if (timeout % 10 == 0) {
                std::cout << "Client works..." << timeout / 10 << "\n";
            }

        } while (--timeout > 0);
        connection.Close();
        exit(0);
    }
}
#endif //#if defined (__linux__)

TEST(Server, Statistic)
{
    const char* name = "RTH.Server.Statistic";
    char buffer[SharedStatistic::kChunkSize] = { 0 };
    ServerStatistic stat;
    SharedStatistic shared(name);
    rth::ipc::SharedMemory memory;

    stat.pid = 12345;
    stat.input.AddCycleTime(1);
    stat.input.AddLockTime(1);

    stat.input.AddDBCounter(9999999999999ULL);
    stat.input.AddValueCounter(9999999999999ULL);
    stat.input.AddErrorCounter(9999999999999ULL);
    stat.input.LastUpdateTimestamp(9999999999999ULL);
    ASSERT_FALSE(shared.IsOpen());

    ASSERT_TRUE(shared.Create());
    ASSERT_TRUE(shared.Update(stat));
    ASSERT_TRUE(
        memory.Open(std::string(std::string(name) + std::string(RTH_IEC61850_SERVER_STAT_SUFFIX))
                        .c_str()));
    ASSERT_TRUE(memory.Size() == SharedStatistic::kChunkSize);

    memory.Lock();
    memcpy(buffer, memory.Data(), SharedStatistic::kChunkSize);
    memory.Unlock();

    /*
   * std::string ToString(const ServerStatistic &statistic) {
    std::stringstream ss;
    ss << "api:" << statistic.api_version << "\n";
    ss << "avg_cycle_ms:" << statistic.input.AvgCycleMilli() << "\n";
    ss << "avg_lock_ms:" << statistic.input.AvgLockMilli() << "\n";
    ss << "block_proc:" << statistic.input.Blocks() << "\n";
    ss << "values_proc:" << statistic.input.Values() << "\n";
    ss << "errors:" << statistic.input.Errors() << "\n";
    ss << "last_update:" << statistic.input.LastUpdateTimestamp() << "\n";
    return ss.str();
  }

   * */

    std::string result = buffer;
    std::string api_str = "api:";
    api_str += RTH_IEC61850_SERVER_API_VERSION;
    ASSERT_FALSE(result.find("pid:12345") == std::string::npos);
    ASSERT_FALSE(result.find(api_str) == std::string::npos);
    ASSERT_FALSE(result.find("avg_cycle_ms:1") == std::string::npos);
    ASSERT_FALSE(result.find("avg_lock_ms:1") == std::string::npos);
    ASSERT_FALSE(result.find("block_proc:9999999999999") == std::string::npos);
    ASSERT_FALSE(result.find("values_proc:9999999999999") == std::string::npos);
    ASSERT_FALSE(result.find("errors:9999999999999") == std::string::npos);
    ASSERT_FALSE(result.find("last_update:9999999999999") == std::string::npos);
}
