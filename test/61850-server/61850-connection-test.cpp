#include "iec61850-common/config.h" // for RTH_IEC61850_DATABLOCK_SIZE
#include "iec61850-common/defs.h" // for DataValue, DataBlock, DataValueType
#include "iec61850-connector/connection.h"
#include "iec61850-server/server-statistic.h"
#include <assert.h> // for assert
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResult::...
#include <gtest/gtest.h> // for AssertionResult, IsNullLiteralHe...
#include <ostream> // for operator<<, basic_ostream::opera...
#include <stdint.h> // for uint32_t
#include <stdlib.h> // for exit, size_t
#include <string> // for string, operator==, basic_string

#if defined(__linux__)
#include <sys/wait.h> // for waitpid
#include <unistd.h> // for usleep, fork
#endif //#if defined(__linux__)

using namespace rth::iec61850;

static void FillTransaction(DataBlock* t, uint32_t id)
{
    /* Лениво чистить существующий блок.
   * Будем создавать новый, а затем запуливать его. */
    DataBlock new_block;
    DataValue trans_array[RTH_IEC61850_DATABLOCK_SIZE];
    DataBlockStream stream(new_block);
    assert(RTH_IEC61850_DATABLOCK_SIZE >= 4);

    new_block.SetID(id);

    trans_array[0].SetName("i8-type");
    trans_array[0].SetType(DataValueTypeInt8);
    trans_array[0].Write<int8_t>(static_cast<int8_t>(id));

    trans_array[1].SetName("i32-type");
    trans_array[1].SetType(DataValueTypeInt32);
    trans_array[1].Write<int32_t>(static_cast<int32_t>(id));

    trans_array[2].SetName("f32-type");
    trans_array[2].SetType(DataValueTypeFloat);
    trans_array[2].Write<float>(id);

    trans_array[3].SetName("f64-type");
    trans_array[3].SetType(DataValueTypeDouble);
    trans_array[3].Write<double>(id);

    stream << trans_array[0];
    stream << trans_array[1];
    stream << trans_array[2];
    stream << trans_array[3];

    *t = *stream.Blocks();
}

/* TODO: Добавить аналогичные тесты для Windows, но использовать потоки, т.к.
* fork в Windows плохо применим.
*/
#if defined(__linux__)
TEST(IEC61850ServerConnection, CheckDataExchangeSingleRecord)
{
    /* Запускает два процесса, связанных одной очередью данных.
   * Родительский процесс созает очередь и заполянет её значениями
   * по одному за раз. Так до тех пор пока он не сгенерирует kMaxMessages
   * сообщенийю.
   * Дочерний процесс подключается к очереди и вычитвает данные.
   * В процессе чтения он проверяет корректность данных по номеру транзакции и
   * содержимому данных */
    const uint32_t kMaxMessages = 64 * 1024;
    const uint32_t kMaxQueueSize = 512;
    const char* name = "RTH.CheckDataExchangeSingleRecord";

    pid_t pid = fork();
    if (pid > 0) {
        /* Родительский процесс. Набиваем очередь данными */
        DataQueue* producer = new DataQueue;
        ASSERT_TRUE(producer->Create(name, kMaxQueueSize)) << "Can't create queue";

        DataBlock* tout = new DataBlock;
        /* Заполняем и отправляем по одному сообщению */
        for (uint32_t i = 0; i < kMaxMessages; i++) {
            FillTransaction(tout, i);
            for (int r = 0; r < 10 && producer->Push(tout) == 0; r++) {
                usleep(1000);
            }
        }

        waitpid(pid, 0, 0);
        delete producer;
        delete tout;

    } else if (pid == 0) {
        /* Дочерний процесс - получаем данные */
        DataQueue* consumer = new DataQueue;
        DataBlock* tin = new DataBlock;
        /* Пытаемся подключится к очереди. Родительскому процессу потребуется
     * какое-то
     * время на создание. Поэтому выполянем подключение в цикле. */
        for (int i = 0; i < 1000 && consumer->Open(name) == false; i++) {
            usleep(100);
        }

        ASSERT_TRUE(consumer->IsOpen()) << "Can't open queue";
        uint32_t trans_id = 0;
        int failed_cnt = 0;

        /* Вычитываем и проверяем сообщения. */
        while (trans_id < kMaxMessages && failed_cnt < 10) {
            if (consumer->Pop(tin)) {
                ASSERT_TRUE(trans_id == tin->GetID());
                ASSERT_EQ(tin->GetSize(), 4U);

                const DataValue* trans_array = tin->GetData();

                ASSERT_TRUE(std::string(trans_array[0].GetName()) == "i8-type");
                ASSERT_EQ(trans_array[0].GetType(), DataValueTypeInt8);
                ASSERT_EQ(trans_array[0].ReadInt8(), static_cast<int8_t>(trans_id));

                ASSERT_TRUE(std::string(trans_array[1].GetName()) == "i32-type");
                ASSERT_EQ(trans_array[1].GetType(), DataValueTypeInt32);
                ASSERT_EQ(trans_array[1].ReadInt32(), static_cast<int32_t>(trans_id));

                ASSERT_TRUE(std::string(trans_array[2].GetName()) == "f32-type");
                ASSERT_EQ(trans_array[2].GetType(), DataValueTypeFloat);
                ASSERT_EQ(trans_array[2].ReadFloat(), static_cast<float>(trans_id));

                ASSERT_TRUE(std::string(trans_array[3].GetName()) == "f64-type");
                ASSERT_EQ(trans_array[3].GetType(), DataValueTypeDouble);
                ASSERT_EQ(trans_array[3].ReadDouble(), static_cast<double>(trans_id));
                trans_id++;
                failed_cnt = 0;
            } else {
                usleep(1000);
                failed_cnt++;
            }
        }
        delete consumer;
        delete tin;
        ASSERT_EQ(trans_id, kMaxMessages);
        exit(0); //< Выход отсюда запретит повторное выполнение тестов форнкутым
        //процессом.
        // Хотя это будет приводить к still reachable паямти в VG. Не слишком
        // страшно
    }
}

TEST(IEC61850ServerConnection, CheckDataExchangeMultiRecord)
{
    /* Запускает два процесса, связанных одной очередью данных.
  * Родительский процесс созает очередь и заполянет её  группами значений.
  * Так до тех пор пока он не сгенерирует kMaxMessages
  * сообщений.
  * Дочерний процесс подключается к очереди и вычитвает данные. Тоже группами.
  * В процессе чтения он проверяет корректность данных по номеру транзакции и
  * содержимому данных.
  * Основная идея - проверить разницу между одиночным / групповым чтением данных
  */
    const size_t kMaxMessages = 64 * 1024;
    const size_t kMaxQueueSize = 512;
    const size_t kBlockSize = 64;
    const char* name = "RTH.CheckDataExchangeMultiRecord";
    pid_t pid = fork();
    if (pid > 0) {
        /* Родительский процесс. Набиваем очередь данными */

        DataQueue* producer = new DataQueue;
        ASSERT_TRUE(producer->Create(name, kMaxQueueSize))
            << "Can't create queue " << producer->GetLastError();
        uint32_t trans_id = 0;

        DataBlock* tout = new DataBlock[kBlockSize];
        while (trans_id < kMaxMessages) {
            /* Проинициализировать группу данных */
            for (uint32_t i = 0; i < kBlockSize; i++) {
                FillTransaction(tout + i, trans_id++);
            }

            /* Записать группу. Группа может записаться не сразу. Например, клиент не
       * успевает
       * вычитывать все сообщения. Поэтому отправка сделана в цикле. */
            for (size_t r = 0, pack_num = 0; r < 100 && pack_num < kBlockSize; r++) {
                pack_num += producer->Push(tout + pack_num, kBlockSize - pack_num);
                if (pack_num != kBlockSize)
                    usleep(1000);
                // std::cout <<"Writer: Sleep\n";
            }
        }

        waitpid(pid, 0, 0);
        delete producer;
        delete[] tout;

    } else if (pid == 0) {
        /* Дочерний процесс - получаем данные */

        DataQueue* consumer = new DataQueue;
        DataBlock* tin = new DataBlock[kBlockSize];

        for (int i = 0; i < 1000 && consumer->Open(name) == false; i++) {
            usleep(100);
        }

        ASSERT_TRUE(consumer->IsOpen()) << "Can't open queue";
        uint32_t trans_id = 0;
        int failed_cnt = 0;

        while (trans_id < kMaxMessages && failed_cnt < 50) {
            size_t result = consumer->Pop(tin, kBlockSize);

            if (result == 0) {
                usleep(1000);
                failed_cnt++;
                // std::cout <<"Reader: Sleep\n";
            } else {
                /* Проходим по группе и читаем данные. Тут мало отличий от одиночного
         * чтения */
                for (size_t i = 0; i < result; i++) {
                    DataBlock* curblock = tin + i;
                    ASSERT_TRUE(trans_id == curblock->GetID());
                    ASSERT_EQ(curblock->GetSize(), 4U);
                    const DataValue* trans_array = curblock->GetData();

                    ASSERT_TRUE(std::string(trans_array[0].GetName()) == "i8-type");
                    ASSERT_EQ(trans_array[0].GetType(), DataValueTypeInt8);
                    ASSERT_EQ(trans_array[0].ReadInt8(), static_cast<int8_t>(trans_id));

                    ASSERT_TRUE(std::string(trans_array[1].GetName()) == "i32-type");
                    ASSERT_EQ(trans_array[1].GetType(), DataValueTypeInt32);
                    ASSERT_EQ(trans_array[1].ReadInt32(), static_cast<int32_t>(trans_id));

                    ASSERT_TRUE(std::string(trans_array[2].GetName()) == "f32-type");
                    ASSERT_EQ(trans_array[2].GetType(), DataValueTypeFloat);
                    ASSERT_EQ(trans_array[2].ReadFloat(), static_cast<float>(trans_id));

                    ASSERT_TRUE(std::string(trans_array[3].GetName()) == "f64-type");
                    ASSERT_EQ(trans_array[3].GetType(), DataValueTypeDouble);
                    ASSERT_EQ(trans_array[3].ReadDouble(), static_cast<double>(trans_id));

                    trans_id++;
                    failed_cnt = 0;
                }
            }
        }
        delete consumer;
        delete[] tin;
        ASSERT_EQ(trans_id, kMaxMessages);
        exit(0); //< Выход отсюда запретит повторное выполнение тестов форнкутым
        //процессом.
        // Хотя это будет приводить к still reachable паямти в VG. Не слишком
        // страшно
    }
}
#endif //#if defined(__linux__)

TEST(IEC61850ServerConnection, Statistic)
{
    const char* name = "RTH.IEC61850ServerConnection.Statistic";

    server::ServerStatistic stat;
    server::SharedStatistic shared(name);

    stat.input.AddCycleTime(1);
    stat.input.AddLockTime(1);

    stat.input.AddDBCounter(9999999999999ULL);
    stat.input.AddValueCounter(9999999999999ULL);
    stat.input.AddErrorCounter(9999999999999ULL);

    ASSERT_FALSE(shared.IsOpen());
    ASSERT_TRUE(shared.Create());
    ASSERT_TRUE(shared.Update(stat));

    /*
 * std::string ToString(const ServerStatistic &statistic) {
std::stringstream ss;
ss << "api:" << statistic.api_version << "\n";
ss << "avg_cycle_ms:" << statistic.input.AvgCycleMilli() << "\n";
ss << "avg_lock_ms:" << statistic.input.AvgLockMilli() << "\n";
ss << "block_proc:" << statistic.input.Blocks() << "\n";
ss << "values_proc:" << statistic.input.Values() << "\n";
ss << "errors:" << statistic.input.Errors() << "\n";
return ss.str();
}*/

    std::string result = connector::GetServerInfo(name);
    std::string api_str = "api:";
    api_str += RTH_IEC61850_SERVER_API_VERSION;
    ASSERT_FALSE(result.find(api_str) == std::string::npos);
    ASSERT_FALSE(result.find("avg_cycle_ms:1") == std::string::npos);
    ASSERT_FALSE(result.find("avg_lock_ms:1") == std::string::npos);
    ASSERT_FALSE(result.find("block_proc:9999999999999") == std::string::npos);
    ASSERT_FALSE(result.find("values_proc:9999999999999") == std::string::npos);
    ASSERT_FALSE(result.find("errors:9999999999999") == std::string::npos);
}
