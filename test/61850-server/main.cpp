#include <gtest/gtest.h> // for InitGoogleTest, RUN_ALL_TESTS
#include <stdlib.h> // for srand
#include <time.h> // for time

int main(int argc, char** argv)
{
    srand(static_cast<unsigned int>(time(0)));
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
