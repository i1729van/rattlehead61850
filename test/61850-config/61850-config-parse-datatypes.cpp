#include "iec61850-config/base-types.h" // for DataType, DataAttribute
#include "iec61850-config/reader.h" // for Reader, ReaderInterface
#include "iec61850-config/xml.h" // for ParseContext
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResu...
#include <gtest/gtest.h> // for IsNullLiteralHelper, Asserti...
#include <stddef.h> // for NULL
#include <string> // for operator==, string, basic_st...

using namespace rth::iec61850;

/* ---------------------------------------------------------------------------
 * Тесты для проверки связки ридера и интерфейса
 * Интерфейс д.б. производным от DataTemplateDefaultOperator
 * ---------------------------------------------------------------------------
 */

/* ---------------------------------------------------------------------------
 *  Простые проверки типов и аттрибутов
 * ---------------------------------------------------------------------------
 */
class SimpleOperator : public configuration::ReaderInterface {
public:
    SimpleOperator()
        : kSps_Sepam204080_cv(16)
        , gSps_Sepam204080_cnt(0)
        , gInSps_Sepam204080(false)
        , gSps_Sepam204080AttribCnt(0)
        , gSps_Sepam204080AttribFlags(0)
    {
    }

    int DataTypeHandler(const configuration::DataType& type,
        const configuration::ParseContext& context)
    {
        gInSps_Sepam204080 = (type.id == "Sps_Sepam204080" && type.cdc == "SPS");
        return context.stop;
    }

    int DataAttributeHandler(const configuration::DataAttribute& attribute,
        const configuration::ParseContext& context)

    {
        if (attribute.type == "Sps_Sepam204080")
            gSps_Sepam204080_cnt++;

        if (gInSps_Sepam204080) {
            if (attribute.name == "stVal" && attribute.bType == "BOOLEAN" && attribute.dchg == "true" && attribute.fc == "ST") {
                gSps_Sepam204080AttribFlags |= 0x1;
                gSps_Sepam204080AttribCnt++;
            }
            if (attribute.name == "q" && attribute.bType == "Quality" && attribute.qchg == "true" && attribute.fc == "ST") {
                gSps_Sepam204080AttribFlags |= 0x2;
                gSps_Sepam204080AttribCnt++;
            }
            if (attribute.name == "t" && attribute.bType == "Timestamp" && attribute.fc == "ST") {
                gSps_Sepam204080AttribFlags |= 0x4;
                gSps_Sepam204080AttribCnt++;
            }
        }
        return context.stop;
    }

public:
    const int kSps_Sepam204080_cv;
    int gSps_Sepam204080_cnt;
    bool gInSps_Sepam204080;
    int gSps_Sepam204080AttribCnt;
    int gSps_Sepam204080AttribFlags;
};

/* ---------------------------------------------------------------------------
 *  Расширенная проверка по типам
 * ---------------------------------------------------------------------------
 */
typedef struct {
    const char* first;
    const char* second;
} ControlPairs;

static bool PairInRange(const ControlPairs* pair, const ControlPairs pairs[]);
static bool PairInRange(const ControlPairs* pair, const ControlPairs pairs[])
{
    std::string first, second;
    for (int i = 0; pairs[i].first; i++) {
        first = pair->first;
        second = pair->second;

        if (first == pairs[i].first && second == pairs[i].second)
            return true;
    }
    return false;
}

class ExtOperator : public configuration::ReaderInterface {
public:
    ExtOperator()
        : kLNodeTypeLimit(13)
        , kDATypeLimit(5)
        , kDOTypeLimit(25)
        , kEnumTypeLimit(8)
        , gLNodeTypeCnt(0)
        , gDATypeCnt(0)
        , gDOTypeCnt(0)
        , gEnumTypeCnt(0)
    {
    }

    int DataTypeHandler(const configuration::DataType& type,
        const configuration::ParseContext& context)
    {
        const ControlPairs lnode_data[] = {
            { "CSWI_Sepam204080", "CSWI" }, { "MES_GGIO_Sepam2040", "GGIO" },
            { "LPHD_Sepam2040", "LPHD" }, { "LLN0_Sepam20", "LLN0" },
            { "V_MMXU_Sepam2080", "MMXU" }, { "MSQI_Sepam204080", "MSQI" },
            { "RDRE_Sepam204080", "RDRE" }, { "XCBR_Sepam2040", "XCBR" },
            { "PTOF_Sepam204080", "PTOF" }, { "PTOV_Sepam204080", "PTOV" },
            { "PTRC_Sepam20", "PTRC" }, { "PTUF_Sepam204080", "PTUF" },
            { "PTUV_Sepam204080", "PTUV" }, { NULL, NULL }
        };

        const ControlPairs da_data[] = {
            { "cValSepam", "" }, { "analogValSepam", "" }, { "operSepam", "" },
            { "cancelSepam", "" }, { "originatorSepam", "" }, { NULL, NULL }
        };

        const ControlPairs do_data[] = {
            { "SPS", "Sps_Sepam204080" }, { "SPS", "SpsEx_Sepam204080" },
            { "INS", "Beh_Sepam204080" }, { "INS", "Health_Sepam204080" },
            { "INS", "CBOpCap_Sepam204080" }, { "INS", "Ins_Sepam204080" },
            { "INS", "InsEx_Sepam204080" }, { "ACT", "Act_Sepam204080" },
            { "ACD", "Acd_Sepam204080" }, { "BCR", "Bcr_Sepam204080" },
            { "MV", "Mv_Sepam204080" }, { "CMV", "Cmv_Sepam204080" },
            { "WYE", "WyeABCr_Sepam204080" }, { "DEL", "Del_Sepam204080" },
            { "SEQ", "Seq_Sepam204080" }, { "SPC", "Spc_Sepam204080" },
            { "SPC", "SpcEx_Sepam204080" }, { "SPC", "SpcStOnly_Sepam204080" },
            { "SPC", "SpcCoOnly_Sepam204080" }, { "DPC", "Pos_Sepam204080" },
            { "DPC", "PosStOnly_Sepam204080" }, { "INC", "Mod_Sepam204080" },
            { "DPL", "Dpl_Sepam204080" }, { "LPL", "LplLLN0_Sepam204080" },
            { "LPL", "Lpl_Sepam204080" }, { NULL, NULL }
        };

        const ControlPairs enum_data[] = {
            { "Beh", "" }, { "Health", "" }, { "ctlModel", "" },
            { "sboClass", "" }, { "dir", "" }, { "seqT", "" },
            { "CBOpCap", "" }, { "orCategory", "" }, { NULL, NULL }
        };

        if (type.element == "LNodeType") {
            ControlPairs pair;
            pair.first = type.id.c_str();
            pair.second = type.lnClass.c_str();
            if (PairInRange(&pair, lnode_data)) {
                gLNodeTypeCnt++;
            }
        } else if (type.element == "DAType") {
            ControlPairs pair;
            pair.first = type.id.c_str();
            pair.second = type.lnClass.c_str();
            if (PairInRange(&pair, da_data)) {
                gDATypeCnt++;
            }
        } else if (type.element == "DOType") {
            ControlPairs pair;
            pair.first = type.cdc.c_str();
            pair.second = type.id.c_str();
            if (PairInRange(&pair, do_data)) {
                gDOTypeCnt++;
            }
        } else if (type.element == "EnumType") {
            ControlPairs pair;
            pair.first = type.id.c_str();
            pair.second = "";
            if (PairInRange(&pair, enum_data)) {
                gEnumTypeCnt++;
            }
        }
        return context.stop;
    }

public:
    const int kLNodeTypeLimit;
    const int kDATypeLimit;
    const int kDOTypeLimit;
    const int kEnumTypeLimit;

    int gLNodeTypeCnt;
    int gDATypeCnt;
    int gDOTypeCnt;
    int gEnumTypeCnt;
};

TEST(ScdSuite, BasicDatatypeReading)
{
    /* Проверить работу ридера с произвольным пользовательсим интефрейсом.
   * SimpleOperator выполянет простые подсечты вхождения узлов */
    const char* filename = "data/B20.icd";
    SimpleOperator op1;
    configuration::Reader reader(op1);
    ASSERT_TRUE(reader.Read(filename, ""));
    ASSERT_EQ(op1.gSps_Sepam204080_cnt, op1.kSps_Sepam204080_cv);
    ASSERT_EQ(op1.gSps_Sepam204080AttribCnt, 3);
    ASSERT_EQ(op1.gSps_Sepam204080AttribFlags, 0x1 | 0x2 | 0x4);

    /* Проверить работу ридера с произвольным пользовательсим интефрейсом.
   * ExtOperator выполянет проверки по описанию типов данных  */
    ExtOperator op2;
    configuration::Reader reader2(op2);
    ASSERT_TRUE(reader2.Read(filename, ""));
    ASSERT_EQ(op2.gLNodeTypeCnt, op2.kLNodeTypeLimit);
    ASSERT_EQ(op2.gDATypeCnt, op2.kDATypeLimit);
    ASSERT_EQ(op2.gDOTypeCnt, op2.kDOTypeLimit);
    ASSERT_EQ(op2.gEnumTypeCnt, op2.kEnumTypeLimit);
}

/*
void DataTypeHandler(const DataType &type) {
  using namespace std;
  cout << "----  Type  ----" << endl;
  cout << "id:" << type.id << endl;
  cout << "iedType:" << type.iedType << endl;
  cout << "lnClass:" << type.lnClass << endl;
  cout << "cdc:" << type.cdc << endl;
  cout << "----------------" << endl;
  cout << endl;
}

void DataAttributeHandler(const DataAttribute &attribute) {
  using namespace std;
  cout << "----  Attribute  ----" << endl;
  cout << "name:" << attribute.name << endl;
  // cout << "sAddr:" << attribute.sAddr  << endl;
  cout << "bType:" << attribute.bType << endl;
  cout << "valKind:" << attribute.valKind << endl;
  cout << "type:" << attribute.type << endl;
  cout << "fc:" << attribute.fc << endl;
  cout << "dchg:" << attribute.dchg << endl;
  cout << "qchg:" << attribute.qchg << endl;
  cout << "dupd:" << attribute.dupd << endl;
  cout << "count:" << attribute.count  << endl;
  cout << "desc:" << attribute.desc  << endl;
  cout << "accessControl:" << attribute.accessControl  << endl;
  cout << "transient:" << attribute.transient  << endl;
  cout << "----------------" << endl;
  cout << endl;
}
*/
