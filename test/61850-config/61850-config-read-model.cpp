#include "iec61850-config/base-types.h" // for FCDA, LogicalNode, Report
#include "iec61850-config/reader.h" // for Reader, ReaderInterface
#include "iec61850-config/xml.h" // for ParseContext
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResu...
#include <gtest/gtest.h> // for AssertionResult, IsNullLiter...
#include <iostream> // for operator<<, basic_ostream::o...
#include <string> // for operator!=, string

using namespace rth::iec61850;

class ReadPerformer : public configuration::ReaderInterface {
public:
    ReadPerformer()
        : gIedCounter(0)
        , gLDCounter(0)
        , gDScounter(0)
        , gFCDACounter(0)
        , gLNCounter(0)
        , gRptCounter(0)
    {
    }

    int IEDHandler(const configuration::IED& ied,
        const configuration::ParseContext& context)
    {
        if (ied.name != "") {
            gIedCounter++;
        }
        // std::cout << "Visit IED: " << ied.name << "\n";
        return context.stop;
    }
    int LogicalDeviceHandler(const configuration::LogicalDevice& ld,
        const configuration::ParseContext& context)
    {
        if (ld.inst != "") {
            gLDCounter++;
        }
        // std::cout << "Visit LD: " << ld.inst << "\n";
        return context.stop;
    }
    int LogicalNodeHandler(const configuration::LogicalNode& ln,
        const configuration::ParseContext& context)
    {
        if (ln.lnType != "" || ln.lnClass != "") {
            gLNCounter++;
        }
        // std::cout << "Visit LN: " << ln.prefix << ln.lnClass << ln.inst << "\n";
        return context.stop;
    }
    int DataSetHandler(const configuration::DataSet& ds,
        const configuration::ParseContext& context)
    {
        if (ds.name != "") {
            gDScounter++;
        }
        // std::cout << "Visit DS: " << ds.name << "\n";
        return context.stop;
    }

    int FCDAHandler(const configuration::FCDA& fcda,
        const configuration::ParseContext& context)
    {
        if (fcda.ldInst != "" || fcda.prefix != "" || fcda.fc != "") {
            gFCDACounter++;
        }
        /* std::cout << "Visit FCDA: " << fcda.prefix << fcda.lnClass << fcda.lnInst
               << fcda.fc << "\n";*/
        return context.stop;
    }

    int ReportHandler(const configuration::Report& rcb,
        const configuration::ParseContext& context)
    {
        if (rcb.name != "" || rcb.datSet != "") {
            gRptCounter++;
        }
        /*std::cout << "Visit RCB: " << rcb.name << " DatSet=" << rcb.datSet
              << " bufTime=" << rcb.bufTime << " buffered=" << rcb.buffered
              << " dchg=" << rcb.dchg << " qchg=" << rcb.qchg
              << " dupd=" << rcb.dupd << " max=" << rcb.max << "\n";*/
        return context.stop;
    }

public:
    int gIedCounter;
    int gLDCounter;
    int gDScounter;
    int gFCDACounter;
    int gLNCounter;
    int gRptCounter;
};

TEST(ScdSuite, BasicModelReading)
{
    const char* filename = "data/B20.icd";
    ReadPerformer op1;
    configuration::Reader rc1(op1);
    ASSERT_TRUE(rc1.Read(filename, "TEMPLATE"));

    std::cout << "IED:" << op1.gIedCounter << " LD:" << op1.gLDCounter
              << " DS:" << op1.gDScounter << " FCDA:" << op1.gFCDACounter
              << " RCB:" << op1.gRptCounter << " LN:" << op1.gLNCounter << "\n";
}

TEST(ScdSuite, RealModelReading)
{
    const char* filename = "data/HSR_MSC-SPB.0.3.scd";
    ReadPerformer op1;
    configuration::Reader rc1(op1);
    ASSERT_TRUE(rc1.Read(filename, "BRTP2_06_BC"));

    std::cout << "IED:" << op1.gIedCounter << " LD:" << op1.gLDCounter
              << " DS:" << op1.gDScounter << " FCDA:" << op1.gFCDACounter
              << " RCB:" << op1.gRptCounter << " LN:" << op1.gLNCounter << "\n";
}

TEST(ScdSuite, RealModelPromiscReading)
{
    const char* filename = "data/HSR_MSC-SPB.0.3.scd";
    ReadPerformer op1;
    configuration::Reader rc1(op1);
    ASSERT_TRUE(rc1.Read(filename, "*"));
    ASSERT_EQ(op1.gIedCounter, 62);
    ASSERT_EQ(op1.gLDCounter, 94);
    ASSERT_EQ(op1.gLNCounter, 1911);
    ASSERT_EQ(op1.gDScounter, 987);
    ASSERT_EQ(op1.gFCDACounter, 10603);
    ASSERT_EQ(op1.gRptCounter, 1042);
    std::cout << "IED:" << op1.gIedCounter << " LD:" << op1.gLDCounter
              << " DS:" << op1.gDScounter << " FCDA:" << op1.gFCDACounter
              << " RCB:" << op1.gRptCounter << " LN:" << op1.gLNCounter << "\n";
}
