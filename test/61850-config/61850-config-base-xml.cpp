#include "iec61850-config/xml.h" // for ParseContext, Parser
#include <assert.h> // for assert
#include <expat.h> // for XML_Error, XML_Error::XML_ERROR_I...
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for IsNullLiteralHelper, TestPartResult
#include <gtest/gtest.h> // for AssertionResult, gtest_ar, ASSERT_EQ
#include <string.h> // for memcmp, strlen, size_t, strcmp

using namespace rth::iec61850::configuration;

typedef struct {
    int enter_cnt;
    int leave_cnt;

} VisitedNodesCounter;

typedef struct {
    char element_name[128];
    int error;

} CharacterParseContext;
int unspec_start(void* userData, const char* name, const char** atts,
    const ParseContext& context);
int unspec_end(void* userData, const char* name, const ParseContext& context);
int character_element_start(void* userData, const char* name, const char** atts,
    const ParseContext& context);

int character_read(void* userData, const char* name, int len,
    const ParseContext& context);
int call_once(void* userData, const char* name, const char** atts,
    const ParseContext& context);

int call_never(void* userData, const char* name, const ParseContext& context);

static const char* gTestBuffer = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
        <breakfast_menu>\
          <food>\
           <name>Belgian Waffles</name>\
           <price>$5.95</price>\
           <description>Two of our famous Belgian Waffles with plenty of real maple syrup</description>\
           <calories>650</calories>\
           </food>\
           <food>\
           <name>Strawberry Belgian Waffles</name>\
           <price>$7.95</price>\
           <description>Light Belgian waffles covered with strawberries and whipped cream</description>\
           <calories>900</calories>\
           </food>\
           </breakfast_menu>";

static const char* gTestDamagedBuffer = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
        <breakfast_menu>\
          <food>\
           <name>Belgian Waffles</name>\
           <price>$5.95</price>\
           <description>Two of our famous Belgian Waffles with plenty of real maple syrup</description>\
           <calories>650</calories>\
           </food>\
           <food>\
           <name>Strawberry Belgian Waffles</name>\
           <price>$7.95</price>\
           <description>Light Belgian waffles covered with strawberries and whipped cream</description>\
           <calories>900</calories>\
           </breakfast_menu>";

int unspec_start(void* userData, const char* name, const char** atts,
    const ParseContext& context)
{
    if (context.stop)
        return 0;

    VisitedNodesCounter* cnt = static_cast<VisitedNodesCounter*>(userData);
    cnt->enter_cnt++;

    // Чисто, чтобы не было ворнингов на пустые аргументы
    if (name > *atts) {
    }
    return 0;
}

int unspec_end(void* userData, const char* name, const ParseContext& context)
{
    if (context.stop)
        return 0;

    VisitedNodesCounter* cnt = static_cast<VisitedNodesCounter*>(userData);
    cnt->leave_cnt++;

    // Чисто, чтобы не было ворнингов на пустые аргументы
    if (name != 0) {
    }
    return 0;
}

int character_element_start(void* userData, const char* name, const char** atts,
    const ParseContext& context)
{
    if (context.stop)
        return 0;

    CharacterParseContext* ctx = static_cast<CharacterParseContext*>(userData);
    strcpy(ctx->element_name, name);

    // Чисто, чтобы не было ворнингов на пустые аргументы
    if (name > *atts) {
    }
    return 0;
}

int character_read(void* userData, const char* name, int len,
    const ParseContext& context)
{
    if (context.stop)
        return 0;

    if (len < 0)
        return 0;

    CharacterParseContext* ctx = static_cast<CharacterParseContext*>(userData);
    size_t ulen = static_cast<size_t>(len);

    static const char* phrase1 = "Belgian Waffles";
    static const char* phrase2 = "Strawberry Belgian Waffles";

    static const size_t phrase1_len = strlen(phrase1);
    static const size_t phrase2_len = strlen(phrase2);

    if (strcmp(ctx->element_name, "name") == 0) {
        if ((memcmp(name, phrase1, phrase1_len > ulen ? ulen : phrase1_len) != 0) && (memcmp(name, phrase2, phrase2_len > ulen ? ulen : phrase2_len) != 0)) {
            ctx->error = 1;
        } else {
            ctx->element_name[0] = '\0';
        }
    }

    if (strcmp(ctx->element_name, "price") == 0) {
        if (memcmp(name, "$5.95", ulen) != 0 && memcmp(name, "$7.95", ulen) != 0) {
            ctx->error = 1;
        } else {
            ctx->element_name[0] = '\0';
        }
    }
    return 0;
}

int call_once(void* userData, const char* name, const char** atts,
    const ParseContext& context)
{
    if (userData || name || atts || context.stop) {
    }

    return 0xAAAA;
}

int call_never(void* userData, const char* name, const ParseContext& context)
{
    if (userData || name || context.stop) {
    }
    assert("Noooooooo!!!" == 0);
    return -1;
}

TEST(XmlSuite, SimpleReading)
{
    ParseContext ctx;
    VisitedNodesCounter counter = { 0, 0 };
    Parser visitor;

    const size_t kTestBufferSize = strlen(gTestBuffer) + 1;
    const size_t kTestDamagedBuffer = strlen(gTestDamagedBuffer) + 1;

    // Тупо проверям кол-во узлов
    visitor.Init();
    visitor.SetUserData(&counter);
    visitor.SetStartElementHandler(unspec_start);
    visitor.SetEndElementHandler(unspec_end);
    visitor.Process(gTestBuffer, kTestBufferSize);

    // Проверить счетчики узлов
    ASSERT_EQ(counter.enter_cnt, 11);
    ASSERT_EQ(counter.leave_cnt, 11);

    // Проверить данные из контекста парсера
    ctx = visitor.GetParseContext();
    ASSERT_GT(ctx.pos_current_byte, 0);
    ASSERT_GT(ctx.pos_current_ln, 0U);
    ASSERT_GT(ctx.pos_current_byte_idx, 0);
    // XML_ERROR_INVALID_TOKEN - может возникать из-за некорретного
    // форматирования.
    // Исключим её, т.к. автоформаттер по-любому порвет фомрматирование строки с
    // xml
    ASSERT_EQ(ctx.parser_error & ~XML_ERROR_INVALID_TOKEN, 0);
    visitor.Release();

    // После релиза, контекст д.б. чистым
    ctx = visitor.GetParseContext();
    ASSERT_EQ(ctx.pos_current_byte, 0);
    ASSERT_EQ(ctx.pos_current_ln, 0U);
    ASSERT_EQ(ctx.pos_current_byte_idx, 0);
    ASSERT_EQ(ctx.parser_error & ~XML_ERROR_INVALID_TOKEN, 0);

    // Повторный запуск того же самого парсера
    visitor.Init();
    visitor.SetUserData(&counter);
    visitor.SetStartElementHandler(unspec_start);
    visitor.SetEndElementHandler(unspec_end);
    const char* ptr = gTestBuffer;
    unsigned int len = 0;

    while (len < kTestBufferSize) {
        visitor.Process(ptr, 1);
        len += 1;
        ptr += 1;
    }

    visitor.Release();
    ASSERT_EQ(counter.enter_cnt, 22);
    ASSERT_EQ(counter.leave_cnt, 22);

    // Остановка чтения
    visitor.Init();
    visitor.SetStartElementHandler(call_once);
    visitor.SetEndElementHandler(call_never);
    visitor.Process(gTestBuffer, kTestBufferSize);
    ctx = visitor.GetParseContext();

    // Проверить коректность возвращяемых значений
    // ctx.handler_rs = return call_once
    ASSERT_EQ(ctx.handler_rs, 0xAAAA);
    ASSERT_EQ(ctx.stop, 1);
    visitor.Release();

    // Чтение косого буфера
    visitor.Init();
    visitor.SetStartElementHandler(unspec_start);
    visitor.SetEndElementHandler(unspec_end);
    visitor.Process(gTestDamagedBuffer, kTestDamagedBuffer);
    ctx = visitor.GetParseContext();

    ASSERT_NE(ctx.parser_error, 0);
    ASSERT_NE(ctx.parser_error, XML_ERROR_INVALID_TOKEN);
    visitor.Release();

    // Чтение текста
    CharacterParseContext char_ctx = { { 0 }, 0 };
    visitor.Init();
    visitor.SetUserData(&char_ctx);
    visitor.SetStartElementHandler(character_element_start);
    visitor.SetCharacterDataHandler(character_read);
    visitor.Process(gTestBuffer, kTestBufferSize);
    visitor.Release();

    ASSERT_EQ(char_ctx.error, 0);
}
