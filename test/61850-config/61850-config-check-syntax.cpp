#include "iec61850-check/configuration-default-test.h" // for DefaultTestSuite
#include "iec61850-check/configuration-test.h" // for TestResult
#include "iec61850-common/errors-defaults.h" // for ErrorsToStream
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult
#include <gtest/gtest.h> // for IsNullLiteral...
#include <iostream> // for cout, operator<<

using namespace rth::iec61850;
using namespace rth::iec61850::configuration;

TEST(ScdSuite, BasicSyntaxChecking)
{
    /* Прочитать файл с оишьками и вывести отчет*/
    const char* filename = "data/B20-error.icd";
    DefaultTestSuite ts1;
    TestResult test_tesult_1 = RunTests(ts1, filename);
    ASSERT_GT(test_tesult_1.GetErrorsCount(), 0U);
    ASSERT_EQ(test_tesult_1.GetErrorsCount(), 25U);
    ErrorsToStream<TestingErrors, std::ostream>(test_tesult_1.Errors(),
        std::cout);

    // Повторне чтение файла с ошибками
    DefaultTestSuite ts2;
    TestResult test_tesult_2 = RunTests(ts2, filename);
    ASSERT_GT(test_tesult_2.GetErrorsCount(), 0U);
    ASSERT_EQ(test_tesult_2.GetErrorsCount(), 25U);

    // Чтение несуществующего файла
    DefaultTestSuite ts3;
    TestResult test_tesult_3 = RunTests(ts3, "~~NoExistingFile~~");
    ASSERT_EQ(test_tesult_3.GetErrorsCount(), 1U);
    ErrorsToStream<TestingErrors, std::ostream>(test_tesult_3.Errors(),
        std::cout);
}

TEST(DISABLED_ScdSuite, BasicSyntaxCheckingMultiCalls)
{
    // Дефолтный набор тестов не допускает многократное использование
    // Должен вылетать по assert
    const char* filename = "data/B20-error.icd";
    DefaultTestSuite ts1;
    TestResult test_tesult_1 = RunTests(ts1, filename);
    TestResult test_tesult_2 = RunTests(ts1, filename);
}
