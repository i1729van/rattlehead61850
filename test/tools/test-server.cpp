#include "base/process.h" // for Daemonize, SetSignalHa...
#include "base/thread.h" // for Millisleep
#include "iec61850-common/app-log.h" // for operator<<, LogMessage
#include "iec61850-common/config.h" // for RTH_IEC61850_DEF_SHMBU...
#include "iec61850-server/server-log.h" // for LogInfo, LogError
#include "iec61850-server/server-settings.h" // for ServerSettings
#include "iec61850-server/server-statistic.h" // for ServerStatistic, Input...
#include "iec61850-server/server.h" // for Server
#include <iostream> // for operator<<, cout, ostream
#include <stdbool.h> // for bool, false, true
#include <stddef.h> // for NULL, size_t
#include <stdint.h> // for uint64_t, uint32_t
#include <string> // for char_traits, operator<<

#if defined(__linux__)
#include <getopt.h>
#include <sys/select.h> // for select, FD_ISSET, FD_SET
#include <unistd.h> // for optarg, required_argument
#endif //#if defined(__linux__)

#if defined(_WIN32)
#include "getopt.h"
#include <windows.h>
#endif //#if defined(_WIN32)

using namespace rth::iec61850;
using namespace rth::iec61850::server;

static ServerSettings parse_parameters(int argc, char** argv,
    bool* run_as_daemon = 0)
{
    ServerSettings settings("Invalid", "Invalid", "Invalid", 0);

    if (argc < 4) {
        std::cout << "Not enough parameters\n";
        std::cout << "--file - path to config file\n";
        std::cout << "--ied - IED name\n";
        std::cout << "--connection - connection name\n";
        std::cout << "--test - check mode [none, default]\n";
        std::cout << "--logmode - logger mode [none, info, warning, error]\n";
        std::cout << "--logaddress - destination ip:port \n";
        std::cout << "--logfile - [stdout]\n";
        std::cout << "--writedelay - delay before writing [microseconds]\n";
        std::cout << "--bufsize - exchange buffer size\n";
        std::cout << "--transsize - max transaction size\n";
#if defined(__linux__)
        std::cout << "--daemon - run as daemon\n";
#endif
        return settings;
    }

    char* filename = NULL;
    char* iedname = NULL;
    char* conname = NULL;
    char* logmode = NULL;
    char* logaddr = NULL;
    char* logfile = NULL;
    char* testmode = NULL;
    bool daemon = false;
    uint32_t write_delay = 10 * 1000;
    size_t trans_size = RTH_IEC61850_DEF_TRANSACTION_SIZE;
    size_t buffer_size = RTH_IEC61850_DEF_SHMBUFFER_SIZE;

    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            { "file", required_argument, 0, 'f' },
            { "ied", required_argument, 0, 'i' },
            { "connection", required_argument, 0, 'c' },
            { "test", required_argument, 0, 't' },
            { "logmode", required_argument, 0, 'l' },
            { "logaddress", required_argument, 0, 'a' },
            { "logfile", required_argument, 0, 's' },
            { "writedelay", required_argument, 0, 'w' },
            { "bufsize", required_argument, 0, 'b' },
            { "transsize", required_argument, 0, 'z' },
            { "daemon", no_argument, 0, 'd' },
            { 0, 0, 0, 0 }
        };

        int c = getopt_long_only(argc, argv, "", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 'c':
            conname = optarg;
            break;

        case 'd':
            daemon = true;
            break;

        case 'f':
            filename = optarg;
            break;
        case 'i':
            iedname = optarg;
            break;

        case 'l':
            logmode = optarg;
            break;
        case 't':
            testmode = optarg;
            break;
        case 'a':
            logaddr = optarg;
            break;
        case 's':
            logfile = optarg;
            break;
        case 'w': {
            std::stringstream delstream(optarg);
            delstream >> write_delay;
            break;
        }
        case 'z': {
            std::stringstream transtream(optarg);
            transtream >> trans_size;

            if (trans_size < RTH_IEC61850_DEF_TRANSACTION_SIZE) {
                trans_size = RTH_IEC61850_DEF_TRANSACTION_SIZE;
                std::cout << "Transaction size must be >= "
                             "RTH_IEC61850_DEF_TRANSACTION_SIZE. "
                          << "Set to " << RTH_IEC61850_DEF_TRANSACTION_SIZE << "\n";
            }
            break;
        }
        case 'b': {
            std::stringstream bufstream(optarg);
            bufstream >> buffer_size;

            if (buffer_size < RTH_IEC61850_DEF_SHMBUFFER_SIZE) {
                buffer_size = RTH_IEC61850_DEF_SHMBUFFER_SIZE;
                std::cout
                    << "Transaction size must be >= RTH_IEC61850_DEF_SHMBUFFER_SIZE. "
                    << "Set to " << RTH_IEC61850_DEF_SHMBUFFER_SIZE << "\n";
            }
            break;
        }
        }
    }

    if (run_as_daemon) {
        *run_as_daemon = daemon;
    }
    if (!filename) {
        std::cout << "Missing argument --file\n";
        return settings;
    }
    if (!iedname) {
        std::cout << "Missing argument --ied\n";
        return settings;
    }
    if (!conname) {
        std::cout << "Missing argument --connection\n";
        return settings;
    }

    return ServerSettings(
        filename, iedname, conname, 102, testmode ? testmode : "default",
        logmode ? logmode : "error", logaddr ? logaddr : "",
        logfile ? logfile : "", buffer_size, trans_size, write_delay);
}

#if defined(__linux__)
static char ReadCommand()
{
    char symb = '0';
    struct timeval tv;
    fd_set fds;
    memset(&fds, 0, sizeof(fds));
    tv.tv_sec = 0;
    tv.tv_usec = 100 * 1000;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    if (select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv) > 0 && FD_ISSET(0, &fds)) {
        if (read(STDIN_FILENO, &symb, 1) < 0) {
            std::cout << "STDIN error\n";
            exit(1);
        }
    }
    return symb;
}
#endif //#if defined(__linux__)

#if defined(_WIN32)
static char ReadCommand()
{
    return getchar();
}
#endif //#if defined(_WIN32)

static uint64_t UnsignedMinus(uint64_t a, uint64_t b)
{
    return a > b ? a - b : b - a;
}

static const LogInfo& operator<<(const LogInfo& log, const char* str)
{
    if (LogInfo::IsEnabled()) {
        const_cast<LogInfo&>(log).GetStream() << std::string(str);
    }
    return log;
}

static const LogInfo& operator<<(const LogInfo& log, uint64_t value)
{
    if (LogInfo::IsEnabled()) {
        const_cast<LogInfo&>(log).GetStream() << value;
    }
    return log;
}

static void PrintStat(const ServerStatistic& current)
{
    static uint64_t cnt = 0;
    static ServerStatistic prev;
    LogInfo() << "-----------------------------------------------------\n";
    LogInfo() << "Measure: " << cnt++ << "\n";
    LogInfo() << "-----------------------------------------------------\n";
    LogInfo() << "Blocks:\t\t" << current.input.Blocks() << "\t["
              << current.input.Blocks() - prev.input.Blocks() << "]"
              << "\n";
    LogInfo() << "Values:\t\t" << current.input.Values() << "\t["
              << current.input.Values() - prev.input.Values() << "]"
              << "\n";
    LogInfo() << "Errors:\t\t" << current.input.Errors() << "\t["
              << current.input.Errors() - prev.input.Errors() << "]"
              << "\n";
    LogInfo() << "AvgCycle,ms:\t" << current.input.AvgCycleMilli() << "\t["
              << UnsignedMinus(current.input.AvgCycleMilli(),
                     prev.input.AvgCycleMilli())
              << "]"
              << "\n";
    LogInfo() << "AvgLock,ms:\t" << current.input.AvgLockMilli() << "\t["
              << UnsignedMinus(current.input.AvgLockMilli(),
                     prev.input.AvgLockMilli())
              << "]"
              << "\n";
    LogInfo() << "-----------------------------------------------------\n";

    prev = current;
}

static bool gStop = false;
static void CtrlCHandler(int sig) { gStop = true; }

int main(int argc, char** argv)
{
    bool is_daemon = false;
    const int kInnerSigInt = 2; // SIGINT : from kill -l
    ServerSettings settings = parse_parameters(argc, argv, &is_daemon);

    if (settings.Port() == 0) {
        return 1;
    }

    if (settings.Port() < 1024) {
        std::cout << "Using system port. Check permissions!\n\n";
    }

#if defined(__linux__)
    if (is_daemon) {
        rth::process::Daemonize();
    }
#endif

    Server server(settings);

    LogInfo() << "---------------------------------------------------------\n";
    LogInfo() << " \tRattlehead test server \n";
    LogInfo() << "---------------------------------------------------------\n";
    LogInfo() << "Config file: " << settings.ConfigFileName() << "\n";
    LogInfo() << "IED: " << settings.DeviceName() << "\n";
    LogInfo() << "Connection: " << settings.ConnectionName() << "\n";

    /* Последледовательность важна, если хотим чистый thread-san.
   * Фишка в том, что если создавать именованные(!) семафоры в одном процессе но
   * с разных тредов,то это сильно напряжет санитайзера. Возможно, это баг.
   * Возможно фича (более подробно в man'ах).
   * В общем, т.к. и SharedStatistic и Server создаеют семафоры, то имеет смысл
   * разнести их по времени. server создает семы паралелльно.
   * Statistic блокирует пока не будет создан. Так что Statistic впереди.
   * Это гарантирует, что они не пересекуться с сервером.*/
    // SharedStatistic shared_statistic(settings.ConnectionName());
    // shared_statistic.Create();

    if (!server.StartServer()) {
        LogError() << "Can't start server\n";
        return 1;
    }

    LogInfo() << "'s' - statistic\n";
    LogInfo() << "'t' - statistic toggle on/off\n";
    LogInfo() << "'q' - exit\n";

    rth::process::SetSignalHandler(kInnerSigInt, CtrlCHandler);

    char cmd = '0';
    bool toggle = false;
    // int stat_update_counter = 0;
    while ((cmd = ReadCommand()) != 'q' && gStop == false) {
        switch (cmd) {
        case 's': {
            PrintStat(server.GetStatistic());
            break;
        }
        case 't':
            toggle = !toggle;
            break;
        }

        if (toggle) {
            PrintStat(server.GetStatistic());
        }

        // Спим в любом случае. stdin может быть отвзяан (запуск в качестве демона, либо
        // лень натсраивать systemd) и ReadCommand будет возвращаться немедленно после вызова.
        // Чтобы не жрать 100% проца немного поспим.
        rth::thread::Millisleep(50);
        /*if (stat_update_counter++ > 10) {
      const ServerStatistic &stat = server.GetStatistic();
      shared_statistic.Update(stat);
      stat_update_counter = 0;
    }*/
    }
    if (gStop) {
        LogInfo() << "SIGINT catched...\n";
    }

    LogInfo() << "Stopping server...\n";
    server.StopServer();

    LogInfo() << "Done\n";

    return 0;
}
