#include "base/thread.h" // for Microsleep, Millisleep
#include "iec61850-common/config.h" // for RTH_IEC61850_SERVER_INPUT_ACK...
#include "iec61850-common/defs.h" // for DataQueue
#include <iostream> // for operator<<, cout, ostream
#include <sstream>
#include <stdbool.h> // for bool
#include <stddef.h> // for NULL, size_t
#include <string> // for string, operator<<, char_traits

#if defined(__linux__)
#include <getopt.h> // for optarg, getopt_long_only, req...
#include <stdlib.h>
#include <string.h>
#include <sys/select.h> // for select, FD_ISSET, FD_SET, FD_...
#include <unistd.h> // for STDIN_FILENO, read
#endif // #if defined (__linux__)

#if defined(_WIN32)
#include "getopt.h" // for optarg, getopt_long_only, req...
#endif // #if defined (_WIN32)

using namespace rth::iec61850;

typedef struct {
    std::string input_queue;
    std::string ack_queue;
    unsigned int timeout_micro;
} MonitorSettings;

static MonitorSettings parse_parameters(int argc, char** argv)
{
    MonitorSettings settings = { "Invalid", "Invalid", 10 * 1000 };

    if (argc < 2) {
        std::cout << "--connection - connection name\n";
        std::cout << "--timeout - scan cycle, microsec\n";
        return settings;
    }

    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            { "connection", required_argument, 0, 'c' },
            { "timeout", required_argument, 0, 't' },
            { 0, 0, 0, 0 }
        };

        int c = getopt_long_only(argc, argv, "", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 'c':
            // std::cout << "Connection: " << optarg << "\n";
            settings.input_queue = optarg;
            settings.input_queue += RTH_IEC61850_SERVER_INPUT_QUEUE_SUFFIX;
            settings.ack_queue = optarg;
            settings.ack_queue += RTH_IEC61850_SERVER_INPUT_ACK_QUEUE_SUFFIX;
            break;

        case 't':
            std::stringstream stream(optarg);
            stream >> settings.timeout_micro;
            break;
        }
    }
    return settings;
}

#if defined(__linux__)
static bool ExitCmd()
{
    char symb = '0';
    struct timeval tv;
    fd_set fds;
    memset(&fds, 0, sizeof(fds));
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    if (select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv) > 0 && FD_ISSET(0, &fds)) {
        if (read(STDIN_FILENO, &symb, 1) < 0) {
            std::cout << "STDIN error\n";
            exit(1);
        }
    }

    return symb == 'q';
}
#endif //#if defined (__linux__)

#if defined(_WIN32)
static bool ExitCmd()
{
    return getchar() == 'q';
}
#endif //#if defined (_WIN32)

int main(int argc, char** argv)
{
    MonitorSettings settings = parse_parameters(argc, argv);

    if (settings.input_queue == "Invalid") {
        std::cout << "Can't parse parameters\n";
        return 1;
    }

    std::cout << "---------------------------------------------------------\n";
    std::cout << " \tShared memory monitor \n";
    std::cout << "---------------------------------------------------------\n";
    std::cout << "InputQueue: " << settings.input_queue << "\n";
    std::cout << "AckQueue: " << settings.input_queue << "\n";

    DataQueue input_queue;
    size_t input_queue_size = 0;

    std::cout << "Press 'q' for exit...\n";
    while (!ExitCmd()) {
        if (!input_queue.IsOpen()) {
            if (!input_queue.Open(settings.input_queue.c_str())) {
                std::cout << "Can't open queue\n";
                rth::thread::Millisleep(1000);
            }
        }

        size_t input_queue_act_size = input_queue.Size();
        if (input_queue_size != input_queue_act_size) {
            input_queue_size = input_queue_act_size;
            std::cout << "InputSize: " << input_queue_size << "\n";
        }
        rth::thread::Microsleep(settings.timeout_micro);
    }

    std::cout << "Done\n";

    /*
    std::cout << "Press 'q' for exit...\n";
    for (char c = 's'; c != 'q'; cin >> c) {
    }

    std::cout << "Stopping server...\n";
    server.StopServer();

    std::cout << "Done\n";*/
    return 0;
}
