#include "base/thread.h" // for Microsleep
#include "base/time.h" // for GetRealTimeMilli
#include "iec61850-common/config.h" // for RTH_IEC61850_DATABLOCK_...
#include "iec61850-common/defs.h" // for DataValue, DataValueTyp...
#include "iec61850-common/errors-defaults.h" // for ErrorsToStream
#include "iec61850-connector/connection.h" // for Connection
#include "iec61850-connector/send-result.h" // for TransactionErrors, Send...
#include <algorithm> // for transform
#include <ctype.h> // for tolower
#include <fstream> // for operator<<, cout, ostream

#include <iostream> // for operator<<, cout, ostream
#include <stddef.h> // for NULL
#include <stdint.h> // for int16_t, int32_t, int64_t
#include <string> // for string, basic_string
#include <vector> // for vector, vector<>::const...

#if defined(__linux__)
#include <getopt.h> // for optarg, required_argument
#endif

#if defined(_WIN32)
#include "getopt.h" // for optarg, required_argument
#endif

using namespace rth::iec61850;
using namespace rth::iec61850::connector;

static DataValueType StringToType(const std::string& type)
{
    std::string lowtype(type);
    std::transform(lowtype.begin(), lowtype.end(), lowtype.begin(), ::tolower);
    struct pairs {
        const char* name;
        DataValueType type;
    } supported_types[] = { { "int8", DataValueTypeInt8 },
        { "uint8", DataValueTypeUInt8 },
        { "int16", DataValueTypeInt16 },
        { "uint16", DataValueTypeUInt16 },
        { "int32", DataValueTypeInt32 },
        { "uint32", DataValueTypeUInt32 },
        { "int64", DataValueTypeInt64 },
        { "uint64", DataValueTypeUInt64 },
        { "f32", DataValueTypeFloat },
        { "f64", DataValueTypeDouble }

    };

    for (unsigned int i = 0;
         i < sizeof(supported_types) / sizeof(supported_types[0]); i++) {
        if (lowtype == supported_types[i].name) {
            return supported_types[i].type;
        }
    }
    return DataValueTypeUnk;
}

/* Создание магических занчений, таких как now и т.п. */
static DataValue DataValueCreateMagicValue(const std::string& tag_name,
    const std::string& tag_type,
    const std::string& tag_value)
{
    DataValueType type = StringToType(tag_type);
    switch (type) {
    case DataValueTypeUInt64:

        if (tag_value == "now") {
            return DataValue(tag_name.c_str(), type, rth::time::GetRealTimeMilli());
        }
        break;

    case DataValueTypeUnk:
    case DataValueTypeInt8:
    case DataValueTypeUInt8:
    case DataValueTypeInt16:
    case DataValueTypeUInt16:
    case DataValueTypeInt32:
    case DataValueTypeUInt32:
    case DataValueTypeInt64:
    case DataValueTypeFloat:
    case DataValueTypeDouble:
    default:
        break;
    }

    return DataValue();
}

/* Формирование DataValue из стоковых значенией */
static DataValue DataValueFromString(const std::string& tag_name,
    const std::string& tag_type,
    const std::string& tag_value)
{
    /* Обработка спец. занчений */
    DataValue magic = DataValueCreateMagicValue(tag_name, tag_type, tag_value);
    if (magic.GetType() != DataValueTypeUnk) {
        return magic;
    }

    DataValueType type = StringToType(tag_type);
    std::stringstream ss(tag_value);
    switch (type) {
    /*--------------------------------------------------------
     * Signed int
     * ------------------------------------------------------*/
    case DataValueTypeInt8:
        int8_t i8;
        ss >> i8;
        return DataValue(tag_name.c_str(), type, i8);

    case DataValueTypeInt16:
        int16_t i16;
        ss >> i16;
        return DataValue(tag_name.c_str(), type, i16);

    case DataValueTypeInt32:
        int32_t i32;
        ss >> i32;
        return DataValue(tag_name.c_str(), type, i32);

    case DataValueTypeInt64:
        int64_t i64;
        ss >> i64;
        return DataValue(tag_name.c_str(), type, i64);

    /*--------------------------------------------------------
     * Unsigned int
     * ------------------------------------------------------*/
    case DataValueTypeUInt8:
        uint8_t ui8;
        ss >> ui8;
        ui8 -= '0';
        return DataValue(tag_name.c_str(), type, ui8);

    case DataValueTypeUInt16:
        uint16_t ui16;
        ss >> ui16;
        return DataValue(tag_name.c_str(), type, ui16);

    case DataValueTypeUInt32:
        uint32_t ui32;
        ss >> ui32;
        return DataValue(tag_name.c_str(), type, ui32);

    case DataValueTypeUInt64:
        uint64_t ui64;
        ss >> ui64;
        return DataValue(tag_name.c_str(), type, ui64);

    /*--------------------------------------------------------
     * Floating point
     * ------------------------------------------------------*/
    case DataValueTypeFloat:
        float f32;
        ss >> f32;
        return DataValue(tag_name.c_str(), type, f32);

    case DataValueTypeDouble:
        double f64;
        ss >> f64;
        return DataValue(tag_name.c_str(), type, f64);

    /*--------------------------------------------------------
     * Other shit
     * ------------------------------------------------------*/
    case DataValueTypeUnk:
        std::cout << "Unsupported type " << tag_type << "\n";
        break;
    default:
        break;
    }

    return DataValue(tag_name.c_str(), DataValueTypeUnk);
}

const int kWriteLimit = RTH_IEC61850_DATABLOCK_SIZE; //<< кол-во записей в
// DataBlockе, которое
//будет
//приводить к отправке блока на сервер
static std::vector<DataValue>* output_values = NULL; //<< Пром. буфер
static Connection* gconn = NULL; //<< Указатель на коннектор

/* СБросить значения в сервер */
static void Flush()
{
    DataBlock block;
    std::vector<DataValue>::const_iterator it = output_values->begin(),
                                           end = output_values->end();
    DataBlockStream stream(block);
    while (it != end) {
        if (!(stream << *it)) {
            std::cout << "Can't add new value to DataBlock\n";
        }
        ++it;
    }

    SendResult result = gconn->Send(block);
    if (result.GetErrorsCount() > 0) {
        rth::iec61850::ErrorsToStream<TransactionErrors, std::ostream>(
            result.GetErrorList(), std::cout);
    }

    output_values->clear();
}

/* Форимрование значениеи складываение в бефер / отправка серверу */
static void UpdateValue(const std::string& tag_name,
    const std::string& tag_type,
    const std::string& tag_value)
{
    /* Не выполняем никаких особых проверок типов, имен и т.п. Просто добаляем в
   * очередь на отправку.
   * Сервер сам разберется [должен разобраться]*/
    output_values->push_back(DataValueFromString(tag_name, tag_type, tag_value));

    /* Сбросить данные в сервер, если мы достигли лимита */
    if (output_values->size() >= kWriteLimit - 1) {
        Flush();
    }
}

/**
 * @brief Чтение из stdin и отправка в сервер
 */
static void StdInMode()
{
    std::string tag_name;
    std::string tag_type;
    std::string tag_value;
    std::string word;

    int step = 0;

    while (true) {
        if (tag_name == "q") {
            break;
        }

        switch (step) {
        case 0:
            std::cout << "Tag: ";
            std::cin >> word;

            if (word != "r" || tag_name.size() == 0)
                tag_name = word;
            else {
                std::cout << "Repeat: " << tag_name << "\n";
            }

            break;

        case 1:
            std::cout << "Type: ";
            std::cin >> word;
            if (word != "r" || tag_name.size() == 0)
                tag_type = word;
            else {
                std::cout << "Repeat: " << tag_type << "\n";
            }
            break;

        case 2:
            std::cout << "Value: ";
            std::cin >> word;

            if (word != "r" || tag_value.size() == 0)
                tag_value = word;
            else {
                std::cout << "Repeat: " << tag_value << "\n";
            }
            std::cout << tag_name << " " << tag_type << " " << tag_value << "\n";
            /* Обновить значение и сразу же отправить в сервер */
            UpdateValue(tag_name, tag_type, tag_value);
            Flush();
            break;
        }

        step = (step + 1) % 3;
    }
}

static void FileMode(const char* filename, unsigned int timeout)
{
    std::ifstream fstream;
    std::string word;
    std::string tag_name;
    std::string tag_type;
    std::string tag_value;
    int step = 0;

    fstream.open(filename);

    while (fstream >> word) {
        if (tag_name == "q") {
            break;
        }

        switch (step) {
        case 0:
            tag_name = word;
            break;

        case 1:
            tag_type = word;
            break;

        case 2:
            tag_value = word;
            std::cout << tag_name << " " << tag_type << " " << tag_value << "\n";
            /* Обновить значение*/
            UpdateValue(tag_name, tag_type, tag_value);
            rth::thread::Microsleep(timeout);
            break;
        }

        step = (step + 1) % 3;
    }

    /* Отпраивть значение если они есть в пром. буфере */
    Flush();
    fstream.close();
}
int main(int argc, char** argv)
{
    Connection* conn = NULL;
    const char* filename = NULL;
    unsigned int nrepeat = 1;
    unsigned int micro_timeout = 10 * 1000;

    if (argc < 2) {
        std::cout << "--connection=NAME - connection name\n";
        std::cout << "[--file=PATH] - path to script file\n";
        std::cout << "[--repeat=N] - repeat script N times \n";
        std::cout
            << "[--timeout=N] - pause between data updates in microseconds \n";
        return 1;
    }

    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            { "connection", required_argument, 0, 'c' },
            { "file", required_argument, 0, 'f' },
            { "repeat", required_argument, 0, 'r' },
            { "timeout", required_argument, 0, 't' },
            { 0, 0, 0, 0 }
        };

        int c = getopt_long_only(argc, argv, "", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 'c':
            std::cout << "Connection: " << optarg << "\n";
            conn = new Connection(optarg, 250);
            break;

        case 'f':
            filename = optarg;
            break;

        case 'r': {
            std::stringstream rss(optarg);
            rss >> nrepeat;
            nrepeat++;
            break;
        }
        case 't': {
            std::stringstream tss(optarg);
            tss >> micro_timeout;
            break;
        }
        default:
            break;
        }
    }

    if (!conn) {
        std::cout << "Missing argument --connection\n";
        return 1;
    }

    if (!conn->Open()) {
        std::cout << "Can't connect to server!\n";
        delete conn;
        return 1;
    }

    std::cout << "---------------------------------------------------------\n";
    std::cout << " \tRattlehead test client \n";
    std::cout << "---------------------------------------------------------\n";

    gconn = conn;
    std::vector<DataValue> values;
    output_values = &values;

    if (filename) {
        std::cout << "File mode: " << filename << "\n";
        std::cout << "Repeat : " << nrepeat << " times \n";
        std::cout << "Delay : " << micro_timeout << " microseconds \n";

        for (unsigned int i = 0; i < nrepeat; i++) {
            std::cout
                << "---------------------------------------------------------\n";
            std::cout << "Cycle: " << i + 1 << "\n";
            std::cout
                << "---------------------------------------------------------\n";
            FileMode(filename, micro_timeout);
        }
    } else {
        std::cout << "StdIn mode \n";
        std::cout << "Tag = 'q' for exit...\n";
        StdInMode();
    }
    delete conn;
}
