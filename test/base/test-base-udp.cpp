#include "base/network.h" // for MakeIpAddress, IpAddress
#include "base/udp-socket.h"
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResult::T...
#include <gtest/gtest.h> // for AssertionResult, GetBoolAssertion...
#include <iostream> // for operator<<, basic_ostream, cout
#include <stddef.h> // for NULL, size_t
#include <string> // for string, char_traits
#include <sys/types.h> // for ssize_t

using namespace rth::network;

TEST(NetworkSuite, BasicUDP)
{
    UdpSocket udp;
    char buffer[128] = { 0 };

    ASSERT_TRUE(udp.Create());
    ASSERT_TRUE(udp.Listen(MakeIpAddress("0.0.0.0:5555")));
    ASSERT_TRUE(udp.Close());

    ASSERT_TRUE(udp.Create());
    ASSERT_TRUE(udp.Listen(MakeIpAddress("0.0.0.0:5555")));
    ASSERT_TRUE(udp.SetProperty("block", "false"));
    ASSERT_EQ(udp.RecvFrom(buffer, sizeof(buffer), NULL), -1);
    ASSERT_EQ(
        udp.SendTo(buffer, sizeof(buffer), MakeIpAddress("127.0.0.1", 65535)),
        static_cast<ssize_t>(sizeof(buffer)));
    ASSERT_TRUE(udp.Close());
}

TEST(DISABLED_NetworkSuite, EchoServerUDP)
{
    const char* kConnString = "0.0.0.0:5555";
    char buffer[128] = { 0 };
    UdpSocket udp;
    ASSERT_TRUE(udp.Create());
    ASSERT_TRUE(udp.Listen(MakeIpAddress(kConnString)));

    std::cout << "UDP EchoServer"
              << "\n";
    std::cout << "Listen: " << kConnString << "\n";
    std::cout << "'q' for exit... \n";

    while (buffer[0] != 'q') {
        IpAddress addr;
        ssize_t read_result = udp.RecvFrom(buffer, 128, &addr);
        std::cout << "Read " << read_result << "\n";

        if (read_result > 0) {
            ssize_t write_result = udp.SendTo(buffer, static_cast<size_t>(read_result), addr);
            std::cout << "Write " << write_result << "\n";
        }
    }

    ASSERT_TRUE(udp.Close());
}
