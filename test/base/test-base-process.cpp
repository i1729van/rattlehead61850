#if defined(__linux__)
#include "base/process.h" // for Create, CurrentPID, TryWait, Proc...
#include "base/thread.h" // for Millisleep
#include "base/time.h" // for GetMonoTimeMilli
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for GetTestTypeId, TestPartResult
#include <gtest/gtest.h> // for AssertionResult, Test, TestInfo (...
#include <signal.h> // for SIGINT
#include <stdint.h> // for uint64_t
#include <string> // for string

using namespace rth;

TEST(ProcessSuite, Create)
{
    uint64_t start = rth::time::GetMonoTimeMilli();
    process::ProcessHandler handler = process::Create("sleep 1");

    int cnt = 0;
    const int kWaitLimit = 15;
    while (process::TryWait(handler) <= 0 && cnt < kWaitLimit) {
        rth::thread::Millisleep(100);
    }
    uint64_t end = rth::time::GetMonoTimeMilli();
    ASSERT_LE(cnt, kWaitLimit);
    ASSERT_GE(end, start + 1000U);
    ASSERT_LE(end, start + 2000U);
}

TEST(ProcessSuite, Kill)
{
    process::ProcessHandler handler = process::Create("sleep 1000");
    rth::thread::Millisleep(100);

    process::Kill(handler);
    int cnt = 0;
    const int kWaitLimit = 15;
    while (process::TryWait(handler) <= 0 && cnt < kWaitLimit) {
        rth::thread::Millisleep(100);
    }

    ASSERT_LE(cnt, kWaitLimit);
}

TEST(ProcessSuite, GetPid)
{
    process::ProcessHandler handler = process::CurrentPID();
    ASSERT_GT(handler, 0);
}

const int kCtrlCSignal = SIGINT;
static int gCtrlC = 0;

const int kUserSignal = SIGUSR1;
static int gUser = 0;

static void CtrlCHandler(int sig)
{
    ASSERT_EQ(sig, kCtrlCSignal);
    gCtrlC++;
}

static void UserSignalHandler(int sig)
{
    ASSERT_EQ(sig, kUserSignal);
    gUser++;
}

TEST(ProcessSuite, Signals)
{
    const int kSendTimes = 10;

    ASSERT_TRUE(process::SetSignalHandler(kCtrlCSignal, CtrlCHandler));
    ASSERT_TRUE(process::SetSignalHandler(kUserSignal, UserSignalHandler));

    for (int i = 0; i < kSendTimes; i++) {
        process::SendSignal(process::CurrentPID(), kCtrlCSignal);
        thread::Millisleep(50);
        process::SendSignal(process::CurrentPID(), kUserSignal);
    }

    ASSERT_EQ(gCtrlC, kSendTimes);
    ASSERT_EQ(gUser, kSendTimes);
}
#endif //#if defined(__linux__)
