#include "base/shared-memory.h" // for SharedMemory
#include <errno.h> // for ENOENT
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResult::T...
#include <gtest/gtest.h> // for AssertionResult, GetBoolAssertion...
#include <stdlib.h> // for NULL, exit, size_t
#include <string> // for string

#if defined(__linux__)
#include <sys/wait.h> // for waitpid
#include <unistd.h> // for usleep, fork, pid_t
#endif //#if defined(__linux__)

using namespace rth::ipc;

TEST(SharedMemorySuite, BasicSharedMemory)
{
    SharedMemory sm1, sm2, sm3, sm4;

    const char* name1 = "RTH.BasicSharedMemory.memory1";
    const char* name2 = "RTH.BasicSharedMemory.memory2";
    const char* name3 = "RTH.BasicSharedMemory.memory3";

    /* Поисполняем фигни*/
    sm1.Close();
    sm1.Close();
    ASSERT_TRUE(sm1.Data() == NULL);
    ASSERT_TRUE(sm1.Size() == 0);

    /* Создать / закрыть */
    ASSERT_TRUE(sm1.Create(name1, 1024));
    EXPECT_NE(sm1.Data(), static_cast<void*>(NULL));
    ASSERT_TRUE(sm1.IsOpen());
    sm1.Close();

    /* Создать, открыть, провреить, закрыть */
    ASSERT_TRUE(sm1.Create(name1, 2048));
    ASSERT_TRUE(sm2.Open(name1));
    ASSERT_TRUE(sm1.IsOpen());
    ASSERT_TRUE(sm2.IsOpen());
    ASSERT_TRUE(sm1.Data() != NULL);
    ASSERT_TRUE(sm2.Data() != NULL);
    ASSERT_TRUE(sm2.Size() == 2048);
    sm2.Close();
    sm1.Close();

    /* Попытка двойного создания одного и того же участа */
    ASSERT_TRUE(sm1.Create(name2, 4096));
    ASSERT_FALSE(sm2.Create(name2, 4096));
    ASSERT_TRUE(sm1.Data() != NULL);
    ASSERT_TRUE(sm2.Data() == NULL);
    sm2.Close();
    sm1.Close();

    /* Попытка двойного создания одного и того же участа, но
   * без флага эксклюзивности */
    /*ASSERT_TRUE(sm1.Create("memory2", 4096, 0));
  ASSERT_TRUE(sm2.Create("memory2", 8192, 0));
  ASSERT_TRUE(sm1.Data() != NULL);
  ASSERT_TRUE(sm2.Data() != NULL);
  EXPECT_EQ(sm1.Size(), 4096);
  EXPECT_EQ(sm2.Size(), 8192);*/

    sm2.Close();
    sm1.Close();

    /* Попытка открыть несуществующую область */
    ASSERT_FALSE(sm1.Open(name3));
    ASSERT_EQ(sm1.GetLastError(), rth::error::kNoEntry);

    ASSERT_TRUE(sm1.Data() == NULL);
    ASSERT_TRUE(sm1.Size() == 0);
    sm1.Close();

    /* Проверка запрета на новые подключения после удаления памяти */
    ASSERT_TRUE(sm1.Create(name1, 2048));
    sm1.Close();
    ASSERT_FALSE(sm1.Open(name1));
    ASSERT_TRUE(sm1.Data() == NULL);
    ASSERT_TRUE(sm1.Size() == 0);

    /* Несколько подключений */
    ASSERT_TRUE(sm1.Create(name1, 2048));
    ASSERT_TRUE(sm2.Open(name1));
    ASSERT_TRUE(sm3.Open(name1));
    ASSERT_TRUE(sm4.Open(name1));
    ASSERT_TRUE(sm2.Size() == 2048);
    ASSERT_TRUE(sm3.Size() == 2048);
    ASSERT_TRUE(sm4.Size() == 2048);

    sm1.Close();

    ASSERT_TRUE(sm2.Data() != NULL);
    ASSERT_TRUE(sm3.Data() != NULL);
    ASSERT_TRUE(sm4.Data() != NULL);
    sm2.Close();
    sm3.Close();
    sm4.Close();
}

/* TODO: Добавить аналогичные тесты для Windows, но использовать потоки, т.к.
* fork в Windows плохо применим.
*/
#if defined(__linux__)
TEST(SharedMemorySuite, InterProcSharedMemory)
{
    /* Тесты на межпроцессное взаимодействие и блокировки */
    const char* kName = "RTH.InterProcSharedMemory";
    const int kSize = 1024 * 64;
    const int kRepeats = 100;
    const int kPoison = 666;
    const int kConnRetrys = 10;
    pid_t pid;

    /*************************************************************
   * Простой тест.
   * Создаем шаренную память в одном процессе, а затем открывай
   * в дочернем.
   **************************************************************/
    pid = fork();
    ASSERT_NE(pid, -1);

    if (pid > 0) {
        /*************************************************************
     * Родитсельский процесс
     **************************************************************/
        /* Создаем облать памяти и ждем, пока дочерний процесс не булет
     * выполнен */
        SharedMemory parent_mem;
        parent_mem.Create(kName, kSize);

        ASSERT_TRUE(parent_mem.IsOpen());

        waitpid(pid, 0, 0);
        parent_mem.Close();

    } else if (pid == 0) {
        /*************************************************************
     * Дочерний процесс
     **************************************************************/

        SharedMemory child_mem;
        /* Даем время родительскому процессу на создание области.
     * Без этого таймаута так же все будет ОК...но Valgrind будет
     * выводить больше мусорных сообщений. */
        usleep(10 * 1000);

        /* Пытаемся подключиться.
     * После подключения проверяем размеры и все такое */
        for (int i = 0; i < kConnRetrys && child_mem.IsOpen() != true; i++) {
            child_mem.Open(kName);
            usleep(10 * 1000);
        }

        ASSERT_TRUE(child_mem.IsOpen());
        ASSERT_TRUE(child_mem.Size() == static_cast<size_t>(kSize));
        ASSERT_FALSE(child_mem.Data() == NULL);

        child_mem.Close();
        // return;
        exit(0);
    }

    pid = fork();
    ASSERT_NE(pid, -1);

    if (pid > 0) {
        /*************************************************************
     * Родитсельский процесс
     **************************************************************/
        /* Идея проверки проста - создать память и начать писать в неё с разных
     * процессов.
     * Алгоритм -> захват блокировки, проверка на то, что значение = 0, затем
     * изменение
     * значения на магисечкое, ожидание, сброс значения в 0, освобождение.
     * Если после захвата блокировки значение будет не 0, это скажет что область
     * уже занята,
     * и мы получили ошибочный доступ */

        SharedMemory parent_mem;
        ASSERT_TRUE(parent_mem.Create(kName, kSize));

        ASSERT_TRUE(parent_mem.IsOpen());

        for (int i = 0; i < kRepeats; i++) {
            int* value;
            ASSERT_TRUE(parent_mem.Lock());

            value = static_cast<int*>(parent_mem.Data());
            EXPECT_EQ(*value, 0);

            *value = kPoison;
            usleep(50 * 1000);
            *value = 0;
            ASSERT_TRUE(parent_mem.Unlock());
        }

        waitpid(pid, 0, 0);
        parent_mem.Close();
    } else if (pid == 0) {
        /*************************************************************
     * Дочерний процесс
     **************************************************************/
        /* Логика такая же, как и в родительском, кроме того, что мы
     * подключаемся, а не создаем область в памяти. */
        SharedMemory child_mem;

        /* Пытаемся подключиться - 10 попакток */
        for (int i = 0; i < kConnRetrys && child_mem.IsOpen() != true; i++) {
            child_mem.Open(kName);
            usleep(10 * 1000);
            ;
        }

        ASSERT_TRUE(child_mem.IsOpen());
        ASSERT_TRUE(child_mem.Size() == static_cast<size_t>(kSize));

        for (int i = 0; i < kRepeats; i++) {
            int* value;
            ASSERT_TRUE(child_mem.Lock());
            value = static_cast<int*>(child_mem.Data());
            EXPECT_EQ(*value, 0);
            *value = kPoison;
            usleep(50 * 1000);
            *value = 0;
            ASSERT_TRUE(child_mem.Unlock());
        }

        child_mem.Close();
        // return;
        exit(0);
    }
}
#endif //#if defined(__linux__)
