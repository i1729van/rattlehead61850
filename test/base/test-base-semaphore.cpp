#include "base/named-semaphore.h" // for NamedSemaphore
#include <errno.h> // for ETIMEDOUT, EAGAIN, EEXIST, ENOENT
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResult::T...
#include <gtest/gtest.h> // for AssertionResult, GetBoolAssertion...
#include <ostream> // for basic_ostream::operator<<
#include <string> // for string

using namespace rth::ipc;

TEST(SharedMemorySuite, BasicNamedSemaphore)
{
    NamedSemaphore sem1, sem2;
    const char* name = "RTH.BasicNamedSemaphore.sem1";
    sem1.Close();
    sem1.Close();

    ASSERT_TRUE(sem1.Create(name, 0)) << sem1.GetLastError();
    sem1.Close();

#if defined(__linux__)
    ASSERT_TRUE(sem1.Create(name, 10));
    ASSERT_EQ(sem1.Value(), 10);
    sem1.Close();
#endif //#if defined(__linux__)

    /*Туда-сюда */
    ASSERT_TRUE(sem1.Create(name, 0));
    ASSERT_TRUE(sem1.Post());
    ASSERT_TRUE(sem1.Wait());
    sem1.Close();

    ASSERT_TRUE(sem1.Create(name, 4));
    ASSERT_TRUE(sem1.Wait());
    ASSERT_TRUE(sem1.Wait());
    ASSERT_TRUE(sem1.Wait());
    ASSERT_TRUE(sem1.Wait());
    sem1.Close();

    /* Проверть счетчик семафора. После создания семафор не постнут -> все попытки
  * подождать должны фэйлится.
  * TryWait - должен вернуть false + код ошибки EAGAIN
  * TimedWait - false + код ошибки таймаута */
    ASSERT_TRUE(sem1.Create(name, 0));
    ASSERT_FALSE(sem1.TryWait());
    ASSERT_EQ(sem1.GetLastError(), rth::error::kTryAgain);

    ASSERT_FALSE(sem1.TimedWait(10));
    ASSERT_EQ(sem1.GetLastError(), rth::error::kTimeout);

    ASSERT_FALSE(sem1.TimedWait(100));
    ASSERT_EQ(sem1.GetLastError(), rth::error::kTimeout);

    ASSERT_FALSE(sem1.TimedWait(1000));
    ASSERT_EQ(sem1.GetLastError(), rth::error::kTimeout);

    ASSERT_FALSE(sem1.TimedWait(1100));
    ASSERT_EQ(sem1.GetLastError(), rth::error::kTimeout);

    sem1.Close();

    /* Повторное создание недопустимо */
    ASSERT_TRUE(sem1.Create(name, 0));
    ASSERT_FALSE(sem2.Create(name, 0));
    ASSERT_NE(sem2.GetLastError(), rth::error::kNoError);
    ASSERT_EQ(sem2.GetLastError(), rth::error::kAlreadyExists);

    sem1.Close();
    sem2.Close();

    /* Открытие несуществующего семаофра */
    ASSERT_FALSE(sem1.Open(name));
    ASSERT_EQ(sem1.GetLastError(), rth::error::kNoEntry);
}
