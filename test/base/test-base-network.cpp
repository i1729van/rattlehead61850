#include "base/network.h" // for IsValidIP, MakeIpAddress, IpAddress
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResult::T...
#include <gtest/gtest.h> // for AssertionResult, GetBoolAssertion...
#include <ostream> // for operator<<
#include <stddef.h> // for NULL
#include <string> // for string

using namespace rth::network;

typedef struct {
    const char* ip;
    long port;

} IpPairs;

TEST(NetworkSuite, BasicIpAddress)
{
    const char* valid_ips[] = {
        "0.0.0.0:1", "127.0.0.1:12", "1.0.0.127:123",
        "0.0.0.0:1234", "127.255.124.111:12345", NULL
    };

    const char* invalid_ips[] = { "1232.555.0.0.0:1",
        "ZZZ.889.654.0:12",
        "0.256.0.0:1234",
        "0.0.0.0:1234554",
        "0.0.0.0:-1234",
        "192.168.1.1:sdf",
        NULL };

    IpAddress addr;

    for (int i = 0; valid_ips[i]; i++) {
        addr = MakeIpAddress(valid_ips[i]);
        ASSERT_TRUE(IsValidIP(addr)) << "Can't parse valid IP " << valid_ips[i];
    }

    for (int i = 0; invalid_ips[i]; i++) {
        addr = MakeIpAddress(invalid_ips[i]);
        ASSERT_FALSE(IsValidIP(addr)) << "Accept invalid IP " << invalid_ips[i];
    }
}

TEST(NetworkSuite, BasicIpAddressPairs)
{
    const IpPairs valid_ips[] = { { "0.0.0.0", 1 },
        { "127.0.0.1", 12 },
        { "1.0.0.127", 123 },
        { "0.0.0.0", 1234 },
        { "127.255.124.111", 65535 } };

    const IpPairs invalid_ips[] = { { "1232.555.0.0.0", 1 }, { "ZZZ.889.654.0", 12 },
        { "0.256.0.0", 1234 }, { "0.0", 574545 },
        { "0", 84873 }, { "0.0.0", 344756 },
        { "0.0.0.0", -23423 } };
    IpAddress addr;

    for (unsigned int i = 0; i < sizeof(valid_ips) / sizeof(valid_ips[0]); i++) {
        addr = MakeIpAddress(valid_ips[i].ip, valid_ips[i].port);
        ASSERT_TRUE(IsValidIP(addr)) << "Can't parse valid IP " << valid_ips[i].ip;
    }

    for (unsigned int i = 0; i < sizeof(invalid_ips) / sizeof(invalid_ips[0]);
         i++) {
        addr = MakeIpAddress(invalid_ips[i].ip, invalid_ips[i].port);
        ASSERT_FALSE(IsValidIP(addr)) << "Accept invalid IP " << invalid_ips[i].ip
                                      << ":" << invalid_ips[i].port;
    }
}
