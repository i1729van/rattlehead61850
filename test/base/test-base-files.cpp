#include "base/file.h" // for File
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResult::T...
#include <gtest/gtest.h> // for AssertionResult, GetBoolAssertion...
#include <stdlib.h> // for rand
#include <string.h> // for memcmp, memset, size_t
#include <string> // for string

using namespace rth::file;

TEST(FilesSuite, BasicTest)
{
    File rw_file;
    File ro_file;
    File excl_file;
    File noex_file;

    const char* filename = "rw";

    unsigned char write_buffer[256];
    unsigned char read_buffer[256];

    for (size_t i = 0; i < sizeof(write_buffer); i++) {
        write_buffer[i] = static_cast<unsigned char>(rand() % 255);
        read_buffer[i] = 0;
    }

    ASSERT_TRUE(rw_file.Create(filename, false));
    ASSERT_FALSE(excl_file.Create(filename, true));
    ASSERT_TRUE(rw_file.Close());

    ASSERT_FALSE(noex_file.Open("-6461d~*&(#4465483ZZppsdQWfsdf.ppood.~~sdsd"));

    // Записать данные (с перезаписью, если файл существует)
    ASSERT_TRUE(rw_file.Create(filename, false));
    ASSERT_EQ(rw_file.Write(write_buffer, sizeof(write_buffer)),
        static_cast<ssize_t>(sizeof(write_buffer)));
    ASSERT_TRUE(rw_file.Close());

    // Открыть на чтение в режиме RW
    ASSERT_TRUE(rw_file.Open(filename, false));
    ASSERT_EQ(rw_file.Read(read_buffer, sizeof(read_buffer)),
        static_cast<ssize_t>(sizeof(write_buffer)));
    ASSERT_EQ(memcmp(read_buffer, write_buffer, sizeof(write_buffer)), 0);

    memset(read_buffer, 0, sizeof(read_buffer));

    // Открыть в режиме RO
    ASSERT_TRUE(ro_file.Open(filename, true));
    ASSERT_LE(ro_file.Write(write_buffer, sizeof(write_buffer)), 0);
    ASSERT_EQ(ro_file.Read(read_buffer, sizeof(read_buffer)),
        static_cast<ssize_t>(sizeof(write_buffer)));
    ASSERT_EQ(memcmp(read_buffer, write_buffer, sizeof(write_buffer)), 0);
}
