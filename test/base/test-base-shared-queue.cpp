#include "base/shared-queue.h" // for SharedQueue
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResult::T...
#include <gtest/gtest.h> // for AssertionResult, GetBoolAssertion...
#include <stddef.h> // for size_t
#include <string> // for string

using namespace rth::ipc;

TEST(SharedMemorySuite, BasicSharedQueueInt)
{
    const char* name1 = "RTH.BasicSharedQueueInt.Queue1";
    const size_t kMaxSize = 128 * 1000;
    const size_t kVecotrSize = 1000;
    size_t tmp = 0;
    size_t push_vector[kVecotrSize] = { 0 };
    size_t pop_vector[kVecotrSize] = { 0 };

    SharedQueue<size_t> q1, q2;

    /******************************************************
   * Недопустимые действия
   *******************************************************/
    q1.Close();
    q1.Close();
    ASSERT_FALSE(q1.IsOpen());
    ASSERT_EQ(q1.Size(), 0U);
    ASSERT_EQ(q1.MaxSize(), 0U);

    /******************************************************
   * Создать удалить
   *******************************************************/
    q1.Create(name1, 1000);
    ASSERT_EQ(q1.GetLastError(), rth::error::kNoError);
    q1.Close();

    /******************************************************
   * Создать - заполнить - удалить
   *******************************************************/
    q1.Create(name1, kMaxSize);
    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }
    q1.Close();

    /******************************************************
   * Создать - извлeчь из пустой - удалить
   *******************************************************/
    q1.Create(name1, kMaxSize);
    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_FALSE(q1.Pop(&tmp));
    }
    q1.Close();

    /******************************************************
   * Создать - заполнить - извлечь - удалить
   *******************************************************/
    q1.Create(name1, kMaxSize);
    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }
    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Pop(&tmp));
        ASSERT_EQ(tmp, i);
    }
    q1.Close();

    /******************************************************
   * Создать - переполнить - извлечь - удалить
   *******************************************************/
    q1.Create(name1, kMaxSize);

    /* Норм. заполнение */
    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }

    /* Переполнение */
    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_FALSE(q1.Push(&i));
    }

    /* Извлечение. Должно совпадать с первой частью */
    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Pop(&tmp));
        ASSERT_EQ(tmp, i);
    }
    q1.Close();

    /******************************************************
   * Две очереди на одной области памяти
   * Просто проверяем создание и открытие
   *******************************************************/
    q1.Create(name1, kMaxSize);
    q2.Open(name1);
    ASSERT_TRUE(q2.Size() == q1.Size());
    ASSERT_TRUE(q2.Size() == 0);
    ASSERT_TRUE(q2.MaxSize() == static_cast<size_t>(kMaxSize));
    q1.Close();
    q2.Close();

    /******************************************************
   * Две очереди на одной области памяти
   * Создать и заполнить через одну
   * Прочитать через вторую
   *******************************************************/
    q1.Create(name1, kMaxSize);
    q2.Open(name1);

    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }
    for (size_t i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q2.Pop(&tmp));
        ASSERT_EQ(tmp, i);
    }
    q1.Close();
    q2.Close();

    /******************************************************
   * Открытие  несуществующей очереди
   *******************************************************/
    ASSERT_FALSE(q1.Open("sdfsdf"));
    ASSERT_EQ(q1.GetLastError(), rth::error::kNoEntry);

    ASSERT_FALSE(q1.IsOpen());
    /******************************************************
   * Двойное создание
   *******************************************************/
    ASSERT_TRUE(q1.Create("double-creation", 1));
    ASSERT_FALSE(q2.Create("double-creation", 1));

    ASSERT_EQ(q2.GetLastError(), rth::error::kAlreadyExists);

    //
    q2.Close();
    q1.Close();

    /******************************************************
   * Работа с массивами данных
   *******************************************************/
    /* Извлечение группы */
    q1.Create(name1, kMaxSize);
    /* Заполнить массив и положить */
    for (size_t i = 0; i < kVecotrSize; i++) {
        push_vector[i] = i;
    }
    ASSERT_EQ(q1.Push(push_vector, kVecotrSize), kVecotrSize);

    /* Прочитать обратно */
    ASSERT_EQ(q1.Pop(pop_vector, kVecotrSize), kVecotrSize);

    /* Проверить */
    for (size_t i = 0; i < kVecotrSize; i++) {
        ASSERT_TRUE(pop_vector[i] == i);
    }
    q1.Close();

    /******************************************************
   * Работа с массивами данных.
   * Попытка переполнения
   *******************************************************/
    /* Извлечение группы */
    q1.Create(name1, kVecotrSize / 2);
    /* Заполнить массив и положить */
    for (size_t i = 0; i < kVecotrSize / 2; i++) {
        push_vector[i] = i;
    }
    for (size_t i = kVecotrSize / 2; i < kVecotrSize; i++) {
        push_vector[i] = 0;
        pop_vector[i] = 0;
    }
    ASSERT_EQ(q1.Push(push_vector, kVecotrSize), kVecotrSize / 2);

    /* Прочитать обратно */
    ASSERT_EQ(q1.Pop(pop_vector, kVecotrSize), kVecotrSize / 2);

    /* Проверить */
    for (size_t i = 0; i < kVecotrSize / 2; i++) {
        ASSERT_TRUE(pop_vector[i] == i);
    }
    for (size_t i = kVecotrSize / 2; i < kVecotrSize; i++) {
        ASSERT_TRUE(pop_vector[i] == 0);
    }
    q1.Close();

    /******************************************************
   * Работа с массивами данных.
   * Извлечение из пустой очереди
   *******************************************************/
    /* Извлечение группы */
    q1.Create(name1, kMaxSize);
    /* Прочитать обратно */
    ASSERT_EQ(q1.Pop(pop_vector, kVecotrSize), 0U);
    q1.Close();

    /******************************************************
  * Работа с массивами данных
  * Передаем группой, читаем по одному
  *******************************************************/
    /* Извлечение группы */
    q1.Create(name1, kMaxSize);
    /* Заполнить массив и положить */
    for (size_t i = 0; i < kVecotrSize; i++) {
        push_vector[i] = i;
    }
    ASSERT_EQ(q1.Push(push_vector, kVecotrSize), kVecotrSize);
    /* Проверить */
    for (size_t i = 0; i < kVecotrSize; i++) {
        ASSERT_TRUE(q1.Pop(&tmp));
        ASSERT_EQ(tmp, i);
    }
    q1.Close();

    /******************************************************
   * Работа с массивами данных
   * Записать по одному, прочитать группой
   *******************************************************/
    /* Извлечение группы */
    q1.Create(name1, kMaxSize);
    /* Заполнить массив и положить */
    for (size_t i = 0; i < kVecotrSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }
    ASSERT_EQ(q1.Pop(pop_vector, kVecotrSize), kVecotrSize);
    /* Проверить */
    for (size_t i = 0; i < kVecotrSize; i++) {
        ASSERT_TRUE(pop_vector[i] == i);
    }
    q1.Close();
}

TEST(SharedMemorySuite, BasicSharedQueueDouble)
{
    double tmp = 0;
    const size_t kMaxSize = 128 * 1000;
    const char* name = "RTH.BasicSharedQueueDouble.Queue1";
    SharedQueue<double> q1, q2;

    q1.Close();
    q1.Close();
    ASSERT_FALSE(q1.IsOpen());
    ASSERT_TRUE(q1.Size() == 0);
    ASSERT_TRUE(q1.MaxSize() == 0);

    /* Создать - удалить */
    q1.Create(name, 1000);
    q1.Close();

    /* Создать - заполнить - удалить*/
    q1.Create(name, kMaxSize);
    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }
    q1.Close();

    /* Создать - извлeчь из пустой - удалить*/
    q1.Create(name, kMaxSize);
    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_FALSE(q1.Pop(&i));
    }
    q1.Close();

    /* Create - заполнить - извлечь - удалить */
    q1.Create(name, kMaxSize);
    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }
    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Pop(&tmp));
        ASSERT_EQ(tmp, i);
    }
    q1.Close();

    /* Создать - переполнить - извлечь - удалить */
    q1.Create(name, kMaxSize);

    /* Норм. заполнение */
    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }

    /* Переполнение */
    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_FALSE(q1.Push(&i));
    }

    /* Извлечение. Должно совпадать с первой частью */
    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Pop(&tmp));
        ASSERT_EQ(tmp, i);
    }
    q1.Close();

    q1.Create(name, kMaxSize);
    q2.Open(name);
    ASSERT_TRUE(q2.Size() == q1.Size());
    ASSERT_TRUE(q2.Size() == 0);
    ASSERT_TRUE(q2.MaxSize() == kMaxSize);
    q1.Close();
    q2.Close();

    /* Создать одну очередь , а прочитать в другой */
    q1.Create(name, kMaxSize);
    q2.Open(name);

    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Push(&i));
    }
    for (double i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q2.Pop(&tmp));
        ASSERT_EQ(tmp, i);
    }
    q1.Close();
    q2.Close();

    /* Открыть несуществующую и проверить */
    ASSERT_FALSE(q1.Open("sdfsdf"));
    ASSERT_FALSE(q1.IsOpen());
}

typedef struct {
    // char ch;
    int val;
    // char *data;
    // bool z;
    unsigned char bit1 : 1;
    unsigned char bit2 : 2;

} SimpleStruct;

TEST(SharedMemorySuite, BasicSharedQueueStruct)
{
    SimpleStruct tmp = { 0, 0, 0 }; //, 0, 0, 0};
    const int kMaxSize = 128 * 1000;
    SharedQueue<SimpleStruct> q1, q2;
    const char* name = "RTH.BasicSharedQueueStruct.Queue1";

    /* Создать - удалить */
    q1.Create(name, 1000);
    q1.Close();

    q1.Create(name, kMaxSize);
    /* Создать - заполнить - удалить*/
    for (int i = 0; i < kMaxSize; i++) {
        tmp.val = i;
        tmp.bit1 = (i % 2 == 0);
        tmp.bit2 = !tmp.bit1;
        ASSERT_TRUE(q1.Push(&tmp));
    }
    q1.Close();

    /* Создать - извлeчь из пустой - удалить*/
    q1.Create(name, kMaxSize);
    for (int i = 0; i < kMaxSize; i++) {
        ASSERT_FALSE(q1.Pop(&tmp));
    }
    q1.Close();

    /* Создать - заполнить - извлечь - удалить */
    q1.Create(name, kMaxSize);
    for (int i = 0; i < kMaxSize; i++) {
        tmp.val = i;
        tmp.bit1 = i % 2;
        tmp.bit2 = !tmp.bit1;
        ASSERT_TRUE(q1.Push(&tmp));
    }
    for (int i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Pop(&tmp));
        ASSERT_TRUE(tmp.val == i);
        ASSERT_EQ(tmp.bit1, (i % 2));
    }
    q1.Close();

    /* Создать - переполнить - извлечь - удалить */
    q1.Create(name, kMaxSize);

    /* Норм. заполнение */
    for (int i = 0; i < kMaxSize; i++) {
        tmp.val = i;
        tmp.bit1 = i % 2;
        tmp.bit2 = !tmp.bit1;
        ASSERT_TRUE(q1.Push(&tmp));
    }

    /* Переполнение */
    for (int i = 0; i < kMaxSize; i++) {
        tmp.val = i;
        tmp.bit1 = i % 2;
        tmp.bit2 = !tmp.bit1;
        ASSERT_FALSE(q1.Push(&tmp));
    }

    /* Извлечение. Должно совпадать с первой частью */
    for (int i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q1.Pop(&tmp));
        ASSERT_TRUE(tmp.val == i);
        ASSERT_EQ(tmp.bit1, (i % 2));
    }

    q1.Close();

    /* Создать одну очередь , а прочитать в другой */
    q1.Create(name, kMaxSize);
    q2.Open(name);

    for (int i = 0; i < kMaxSize; i++) {
        tmp.val = i;
        tmp.bit1 = i % 2;
        tmp.bit2 = !tmp.bit1;
        ASSERT_TRUE(q1.Push(&tmp));
    }
    for (int i = 0; i < kMaxSize; i++) {
        ASSERT_TRUE(q2.Pop(&tmp));
        ASSERT_TRUE(tmp.val == i);
        ASSERT_EQ(tmp.bit1, (i % 2));
    }
    q2.Close();
    q1.Close();
}
