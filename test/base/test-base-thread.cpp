#include "base/thread.h" // for Callable, Thread, MutexAutoLock
#include <assert.h> // for assert
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for IsNullLiteralHelper, TestPartResult
#include <gtest/gtest.h> // for AssertionResult, Test, TestInfo (...
#include <string.h> // for strcmp, strcpy
#include <string> // for string

using namespace rth::thread;
static void dummy(void) {}

TEST(ThreadSuite, BasicThreadOps)
{
    const int kTestThreads = 4;

    Thread* threads[kTestThreads];
    Callable<void> cb[kTestThreads];

    for (int i = 0; i < 4; i++) {
        cb[i] = Callable<void>(dummy);
        threads[i] = new Thread(cb[i]);
    }

    Millisleep(100);
    for (int i = 0; i < 4; i++) {
        ASSERT_TRUE(threads[i]->Join());
    }

    for (int i = 0; i < 4; i++) {
        ASSERT_FALSE(threads[i]->Join());
        delete threads[i];
    }
}

class TestCallable {
public:
    explicit TestCallable(int start_value)
        : value_(start_value)
    {
    }
    void operator()()
    {
        MutexAutoLock lock(mutex_);
        value_ = 100;
    }
    int GetValue()
    {
        MutexAutoLock lock(mutex_);
        return value_;
    }

private:
    Mutex mutex_;
    int value_;
};

static rth::thread::Mutex gSharedMutex;
static bool gNonArgFuncCalled = false;
static int gIntFuncValue = false;
static char gCharValue[128] = { 0 };
static int kCheckValue = 0xAA;
static const char* kCheckString = "OLOLO!!!";

static void NonArgFunc(void)
{
    MutexAutoLock lock(gSharedMutex);
    gNonArgFuncCalled = true;
}

static void IntFunc(int z)
{
    MutexAutoLock lock(gSharedMutex);
    gIntFuncValue = z;
}

static void VoidPtrFunc(void* ptr)
{
    MutexAutoLock lock(gSharedMutex);
    *static_cast<int*>(ptr) = kCheckValue;
}

static void ConstCharPtrFunc(const char* str)
{
    MutexAutoLock lock(gSharedMutex);
    strcpy(gCharValue, str);
}

TEST(ThreadSuite, BasicCallable)
{
    TestCallable callable(123);
    ASSERT_TRUE(Thread(callable).Join());
    ASSERT_EQ(callable.GetValue(), 100);

    int data = -1;
    Callable<void> noarg(NonArgFunc);
    Callable<int> intarg(IntFunc, kCheckValue);
    Callable<void*> ptr_arg(VoidPtrFunc, &data);
    Callable<const char*> cchar_arg(ConstCharPtrFunc, kCheckString);

    Thread(noarg).Join();
    Thread(intarg).Join();
    Thread(ptr_arg).Join();
    Thread(cchar_arg).Join();

    ASSERT_TRUE(gNonArgFuncCalled);
    ASSERT_EQ(gIntFuncValue, kCheckValue);
    ASSERT_EQ(data, kCheckValue);
    ASSERT_EQ(strcmp(gCharValue, kCheckString), 0);
}

const int kPoisonValue = 0xAAAA;
const int kNormalValue = 0x5555;
const int kStartValue = 666;

typedef struct {
    Mutex mutex;
    int value;
} ThreadContext;

static void Poisoner(ThreadContext* ctx)
{
    for (int i = 0; i < 5; i++) {
        ctx->mutex.Lock();

        assert(ctx->value == kStartValue || ctx->value == kNormalValue);
        ctx->value = kPoisonValue;
        Millisleep(200);
        assert(ctx->value == kPoisonValue);
        ctx->value = kNormalValue;

        ctx->mutex.Unlock();
    }
}

TEST(ThreadSuite, BasicThreadAndLock)
{
    const int kTestThreads = 4;
    ThreadContext ctx;
    Thread* threads[kTestThreads];
    Callable<ThreadContext*> cb[kTestThreads];
    ctx.value = kStartValue;

    for (int i = 0; i < 4; i++) {
        cb[i] = Callable<ThreadContext*>(Poisoner, &ctx);
        threads[i] = new Thread(cb[i]);
    }

    for (int i = 0; i < 4; i++) {
        threads[i]->Join();
        delete threads[i];
    }

    ASSERT_EQ(ctx.value, kNormalValue);
}
