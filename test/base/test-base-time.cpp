#include "base/thread.h" // for Microsleep, Millisleep
#include "base/time.h" // for GetMonoTimeMilli
#include <gtest/gtest-message.h> // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, TestPartResult::T...
#include <gtest/gtest.h> // for AssertionResult, CmpHelperGE, Cmp...
#include <stdint.h> // for uint64_t

using namespace rth::time;
using namespace rth::thread;

TEST(ThreadSuite, BasicTime)
{
    uint64_t begin = GetMonoTimeMilli();
    Microsleep(100 * 1000);
    uint64_t end = GetMonoTimeMilli();

#if defined(__linux__)
    ASSERT_GE(end, begin + 100U);
    ASSERT_LE(end, begin + 150U);
#endif //#if defined(__linux__)

#if defined(_WIN32)
    ASSERT_GE(end, begin + 70U);
    ASSERT_LE(end, begin + 210U);
#endif //#if defined(_WIN32)

    begin = GetMonoTimeMilli();
    Millisleep(100);
    end = GetMonoTimeMilli();

#if defined(__linux__)
    ASSERT_GE(end, begin + 100U);
    ASSERT_LE(end, begin + 150U);
#endif //#if defined(__linux__)

#if defined(_WIN32)
    ASSERT_GE(end, begin + 70U);
    ASSERT_LE(end, begin + 210U);
#endif //#if defined(_WIN32)
}
