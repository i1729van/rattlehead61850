# IEC61850_ROOT - root path for searching IEC61850
# IEC61850_Static - ON - find static libraries. OFF - find dyn. libraries
# RETURN
# IEC61850_LIBRARY - full path to IEC61850 library
# IEC61850_INCLUDE_DIR - full path to headers

find_path(IEC61850_INCLUDE_DIR NAMES libiec61850 HINTS ${IEC61850_ROOT}/include)

IF(MSVC)
  IF(${IEC61850_Static})
    set(IEC61850_LIBRARY  ${IEC61850_ROOT}/lib/libiec61850.lib)
  ELSE()
    set(IEC61850_LIBRARY  ${IEC61850_ROOT}/lib/iec61850.lib)
  ENDIF()
  IF(NOT EXISTS ${IEC61850_LIBRARY})
    message(FATAL_ERROR "libIEC61850 is missing in ${IEC61850_ROOT}")
  ENDIF()
ELSE()
  IF (${IEC61850_Static})
    set(libNames iec61850.a libiec61850.a iec61850.lib libiec61850.lib)
  else()
    set(libNames iec61850.so libiec61850.so iec61850.dll libiec61850.dll)
  ENDIF()
  IF(CMAKE_SYSTEM_NAME STREQUAL "Windows")
    find_library(IEC61850_LIBRARY NAMES ${libNames} HINTS ${IEC61850_ROOT}/lib)
  ELSE()
    find_library(IEC61850_LIBRARY NAMES ${libNames})
  ENDIF()
ENDIF()
