# VOLCANO_ROOT - root path for Volcano
# VOLCANO_LIBRARY - full path to IEC61850 library
# VOLCANO_INCLUDE_DIR - full path to headers
# VOLCANO_Static - static link

#----------------------------------------------------
# Find Volcano
#----------------------------------------------------
# Пройти по путям перечисленным в CMAKE_FIND_ROOT_PATH
# и найти заголовки и либы Rattlehead'a

IF(MSVC)
  IF(${VOLCANO_Static})
    set(libNames libvolcano.lib)
  ELSE()
    set(libNames volcano.lib)
  ENDIF()
ELSE()
  IF(${VOLCANO_Static})
    set(libNames volcano.a libvolcano.a)
  ELSE()
    set(libNames volcano.so volcano.dll libvolcano.so libvolcano.dll)
  ENDIF()
ENDIF()

FOREACH(curPath ${VOLCANO_ROOT})
  # CMAKE_FIND_ROOT_PATH_BOTH - грязный хак для кросс-компиляции. Но пока это наименьшее зло.
  # Суть проблемы:
  # Для кросса крайне удобно прописывать CMAKE_FIND_ROOT_PATH и CMAKE_FIND_ROOT_PATH_MODE* = ONLY
  # внутри ТС файла. Это позволяет сходу находить системные либы в директориях кросс-компилятора
  # , игнорируя либы для других платформ
  # Оборотная стороны - пути сложно кастомизировать и менять. К тому же многие find_* функции 
  # будут игнорить пути, переданные в HINTS, и искать только в CMAKE_FIND_ROOT_PATH
  # В общем, пока CMAKE_FIND_ROOT_PATH_BOTH внутри двух функцаек выглядит оптимальным решением.
  find_path(VOLCANO_INCLUDE_DIR NAMES vc HINTS ${curPath}/include/volcano CMAKE_FIND_ROOT_PATH_BOTH)
  find_file(VOLCANO_LIBRARY NAMES ${libNames} HINTS ${curPath}/lib CMAKE_FIND_ROOT_PATH_BOTH)
ENDFOREACH()

IF (NOT (VOLCANO_INCLUDE_DIR AND VOLCANO_LIBRARY))
  message(FATAL_ERROR "Volcano is missing")
ENDIF()
